﻿namespace Naalwar
{
    partial class FrmJobCardReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJobCardReport));
            this.DtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.DtpToDate = new System.Windows.Forms.DateTimePicker();
            this.grReport = new System.Windows.Forms.GroupBox();
            this.Panrpt = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.CmbRptType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CmbShift = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grReport.SuspendLayout();
            this.Panrpt.SuspendLayout();
            this.SuspendLayout();
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpFromDate.Location = new System.Drawing.Point(90, 5);
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(133, 26);
            this.DtpFromDate.TabIndex = 0;
            // 
            // DtpToDate
            // 
            this.DtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpToDate.Location = new System.Drawing.Point(90, 37);
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(133, 26);
            this.DtpToDate.TabIndex = 1;
            // 
            // grReport
            // 
            this.grReport.Controls.Add(this.Panrpt);
            this.grReport.Controls.Add(this.BtnExit);
            this.grReport.Controls.Add(this.BtnPrint);
            this.grReport.Controls.Add(this.CmbRptType);
            this.grReport.Controls.Add(this.label3);
            this.grReport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grReport.Location = new System.Drawing.Point(8, -1);
            this.grReport.Name = "grReport";
            this.grReport.Size = new System.Drawing.Size(412, 207);
            this.grReport.TabIndex = 2;
            this.grReport.TabStop = false;
            // 
            // Panrpt
            // 
            this.Panrpt.Controls.Add(this.CmbShift);
            this.Panrpt.Controls.Add(this.label4);
            this.Panrpt.Controls.Add(this.DtpToDate);
            this.Panrpt.Controls.Add(this.DtpFromDate);
            this.Panrpt.Controls.Add(this.label1);
            this.Panrpt.Controls.Add(this.label2);
            this.Panrpt.Location = new System.Drawing.Point(4, 57);
            this.Panrpt.Name = "Panrpt";
            this.Panrpt.Size = new System.Drawing.Size(397, 105);
            this.Panrpt.TabIndex = 218;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "From Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "To Date";
            // 
            // BtnExit
            // 
            this.BtnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Image = ((System.Drawing.Image)(resources.GetObject("BtnExit.Image")));
            this.BtnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnExit.Location = new System.Drawing.Point(341, 168);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(57, 30);
            this.BtnExit.TabIndex = 217;
            this.BtnExit.Text = "Exit";
            this.BtnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnExit.UseVisualStyleBackColor = false;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.BackColor = System.Drawing.Color.White;
            this.BtnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPrint.Location = new System.Drawing.Point(270, 168);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(64, 30);
            this.BtnPrint.TabIndex = 216;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnPrint.UseVisualStyleBackColor = false;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // CmbRptType
            // 
            this.CmbRptType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbRptType.FormattingEnabled = true;
            this.CmbRptType.Items.AddRange(new object[] {
            "Jobcard Live",
            "Jobcard Closed",
            "Fabric Roll Stock",
            "Fabric Roll Stock Detial",
            "QC Fabric Roll Stock"});
            this.CmbRptType.Location = new System.Drawing.Point(94, 25);
            this.CmbRptType.Name = "CmbRptType";
            this.CmbRptType.Size = new System.Drawing.Size(304, 26);
            this.CmbRptType.TabIndex = 5;
            this.CmbRptType.SelectedIndexChanged += new System.EventHandler(this.CmbRptType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Report Type";
            // 
            // CmbShift
            // 
            this.CmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbShift.FormattingEnabled = true;
            this.CmbShift.Items.AddRange(new object[] {
            "Jobcard Live",
            "Jobcard Closed",
            "Fabric Roll Stock",
            "QC Fabric Roll Stock"});
            this.CmbShift.Location = new System.Drawing.Point(90, 69);
            this.CmbShift.Name = "CmbShift";
            this.CmbShift.Size = new System.Drawing.Size(304, 26);
            this.CmbShift.TabIndex = 220;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 18);
            this.label4.TabIndex = 219;
            this.label4.Text = "Shift";
            // 
            // FrmJobCardReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(425, 211);
            this.Controls.Add(this.grReport);
            this.MaximizeBox = false;
            this.Name = "FrmJobCardReport";
            this.Text = "JobCard Report";
            this.Load += new System.EventHandler(this.FrmJobCardReport_Load);
            this.grReport.ResumeLayout(false);
            this.grReport.PerformLayout();
            this.Panrpt.ResumeLayout(false);
            this.Panrpt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DtpFromDate;
        private System.Windows.Forms.DateTimePicker DtpToDate;
        private System.Windows.Forms.GroupBox grReport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbRptType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Panel Panrpt;
        private System.Windows.Forms.ComboBox CmbShift;
        private System.Windows.Forms.Label label4;
    }
}