﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Naalwar
{
    public class SQLDBHelper
    {
        public DataTable GetDataWithParam(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection DbConn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = DbConn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet GetDataWithParamMultiple(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection DbConn)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = DbConn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetDataWithoutParam(CommandType cmdType, string ProcedureName, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetDataWithoutParamForImport(CommandType cmdType, string ProcedureName, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet GetDataWithoutParamMultiple(CommandType cmdType, string ProcedureName, SqlConnection conn)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection conn, int Outpara)
        {
            int Id = 0;
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(para[Outpara].Value.ToString());//Solved
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection conn)
        {
            int Id = 0;
            try
            {
                if(conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                Id = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlConnection conn)
        {
            int Id = 0;
            try
            {
                conn.Close();
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText,
                    Connection = conn
                };
                Id = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
    }
}
