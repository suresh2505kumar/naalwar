﻿namespace Naalwar
{
    partial class FrmRollReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRollReport));
            this.lblShift = new System.Windows.Forms.Label();
            this.cmbShift = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpQCDate = new System.Windows.Forms.DateTimePicker();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.btn = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.grFront.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblShift
            // 
            this.lblShift.AutoSize = true;
            this.lblShift.Location = new System.Drawing.Point(67, 83);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(36, 18);
            this.lblShift.TabIndex = 226;
            this.lblShift.Text = "Shift";
            // 
            // cmbShift
            // 
            this.cmbShift.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShift.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbShift.FormattingEnabled = true;
            this.cmbShift.Location = new System.Drawing.Point(103, 79);
            this.cmbShift.Name = "cmbShift";
            this.cmbShift.Size = new System.Drawing.Size(179, 26);
            this.cmbShift.TabIndex = 225;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 224;
            this.label1.Text = "Date";
            // 
            // dtpQCDate
            // 
            this.dtpQCDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpQCDate.Location = new System.Drawing.Point(103, 40);
            this.dtpQCDate.Name = "dtpQCDate";
            this.dtpQCDate.Size = new System.Drawing.Size(130, 26);
            this.dtpQCDate.TabIndex = 223;
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.btnPrint);
            this.grFront.Controls.Add(this.btn);
            this.grFront.Controls.Add(this.dtpQCDate);
            this.grFront.Controls.Add(this.label1);
            this.grFront.Controls.Add(this.lblShift);
            this.grFront.Controls.Add(this.cmbShift);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(7, 5);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(395, 189);
            this.grFront.TabIndex = 227;
            this.grFront.TabStop = false;
            // 
            // btn
            // 
            this.btn.Location = new System.Drawing.Point(313, 15);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(75, 28);
            this.btn.TabIndex = 227;
            this.btn.Text = "button1";
            this.btn.UseVisualStyleBackColor = true;
            this.btn.Visible = false;
            this.btn.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(324, 153);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 228;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // FrmRollReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(410, 206);
            this.Controls.Add(this.grFront);
            this.Name = "FrmRollReport";
            this.Text = "Roll Report";
            this.Load += new System.EventHandler(this.FrmRollReport_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblShift;
        private System.Windows.Forms.ComboBox cmbShift;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpQCDate;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.Button btn;
        private System.Windows.Forms.Button btnPrint;
    }
}