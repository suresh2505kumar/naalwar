﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;

namespace Naalwar
{
    public partial class Frmlogin : Form
    {
        public Frmlogin()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (UsernameTextBox.Text == string.Empty)
            {
                MessageBox.Show("Enter User Name");
                UsernameTextBox.Focus();
                return;
            }
            if (PasswordTextBox.Text == "")
            {
                MessageBox.Show("Enter Password");
                PasswordTextBox.Focus();
                return;
            }
            SqlConnection conn1 = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
            string quy1 = "Select distinct yrtype,dbname from yearmaster where yrtype='" + cboyear.Text + "' and companyid=" + Genclass.data1 + "";
            Genclass.cmd = new SqlCommand(quy1, conn1);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            Genclass.data5 = tap1.Rows[0]["Dbname"].ToString();
            SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
            this.Visible = false;
            this.Hide();
            FrmMain contc = new FrmMain();
            contc.Show();
        }

        private void Frmlogin_Load(object sender, EventArgs e)
        {
            cboyear.DataSource = null;
            Genclass.data1 = Convert.ToInt16(Decrypt(ConfigurationManager.AppSettings["Data1"]));
            Genclass.data2 = Decrypt(ConfigurationManager.AppSettings["Data2"]);
            Genclass.data3 = Decrypt(ConfigurationManager.AppSettings["Data3"]);
            Genclass.data4 = Decrypt(ConfigurationManager.AppSettings["Data4"]);
            Genclass.data5 = Decrypt(ConfigurationManager.AppSettings["Data5"]);
            Genclass.data6 = Decrypt(ConfigurationManager.AppSettings["Data6"]);
            SqlConnection conn1 = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
            conn1.Open();
            string qur = "Select * from yearmaster";
            SqlCommand cmd = new SqlCommand(qur, conn1);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cboyear.DataSource = null;
            cboyear.DataSource = tab;
            cboyear.DisplayMember = "Yrtype";
            cboyear.ValueMember = "Yearid";
            conn1.Close();
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "myla123";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        private void label3_Click(object sender, EventArgs e)
        {

            string connetionString = null;
            SqlConnection conn;
            connetionString = "Data Source=" + Genclass.data2 + ";Initial Catalog=" + Genclass.data5 + ";User ID=" + Genclass.data3 + ";Password=" + Genclass.data4 + "";
            conn = new SqlConnection(connetionString);
            try
            {
                conn.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Can not open connection ! ");
                return;
            }
        }
    }
}
