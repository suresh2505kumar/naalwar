﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Naalwar
{
    public partial class FrmSortDet : Form
    {
        public FrmSortDet()
        {
            InitializeComponent();
        }
        string uid = string.Empty;
        int mode = 0;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsYarnWarp = new BindingSource();
        BindingSource bsYarnWept = new BindingSource();
        int SId = 0;
        int SelectedId = 0;
        decimal SortUid = 0;
        private void Label4_Click(object sender, EventArgs e)
        {
        }

        private void FrmSortDet_Load(object sender, EventArgs e)
        {
            BtnSortIdendification.Visible = false;
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            chkact.Checked = true;
            HFGP.RowHeadersVisible = false;
            this.HFGP.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            frafnt.Visible = true;
            Fraedit.Visible = false;
            Loadgrid();
            HFGP.Focus();
            chkact.Checked = true;
            LoadItemType();
            LoadTax();
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = GetTax();
                cmbPercentage.DataSource = null;
                cmbPercentage.DisplayMember = "GeneralName";
                cmbPercentage.ValueMember = "TaxPer";
                cmbPercentage.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void LoadItemType()
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@TypeM_Uid",29),
                    new SqlParameter("@Active",1),
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                cmbItemType.DataSource = null;
                cmbItemType.DisplayMember = "GeneralName";
                cmbItemType.ValueMember = "Uid";
                cmbItemType.DataSource = dt;
                cmbItemType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            mode = 1;
            frafnt.Visible = false;
            panadd.Visible = false;
            Fraedit.Visible = true;
            checkBox1.Checked = true;
            Genclass.Module.ClearTextBox(this, Fraedit);
        }

        private void Fun()
        {
            if (txtrollborder.Text == "")
            {
                txtrollborder.Text = "";
            }
            if (txtdesign.Text == "")
            {
                txtdesign.Text = "";
            }
            if (txtcrimp.Text == "")
            {
                txtcrimp.Text = "";
            }
            if (txtpickwheel.Text == "")
            {
                txtpickwheel.Text = "";
            }
            if (txtcrimpweft.Text == "")
            {
                txtcrimpweft.Text = "";
            }
            if (txtwt.Text == "")
            {
                txtwt.Text = "";
            }
            if (txtgsm.Text == "")
            {
                txtgsm.Text = "";
            }
            if (txtgtlen.Text == "")
            {
                txtgtlen.Text = "0";
            }
            if (txtgtwei.Text == "")
            {
                txtgtwei.Text = "0";
            }
            if (txttotalend.Text == "")
            {
                txttotalend.Text = "0";
            }
            if (txtreedspace.Text == "")
            {
                txtreedspace.Text = "";
            }
        }
        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "sortno";
                Genclass.FSSQLSortStr1 = "rollno";
                Genclass.FSSQLSortStr2 = "design";
                Genclass.FSSQLSortStr3 = "WarpCount";
                Genclass.FSSQLSortStr4 = "WeptCount";
                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                }
                if (Txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + Txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + Txtscr4.Text + "%'";
                    }
                }
                if (Txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + Txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + Txtscr5.Text + "%'";
                    }
                }
                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "uid <> 0";
                }

                if (chkact.Checked == true)
                {
                    string quy = "select * from  sortdet where active=1 and " + Genclass.StrSrch + "  ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = "select * from  sortdet where active= 0 and " + Genclass.StrSrch + "  ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Regular);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 200;
                HFGP.Columns[2].Width = 175;
                HFGP.Columns[3].Width = 163;
                HFGP.Columns[4].Width = 175;
                HFGP.Columns[5].Width = 131;
                HFGP.Columns[2].Visible = false;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Width = 90;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;
                HFGP.Columns[15].Visible = false;
                HFGP.Columns[16].Visible = false;
                HFGP.Columns[17].Visible = false;
                HFGP.Columns[18].Visible = false;
                HFGP.Columns[19].Visible = false;
                HFGP.Columns[20].Visible = false;
                HFGP.Columns[21].Visible = false;
                HFGP.Columns[22].Visible = false;
                HFGP.Columns[23].Visible = false;
                HFGP.Columns[24].Visible = false;
                HFGP.Columns[25].Visible = false;
                HFGP.Columns[26].Visible = false;
                HFGP.Columns[27].Visible = false;
                HFGP.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void Chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void HFGP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        private void Butnsave_Click(object sender, EventArgs e)
        {
            //fun();
            //if (txtwarp.Text == string.Empty)
            //{
            //    MessageBox.Show("select Warp count", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtwarp.Focus();
            //    return;
            //}
            //else if (txtwept.Text == string.Empty)
            //{
            //    MessageBox.Show("select wep count", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtwept.Focus();
            //    return;
            //}
            //else if (txtwt.Text == string.Empty)
            //{
            //    MessageBox.Show("Enter WT/ Mtr", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtwt.Focus();
            //    return;
            //}
            string SortNo = txtsortno.Text;
            string RollNo = txtrollborder.Text;
            string Design = txtdesign.Text;
            string WarpCount = txtwarp.Text;
            string WeptCount = txtwept.Text;
            string EPI = txtepi.Text;
            string PPI = txtppi.Text;
            string GreyWidth = txtgrwywidth.Text;
            if (txttotalend.Text == string.Empty)
            {
                txttotalend.Text = "0";
            }
            string TotalEnds = txttotalend.Text;
            string Reed = txtreed.Text;
            string ReedSpace = txtreedspace.Text;
            string PickWheel = txtpickwheel.Text;
            string CrimpWarp = txtcrimp.Text;
            string CrimpWeft = txtcrimpweft.Text;
            string WTMtr = txtwt.Text;
            string GSM = txtgsm.Text;
            if (txtgtlen.Text == string.Empty)
            {
                txtgtlen.Text = "0";
            }
            string GTLEN = txtgtlen.Text;
            if (txtgtwei.Text == string.Empty)
            {
                txtgtwei.Text = "0";
            }
            string GTwei = txtgtwei.Text;
            if (txtwarp.Tag == null)
            {
                txtwarp.Tag = "0";
            }
            if (txtwept.Tag == null)
            {
                txtwept.Tag = "0";
            }

            string WarpId = txtwarp.Tag.ToString();
            string WeptId = txtwept.Tag.ToString();
            if (txtHSNCode.Text == string.Empty)
            {
                txtHSNCode.Text = "0";
            }
            string HSNCode = txtHSNCode.Text;
            if (cmbPercentage.SelectedIndex == -1)
            {
                cmbPercentage.SelectedIndex = 0;
            }
            string GSTPercentage = cmbPercentage.SelectedValue.ToString();
            if (txtSellingPriceMtr.Text == string.Empty)
            {
                txtSellingPriceMtr.Text = "0";
            }
            if (txtSellingPriceKg.Text == string.Empty)
            {
                txtSellingPriceKg.Text = "0";
            }
            string SellingPrice = txtSellingPriceKg.Text;
            string ItemType = cmbItemType.SelectedValue.ToString();
            string SellingPriceMtr = txtSellingPriceMtr.Text;

            int act = 0;
            if (chkact.Checked == true)
            {
                act = 1;
            }
            else
            {
                act = 0;
            }

            if (mode == 1)
            {
                SqlParameter[] para = {
                    new SqlParameter("@sortno",SortNo),
                    new SqlParameter("@rollno",RollNo),
                    new SqlParameter("@design",Design),
                    new SqlParameter("@WarpCount",WarpCount),
                    new SqlParameter("@WeptCount",WeptCount),
                    new SqlParameter("@EPI",EPI),
                    new SqlParameter("@PPI",PPI),
                    new SqlParameter("@GreyWidth",GreyWidth),
                    new SqlParameter("@TotalEnds",TotalEnds),
                    new SqlParameter("@Reed",Reed),
                    new SqlParameter("@ReedSpace",ReedSpace),
                    new SqlParameter("@PickWheel",PickWheel),
                    new SqlParameter("@CrimpWarp",CrimpWarp),
                    new SqlParameter("@CrimpWeft",CrimpWeft),
                    new SqlParameter("@WTMtr",WTMtr),
                    new SqlParameter("@GSM",GSM),
                    new SqlParameter("@GTLEN",GTLEN),
                    new SqlParameter("@GTwei",GTwei),
                    new SqlParameter("@active",act),
                    new SqlParameter("@Width",Width),
                    new SqlParameter("@WarpId",WarpId),
                    new SqlParameter("@WeptId",WeptId),
                    new SqlParameter("@mode",mode),
                    new SqlParameter("@uid","0"),
                    new SqlParameter("@HSNCode",HSNCode),
                    new SqlParameter("@GSTPercentage",GSTPercentage),
                    new SqlParameter("@SellingPrice",SellingPrice),
                    new SqlParameter("@ItemType",ItemType),
                    new SqlParameter("@SellingPriceMtr",SellingPriceMtr),
                    new SqlParameter("@ReturnId",SqlDbType.Int)
                };
                para[29].Direction = ParameterDirection.Output;
                SortUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SORTADD", para, conn, 29);
            }
            else
            {
                SqlParameter[] para = {
                    new SqlParameter("@sortno",SortNo),
                    new SqlParameter("@rollno",RollNo),
                    new SqlParameter("@design",Design),
                    new SqlParameter("@WarpCount",WarpCount),
                    new SqlParameter("@WeptCount",WeptCount),
                    new SqlParameter("@EPI",EPI),
                    new SqlParameter("@PPI",PPI),
                    new SqlParameter("@GreyWidth",GreyWidth),
                    new SqlParameter("@TotalEnds",TotalEnds),
                    new SqlParameter("@Reed",Reed),
                    new SqlParameter("@ReedSpace",ReedSpace),
                    new SqlParameter("@PickWheel",PickWheel),
                    new SqlParameter("@CrimpWarp",CrimpWarp),
                    new SqlParameter("@CrimpWeft",CrimpWeft),
                    new SqlParameter("@WTMtr",WTMtr),
                    new SqlParameter("@GSM",GSM),
                    new SqlParameter("@GTLEN",GTLEN),
                    new SqlParameter("@GTwei",GTwei),
                    new SqlParameter("@active",act),
                    new SqlParameter("@Width",Width),
                    new SqlParameter("@WarpId",WarpId),
                    new SqlParameter("@WeptId",WeptId),
                    new SqlParameter("@mode",mode),
                    new SqlParameter("@uid",Convert.ToInt32(uid)),
                    new SqlParameter("@HSNCode",HSNCode),
                    new SqlParameter("@GSTPercentage",GSTPercentage),
                    new SqlParameter("@SellingPrice",SellingPrice),
                    new SqlParameter("@ItemType",ItemType),
                    new SqlParameter("@SellingPriceMtr",SellingPriceMtr),
                    new SqlParameter("@ReturnId",SqlDbType.Int)
                };
                para[29].Direction = ParameterDirection.Output;
                SortUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SORTADD", para, conn, 29);
            }
            BtnSortIdendification.Visible = true;
            butnsave.Enabled = false;
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BtnSortIdendification_Click(sender, e);
            //Loadgrid();
            //Fraedit.Visible = false;
            //frafnt.Visible = true;
            //panadd.Visible = true;
            //chkact.Checked = true;
        }

        private void Butedit_Click(object sender, EventArgs e)
        {
            SelectedId = 1;
            checkBox1.Checked = true;
            mode = 2;
            panadd.Visible = false;
            frafnt.Visible = false;
            Fraedit.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtsortno.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            txtrollborder.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdesign.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtwarp.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtwept.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtepi.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtppi.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtgrwywidth.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txttotalend.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtreed.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtreedspace.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtpickwheel.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtcrimp.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtcrimpweft.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            txtwt.Text = HFGP.Rows[i].Cells[15].Value.ToString();
            txtgsm.Text = HFGP.Rows[i].Cells[16].Value.ToString();
            txtgtlen.Text = HFGP.Rows[i].Cells[17].Value.ToString();
            txtgtwei.Text = HFGP.Rows[i].Cells[18].Value.ToString();
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtwarp.Tag = HFGP.Rows[i].Cells[20].Value.ToString();
            txtwept.Tag = HFGP.Rows[i].Cells[21].Value.ToString();
            txtHSNCode.Text = HFGP.Rows[i].Cells[23].Value.ToString();
            cmbPercentage.Text = HFGP.Rows[i].Cells[24].Value.ToString();
            txtSellingPriceKg.Text = HFGP.Rows[i].Cells[25].Value.ToString();
            cmbItemType.SelectedValue = HFGP.Rows[i].Cells[26].Value.ToString();
            txtSellingPriceMtr.Text = HFGP.Rows[i].Cells[27].Value.ToString();
            SortUid = Convert.ToDecimal(HFGP.Rows[i].Cells[0].Value.ToString());
            cmbItemType.SelectedValue = HFGP.Rows[i].Cells[26].Value.ToString();

            if (HFGP.Rows[i].Cells[26].Value.ToString() == "1071")
            {
                BtnSortIdendification.Visible = true;
                BtnSortIdendification_Click(sender, e);
            }
            else
            {
                BtnSortIdendification.Visible = false;
            }
            SelectedId = 0;
        }

        private void Buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Fraedit.Visible = false;
            frafnt.Visible = true;
            panadd.Visible = true;

            Loadgrid();
            BtnSortIdendification.Visible = false;
        }

        private void Butexit_Click(object sender, EventArgs e)
        {
            conn.Open();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            mode = 3;
            qur.CommandText = "";
            int j = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[j].Cells[0].Value.ToString();
            string message = "Are You Sure to Delete this Party ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons);
            if (result == DialogResult.Yes)
            {
                qur.CommandText = "update sortdet set active=0 where uid='" + uid + "'";
                qur.ExecuteNonQuery();
            }
        }

        private void Txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtwept_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                SId = 1;

                int Active = 1;
                SqlParameter[] para = { new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetyarnMaster", para, conn);
                bsYarnWept.DataSource = dt;
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "YarnId";
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[1].Name = "YarnName";
                DataGridCommon.Columns[1].HeaderText = "YarnName";
                DataGridCommon.Columns[1].DataPropertyName = "YarnShortName";
                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsYarnWept;
                Point p = Genclass.FindLocation(txtwept);
                grSearch.Location = new Point(p.X, p.Y + 25);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (SId == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtwept.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtwept.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtwarp.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtwarp.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtwarp_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                SId = 2;
                int Active = 1;
                SqlParameter[] para = { new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetyarnMaster", para, conn);
                bsYarnWarp.DataSource = dt;
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "YarnId";
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[1].Name = "YarnName";
                DataGridCommon.Columns[1].HeaderText = "YarnName";
                DataGridCommon.Columns[1].DataPropertyName = "YarnShortName";
                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsYarnWarp;
                Point p = Genclass.FindLocation(txtwarp);
                grSearch.Location = new Point(p.X - 230, p.Y + 25);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtwarp_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtwept_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    if (SId == 1)
                    {
                        string f = txtwept.Text.Replace("[", "");
                        bsYarnWept.Filter = string.Format("YarnShortName Like '%{0}%'", f);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtwarp_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    if (SId == 2)
                    {
                        string f = txtwarp.Text.Replace("[", "");
                        bsYarnWarp.Filter = string.Format("YarnShortName Like '%{0}%'", f);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (SId == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtwept.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtwept.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtwarp.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtwarp.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtwt_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtwt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtepi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void txtppi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void CmbItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbItemType.Text == "Terry Fabrics" && txtsortno.Text != string.Empty && SelectedId == 0)
                {
                    //int act = 0;
                    //if (chkact.Checked == true)
                    //{
                    //    act = 1;
                    //}
                    //else
                    //{
                    //    act = 0;
                    //}

                    //if (mode == 1)
                    //{
                    //    SqlParameter[] para = {
                    //        new SqlParameter("@sortno",txtsortno.Text),
                    //        new SqlParameter("@rollno",DBNull.Value),
                    //        new SqlParameter("@design",DBNull.Value),
                    //        new SqlParameter("@WarpCount",DBNull.Value),
                    //        new SqlParameter("@WeptCount",DBNull.Value),
                    //        new SqlParameter("@EPI",DBNull.Value),
                    //        new SqlParameter("@PPI",DBNull.Value),
                    //        new SqlParameter("@GreyWidth",DBNull.Value),
                    //        new SqlParameter("@TotalEnds",DBNull.Value),
                    //        new SqlParameter("@Reed",DBNull.Value),
                    //        new SqlParameter("@ReedSpace",DBNull.Value),
                    //        new SqlParameter("@PickWheel",DBNull.Value),
                    //        new SqlParameter("@CrimpWarp",DBNull.Value),
                    //        new SqlParameter("@CrimpWeft",DBNull.Value),
                    //        new SqlParameter("@WTMtr",DBNull.Value),
                    //        new SqlParameter("@GSM",DBNull.Value),
                    //        new SqlParameter("@GTLEN",DBNull.Value),
                    //        new SqlParameter("@GTwei",DBNull.Value),
                    //        new SqlParameter("@active",act),
                    //        new SqlParameter("@Width",DBNull.Value),
                    //        new SqlParameter("@WarpId",DBNull.Value),
                    //        new SqlParameter("@WeptId",DBNull.Value),
                    //        new SqlParameter("@mode",mode),
                    //        new SqlParameter("@uid","0"),
                    //        new SqlParameter("@HSNCode",DBNull.Value),
                    //        new SqlParameter("@GSTPercentage",DBNull.Value),
                    //        new SqlParameter("@SellingPrice",DBNull.Value),
                    //        new SqlParameter("@ItemType",cmbItemType.SelectedValue),
                    //        new SqlParameter("@SellingPriceMtr",DBNull.Value)
                    //    };
                    //    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SORTADD", para, conn);
                    //}
                    //else
                    //{
                    //    SqlParameter[] para = {
                    //        new SqlParameter("@sortno",txtsortno.Text),
                    //        new SqlParameter("@rollno",txtrollborder.Text),
                    //        new SqlParameter("@design",txtdesign.Text),
                    //        new SqlParameter("@WarpCount",txtwarp.Text),
                    //        new SqlParameter("@WeptCount",txtwept.Text),
                    //        new SqlParameter("@EPI",txtepi.Text),
                    //        new SqlParameter("@PPI",txtppi.Text),
                    //        new SqlParameter("@GreyWidth",txtgrwywidth.Text),
                    //        new SqlParameter("@TotalEnds",Convert.ToDecimal(txttotalend.Text)),
                    //        new SqlParameter("@Reed",txtreed.Text),
                    //        new SqlParameter("@ReedSpace",txtreedspace.Text),
                    //        new SqlParameter("@PickWheel",txtpickwheel.Text),
                    //        new SqlParameter("@CrimpWarp",txtcrimp.Text),
                    //        new SqlParameter("@CrimpWeft",txtcrimpweft.Text),
                    //        new SqlParameter("@WTMtr",txtwt.Text),
                    //        new SqlParameter("@GSM",txtgsm.Text),
                    //        new SqlParameter("@GTLEN",Convert.ToDecimal(txtgtlen.Text)),
                    //        new SqlParameter("@GTwei",Convert.ToDecimal(txtgtwei.Text)),
                    //        new SqlParameter("@active",act),
                    //        new SqlParameter("@Width",DBNull.Value),
                    //        new SqlParameter("@WarpId",txtwarp.Tag),
                    //        new SqlParameter("@WeptId",txtwept.Tag),
                    //        new SqlParameter("@mode",mode),
                    //        new SqlParameter("@uid",Convert.ToInt32(uid)),
                    //        new SqlParameter("@HSNCode",txtHSNCode.Text),
                    //        new SqlParameter("@GSTPercentage",cmbPercentage.SelectedValue),
                    //        new SqlParameter("@SellingPrice",txtSellingPriceKg.Text),
                    //        new SqlParameter("@ItemType",cmbItemType.SelectedValue),
                    //        new SqlParameter("@SellingPriceMtr",txtSellingPriceMtr.Text)
                    //    };
                    //    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SORTADD", para, conn);
                    //}
                    //BtnSortIdendification.Visible = true;
                }
                else
                {
                    //if (cmbItemType.Text == "Terry Fabrics")
                    //{
                    //    BtnSortIdendification.Visible = true;
                    //}
                    //else
                    //{
                    //    BtnSortIdendification.Visible = false;
                    //}

                    //MessageBox.Show("Ener Sort Name","Inormation,",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    txtsortno.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void BtnSortIdendification_Click(object sender, EventArgs e)
        {
            try
            {
                FrmSortIdentification frmSortIdentification = new FrmSortIdentification()
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                frmSortIdentification.Show(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void grSearch_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Fraedit_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
