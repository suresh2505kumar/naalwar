﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace Naalwar
{
    public partial class Frmcustord : Form
    {
        public Frmcustord()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }

        string uid = "";
        //int mode = 0;

        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
  

        private void loadexcise()
        {
            conn.Open();
            string qur = "Select UId,GeneralName from GENERALM  where TypeM_UId =5 and active=1";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cboexcise.DataSource = null;
            cboexcise.DataSource = tab;
            cboexcise.DisplayMember = "GeneralName";
            cboexcise.ValueMember = "uid";
            cboexcise.SelectedIndex = -1;
            conn.Close();

        }


        private void loadtax()
        {

            conn.Open();
            string qur = "select a.UId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotax.DataSource = null;
            cbotax.DataSource = tab;
            cbotax.DisplayMember = "GeneralName";
            cbotax.ValueMember = "uid";
            cbotax.SelectedIndex = -1;
            conn.Close();


        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "Name";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }

                string quy = "Select distinct a.Uid,DocNo,DocDate,dcno,dcdate,Name,a.partyuid from transactionsp a inner join  partym b on a.partyuid=b.uid  where a.doctypeid=80  and  " + Genclass.StrSrch + "";
                Genclass.cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

              
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 90;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Width = 400;
               
              

   

                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }


        private void Titlep()
        {
            HFIT.ColumnCount = 5;
            HFIT.Columns[0].Name = "Itemname";
            //HFIT.Columns[1].Name = "Uom";
            //HFIT.Columns[2].Name = "Addnotes";
            HFIT.Columns[1].Name = "Price";
            HFIT.Columns[2].Name = "Qty";
            HFIT.Columns[3].Name = "BasicValue";
            HFIT.Columns[4].Name = "Itemuid";
            //HFIT.Columns[6].Name = "Refuid";


            HFIT.Columns[0].Width = 300;
            HFIT.Columns[1].Width = 100;
            HFIT.Columns[2].Width = 100;
            HFIT.Columns[3].Width = 100;
            HFIT.Columns[4].Visible = false;

            //HFIT.Columns[5].Visible = false;
 
        }

        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;

                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
 
        private void loadput()
        {
            conn.Open();

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + "";
                //Genclass.strsql = "select distinct  c.uid,c.name from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 group by c.uid,c.name,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";

                Genclass.FSSQLSortStr = "Name";

            }

            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtitid,txtitem, Editpan);
                Genclass.strsql = "select uid,Itemname as Item from Itemm where active=1 and  companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "docno";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 400;


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();
            conn.Close();


        }

        private void txtdcno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 2;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtdcno.Text = "";
                txtdcno.Focus();
            }
        }



        private void button2_Click(object sender, System.EventArgs e)
        {
            Editpan.Visible = false;
            Taxpan.Visible = true;

        }

        private void btnaddrcan_Click(object sender, System.EventArgs e)
        {
            Taxpan.Visible = false;
            Genpan.Visible = true;

        }

        private void HFIT_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {




        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void HFIT_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

        }

        //private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (HFIT.RowCount - 2 > 0)
        //    {
        //        Genclass.sum = 0;
        //        txtamt.Text = null;
        //        txtamttot.Text = null;
        //        if (HFIT.RowCount > 2)
        //        {
        //            for (int i = 0; i < HFIT.RowCount - 1; i++)
        //            {
        //                if (HFIT.Rows[i].Cells[4].Value.ToString() == "" || HFIT.Rows[i].Cells[4].Value.ToString() == null)
        //                {
        //                    return;
        //                }

        //                else
        //                {
        //                    //int sum1 = (int.TryParse(HFIT.Rows[i].Cells[2].Value.ToString(), out quantity) && int.TryParse(HFIT.Rows[i].Cells[4].Value.ToString(), out rate));
        //                    Genclass.sum = Genclass.sum + Convert.ToInt16(HFIT.Rows[i].Cells[4].Value.ToString());

        //                    txtamttot.Text = Genclass.sum.ToString();
        //                    txtamt.Text = txtamttot.Text;
        //                }
        //            }
        //        }
        //    }
        //}


        private void btnexit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtper_TextChanged(object sender, System.EventArgs e)
        {
            int val1;
            int val2;
            int val3;
            val1 = Convert.ToInt16(txtamttot.Text);
            val2 = Convert.ToInt16(txtper.Text);
            val3 = (val1 * val2) / 100;
            txtdis.Text = Convert.ToString(val3);
            CalcNetAmt();

        }

        //private void textBox2_TextChanged(object sender, System.EventArgs e)
        //{

        //}
        private void CalcNetAmt()
        {

            int val4;
            int val5;
            //int roff;
            int totamt;

            totamt = Convert.ToInt16(txtamttot.Text);

            int dis = Convert.ToInt16(txtdis.Text);
            if (txtper.Text == "")
            {
                TxtNetAmt.Text = txtamttot.Text + txtexcise.Text + txttax.Text + txtpf.Text;
                val4 = Convert.ToInt16(TxtNetAmt.Text);
                val5 = val4 - Convert.ToInt16(txtdis.Text);
                // Genclass.strfin = totamt + Convert.ToInt16(txtpf.Text) - dis;

                TxtNetAmt.Text = Convert.ToString(val5);
            }
            else
            {
                TxtNetAmt.Text = "0.00";


                TxtNetAmt.Text = txtamttot.Text + txtexcise.Text + txttax.Text + txtpf.Text;
                int cal5 = Convert.ToInt16(TxtNetAmt.Text);
                val4 = Convert.ToInt16(TxtNetAmt.Text) - Convert.ToInt16(txtdis.Text);
                //val5 = val4 - Convert.ToInt16(txtdis.Text);
                //txtpf.Text = "0.00";
                string cal1 = txtpf.Text;


                TxtNetAmt.Text = Convert.ToString(val4);
                Genclass.strfin = TxtNetAmt.Text;
            }

            //TxtRoff.Text =val4 -Convert.ToString(TxtNetAmt.Text);

        }

        private void cboexcise_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }
        private void exciseduty()
        {

            if (cboexcise.SelectedValue == null)
            {
                return;

            }
            else
            {
                conn.Open();
                {
                    Genclass.strsql = "Select * from GeneralM where TypeM_Uid=5 and Uid= " + cboexcise.SelectedValue + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    LblED1.Text = tap1.Rows[0]["F1"].ToString();
                    LblCess.Text = tap1.Rows[0]["F2"].ToString();
                    LblHECess.Text = tap1.Rows[0]["F3"].ToString();

                }
            }



        }


        private void Taxduty()
        {

            if (cbotax.SelectedValue == null)
            {
                return;

            }
            else
            {
                conn.Open();
                {
                    Genclass.strsql = "Select * from GeneralM where  Uid= " + cbotax.SelectedValue + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    lbltax.Text = tap1.Rows[0]["F1"].ToString();


                }
                conn.Close();
            }



        }

        private void cboexcise_Click(object sender, System.EventArgs e)
        {
            if (cboexcise.SelectedValue != null)
            {
                exciseduty();
                txtted.Text = "";
                CalcNetAmt();
                int ed;
                int Cess;
                int HECess;
                int excise;
                int lbled;

                //val3 = (val1 * val2) / 100;
                //txtdis.Text = Convert.ToString(val3);
                lbled = Convert.ToInt16(LblED1.Text);
                ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
                txtted.Text = Convert.ToString(ed);
                Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
                txtcess.Text = Convert.ToString(Cess);
                HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
                txthecess.Text = Convert.ToString(HECess);
                excise = ed + Cess + HECess;
                txtexcise.Text = Convert.ToString(excise);

            }
            else
            {
                txtted.Text = "0.00";
                txtcess.Text = "0.00";
                txthecess.Text = "0.00";
                txtexcise.Text = "";
                LblED1.Text = "";
                LblCess.Text = "";
                LblHECess.Text = "";



            }

        }

        private void btnsave_Click(object sender, System.EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }
            if (txttax.Text == "")
            {
                MessageBox.Show("Enter the Tax");
                txttax.Focus();
            }

            conn.Open();
     
            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
        }

        private void cbotax_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        private void cbotax_Click(object sender, System.EventArgs e)
        {
            if (cbotax.SelectedValue != null)
            {
                int ed;
                int Cess;
                int HECess;
                int excise;
                int lbled;
                int tax;
                int vat;

                lbled = Convert.ToInt16(LblED1.Text);
                ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
                txtted.Text = Convert.ToString(ed);
                Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
                txtcess.Text = Convert.ToString(Cess);
                HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
                txthecess.Text = Convert.ToString(HECess);
                excise = ed + Cess + HECess;
                txtexcise.Text = Convert.ToString(excise);
                CalcNetAmt();
                Taxduty();
                tax = Convert.ToInt16(lbltax.Text);
                vat = Convert.ToInt16(TxtNetAmt.Text) * tax / 100;
                txttax.Text = Convert.ToString(vat);
                CalcNetAmt();
            }
        }

        private void button3_Click(object sender, System.EventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {
            if (txtdcno.Text == "")
            {
                return;

            }
            else
            {
                //Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0) as pqty,b.itemuid,b.uid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid where a.uid=" + txtgrnid.Text + " group by itemname,b.pqty,b.itemuid,b.uid  having b.pqty-isnull(sum(d.pqty),0) >0 ";
                //Genclass.strsql = "select  d.itemname,e.generalname as uom,'' as Price,b.pqty-isnull(sum(c.pqty),0) as qty,'' as BasicValue,b.itemuid,b.uid  from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid left join stransactionsplist c  on b.itemuid=c.itemuid  and c.doctypeid=40 left join itemm d on b.itemuid=d.uid left join generalm e on d.uom_uid=e.uid  inner join partym f on a.partyuid=f.uid where b.doctypeid=30  group by   a.docno,b.pqty,d.itemname,e.generalname,a.docno,b.itemuid,b.uid,f.name,a.partyuid having b.pqty-isnull(sum(c.pqty),0)>0 ";
                Genclass.strsql = "select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,'' as Price,'' as BasicValue from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                for (int i = 0; i < tap1.Rows.Count; i++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
                }

            }
        }


        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int i = HFIT.SelectedCells[0].RowIndex;


            HFIT.Rows[i].Cells[4].Value = Convert.ToInt16(HFIT.Rows[i].Cells[2].Value) * Convert.ToInt16(HFIT.Rows[i].Cells[3].Value);
        }

        private void Frmcustord_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            this.Size = new Size(1109, 532);
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            HFGP.RowHeadersVisible = false;
            HFGP.ColumnHeadersVisible = false;
            Genpan.Visible = true;
            Editpan.Visible = false;
            Taxpan.Visible = false;
            Titlep();
            
            loadexcise();
            loadtax();
            //txtted.Text = "0.00";
            //txtcess.Text = "0.00";
            //txthecess.Text = "0.00";
            txtexcise.Text = "";
            ////txtpf.Text = "0.00";
            //txtdis.Text = "0.00";
            txttax.Text = "";
            
            this.HFGP.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);

            LblED1.Text = "";
            LblCess.Text = "";
            LblHECess.Text = "";
            Loadgrid();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Genpan.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            Genclass.Module.Gendocno();
            Taxpan.Visible = false;
            Editpan.Visible = true;
            DTPDOCDT.Focus();

        }

        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 1;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (txtpuid.Text == "")
                {
                    MessageBox.Show("Select the Party");
                    return;
                }
                Genclass.type = 2;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtitem.Text = "";
                txtitem.Focus();
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(txtrate.Text))
            {
                txtrate.Text = "0";
            }

            double stracc;

            stracc = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtrate.Text);
            txtvalue.Text = stracc.ToString();

            //txtvalue.Text
        }

        private void txtrate_TextChanged(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(txtqty.Text))
            {
                txtqty.Text = "0";
            }


            double stracc;

            stracc = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtrate.Text);
            txtvalue.Text = stracc.ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtitem.Text == "")
            {
                MessageBox.Show("Select the Item");
                txtitem.Focus();
                return;
            }
            if (txtqty.Text == "")
            {
                MessageBox.Show("Enter the Quantity");
                txtqty.Focus();
                return;
            }
            if (txtrate.Text == "")
            {
                MessageBox.Show("Enter the Rate");
                txtrate.Focus();
                return;
            }

            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                if (txtitid.Text == HFIT.Rows[i].Cells[4].Value.ToString())
                {
                    MessageBox.Show("Item Already Added");
                    txtitem.Text = "";

                    txtitid.Text = "";
                    txtitem.Focus();
                    return;
                }
            }

            HFIT.AutoGenerateColumns = false;

            var index = HFIT.Rows.Add();
            HFIT.Rows[index].Cells[0].Value = txtitem.Text;
            HFIT.Rows[index].Cells[1].Value = txtqty.Text;
            HFIT.Rows[index].Cells[2].Value = txtrate.Text;
            HFIT.Rows[index].Cells[3].Value = txtvalue.Text;
            HFIT.Rows[index].Cells[4].Value = txtitid.Text;
            //HFIT.Rows[index].Cells[5].Value = 0;

            txtitid.Text = "";
            txtitem.Text = "";
            txtqty.Text = "0";
            txtrate.Text = "0";
            txtvalue.Text = "0";
            txtitem.Focus();
        }

        private void btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }


    }
}
