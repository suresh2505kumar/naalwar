﻿namespace Naalwar
{
    partial class FrmSortDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSortDet));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Fraedit = new System.Windows.Forms.Panel();
            this.BtnSortIdendification = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txtgtwei = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtcrimpweft = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtpickwheel = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtwarp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtreed = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtgrwywidth = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtwept = new System.Windows.Forms.TextBox();
            this.txtppi = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtdesign = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtgsm = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtwt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtcrimp = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtreedspace = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txttotalend = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtepi = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtrollborder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsortno = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgtlen = new System.Windows.Forms.TextBox();
            this.txtHSNCode = new System.Windows.Forms.TextBox();
            this.txtSellingPriceKg = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbItemType = new System.Windows.Forms.ComboBox();
            this.txtSellingPriceMtr = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbPercentage = new System.Windows.Forms.ComboBox();
            this.frafnt = new System.Windows.Forms.Panel();
            this.Txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.btnadd = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.butexit = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.butnsave = new System.Windows.Forms.Button();
            this.Fraedit.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.frafnt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // Fraedit
            // 
            this.Fraedit.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Fraedit.Controls.Add(this.BtnSortIdendification);
            this.Fraedit.Controls.Add(this.checkBox1);
            this.Fraedit.Controls.Add(this.grSearch);
            this.Fraedit.Controls.Add(this.label3);
            this.Fraedit.Controls.Add(this.txtgtwei);
            this.Fraedit.Controls.Add(this.label26);
            this.Fraedit.Controls.Add(this.txtcrimpweft);
            this.Fraedit.Controls.Add(this.label2);
            this.Fraedit.Controls.Add(this.label28);
            this.Fraedit.Controls.Add(this.txtpickwheel);
            this.Fraedit.Controls.Add(this.label25);
            this.Fraedit.Controls.Add(this.txtwarp);
            this.Fraedit.Controls.Add(this.label5);
            this.Fraedit.Controls.Add(this.label24);
            this.Fraedit.Controls.Add(this.txtreed);
            this.Fraedit.Controls.Add(this.label17);
            this.Fraedit.Controls.Add(this.txtgrwywidth);
            this.Fraedit.Controls.Add(this.label19);
            this.Fraedit.Controls.Add(this.txtwept);
            this.Fraedit.Controls.Add(this.txtppi);
            this.Fraedit.Controls.Add(this.label22);
            this.Fraedit.Controls.Add(this.txtdesign);
            this.Fraedit.Controls.Add(this.label11);
            this.Fraedit.Controls.Add(this.txtgsm);
            this.Fraedit.Controls.Add(this.label12);
            this.Fraedit.Controls.Add(this.txtwt);
            this.Fraedit.Controls.Add(this.label10);
            this.Fraedit.Controls.Add(this.txtcrimp);
            this.Fraedit.Controls.Add(this.label8);
            this.Fraedit.Controls.Add(this.txtreedspace);
            this.Fraedit.Controls.Add(this.label7);
            this.Fraedit.Controls.Add(this.txttotalend);
            this.Fraedit.Controls.Add(this.label6);
            this.Fraedit.Controls.Add(this.txtepi);
            this.Fraedit.Controls.Add(this.label4);
            this.Fraedit.Controls.Add(this.txtrollborder);
            this.Fraedit.Controls.Add(this.label1);
            this.Fraedit.Controls.Add(this.txtsortno);
            this.Fraedit.Controls.Add(this.Phone);
            this.Fraedit.Controls.Add(this.txtgtlen);
            this.Fraedit.Controls.Add(this.txtHSNCode);
            this.Fraedit.Controls.Add(this.txtSellingPriceKg);
            this.Fraedit.Controls.Add(this.label15);
            this.Fraedit.Controls.Add(this.label14);
            this.Fraedit.Controls.Add(this.label13);
            this.Fraedit.Controls.Add(this.label16);
            this.Fraedit.Controls.Add(this.cmbItemType);
            this.Fraedit.Controls.Add(this.txtSellingPriceMtr);
            this.Fraedit.Controls.Add(this.label18);
            this.Fraedit.Controls.Add(this.cmbPercentage);
            this.Fraedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fraedit.Location = new System.Drawing.Point(0, -1);
            this.Fraedit.Name = "Fraedit";
            this.Fraedit.Size = new System.Drawing.Size(796, 475);
            this.Fraedit.TabIndex = 0;
            this.Fraedit.Paint += new System.Windows.Forms.PaintEventHandler(this.Fraedit_Paint);
            // 
            // BtnSortIdendification
            // 
            this.BtnSortIdendification.Location = new System.Drawing.Point(637, 396);
            this.BtnSortIdendification.Name = "BtnSortIdendification";
            this.BtnSortIdendification.Size = new System.Drawing.Size(92, 28);
            this.BtnSortIdendification.TabIndex = 406;
            this.BtnSortIdendification.Text = "Terry";
            this.BtnSortIdendification.UseVisualStyleBackColor = true;
            this.BtnSortIdendification.Click += new System.EventHandler(this.BtnSortIdendification_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(126, 447);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(65, 22);
            this.checkBox1.TabIndex = 227;
            this.checkBox1.Text = "Active";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(254, 18);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(382, 300);
            this.grSearch.TabIndex = 394;
            this.grSearch.Visible = false;
            this.grSearch.Paint += new System.Windows.Forms.PaintEventHandler(this.grSearch_Paint);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::Naalwar.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(182, 268);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(100, 28);
            this.btnSelect.TabIndex = 394;
            this.btnSelect.Text = "Select (F6)";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(284, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(95, 28);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F7)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(375, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(419, 295);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 21);
            this.label3.TabIndex = 262;
            this.label3.Text = "G TOWEL WEI";
            // 
            // txtgtwei
            // 
            this.txtgtwei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgtwei.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgtwei.Location = new System.Drawing.Point(532, 292);
            this.txtgtwei.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgtwei.Name = "txtgtwei";
            this.txtgtwei.Size = new System.Drawing.Size(191, 26);
            this.txtgtwei.TabIndex = 237;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(389, 200);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(135, 21);
            this.label26.TabIndex = 260;
            this.label26.Text = "Pick Wheel Combi";
            // 
            // txtcrimpweft
            // 
            this.txtcrimpweft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcrimpweft.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrimpweft.ForeColor = System.Drawing.Color.Black;
            this.txtcrimpweft.Location = new System.Drawing.Point(532, 230);
            this.txtcrimpweft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrimpweft.Name = "txtcrimpweft";
            this.txtcrimpweft.Size = new System.Drawing.Size(191, 26);
            this.txtcrimpweft.TabIndex = 233;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(20, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sort Details";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(34, 200);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(88, 21);
            this.label28.TabIndex = 258;
            this.label28.Text = "Reed Space";
            // 
            // txtpickwheel
            // 
            this.txtpickwheel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpickwheel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpickwheel.Location = new System.Drawing.Point(532, 197);
            this.txtpickwheel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpickwheel.Name = "txtpickwheel";
            this.txtpickwheel.Size = new System.Drawing.Size(188, 26);
            this.txtpickwheel.TabIndex = 231;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(431, 73);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 21);
            this.label25.TabIndex = 254;
            this.label25.Text = "Warp Count";
            // 
            // txtwarp
            // 
            this.txtwarp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwarp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwarp.Location = new System.Drawing.Point(532, 70);
            this.txtwarp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwarp.Name = "txtwarp";
            this.txtwarp.Size = new System.Drawing.Size(188, 26);
            this.txtwarp.TabIndex = 222;
            this.txtwarp.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtwarp_MouseClick);
            this.txtwarp.TextChanged += new System.EventHandler(this.txtwarp_TextChanged);
            this.txtwarp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtwarp_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(493, 107);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 21);
            this.label5.TabIndex = 251;
            this.label5.Text = "EPI";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(480, 169);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 21);
            this.label24.TabIndex = 249;
            this.label24.Text = "Reed";
            // 
            // txtreed
            // 
            this.txtreed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtreed.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtreed.ForeColor = System.Drawing.Color.Black;
            this.txtreed.Location = new System.Drawing.Point(532, 166);
            this.txtreed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtreed.Name = "txtreed";
            this.txtreed.Size = new System.Drawing.Size(188, 26);
            this.txtreed.TabIndex = 229;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(90, 139);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 21);
            this.label17.TabIndex = 248;
            this.label17.Text = "PPI";
            // 
            // txtgrwywidth
            // 
            this.txtgrwywidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrwywidth.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrwywidth.Location = new System.Drawing.Point(532, 136);
            this.txtgrwywidth.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrwywidth.Name = "txtgrwywidth";
            this.txtgrwywidth.Size = new System.Drawing.Size(188, 26);
            this.txtgrwywidth.TabIndex = 227;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(29, 107);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 21);
            this.label19.TabIndex = 246;
            this.label19.Text = "Wept Count";
            // 
            // txtwept
            // 
            this.txtwept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwept.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwept.Location = new System.Drawing.Point(126, 104);
            this.txtwept.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwept.Name = "txtwept";
            this.txtwept.Size = new System.Drawing.Size(222, 26);
            this.txtwept.TabIndex = 224;
            this.txtwept.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtwept_MouseClick);
            this.txtwept.TextChanged += new System.EventHandler(this.txtwept_TextChanged);
            // 
            // txtppi
            // 
            this.txtppi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtppi.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtppi.Location = new System.Drawing.Point(126, 136);
            this.txtppi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtppi.Name = "txtppi";
            this.txtppi.Size = new System.Drawing.Size(222, 26);
            this.txtppi.TabIndex = 226;
            this.txtppi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtppi_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(66, 73);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 21);
            this.label22.TabIndex = 245;
            this.label22.Text = "Design";
            // 
            // txtdesign
            // 
            this.txtdesign.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdesign.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdesign.Location = new System.Drawing.Point(126, 70);
            this.txtdesign.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdesign.Name = "txtdesign";
            this.txtdesign.Size = new System.Drawing.Size(222, 26);
            this.txtdesign.TabIndex = 221;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(480, 264);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 21);
            this.label11.TabIndex = 244;
            this.label11.Text = "GSM";
            // 
            // txtgsm
            // 
            this.txtgsm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgsm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgsm.Location = new System.Drawing.Point(532, 261);
            this.txtgsm.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgsm.Name = "txtgsm";
            this.txtgsm.Size = new System.Drawing.Size(191, 26);
            this.txtgsm.TabIndex = 235;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(52, 264);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 21);
            this.label12.TabIndex = 242;
            this.label12.Text = "WT/ Mtr";
            // 
            // txtwt
            // 
            this.txtwt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwt.Location = new System.Drawing.Point(126, 261);
            this.txtwt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwt.Name = "txtwt";
            this.txtwt.Size = new System.Drawing.Size(222, 26);
            this.txtwt.TabIndex = 234;
            this.txtwt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtwt_KeyDown);
            this.txtwt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtwt_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(17, 233);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 21);
            this.label10.TabIndex = 237;
            this.label10.Text = "Crimp% Warp";
            // 
            // txtcrimp
            // 
            this.txtcrimp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcrimp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrimp.Location = new System.Drawing.Point(126, 230);
            this.txtcrimp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrimp.Name = "txtcrimp";
            this.txtcrimp.Size = new System.Drawing.Size(222, 26);
            this.txtcrimp.TabIndex = 232;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(42, 169);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 21);
            this.label8.TabIndex = 9;
            this.label8.Text = "Total Ends";
            // 
            // txtreedspace
            // 
            this.txtreedspace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtreedspace.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtreedspace.Location = new System.Drawing.Point(126, 197);
            this.txtreedspace.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtreedspace.Name = "txtreedspace";
            this.txtreedspace.Size = new System.Drawing.Size(222, 26);
            this.txtreedspace.TabIndex = 230;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(434, 139);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 21);
            this.label7.TabIndex = 7;
            this.label7.Text = "Grey Width";
            // 
            // txttotalend
            // 
            this.txttotalend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotalend.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalend.Location = new System.Drawing.Point(126, 166);
            this.txttotalend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txttotalend.Name = "txttotalend";
            this.txttotalend.Size = new System.Drawing.Size(222, 26);
            this.txttotalend.TabIndex = 228;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(424, 233);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 21);
            this.label6.TabIndex = 4;
            this.label6.Text = "Crimp% Weft";
            // 
            // txtepi
            // 
            this.txtepi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtepi.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtepi.Location = new System.Drawing.Point(532, 104);
            this.txtepi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtepi.Name = "txtepi";
            this.txtepi.Size = new System.Drawing.Size(188, 26);
            this.txtepi.TabIndex = 225;
            this.txtepi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtepi_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(365, 40);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Id Thread/Roll Border";
            this.label4.Click += new System.EventHandler(this.Label4_Click);
            // 
            // txtrollborder
            // 
            this.txtrollborder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrollborder.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrollborder.Location = new System.Drawing.Point(532, 37);
            this.txtrollborder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrollborder.Name = "txtrollborder";
            this.txtrollborder.Size = new System.Drawing.Size(188, 26);
            this.txtrollborder.TabIndex = 220;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(59, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sort No";
            // 
            // txtsortno
            // 
            this.txtsortno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsortno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsortno.Location = new System.Drawing.Point(126, 37);
            this.txtsortno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsortno.Name = "txtsortno";
            this.txtsortno.Size = new System.Drawing.Size(222, 26);
            this.txtsortno.TabIndex = 219;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.ForeColor = System.Drawing.Color.Black;
            this.Phone.Location = new System.Drawing.Point(18, 295);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(104, 21);
            this.Phone.TabIndex = 7;
            this.Phone.Text = "G TOWEL LEN";
            // 
            // txtgtlen
            // 
            this.txtgtlen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgtlen.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgtlen.Location = new System.Drawing.Point(126, 292);
            this.txtgtlen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgtlen.Name = "txtgtlen";
            this.txtgtlen.Size = new System.Drawing.Size(222, 26);
            this.txtgtlen.TabIndex = 236;
            // 
            // txtHSNCode
            // 
            this.txtHSNCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSNCode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHSNCode.Location = new System.Drawing.Point(126, 325);
            this.txtHSNCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtHSNCode.Name = "txtHSNCode";
            this.txtHSNCode.Size = new System.Drawing.Size(222, 26);
            this.txtHSNCode.TabIndex = 396;
            // 
            // txtSellingPriceKg
            // 
            this.txtSellingPriceKg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSellingPriceKg.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSellingPriceKg.Location = new System.Drawing.Point(126, 358);
            this.txtSellingPriceKg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSellingPriceKg.Name = "txtSellingPriceKg";
            this.txtSellingPriceKg.Size = new System.Drawing.Size(222, 26);
            this.txtSellingPriceKg.TabIndex = 400;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(31, 361);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 21);
            this.label15.TabIndex = 399;
            this.label15.Text = "Rate Per KG";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(4, 394);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 21);
            this.label14.TabIndex = 397;
            this.label14.Text = "GST Precentage";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(43, 328);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 21);
            this.label13.TabIndex = 395;
            this.label13.Text = "HSN Code";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(446, 331);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 21);
            this.label16.TabIndex = 402;
            this.label16.Text = "Item Type";
            // 
            // cmbItemType
            // 
            this.cmbItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemType.FormattingEnabled = true;
            this.cmbItemType.Location = new System.Drawing.Point(532, 328);
            this.cmbItemType.Name = "cmbItemType";
            this.cmbItemType.Size = new System.Drawing.Size(197, 26);
            this.cmbItemType.TabIndex = 401;
            this.cmbItemType.SelectedIndexChanged += new System.EventHandler(this.CmbItemType_SelectedIndexChanged);
            // 
            // txtSellingPriceMtr
            // 
            this.txtSellingPriceMtr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSellingPriceMtr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSellingPriceMtr.Location = new System.Drawing.Point(532, 362);
            this.txtSellingPriceMtr.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSellingPriceMtr.Name = "txtSellingPriceMtr";
            this.txtSellingPriceMtr.Size = new System.Drawing.Size(197, 26);
            this.txtSellingPriceMtr.TabIndex = 405;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(425, 365);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 21);
            this.label18.TabIndex = 404;
            this.label18.Text = "Rate Per Mtr";
            // 
            // cmbPercentage
            // 
            this.cmbPercentage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPercentage.FormattingEnabled = true;
            this.cmbPercentage.Location = new System.Drawing.Point(126, 391);
            this.cmbPercentage.Name = "cmbPercentage";
            this.cmbPercentage.Size = new System.Drawing.Size(222, 26);
            this.cmbPercentage.TabIndex = 403;
            // 
            // frafnt
            // 
            this.frafnt.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.frafnt.Controls.Add(this.Txtscr5);
            this.frafnt.Controls.Add(this.Txtscr4);
            this.frafnt.Controls.Add(this.Txtscr3);
            this.frafnt.Controls.Add(this.txtscr1);
            this.frafnt.Controls.Add(this.label9);
            this.frafnt.Controls.Add(this.HFGP);
            this.frafnt.Location = new System.Drawing.Point(1, -1);
            this.frafnt.Name = "frafnt";
            this.frafnt.Size = new System.Drawing.Size(795, 474);
            this.frafnt.TabIndex = 222;
            // 
            // Txtscr5
            // 
            this.Txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr5.Location = new System.Drawing.Point(560, 42);
            this.Txtscr5.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr5.Name = "Txtscr5";
            this.Txtscr5.Size = new System.Drawing.Size(225, 24);
            this.Txtscr5.TabIndex = 227;
            this.Txtscr5.TextChanged += new System.EventHandler(this.Txtscr5_TextChanged);
            // 
            // Txtscr4
            // 
            this.Txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr4.Location = new System.Drawing.Point(436, 42);
            this.Txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr4.Name = "Txtscr4";
            this.Txtscr4.Size = new System.Drawing.Size(125, 24);
            this.Txtscr4.TabIndex = 226;
            this.Txtscr4.TextChanged += new System.EventHandler(this.Txtscr4_TextChanged);
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(262, 42);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(176, 24);
            this.Txtscr3.TabIndex = 225;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(4, 42);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(259, 24);
            this.txtscr1.TabIndex = 223;
            this.txtscr1.TextChanged += new System.EventHandler(this.Txtscr1_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(5, 7);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 21);
            this.label9.TabIndex = 222;
            this.label9.Text = "Sort Details";
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.Color.White;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(3, 67);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(784, 402);
            this.HFGP.TabIndex = 4;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Location = new System.Drawing.Point(2, 480);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(792, 35);
            this.panadd.TabIndex = 223;
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(435, 5);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderSize = 0;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(514, 2);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(91, 30);
            this.btnadd.TabIndex = 184;
            this.btnadd.Text = "Add new";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.Btnadd_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(730, 2);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(58, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.Buttnext1_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(605, 2);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.Butedit_Click);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(659, 2);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(71, 30);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.Butexit_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(637, 481);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 225;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // butnsave
            // 
            this.butnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.butnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butnsave.Image = ((System.Drawing.Image)(resources.GetObject("butnsave.Image")));
            this.butnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butnsave.Location = new System.Drawing.Point(573, 481);
            this.butnsave.Name = "butnsave";
            this.butnsave.Size = new System.Drawing.Size(63, 30);
            this.butnsave.TabIndex = 224;
            this.butnsave.Text = "Save";
            this.butnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butnsave.UseVisualStyleBackColor = false;
            this.butnsave.Click += new System.EventHandler(this.Butnsave_Click);
            // 
            // FrmSortDet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(804, 520);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.butnsave);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.Fraedit);
            this.Controls.Add(this.frafnt);
            this.Name = "FrmSortDet";
            this.Text = "Sort Details";
            this.Load += new System.EventHandler(this.FrmSortDet_Load);
            this.Fraedit.ResumeLayout(false);
            this.Fraedit.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.frafnt.ResumeLayout(false);
            this.frafnt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Fraedit;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtcrimpweft;
        protected System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtpickwheel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtwarp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtreed;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtgrwywidth;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtwept;
        private System.Windows.Forms.TextBox txtppi;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtdesign;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtgsm;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtwt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtcrimp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtreedspace;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txttotalend;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtepi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtrollborder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtsortno;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgtlen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtgtwei;
        private System.Windows.Forms.Panel frafnt;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button butnsave;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Txtscr5;
        private System.Windows.Forms.TextBox Txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtHSNCode;
        private System.Windows.Forms.TextBox txtSellingPriceKg;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbPercentage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbItemType;
        private System.Windows.Forms.TextBox txtSellingPriceMtr;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button BtnSortIdendification;
    }
}