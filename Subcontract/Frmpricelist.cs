﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace Naalwar
{
    public partial class Frmpricelist : Form
    {
        public Frmpricelist()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        int uid = 0;
        //int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        SqlCommand qur = new SqlCommand();

        private void Frmpricelist_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(Genpan);
            Genclass.Module.buttonstylepanel(updatpan);
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            //Genclass.Module.buttonstyleform(this);
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.HFGP.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            this.HFG1.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFG1.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            this.HFG2.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFG2.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.RowHeadersVisible = false;
            //HFG1.EnableHeadersVisualStyles = false;
            //HFG1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFG1.RowHeadersVisible = false;
            //HFG2.EnableHeadersVisualStyles = false;
            //HFG2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFG2.RowHeadersVisible = false;
            Genclass.Module.ClearTextBox(this, Genpan);
            txtmm.Text = "10";

            if (Genclass.Gbtxtid == 130)
            {
                label5.Text = "Sales Price List";
            }
            else
            {

                label5.Text = "Purchase Price List";
            }
            updatpan.Visible = false;
            //DataGridViewCheckBoxColumn colCB = new DataGridViewCheckBoxColumn();
            //DatagridViewCheckBoxHeaderCell cbHeader = new DatagridViewCheckBoxHeaderCell();
            //colCB.HeaderCell = cbHeader;
            //HFG1.Columns.Add(colCB);
            Titlep1();
            Loadgrid1();
            Titlep2();
       
        }
        private void Titlep1()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";

            checkColumn.TrueValue = true;
            checkColumn.FalseValue = false;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFG1.Columns.Add(checkColumn);
        }
          
        private void Titlep2()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";

            checkColumn.TrueValue = true;
            checkColumn.FalseValue = false;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFG2.Columns.Add(checkColumn);
        }
        private void loadput()
        {
            conn.Close();
            conn.Open();
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.fieldSix = "";
            Genclass.fieldSeven = "";


            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Party", Genclass.strsql, this, txtpuid, txtname, Genpan);
                Genclass.strsql = "select top 25 uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + " and type='Customer' order by name ";

                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 7)
            {
                Genclass.Module.Partylistviewcont("uid", "Party", Genclass.strsql, this, txtpuid, txtname, Genpan);
                Genclass.strsql = "select top 25 uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + " and type='Supplier' order by name ";

                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 5)
            {
                Genclass.Module.Partylistviewcont("uid", "Party", Genclass.strsql, this, txtpartyuid, txtparty, updatpan);
                Genclass.strsql = "select top 25 uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + " and type='Customer' order by name ";

                Genclass.FSSQLSortStr = "Name";

            }


            else if (Genclass.type == 4)
            {
                Genclass.Module.Partylistviewcont("uid", "Party", Genclass.strsql, this, txtpuid, txtname, Genpan);
                Genclass.strsql = "select top 25 uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + " and type='Supplier' order by name ";

                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont3("uid", "Item","itemcode", Genclass.strsql, this, txtitid, txtitem,txtcode, Genpan);
                Genclass.strsql = "select top 25 uid,ItemName,itemcode  from Itemm  where active=1  and companyid=" + Genclass.data1 + " ";
                Genclass.FSSQLSortStr = "ItemName";
                Genclass.FSSQLSortStr1 = "itemcode";
            }

            else if (Genclass.type == 3)
            {
                Genclass.Module.Partylistviewcont("uid", "UoM", Genclass.strsql, this, txtuomid, txtuom, Genpan);
                Genclass.strsql = "select uid,generalname as UoM  from Generalm where active=1 and typem_uid=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "generalname";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 400;
            if (Genclass.type == 2)
            {

                dt.Columns[2].Width = 200;
            }


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();
            conn.Close();


        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Itemname";
                //Genclass.FSSQLSortStr1 = "Docdate";
                //Genclass.FSSQLSortStr2 = "Dcno";
                //Genclass.FSSQLSortStr3 = "Dcdate";
                //Genclass.FSSQLSortStr4 = "Name";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                //if (Txtscr2.Text != "")
                //{
                //    if (Genclass.StrSrch == "")
                //    {
                //        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                //    }
                //    else
                //    {
                //        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                //    }

                //}

                //if (Txtscr3.Text != "")
                //{
                //    if (Genclass.StrSrch == "")
                //    {
                //        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                //    }
                //    else
                //    {
                //        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                //    }

                //}

                //if (txtscr4.Text != "")
                //{
                //    if (Genclass.StrSrch == "")
                //    {
                //        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                //    }
                //    else
                //    {
                //        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                //    }

                //}

                //if (txtscr5.Text != "")
                //{
                //    if (Genclass.StrSrch == "")
                //    {
                //        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                //    }
                //    else
                //    {
                //        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                //    }

                //}





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                //else if (Txtscr2.Text != "")
                //{
                //    Genclass.StrSrch = " " + Genclass.StrSrch;
                //}

                //else if (Txtscr3.Text != "")
                //{
                //    Genclass.StrSrch = " " + Genclass.StrSrch;
                //}
                //else if (txtscr4.Text != "")
                //{
                //    Genclass.StrSrch = " " + Genclass.StrSrch;
                //}
                //else if (txtscr5.Text != "")
                //{
                //    Genclass.StrSrch = " " + Genclass.StrSrch;
                //}
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }

                //string quy = "select a.uid,itemname as Item,Price,d.F1 as CGST,e.F1 as SGST,f.F1 as IGST,disper as Discount from pur_price_list a inner join ItemM b on a.itemuid=b.uid  inner join generalm d on a.cgstid=d.uid inner join generalm e on a.sgstid=e.uid inner join generalm f on a.igstid=f.uid where eff_to is null and a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and  " + Genclass.StrSrch + "";
                if (txtstate.Text == "Tamil Nadu 33")
                {
                    string quy = "select a.uid,itemname as Item,e.GeneralName as UoM,Price,convert(decimal(18,2),h.f1/2,105) as CGST,convert(decimal(18,2),h.f1/2,105)   as SGST,disper as Dis,Eff_from,b.uid as itemid from pur_price_list a left join ItemM b on a.itemuid=b.uid left join ItemGroup c on b.itemgroup_uid=c.uid      left join generalm e on b.uom_uid=e.uid left join generalm h on b.tax=h.uid where eff_to is null  and a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and  " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
                    this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                    HFGP.AutoGenerateColumns = false;
                    HFGP.Refresh();
                    HFGP.DataSource = null;
                    HFGP.Rows.Clear();


                    HFGP.ColumnCount = tap.Columns.Count;
                    Genclass.i = 0;

                    foreach (DataColumn column in tap.Columns)
                    {
                        HFGP.Columns[Genclass.i].Name = column.ColumnName;
                        HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                        HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }




                    HFGP.Columns[0].Visible = false;

                    HFGP.Columns[1].Width = 290;
                    HFGP.Columns[2].Width = 80;
                    HFGP.Columns[3].Width = 50;
                    HFGP.Columns[4].Width = 50;
                    HFGP.Columns[5].Width = 80;
                    HFGP.Columns[6].Width = 60;

                    HFGP.Columns[7].Width = 95;
                    HFGP.Columns[8].Visible = false;



                    HFGP.DataSource = tap;
                }
                else
                {
                    string quy = "select a.uid,itemname as Item,e.GeneralName as UoM,Price,c.Sname as IGST,disper as Dis,Eff_from from pur_price_list a inner join ItemM b on a.itemuid=b.uid  inner join ItemGroup c on b.itemgroup_uid=c.uid  left join generalm e on b.uom_uid=e.uid  where eff_to is null and a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and  " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
                    this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                    HFGP.AutoGenerateColumns = false;
                    HFGP.Refresh();
                    HFGP.DataSource = null;
                    HFGP.Rows.Clear();


                    HFGP.ColumnCount = tap.Columns.Count;
                    Genclass.i = 0;

                    foreach (DataColumn column in tap.Columns)
                    {
                        HFGP.Columns[Genclass.i].Name = column.ColumnName;
                        HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                        HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }



                    HFGP.Columns[0].Visible = false;

                    HFGP.Columns[1].Width = 300;
                    HFGP.Columns[2].Width = 60;
                    HFGP.Columns[3].Width = 80;
                    HFGP.Columns[4].Width = 80;
                    HFGP.Columns[5].Width = 45;

                    HFGP.Columns[6].Width = 90;

                    HFGP.DataSource = tap;
                }







            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        public void Loadtax()
        {
            conn.Open();


            //cboigst.DataSource = null;

            //cboigst.SelectedIndex = -1;
            if (txtstate.Text != "")
            {
                string qur1 = " select a.UId,f1 as SGST from itemm a inner join generalm b on a.tax=b.uid where a.companyid=" + Genclass.data1 + "  and a.uid="+ txtitid.Text +" order by Generalname";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);
                if (tab1.Rows.Count > 0)
                {
                    txttaxper.Text = tab1.Rows[0]["SGST"].ToString();
                    txttxid.Text = tab1.Rows[0]["UId"].ToString();
                }

                if (txtstate.Text == "Tamil Nadu 33")
                {

                    txtitem.Width = 388;
                    label22.Visible = true;
                    txtcgta.Visible = true;
                    label4.Text = "SGST%";
                    double dq = Convert.ToDouble(tab1.Rows[0]["SGST"].ToString()) / 2;
                    txttaxper.Text = dq.ToString();
                    txtcgta.Text = dq.ToString();
                    //cboigst.DataSource = null;
                    //cboigst.DataSource = tab1;
                    //cboigst.DisplayMember = "SGST";
                    //cboigst.ValueMember = "Uid";
                    ////cboSGST.SelectedIndex = -1;
                }
                else
                {
                    txtitem.Width = 459;
                    label22.Visible = false;
                    txtcgta.Visible = false;
                    label4.Text = "IGST%";
                    //cboigst.DataSource = null;
                    //cboigst.DataSource = tab2;
                    //cboigst.DisplayMember = "IGST";
                    //cboigst.ValueMember = "uid";
                    ////cboigst.SelectedIndex = -1;

                }

            }
            conn.Close();
        }

        private void Loadgrid1()
        {
            conn.Close();
            conn.Open();

            {

                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "name";

                if (txtparty.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtparty.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtparty.Text + "%'";
                    }
                }

                if (txtparty.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "uid <> 0";
                }
                if (txtparty.Text == "")
                {
                    string quy = "select name as party,uid from partym where companyid =" + Genclass.data1 + "  order by name";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                  string quy = "select name as party,uid from partym where companyid =" + Genclass.data1 + " and " + Genclass.StrSrch + " order by name";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                HFG1.AutoGenerateColumns = false;
                HFG1.Refresh();
                HFG1.DataSource = null;
                HFG1.Rows.Clear();


                HFG1.ColumnCount = tap.Columns.Count + 1;
                Genclass.i = 1;
                foreach (DataColumn column in tap.Columns)
                {
                    HFG1.Columns[Genclass.i].Name = column.ColumnName;
                    HFG1.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFG1.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFG1.Columns[1].Width = 350;
                HFG1.Columns[2].Visible = false;
                HFG1.DataSource = tap;
            }

            
        }

        private void Loadgrid2()
        {
            conn.Close();
            conn.Open();

            {

                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "itemname";

                if (txtoite.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtoite.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtoite.Text + "%'";
                    }
                }

                if (txtoite.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                if (txtoite.Text == "")
                {
                    string quy = "select itemname,c.uid from pur_price_list a left join PartyM b on a.Suppuid=b.Uid left join ItemM c on a.Itemuid=c.uid  where a.companyid =" + Genclass.data1 + "  and a.Suppuid=" + txtpartyuid.Text + "  and  " + Genclass.StrSrch + " order by itemname  ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
               {
                   string quy = "select itemname,c.uid from pur_price_list a left join PartyM b on a.Suppuid=b.Uid left join ItemM c on a.Itemuid=c.uid  where a.companyid =" + Genclass.data1 + "  and a.Suppuid=" + txtpartyuid.Text + " order by itemname ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
              
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                HFG2.AutoGenerateColumns = false;
                HFG2.Refresh();
                HFG2.DataSource = null;
                HFG2.Rows.Clear();


                HFG2.ColumnCount = tap.Columns.Count + 1;
                Genclass.i = 1;
                foreach (DataColumn column in tap.Columns)
                {
                    HFG2.Columns[Genclass.i].Name = column.ColumnName;
                    HFG2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFG2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFG2.Columns[1].Width = 350;
                HFG2.Columns[2].Visible = false;
                HFG2.DataSource = tap;
            }


        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 1;
                loadput();

            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    if (txtpuid.Text == "")
            //    {
            //        MessageBox.Show("Select the Party");
            //        return;
            //    }

            //    Genclass.type = 2;
            //    loadput();
            //    //Loadtax();

            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtitem.Text = "";
            //    txtitem.Focus();
            //}
        }


        private void txtitid_TextChanged(object sender, EventArgs e)
        {
            Loadtax();
            txtuom.Focus();
        }

        private void buttrqok_Click(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Select the Party");
                txtname.Focus();
                return;
            }

            if (txtitem.Text == "")
            {
                MessageBox.Show("Select the Item");
                txtitem.Focus();
                return;
            }

            //if (txtuom.Text == "")
            //{
            //    MessageBox.Show("Select the UoM");
            //    txtuom.Focus();
            //    return;
            //}

            if (txtrate.Text == "")
            {
                MessageBox.Show("Select the Rate");
                txtrate.Focus();
                return;
            }

            if (txtdis.Text == "")
            {
                txtdis.Text = "0";
            }

            conn.Open();

            //DateTime date1 = Dtpdt.Value;
            //string date2 = date1.ToString("dd-MM-yyyy");
            //DateTime date3 = date2;

            if (uid == 0)
            {

                Genclass.strsql = "select * from pur_price_list where suppuid=" + txtpuid.Text + " and itemuid=" + txtitid.Text + " and eff_to is null";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap6 = new DataTable();
                aptr6.Fill(tap6);



                if (tap6.Rows.Count > 0)
                {
                    tpuid = tap6.Rows[0]["uid"].ToString();
                    qur.CommandText = "update pur_price_list set eff_to='" + Dtpdt.Text + "' where uid= " + tpuid + "";
                    qur.ExecuteNonQuery();
                }
                if (txttaxper.Text != "" && txtcgta.Text != "" || txtcgta.Text != "")
                {
                    qur.CommandText = "Insert into pur_price_list values (" + txtpuid.Text + "," + txtitid.Text + "," + txtrate.Text + ",'" + Dtpdt.Text + "',Null,Null,'" + DateTime.Now + "',Null,Null,null,0," + Genclass.data1 + "," + Genclass.Yearid + ",0)";
                    qur.ExecuteNonQuery();
                }





            }
            else
            {
                Genclass.strsql = "select * from pur_price_list where suppuid=" + txtpuid.Text + " and itemuid=" + txtitid.Text + " and eff_to is null and  eff_from < '" + Dtpdt.Text + "'";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap6 = new DataTable();
                aptr6.Fill(tap6);
                if (tap6.Rows.Count == 0)
                {


                    qur.CommandText = "update pur_price_list set price=" + txtrate.Text + " where uid= " + uid + "";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    MessageBox.Show("Can not update the record check the previous date ");
                }
            }
            uid = 0;
            txtitem.Text = "";
            txtrate.Text = "";
            txtdis.Text = "";
            txtuom.Text = "";
            txtcgta.Text = "";
            txttaxper.Text = "";
            conn.Close();
            Loadgrid();
            MessageBox.Show("Record Updated");
            txtitem.Focus();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP_DoubleClick(object sender, EventArgs e)
        {
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = Convert.ToInt16(HFGP.Rows[i].Cells[0].Value.ToString());
            Genclass.strsql = "select itemname as Item,b.uid as itid,Price,disper,eff_from,uomid,generalname from pur_price_list a inner join ItemM b on a.itemuid=b.uid left join generalm c on a.uomid=c.uid   where eff_to is null and a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and a.uid=" + uid + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap6 = new DataTable();
            aptr6.Fill(tap6);

            txtitem.Text = tap6.Rows[0]["Item"].ToString();
            txtitid.Text = tap6.Rows[0]["itid"].ToString();
            txtrate.Text = tap6.Rows[0]["Price"].ToString();
            txtuomid.Text = tap6.Rows[0]["uomid"].ToString();
            txtuom.Text = tap6.Rows[0]["generalname"].ToString();
            //cboigst.Text = tap6.Rows[0]["IGST"].ToString();
            txtmm.Text = tap6.Rows[0]["disper"].ToString();
            Dtpdt.Text = tap6.Rows[0]["eff_from"].ToString();
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpuid_TextChanged_1(object sender, EventArgs e)
        {
            Genclass.strsql = "select a.uid,generalname from generalm a inner join partym b on a.uid=b.stateuid where b.uid=" + txtpuid.Text + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap6 = new DataTable();
            aptr6.Fill(tap6);

            txtstid.Text = tap6.Rows[0]["uid"].ToString();
            txtstate.Text = tap6.Rows[0]["generalname"].ToString();
            //Loadtax();
            Loadgrid();
            Dtpdt.Focus();

            //Genclass.strsql = "select a.uid,generalname from generalm a inner join partym b on a.uid=b.stateuid where b.uid=" + txtpuid.Text + "";
            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap6 = new DataTable();
            //aptr6.Fill(tap6);

        }

        private void txtitid_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txttaxper_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtitem.Text == "")
                {
                    MessageBox.Show("Select the UoM");
                    return;
                }

                Genclass.type = 3;
                loadput();
                //Loadtax();
                //txtrate.Focus();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtuom.Text = "";
                txtuom.Focus();
            }
        }

        private void txtmm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtmm.Text == "")
                {
                    MessageBox.Show("Select the UoM");
                    return;
                }

                Genclass.type = 4;
                loadput();
                //Loadtax();

            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtmm.Text = "";
                txtmm.Focus();
            }
        }

        private void txtdis_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttrqok_Click(sender, e);
            }
        }

        private void txtuomid_TextChanged(object sender, EventArgs e)
        {
            txtrate.Focus();
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            if (Genclass.Gbtxtid == 130)
            {
                Genclass.type = 1;
                loadput();
            }

            else
            {
                Genclass.type = 7;
                loadput();
            }
        }

        private void txtitem_Click(object sender, EventArgs e)
        {
            if (txtpuid.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }

            Genclass.type = 2;
            loadput();
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_Click(object sender, EventArgs e)
        {
            if (txtitem.Text == "")
            {
                MessageBox.Show("Select the UoM");
                return;
            }

            Genclass.type = 3;
            loadput();
        }

        private void txtstate_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            updatpan.Visible = true;
            //Loadgrid2();
            
        }

        private void updatpan_Paint(object sender, PaintEventArgs e)
        {
      
        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {
        
        }

        private void HFG2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkact_Click(object sender, EventArgs e)
        {
            if (chkact.Checked == true)
            {
                for (int i = 0; i < HFG1.RowCount; i++)
                {
                    HFG1[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < HFG1.RowCount; i++)
                {
                    HFG1[0, i].Value = false;
                }
            }

        }


        private void HFG1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)HFG1.Rows[HFG1.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void HFG1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFG2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)HFG2.Rows[HFG2.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void txtparty_TextChanged(object sender, EventArgs e)
        {
            //Loadgrid1();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                for (int i = 0; i < HFG2.RowCount; i++)
                {
                    HFG2[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < HFG2.RowCount; i++)
                {
                    HFG2[0, i].Value = false;
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            updatpan.Visible = false;
            Genpan.Visible = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

            //int i = 0;
            int j = 0;
   
            conn.Close();
            conn.Open();
                //qur.CommandText = "delete from tmpitid";
                //qur.ExecuteNonQuery();
                qur.CommandText = "delete from tmpitid2";
                qur.ExecuteNonQuery();

                //foreach (DataGridViewRow row in HFG1.Rows)
                //{
                //    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

                //    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                //    {
                //        qur.CommandText = "delete from tmpitid where itemid=" + HFG1.Rows[i].Cells[2].Value.ToString() + "";
                //        qur.ExecuteNonQuery();
                //        qur.CommandText = "insert into tmpitid values (" + HFG1.Rows[i].Cells[2].Value.ToString() + ")";
                //        qur.ExecuteNonQuery();
                //    }

                //    i = i + 1;

                //}
         

                foreach (DataGridViewRow row in HFG2.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        if (HFG2.Rows[j].Cells[2].Value.ToString() != null)
                        {
                            qur.CommandText = "delete from tmpitid2 where itemid=" + HFG2.Rows[j].Cells[2].Value.ToString() + "";
                            qur.ExecuteNonQuery();
                            qur.CommandText = "insert into tmpitid2 values (" + HFG2.Rows[j].Cells[2].Value.ToString() + ")";
                            qur.ExecuteNonQuery();
                        }
                    }

                    j = j + 1;

                }

                Genclass.strsql = "select itemid from tmpitid2";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                //Genclass.strsql = "select itemid,isnull(b.price,0) as price from tmpitid2 a left join pur_price_list b on a.itemid=b.itemuid where companyid="+Genclass.data1 +" group by itemid,price";
                //Genclass.strsql = "select a.itemuid,isnull(price,0) as pric    from pur_price_list a inner join tmpitid2 b on a.itemuid=b.itemid where eff_to is null  ";
                Genclass.strsql = "select itemid,isnull(b.price,0) as price from tmpitid2 a left join pur_price_list b on a.itemid=b.itemuid where eff_to is null  group by itemid,price ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count>0)
                {
                   
                        for (int m = 0; m < tap2.Rows.Count; m++)
                        {
                            Genclass.strsql = "select itemid,isnull(b.price,0) as price from tmpitid2 a inner join pur_price_list b on a.itemid=b.itemuid where eff_to is null and b.suppuid=" + txtpartyuid.Text + " and b.itemuid=" + tap2.Rows[m]["itemid"].ToString() + "    group by itemid,price ";
                            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                            DataTable tap3 = new DataTable();
                            aptr3.Fill(tap3);
                            if (tap3.Rows.Count > 0)
                            {
                                //qur.CommandText = "UPDATE pur_price_list set eff_to='" + DateTime.Now + "', where suppuid=" + txtpuid.Text + " and itemuid=" + tap2.Rows[m]["itemid"].ToString() + "";
                                //qur.ExecuteNonQuery();
                                Genclass.strsql = "select itemuid,isnull(price,0) as price from pur_price_list  where eff_to is null and suppuid=" + txtpartyuid.Text + " and itemuid=" + tap2.Rows[m]["itemid"].ToString() + "    ";
                                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                                DataTable tap4 = new DataTable();
                                aptr4.Fill(tap4);
                                if (tap4.Rows.Count > 0)
                                {
                                    string val9 = tap4.Rows[0]["price"].ToString();
                                }
                                qur.CommandText = "Insert into pur_price_list values (" + txtpuid.Text + "," + tap2.Rows[m]["itemid"].ToString() + ", " + tap4.Rows[0]["price"].ToString() + ",'" + DateTime.Now + "',Null,Null,'" + DateTime.Now + "',Null,Null,null,0," + Genclass.data1 + "," + Genclass.Yearid + ",0)";
                                qur.ExecuteNonQuery();
                            }
                            else


                            {
                                Genclass.strsql = "select itemuid,isnull(price,0) as price from pur_price_list  where eff_to is null and suppuid=" + txtpuid.Text + " and itemuid=" + tap2.Rows[m]["itemid"].ToString() + "   ";
                                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                                DataTable tap4 = new DataTable();
                                aptr4.Fill(tap4);
                                if (tap4.Rows.Count > 0)
                                {
                                    string val9 = tap4.Rows[0]["price"].ToString();
                                }
                                qur.CommandText = "Insert into pur_price_list values (" + txtpuid.Text + "," + tap2.Rows[m]["itemid"].ToString() + ", " + tap4.Rows[0]["price"].ToString() + ",null,Null,Null,'" + DateTime.Now + "',Null,Null,null,0," + Genclass.data1 + "," + Genclass.Yearid + ",0)";
                                qur.ExecuteNonQuery();
                            }
                        }
                                    }
                updatpan.Visible = false;
                conn.Close();
                Loadgrid();
            
            }

        private void txtparty_Click(object sender, EventArgs e)
        {
           
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
              
                
                    Genclass.type = 5;
                    loadput();
                
              

            }

        private void txtpartyuid_TextChanged(object sender, EventArgs e)
        {
            Loadgrid2();
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                string message = "Are You Sure to Inctive this item ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                int i = HFGP.SelectedCells[0].RowIndex;
                int uid = HFGP.SelectedCells[0].RowIndex;

                conn.Close();
                conn.Open();

              


                qur.CommandText = "delete from pur_price_list  where itemuid=" + HFGP.CurrentRow.Cells[8].Value.ToString() + "  and Eff_from='" + HFGP.CurrentRow.Cells[7].Value.ToString() + "' and suppuid=" + txtpuid.Text + " and companyid=" + Genclass.data1 + "";
                qur.ExecuteNonQuery();


                Genclass.strsql = "select max(uid) as uid from pur_price_list  where  suppuid=" + txtpuid.Text + " and itemuid=" + HFGP.CurrentRow.Cells[8].Value.ToString() + "   ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap4 = new DataTable();
                aptr4.Fill(tap4);
                if (tap4.Rows.Count > 0)
                {
                    qur.CommandText = "update pur_price_list set eff_to=null where uid=" + tap4.Rows[0]["uid"].ToString() + " ";
                    qur.ExecuteNonQuery();
                }
                Loadgrid();

            }
            }
        }
          
              

        }
        }
    
