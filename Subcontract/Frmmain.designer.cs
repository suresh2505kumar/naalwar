﻿namespace Naalwar
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printPreviewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnuit_Grp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu_subgrp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu_cat = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_uom = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_CGST = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.taxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beamNoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spindleWeightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_mac = new System.Windows.Forms.ToolStripMenuItem();
            this.mendorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_emp = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_terms = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_addc = new System.Windows.Forms.ToolStripMenuItem();
            this.termsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_Hsn = new System.Windows.Forms.ToolStripMenuItem();
            this.machineMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yarnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pLYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yarnTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cTNPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yarnMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_Item = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_par = new System.Windows.Forms.ToolStripMenuItem();
            this.mun_cusparty = new System.Windows.Forms.ToolStripMenuItem();
            this.Mun_suppparty = new System.Windows.Forms.ToolStripMenuItem();
            this.jobOrderVendorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.millToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_Pricelist = new System.Windows.Forms.ToolStripMenuItem();
            this.salesPriceListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasePriceListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortingDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_tran = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gRNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billAccountingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_som = new System.Windows.Forms.ToolStripMenuItem();
            this.salesDCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_salesm = new System.Windows.Forms.ToolStripMenuItem();
            this.Mnu_salret = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openingStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returnableDCToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nonReturnableReceiptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nonReturnableDCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.moneyReceiptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.subcontractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packingSlipToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.yarnIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yarnReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fabricIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weptIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internalOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loomBookingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortBookingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeJobCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openingStockToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.warbBookingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mISToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dCLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftWiseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qCReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rollReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderPendingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobcardReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dCRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.printToolStripButton,
            this.printPreviewToolStripButton,
            this.toolStripSeparator2,
            this.helpToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(632, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            this.toolStrip.Visible = false;
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "New";
            this.newToolStripButton.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Open";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Save";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "Print";
            // 
            // printPreviewToolStripButton
            // 
            this.printPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printPreviewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripButton.Image")));
            this.printPreviewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.printPreviewToolStripButton.Name = "printPreviewToolStripButton";
            this.printPreviewToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printPreviewToolStripButton.Text = "Print Preview";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "Help";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 512);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(668, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.Mnu_tran,
            this.mISToolStripMenuItem,
            this.reportToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(668, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem,
            this.Mnuit_Grp,
            this.mnu_subgrp,
            this.mnu_cat,
            this.Mnu_uom,
            this.exitToolStripMenuItem,
            this.Mnu_CGST,
            this.exitToolStripMenuItem1,
            this.taxToolStripMenuItem,
            this.beamNoToolStripMenuItem,
            this.spindleWeightToolStripMenuItem,
            this.Mnu_mac,
            this.mendorToolStripMenuItem,
            this.Mnu_emp,
            this.Mnu_terms,
            this.Mnu_addc,
            this.termsToolStripMenuItem,
            this.Mnu_Hsn,
            this.machineMasterToolStripMenuItem,
            this.yarnToolStripMenuItem,
            this.Mnu_Item,
            this.Mnu_par,
            this.Mnu_Pricelist,
            this.exitToolStripMenuItem2,
            this.sortingDetailsToolStripMenuItem});
            this.fileMenu.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(58, 20);
            this.fileMenu.Text = "Master";
            // 
            // companyToolStripMenuItem
            // 
            this.companyToolStripMenuItem.Name = "companyToolStripMenuItem";
            this.companyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.companyToolStripMenuItem.Text = "Company";
            this.companyToolStripMenuItem.Visible = false;
            this.companyToolStripMenuItem.Click += new System.EventHandler(this.companyToolStripMenuItem_Click);
            // 
            // Mnuit_Grp
            // 
            this.Mnuit_Grp.ImageTransparentColor = System.Drawing.Color.Black;
            this.Mnuit_Grp.Name = "Mnuit_Grp";
            this.Mnuit_Grp.Size = new System.Drawing.Size(180, 22);
            this.Mnuit_Grp.Text = "Item Group";
            this.Mnuit_Grp.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // mnu_subgrp
            // 
            this.mnu_subgrp.Name = "mnu_subgrp";
            this.mnu_subgrp.Size = new System.Drawing.Size(180, 22);
            this.mnu_subgrp.Text = "Brand";
            this.mnu_subgrp.Click += new System.EventHandler(this.mnu_subgrp_Click);
            // 
            // mnu_cat
            // 
            this.mnu_cat.Name = "mnu_cat";
            this.mnu_cat.Size = new System.Drawing.Size(180, 22);
            this.mnu_cat.Text = "Category";
            this.mnu_cat.Click += new System.EventHandler(this.mnu_cat_Click);
            // 
            // Mnu_uom
            // 
            this.Mnu_uom.Name = "Mnu_uom";
            this.Mnu_uom.Size = new System.Drawing.Size(180, 22);
            this.Mnu_uom.Text = "UoM";
            this.Mnu_uom.Click += new System.EventHandler(this.Mnu_uom_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Process";
            this.exitToolStripMenuItem.Visible = false;
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // Mnu_CGST
            // 
            this.Mnu_CGST.Name = "Mnu_CGST";
            this.Mnu_CGST.Size = new System.Drawing.Size(180, 22);
            this.Mnu_CGST.Text = "Tax Structure";
            this.Mnu_CGST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Mnu_CGST.Click += new System.EventHandler(this.Mnu_CGST_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem1.Text = "ExciseDuty";
            this.exitToolStripMenuItem1.Visible = false;
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // taxToolStripMenuItem
            // 
            this.taxToolStripMenuItem.Name = "taxToolStripMenuItem";
            this.taxToolStripMenuItem.ShowShortcutKeys = false;
            this.taxToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.taxToolStripMenuItem.Text = "Tax";
            this.taxToolStripMenuItem.Visible = false;
            this.taxToolStripMenuItem.Click += new System.EventHandler(this.taxToolStripMenuItem_Click);
            // 
            // beamNoToolStripMenuItem
            // 
            this.beamNoToolStripMenuItem.Name = "beamNoToolStripMenuItem";
            this.beamNoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.beamNoToolStripMenuItem.Text = "Beam ";
            this.beamNoToolStripMenuItem.Click += new System.EventHandler(this.beamNoToolStripMenuItem_Click);
            // 
            // spindleWeightToolStripMenuItem
            // 
            this.spindleWeightToolStripMenuItem.Name = "spindleWeightToolStripMenuItem";
            this.spindleWeightToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.spindleWeightToolStripMenuItem.Text = "Spindle";
            this.spindleWeightToolStripMenuItem.Click += new System.EventHandler(this.spindleWeightToolStripMenuItem_Click);
            // 
            // Mnu_mac
            // 
            this.Mnu_mac.Name = "Mnu_mac";
            this.Mnu_mac.Size = new System.Drawing.Size(180, 22);
            this.Mnu_mac.Text = "Machine";
            this.Mnu_mac.Visible = false;
            this.Mnu_mac.Click += new System.EventHandler(this.Mnu_mac_Click);
            // 
            // mendorToolStripMenuItem
            // 
            this.mendorToolStripMenuItem.Name = "mendorToolStripMenuItem";
            this.mendorToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mendorToolStripMenuItem.Text = "Mendor";
            this.mendorToolStripMenuItem.Click += new System.EventHandler(this.mendorToolStripMenuItem_Click);
            // 
            // Mnu_emp
            // 
            this.Mnu_emp.Name = "Mnu_emp";
            this.Mnu_emp.Size = new System.Drawing.Size(180, 22);
            this.Mnu_emp.Text = "Employee";
            this.Mnu_emp.Visible = false;
            this.Mnu_emp.Click += new System.EventHandler(this.Mnu_emp_Click);
            // 
            // Mnu_terms
            // 
            this.Mnu_terms.Name = "Mnu_terms";
            this.Mnu_terms.Size = new System.Drawing.Size(180, 22);
            this.Mnu_terms.Text = "Terms && Conditions";
            this.Mnu_terms.Visible = false;
            this.Mnu_terms.Click += new System.EventHandler(this.Mnu_terms_Click);
            // 
            // Mnu_addc
            // 
            this.Mnu_addc.Name = "Mnu_addc";
            this.Mnu_addc.Size = new System.Drawing.Size(180, 22);
            this.Mnu_addc.Text = "Additional Charges";
            this.Mnu_addc.Visible = false;
            this.Mnu_addc.Click += new System.EventHandler(this.Mnu_addc_Click);
            // 
            // termsToolStripMenuItem
            // 
            this.termsToolStripMenuItem.Name = "termsToolStripMenuItem";
            this.termsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.termsToolStripMenuItem.Text = "Terms";
            this.termsToolStripMenuItem.Click += new System.EventHandler(this.termsToolStripMenuItem_Click);
            // 
            // Mnu_Hsn
            // 
            this.Mnu_Hsn.Name = "Mnu_Hsn";
            this.Mnu_Hsn.Size = new System.Drawing.Size(180, 22);
            this.Mnu_Hsn.Text = "HSN";
            this.Mnu_Hsn.Visible = false;
            this.Mnu_Hsn.Click += new System.EventHandler(this.Mnu_Hsn_Click);
            // 
            // machineMasterToolStripMenuItem
            // 
            this.machineMasterToolStripMenuItem.Name = "machineMasterToolStripMenuItem";
            this.machineMasterToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.machineMasterToolStripMenuItem.Text = "Loom";
            this.machineMasterToolStripMenuItem.Click += new System.EventHandler(this.machineMasterToolStripMenuItem_Click);
            // 
            // yarnToolStripMenuItem
            // 
            this.yarnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pLYToolStripMenuItem,
            this.countToolStripMenuItem,
            this.yarnTypeToolStripMenuItem,
            this.cTNPCToolStripMenuItem,
            this.blendToolStripMenuItem,
            this.itemTypeToolStripMenuItem,
            this.yarnMasterToolStripMenuItem});
            this.yarnToolStripMenuItem.Name = "yarnToolStripMenuItem";
            this.yarnToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.yarnToolStripMenuItem.Text = "Yarn";
            // 
            // pLYToolStripMenuItem
            // 
            this.pLYToolStripMenuItem.Name = "pLYToolStripMenuItem";
            this.pLYToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pLYToolStripMenuItem.Text = "PLY";
            this.pLYToolStripMenuItem.Click += new System.EventHandler(this.pLYToolStripMenuItem_Click);
            // 
            // countToolStripMenuItem
            // 
            this.countToolStripMenuItem.Name = "countToolStripMenuItem";
            this.countToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.countToolStripMenuItem.Text = "Count";
            this.countToolStripMenuItem.Click += new System.EventHandler(this.countToolStripMenuItem_Click);
            // 
            // yarnTypeToolStripMenuItem
            // 
            this.yarnTypeToolStripMenuItem.Name = "yarnTypeToolStripMenuItem";
            this.yarnTypeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.yarnTypeToolStripMenuItem.Text = "Yarn Type";
            this.yarnTypeToolStripMenuItem.Click += new System.EventHandler(this.yarnTypeToolStripMenuItem_Click);
            // 
            // cTNPCToolStripMenuItem
            // 
            this.cTNPCToolStripMenuItem.Name = "cTNPCToolStripMenuItem";
            this.cTNPCToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cTNPCToolStripMenuItem.Text = "CTN PC";
            this.cTNPCToolStripMenuItem.Click += new System.EventHandler(this.cTNPCToolStripMenuItem_Click);
            // 
            // blendToolStripMenuItem
            // 
            this.blendToolStripMenuItem.Name = "blendToolStripMenuItem";
            this.blendToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.blendToolStripMenuItem.Text = "Blend";
            this.blendToolStripMenuItem.Click += new System.EventHandler(this.blendToolStripMenuItem_Click);
            // 
            // itemTypeToolStripMenuItem
            // 
            this.itemTypeToolStripMenuItem.Name = "itemTypeToolStripMenuItem";
            this.itemTypeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.itemTypeToolStripMenuItem.Text = "Item Type";
            this.itemTypeToolStripMenuItem.Click += new System.EventHandler(this.itemTypeToolStripMenuItem_Click);
            // 
            // yarnMasterToolStripMenuItem
            // 
            this.yarnMasterToolStripMenuItem.Name = "yarnMasterToolStripMenuItem";
            this.yarnMasterToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.yarnMasterToolStripMenuItem.Text = "Yarn Master";
            this.yarnMasterToolStripMenuItem.Click += new System.EventHandler(this.yarnMasterToolStripMenuItem_Click);
            // 
            // Mnu_Item
            // 
            this.Mnu_Item.Image = ((System.Drawing.Image)(resources.GetObject("Mnu_Item.Image")));
            this.Mnu_Item.Name = "Mnu_Item";
            this.Mnu_Item.Size = new System.Drawing.Size(180, 22);
            this.Mnu_Item.Text = "Item";
            this.Mnu_Item.Visible = false;
            this.Mnu_Item.Click += new System.EventHandler(this.Mnu_item_Click);
            // 
            // Mnu_par
            // 
            this.Mnu_par.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mun_cusparty,
            this.Mun_suppparty,
            this.jobOrderVendorToolStripMenuItem,
            this.millToolStripMenuItem});
            this.Mnu_par.Name = "Mnu_par";
            this.Mnu_par.Size = new System.Drawing.Size(180, 22);
            this.Mnu_par.Text = "Party";
            this.Mnu_par.Click += new System.EventHandler(this.Mnu_par_Click);
            // 
            // mun_cusparty
            // 
            this.mun_cusparty.Name = "mun_cusparty";
            this.mun_cusparty.Size = new System.Drawing.Size(180, 22);
            this.mun_cusparty.Text = "Customer";
            this.mun_cusparty.Click += new System.EventHandler(this.mun_cusparty_Click);
            // 
            // Mun_suppparty
            // 
            this.Mun_suppparty.Name = "Mun_suppparty";
            this.Mun_suppparty.Size = new System.Drawing.Size(180, 22);
            this.Mun_suppparty.Text = "Supplier";
            this.Mun_suppparty.Click += new System.EventHandler(this.Mun_suppparty_Click);
            // 
            // jobOrderVendorToolStripMenuItem
            // 
            this.jobOrderVendorToolStripMenuItem.Name = "jobOrderVendorToolStripMenuItem";
            this.jobOrderVendorToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.jobOrderVendorToolStripMenuItem.Text = "Job Order Vendor";
            this.jobOrderVendorToolStripMenuItem.Click += new System.EventHandler(this.jobOrderVendorToolStripMenuItem_Click);
            // 
            // millToolStripMenuItem
            // 
            this.millToolStripMenuItem.Name = "millToolStripMenuItem";
            this.millToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.millToolStripMenuItem.Text = "Mill ";
            this.millToolStripMenuItem.Click += new System.EventHandler(this.millToolStripMenuItem_Click);
            // 
            // Mnu_Pricelist
            // 
            this.Mnu_Pricelist.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesPriceListToolStripMenuItem,
            this.purchasePriceListToolStripMenuItem});
            this.Mnu_Pricelist.Name = "Mnu_Pricelist";
            this.Mnu_Pricelist.Size = new System.Drawing.Size(180, 22);
            this.Mnu_Pricelist.Text = "Price List";
            this.Mnu_Pricelist.Click += new System.EventHandler(this.Mnu_Pricelist_Click_1);
            // 
            // salesPriceListToolStripMenuItem
            // 
            this.salesPriceListToolStripMenuItem.Name = "salesPriceListToolStripMenuItem";
            this.salesPriceListToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salesPriceListToolStripMenuItem.Text = "Sales Price List";
            this.salesPriceListToolStripMenuItem.Click += new System.EventHandler(this.salesPriceListToolStripMenuItem_Click);
            // 
            // purchasePriceListToolStripMenuItem
            // 
            this.purchasePriceListToolStripMenuItem.Name = "purchasePriceListToolStripMenuItem";
            this.purchasePriceListToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.purchasePriceListToolStripMenuItem.Text = "Purchase Price List";
            this.purchasePriceListToolStripMenuItem.Click += new System.EventHandler(this.purchasePriceListToolStripMenuItem_Click);
            // 
            // sortingDetailsToolStripMenuItem
            // 
            this.sortingDetailsToolStripMenuItem.Name = "sortingDetailsToolStripMenuItem";
            this.sortingDetailsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sortingDetailsToolStripMenuItem.Text = "Sorting Details";
            this.sortingDetailsToolStripMenuItem.Click += new System.EventHandler(this.sortingDetailsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem2
            // 
            this.exitToolStripMenuItem2.Name = "exitToolStripMenuItem2";
            this.exitToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem2.Text = "Terry Sort";
            this.exitToolStripMenuItem2.Click += new System.EventHandler(this.exitToolStripMenuItem2_Click);
            // 
            // Mnu_tran
            // 
            this.Mnu_tran.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseToolStripMenuItem,
            this.salesToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.accountsToolStripMenuItem,
            this.subcontractToolStripMenuItem,
            this.productionToolStripMenuItem,
            this.stockToolStripMenuItem,
            this.warbBookingToolStripMenuItem});
            this.Mnu_tran.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mnu_tran.Name = "Mnu_tran";
            this.Mnu_tran.Size = new System.Drawing.Size(89, 20);
            this.Mnu_tran.Text = "Transactions";
            // 
            // purchaseToolStripMenuItem
            // 
            this.purchaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseOrderToolStripMenuItem,
            this.gRNToolStripMenuItem,
            this.billAccountingToolStripMenuItem});
            this.purchaseToolStripMenuItem.Name = "purchaseToolStripMenuItem";
            this.purchaseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.purchaseToolStripMenuItem.Text = "Purchase";
            // 
            // purchaseOrderToolStripMenuItem
            // 
            this.purchaseOrderToolStripMenuItem.Name = "purchaseOrderToolStripMenuItem";
            this.purchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.purchaseOrderToolStripMenuItem.Text = "Purchase Order";
            this.purchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem_Click);
            // 
            // gRNToolStripMenuItem
            // 
            this.gRNToolStripMenuItem.Name = "gRNToolStripMenuItem";
            this.gRNToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.gRNToolStripMenuItem.Text = "GRN";
            this.gRNToolStripMenuItem.Visible = false;
            this.gRNToolStripMenuItem.Click += new System.EventHandler(this.gRNToolStripMenuItem_Click);
            // 
            // billAccountingToolStripMenuItem
            // 
            this.billAccountingToolStripMenuItem.Name = "billAccountingToolStripMenuItem";
            this.billAccountingToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.billAccountingToolStripMenuItem.Text = "Bill Accounting";
            this.billAccountingToolStripMenuItem.Click += new System.EventHandler(this.billAccountingToolStripMenuItem_Click);
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Mnu_som,
            this.salesDCToolStripMenuItem,
            this.Mnu_salesm,
            this.Mnu_salret});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salesToolStripMenuItem.Text = "Sales";
            this.salesToolStripMenuItem.Visible = false;
            // 
            // Mnu_som
            // 
            this.Mnu_som.Name = "Mnu_som";
            this.Mnu_som.Size = new System.Drawing.Size(146, 22);
            this.Mnu_som.Text = "Sales Order ";
            this.Mnu_som.Click += new System.EventHandler(this.Mnu_som_Click);
            // 
            // salesDCToolStripMenuItem
            // 
            this.salesDCToolStripMenuItem.Name = "salesDCToolStripMenuItem";
            this.salesDCToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.salesDCToolStripMenuItem.Text = "Sales DC";
            this.salesDCToolStripMenuItem.Click += new System.EventHandler(this.salesDCToolStripMenuItem_Click);
            // 
            // Mnu_salesm
            // 
            this.Mnu_salesm.Name = "Mnu_salesm";
            this.Mnu_salesm.Size = new System.Drawing.Size(146, 22);
            this.Mnu_salesm.Text = "Sales Invoice";
            this.Mnu_salesm.Click += new System.EventHandler(this.Mnu_salesm_Click);
            // 
            // Mnu_salret
            // 
            this.Mnu_salret.Name = "Mnu_salret";
            this.Mnu_salret.Size = new System.Drawing.Size(146, 22);
            this.Mnu_salret.Text = "Sales Return";
            this.Mnu_salret.Click += new System.EventHandler(this.Mnu_salret_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openingStockToolStripMenuItem,
            this.stockInToolStripMenuItem,
            this.stockOutToolStripMenuItem,
            this.returnableDCToolStripMenuItem1,
            this.nonReturnableReceiptToolStripMenuItem1,
            this.nonReturnableDCToolStripMenuItem});
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            this.inventoryToolStripMenuItem.Visible = false;
            // 
            // openingStockToolStripMenuItem
            // 
            this.openingStockToolStripMenuItem.Name = "openingStockToolStripMenuItem";
            this.openingStockToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.openingStockToolStripMenuItem.Text = "Opening Stock";
            this.openingStockToolStripMenuItem.Click += new System.EventHandler(this.openingStockToolStripMenuItem_Click);
            // 
            // stockInToolStripMenuItem
            // 
            this.stockInToolStripMenuItem.Name = "stockInToolStripMenuItem";
            this.stockInToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.stockInToolStripMenuItem.Text = "Stock In";
            this.stockInToolStripMenuItem.Click += new System.EventHandler(this.stockInToolStripMenuItem_Click);
            // 
            // stockOutToolStripMenuItem
            // 
            this.stockOutToolStripMenuItem.Name = "stockOutToolStripMenuItem";
            this.stockOutToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.stockOutToolStripMenuItem.Text = "Stock Out";
            this.stockOutToolStripMenuItem.Click += new System.EventHandler(this.stockOutToolStripMenuItem_Click);
            // 
            // returnableDCToolStripMenuItem1
            // 
            this.returnableDCToolStripMenuItem1.Name = "returnableDCToolStripMenuItem1";
            this.returnableDCToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.returnableDCToolStripMenuItem1.Text = "Returnable DC";
            this.returnableDCToolStripMenuItem1.Click += new System.EventHandler(this.returnableDCToolStripMenuItem1_Click);
            // 
            // nonReturnableReceiptToolStripMenuItem1
            // 
            this.nonReturnableReceiptToolStripMenuItem1.Name = "nonReturnableReceiptToolStripMenuItem1";
            this.nonReturnableReceiptToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.nonReturnableReceiptToolStripMenuItem1.Text = "Non Returnable Receipt";
            this.nonReturnableReceiptToolStripMenuItem1.Click += new System.EventHandler(this.nonReturnableReceiptToolStripMenuItem1_Click);
            // 
            // nonReturnableDCToolStripMenuItem
            // 
            this.nonReturnableDCToolStripMenuItem.Name = "nonReturnableDCToolStripMenuItem";
            this.nonReturnableDCToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.nonReturnableDCToolStripMenuItem.Text = "Non returnable DC";
            this.nonReturnableDCToolStripMenuItem.Click += new System.EventHandler(this.nonReturnableDCToolStripMenuItem_Click);
            // 
            // accountsToolStripMenuItem
            // 
            this.accountsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.moneyReceiptToolStripMenuItem1});
            this.accountsToolStripMenuItem.Name = "accountsToolStripMenuItem";
            this.accountsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.accountsToolStripMenuItem.Text = "Accounts";
            this.accountsToolStripMenuItem.Visible = false;
            this.accountsToolStripMenuItem.Click += new System.EventHandler(this.accountsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.toolStripMenuItem1.Text = "Money Payment";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click_1);
            // 
            // moneyReceiptToolStripMenuItem1
            // 
            this.moneyReceiptToolStripMenuItem1.Name = "moneyReceiptToolStripMenuItem1";
            this.moneyReceiptToolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.moneyReceiptToolStripMenuItem1.Text = "Money Receipt";
            this.moneyReceiptToolStripMenuItem1.Click += new System.EventHandler(this.moneyReceiptToolStripMenuItem1_Click);
            // 
            // subcontractToolStripMenuItem
            // 
            this.subcontractToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.packingSlipToolStripMenuItem1,
            this.yarnIssueToolStripMenuItem,
            this.yarnReceiptToolStripMenuItem,
            this.fabricIssueToolStripMenuItem,
            this.weptIssueToolStripMenuItem});
            this.subcontractToolStripMenuItem.Name = "subcontractToolStripMenuItem";
            this.subcontractToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.subcontractToolStripMenuItem.Text = "Subcontract";
            // 
            // packingSlipToolStripMenuItem1
            // 
            this.packingSlipToolStripMenuItem1.Name = "packingSlipToolStripMenuItem1";
            this.packingSlipToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.packingSlipToolStripMenuItem1.Text = "Yarn Packing Slip";
            this.packingSlipToolStripMenuItem1.Click += new System.EventHandler(this.packingSlipToolStripMenuItem1_Click);
            // 
            // yarnIssueToolStripMenuItem
            // 
            this.yarnIssueToolStripMenuItem.Name = "yarnIssueToolStripMenuItem";
            this.yarnIssueToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.yarnIssueToolStripMenuItem.Text = "Yarn Issue";
            this.yarnIssueToolStripMenuItem.Click += new System.EventHandler(this.yarnIssueToolStripMenuItem_Click);
            // 
            // yarnReceiptToolStripMenuItem
            // 
            this.yarnReceiptToolStripMenuItem.Name = "yarnReceiptToolStripMenuItem";
            this.yarnReceiptToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.yarnReceiptToolStripMenuItem.Text = "Beam Receipt";
            this.yarnReceiptToolStripMenuItem.Click += new System.EventHandler(this.yarnReceiptToolStripMenuItem_Click);
            // 
            // fabricIssueToolStripMenuItem
            // 
            this.fabricIssueToolStripMenuItem.Name = "fabricIssueToolStripMenuItem";
            this.fabricIssueToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.fabricIssueToolStripMenuItem.Text = "Fabric Issue";
            this.fabricIssueToolStripMenuItem.Click += new System.EventHandler(this.fabricIssueToolStripMenuItem_Click);
            // 
            // weptIssueToolStripMenuItem
            // 
            this.weptIssueToolStripMenuItem.Name = "weptIssueToolStripMenuItem";
            this.weptIssueToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.weptIssueToolStripMenuItem.Text = "Wept Issue";
            this.weptIssueToolStripMenuItem.Click += new System.EventHandler(this.weptIssueToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.internalOrderToolStripMenuItem,
            this.jobCardToolStripMenuItem,
            this.loomBookingToolStripMenuItem,
            this.sortBookingToolStripMenuItem,
            this.closeJobCardToolStripMenuItem});
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.productionToolStripMenuItem.Text = "Production";
            // 
            // internalOrderToolStripMenuItem
            // 
            this.internalOrderToolStripMenuItem.Name = "internalOrderToolStripMenuItem";
            this.internalOrderToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.internalOrderToolStripMenuItem.Text = "Internal Order";
            this.internalOrderToolStripMenuItem.Click += new System.EventHandler(this.internalOrderToolStripMenuItem_Click);
            // 
            // jobCardToolStripMenuItem
            // 
            this.jobCardToolStripMenuItem.Name = "jobCardToolStripMenuItem";
            this.jobCardToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.jobCardToolStripMenuItem.Text = "JobCard";
            this.jobCardToolStripMenuItem.Click += new System.EventHandler(this.jobCardToolStripMenuItem_Click);
            // 
            // loomBookingToolStripMenuItem
            // 
            this.loomBookingToolStripMenuItem.Name = "loomBookingToolStripMenuItem";
            this.loomBookingToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.loomBookingToolStripMenuItem.Text = "Loom Booking";
            this.loomBookingToolStripMenuItem.Click += new System.EventHandler(this.loomBookingToolStripMenuItem_Click);
            // 
            // sortBookingToolStripMenuItem
            // 
            this.sortBookingToolStripMenuItem.Name = "sortBookingToolStripMenuItem";
            this.sortBookingToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.sortBookingToolStripMenuItem.Text = "Sort Booking";
            this.sortBookingToolStripMenuItem.Click += new System.EventHandler(this.sortBookingToolStripMenuItem_Click);
            // 
            // closeJobCardToolStripMenuItem
            // 
            this.closeJobCardToolStripMenuItem.Name = "closeJobCardToolStripMenuItem";
            this.closeJobCardToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.closeJobCardToolStripMenuItem.Text = "Close JobCard";
            this.closeJobCardToolStripMenuItem.Click += new System.EventHandler(this.closeJobCardToolStripMenuItem_Click);
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openingStockToolStripMenuItem1});
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stockToolStripMenuItem.Text = "Stock";
            // 
            // openingStockToolStripMenuItem1
            // 
            this.openingStockToolStripMenuItem1.Name = "openingStockToolStripMenuItem1";
            this.openingStockToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.openingStockToolStripMenuItem1.Text = "Opening Stock";
            this.openingStockToolStripMenuItem1.Click += new System.EventHandler(this.openingStockToolStripMenuItem1_Click);
            // 
            // warbBookingToolStripMenuItem
            // 
            this.warbBookingToolStripMenuItem.Name = "warbBookingToolStripMenuItem";
            this.warbBookingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.warbBookingToolStripMenuItem.Text = "Warp Booking";
            this.warbBookingToolStripMenuItem.Click += new System.EventHandler(this.warbBookingToolStripMenuItem_Click);
            // 
            // mISToolStripMenuItem
            // 
            this.mISToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dCLedgerToolStripMenuItem});
            this.mISToolStripMenuItem.Name = "mISToolStripMenuItem";
            this.mISToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.mISToolStripMenuItem.Text = "MIS";
            this.mISToolStripMenuItem.Click += new System.EventHandler(this.mISToolStripMenuItem_Click);
            // 
            // dCLedgerToolStripMenuItem
            // 
            this.dCLedgerToolStripMenuItem.Name = "dCLedgerToolStripMenuItem";
            this.dCLedgerToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.dCLedgerToolStripMenuItem.Text = "DC Ledger";
            this.dCLedgerToolStripMenuItem.Click += new System.EventHandler(this.dCLedgerToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shiftWiseReportToolStripMenuItem,
            this.qCReportToolStripMenuItem,
            this.rollReportToolStripMenuItem,
            this.stockReportToolStripMenuItem,
            this.salesOrderPendingToolStripMenuItem,
            this.toolStripMenuItem2,
            this.stockLedgerToolStripMenuItem,
            this.jobcardReportToolStripMenuItem,
            this.dCRegisterToolStripMenuItem});
            this.reportToolStripMenuItem.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // shiftWiseReportToolStripMenuItem
            // 
            this.shiftWiseReportToolStripMenuItem.Name = "shiftWiseReportToolStripMenuItem";
            this.shiftWiseReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.shiftWiseReportToolStripMenuItem.Text = "Shift Wise Report";
            this.shiftWiseReportToolStripMenuItem.Click += new System.EventHandler(this.shiftWiseReportToolStripMenuItem_Click);
            // 
            // qCReportToolStripMenuItem
            // 
            this.qCReportToolStripMenuItem.Name = "qCReportToolStripMenuItem";
            this.qCReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.qCReportToolStripMenuItem.Text = "QC Report";
            this.qCReportToolStripMenuItem.Click += new System.EventHandler(this.qCReportToolStripMenuItem_Click);
            // 
            // rollReportToolStripMenuItem
            // 
            this.rollReportToolStripMenuItem.Name = "rollReportToolStripMenuItem";
            this.rollReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.rollReportToolStripMenuItem.Text = "Roll Report";
            this.rollReportToolStripMenuItem.Visible = false;
            this.rollReportToolStripMenuItem.Click += new System.EventHandler(this.rollReportToolStripMenuItem_Click);
            // 
            // stockReportToolStripMenuItem
            // 
            this.stockReportToolStripMenuItem.Name = "stockReportToolStripMenuItem";
            this.stockReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.stockReportToolStripMenuItem.Text = "Register";
            this.stockReportToolStripMenuItem.Visible = false;
            this.stockReportToolStripMenuItem.Click += new System.EventHandler(this.stockReportToolStripMenuItem_Click);
            // 
            // salesOrderPendingToolStripMenuItem
            // 
            this.salesOrderPendingToolStripMenuItem.Name = "salesOrderPendingToolStripMenuItem";
            this.salesOrderPendingToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.salesOrderPendingToolStripMenuItem.Text = "Sales Order Pending";
            this.salesOrderPendingToolStripMenuItem.Visible = false;
            this.salesOrderPendingToolStripMenuItem.Click += new System.EventHandler(this.salesOrderPendingToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.toolStripMenuItem2.Text = "Customer Outstanding Report";
            this.toolStripMenuItem2.Visible = false;
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click_1);
            // 
            // stockLedgerToolStripMenuItem
            // 
            this.stockLedgerToolStripMenuItem.Name = "stockLedgerToolStripMenuItem";
            this.stockLedgerToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.stockLedgerToolStripMenuItem.Text = "Stock Statement";
            this.stockLedgerToolStripMenuItem.Click += new System.EventHandler(this.stockLedgerToolStripMenuItem_Click);
            // 
            // jobcardReportToolStripMenuItem
            // 
            this.jobcardReportToolStripMenuItem.Name = "jobcardReportToolStripMenuItem";
            this.jobcardReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.jobcardReportToolStripMenuItem.Text = "Jobcard Report";
            this.jobcardReportToolStripMenuItem.Click += new System.EventHandler(this.jobcardReportToolStripMenuItem_Click);
            // 
            // dCRegisterToolStripMenuItem
            // 
            this.dCRegisterToolStripMenuItem.Name = "dCRegisterToolStripMenuItem";
            this.dCRegisterToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.dCRegisterToolStripMenuItem.Text = "DC Register";
            this.dCRegisterToolStripMenuItem.Click += new System.EventHandler(this.dCRegisterToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Naalwar.Properties.Resources.ACV_Background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(668, 534);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FrmMain";
            this.Text = "NAALWAR TEXTILE";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frmmain_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripButton printPreviewToolStripButton;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem Mnuit_Grp;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Mnu_Item;
        private System.Windows.Forms.ToolStripMenuItem mnu_subgrp;
        private System.Windows.Forms.ToolStripMenuItem mnu_cat;
        private System.Windows.Forms.ToolStripMenuItem Mnu_uom;
        private System.Windows.Forms.ToolStripMenuItem Mnu_tran;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem taxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem Mnu_mac;
        private System.Windows.Forms.ToolStripMenuItem Mnu_emp;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Mnu_Hsn;
        private System.Windows.Forms.ToolStripMenuItem Mnu_CGST;
        private System.Windows.Forms.ToolStripMenuItem Mnu_Pricelist;
        private System.Windows.Forms.ToolStripMenuItem Mnu_par;
        private System.Windows.Forms.ToolStripMenuItem Mnu_addc;
        private System.Windows.Forms.ToolStripMenuItem Mnu_terms;
        private System.Windows.Forms.ToolStripMenuItem mun_cusparty;
        private System.Windows.Forms.ToolStripMenuItem Mun_suppparty;
        private System.Windows.Forms.ToolStripMenuItem salesPriceListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasePriceListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gRNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billAccountingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesDCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openingStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returnableDCToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nonReturnableReceiptToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nonReturnableDCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moneyReceiptToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem Mnu_salret;
        private System.Windows.Forms.ToolStripMenuItem salesOrderPendingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem Mnu_salesm;
        private System.Windows.Forms.ToolStripMenuItem Mnu_som;
        private System.Windows.Forms.ToolStripMenuItem stockLedgerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subcontractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yarnIssueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobOrderVendorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem millToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beamNoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spindleWeightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yarnReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packingSlipToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sortingDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internalOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem machineMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openingStockToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem warbBookingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yarnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yarnMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pLYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem countToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yarnTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cTNPCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortBookingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shiftWiseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qCReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fabricIssueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rollReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mendorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeJobCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weptIssueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobcardReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loomBookingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dCRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mISToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dCLedgerToolStripMenuItem;
    }
}



