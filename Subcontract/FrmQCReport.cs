﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmQCReport : Form
    {
        public FrmQCReport()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if(cmbType.Text == "Qc Type")
            {
                Genclass.Dtype = 2;
                Genclass.ReportType = "QCReport";
                Genclass.ReortDate = Convert.ToDateTime(dtpQCDate.Text);
                Genclass.parameter = cmbQCSt.Text;
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.MdiParent = this.MdiParent;
                rpt.WindowState = FormWindowState.Maximized;
                rpt.Show();
            }
            else if (cmbType.Text == "Mendor Wise")
            {
                Genclass.Dtype = 3;
                Genclass.ReportType = cmbRT.Text;
                Genclass.ReortDate = Convert.ToDateTime(dtpQCDate.Text);
                Genclass.parameter = txtMendor.Text;
                Genclass.parameter1 = cmbShift.SelectedValue.ToString();
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.MdiParent = this.MdiParent;
                rpt.WindowState = FormWindowState.Maximized;
                rpt.Show();
            }
            else
            {
                Genclass.Dtype = 4;
                Genclass.ReportType = "QCShift";
                Genclass.ReortDate = Convert.ToDateTime(dtpQCDate.Text);
                Genclass.parameter = cmbShift.SelectedValue.ToString();
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.MdiParent = this.MdiParent;
                rpt.WindowState = FormWindowState.Maximized;
                rpt.Show();
            }
        }

        private void LoadQCGrid(DataSet ds)
        {
            throw new NotImplementedException();
        }

        private void FrmQCReport_Load(object sender, EventArgs e)
        {
            lblQC.Visible = false;
            cmbQCSt.Visible = false;
            lblMendor.Visible = false;
            txtMendor.Visible = false;
            lblShift.Visible = false;
            cmbShift.Visible = false;
            lblMendorRptType.Visible = false;
            cmbRT.Visible = false;
            cmbType.SelectedIndex = 0;
        }

        private void LoadShift()
        {
            try
            {
                string Query = "Select Uid,GeneralName as ShiftName from GeneralM Where TypeM_Uid = 27";
                SqlDataAdapter da = new SqlDataAdapter(Query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cmbShift.DataSource = null;
                cmbShift.DisplayMember = "ShiftName";
                cmbShift.ValueMember = "Uid";
                DataRow row = dt.NewRow();
                row["Uid"] = 0;
                row["ShiftName"] = "All";
                dt.Rows.Add(row);
                cmbShift.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadMendor()
        {
            try
            {
                string Query = "Select F2,GeneralName as ShiftName from GeneralM Where TypeM_Uid = 28";
                SqlDataAdapter da = new SqlDataAdapter(Query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string Name = dt.Rows[i]["F2"].ToString();
                        coll.Add(Name);
                    }
                }
                txtMendor.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtMendor.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtMendor.AutoCompleteCustomSource = coll;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(cmbType.Text == "Qc Type")
                {
                    lblQC.Visible = true;
                    cmbQCSt.Visible = true;
                    lblMendor.Visible = false;
                    txtMendor.Visible = false;
                    lblShift.Visible = false;
                    cmbShift.Visible = false;
                    lblMendorRptType.Visible = false;
                    cmbRT.Visible = false;
                }
                else if(cmbType.Text == "Mendor Wise")
                {
                    lblQC.Visible = false;
                    cmbQCSt.Visible = false;
                    lblMendor.Visible = true;
                    txtMendor.Visible = true;
                    lblShift.Visible = true;
                    cmbShift.Visible = true;
                    lblMendorRptType.Visible = true;
                    cmbRT.Visible = true;
                    LoadMendor();
                    LoadShift();
                }
                else
                {
                    lblQC.Visible = false;
                    cmbQCSt.Visible = false;
                    lblMendor.Visible = false;
                    txtMendor.Visible = false;
                    lblShift.Visible = true;
                    cmbShift.Visible = true;
                    lblMendorRptType.Visible = false;
                    cmbRT.Visible = false;
                    LoadShift();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cmbShift_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbRT_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbRT.Text == "Mendor Wise")
            {
                lblMendor.Visible = true;
                txtMendor.Visible = true;
            }
            else
            {
                lblMendor.Visible = false;
                txtMendor.Visible = false;
            }
        }
    }
}
