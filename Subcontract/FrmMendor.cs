﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmMendor : Form
    {
        public FrmMendor()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void FrmMendor_Load(object sender, EventArgs e)
        {
            LoadButton(3);
            LoadMendor();
        }

        protected void LoadButton(int id)
        {
            if (id == 1)//Add
            {
                grFront.Visible = false;
                grBack.Visible = true;
                btnAdd.Visible = false;
                btnEdit.Visible = false;
                btnExit.Visible = false;
                btnSave.Visible = true;
                btnAddCancel.Visible = true;
                btnSave.Text = "Save";
            }
            else if (id == 2)//Edit
            {
                grFront.Visible = false;
                grBack.Visible = true;
                btnAdd.Visible = false;
                btnEdit.Visible = false;
                btnExit.Visible = false;
                btnSave.Visible = true;
                btnAddCancel.Visible = true;
                btnSave.Text = "Update";
            }
            else// Load or Cancel
            {
                grFront.Visible = true;
                grBack.Visible = false;
                btnAdd.Visible = true;
                btnEdit.Visible = true;
                btnExit.Visible = true;
                btnSave.Visible = false;
                btnAddCancel.Visible = false;
                btnSave.Text = "Save";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(1);
                txtMendorCode.Text = string.Empty;
                txtMendorName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                LoadMendor();
                LoadButton(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadMendor()
        {
            try
            {
                string Query = "Select * from GeneralM Where TypeM_Uid = 28 order by GeneralName";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                DataGridMendor.DataSource = null;
                DataGridMendor.AutoGenerateColumns = false;
                DataGridMendor.ColumnCount = 6;
                DataGridMendor.Columns[0].Name = "Uid";
                DataGridMendor.Columns[0].HeaderText = "Uid";
                DataGridMendor.Columns[0].DataPropertyName = "Uid";
                DataGridMendor.Columns[0].Visible = false;

                DataGridMendor.Columns[1].Name = "TypeM_Uid";
                DataGridMendor.Columns[1].HeaderText = "TypeM_Uid";
                DataGridMendor.Columns[1].DataPropertyName = "TypeM_Uid";
                DataGridMendor.Columns[1].Visible = false;

                DataGridMendor.Columns[2].Name = "GeneralName";
                DataGridMendor.Columns[2].HeaderText = "Code";
                DataGridMendor.Columns[2].DataPropertyName = "GeneralName";
                DataGridMendor.Columns[2].Width = 150;

                DataGridMendor.Columns[3].Name = "Active";
                DataGridMendor.Columns[3].HeaderText = "Active";
                DataGridMendor.Columns[3].DataPropertyName = "Active";
                DataGridMendor.Columns[3].Visible = false;

                DataGridMendor.Columns[4].Name = "F2";
                DataGridMendor.Columns[4].HeaderText = "Name";
                DataGridMendor.Columns[4].DataPropertyName = "F2";
                DataGridMendor.Columns[4].Width = 320;

                DataGridMendor.Columns[5].Name = "CompanyId";
                DataGridMendor.Columns[5].HeaderText = "CompanyId";
                DataGridMendor.Columns[5].DataPropertyName = "CompanyId";
                DataGridMendor.Columns[5].Visible = false;
                DataGridMendor.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtMendorName.Text != string.Empty || txtMendorCode.Text != string.Empty)
                {
                    int Active = 0;
                    if(chckActive.Checked == true)
                    {
                        Active = 1;
                    }
                    else
                    {
                        Active = 0;
                    }
                    string Tag = string.Empty;
                    int Uid = 0;
                    if(btnSave.Text == "Save")
                    {
                        Tag = "Insert";
                    }
                    else
                    {
                        Tag = "Update";
                    }
                    if(txtMendorCode.Tag == null || txtMendorCode.Tag.ToString() == "")
                    {
                        Uid = 0;
                    }
                    else
                    {
                        Uid = Convert.ToInt32(txtMendorCode.Tag.ToString());
                    }
                    SqlParameter[] parameters = {
                        new SqlParameter("@TypeM_Uid",28),
                        new SqlParameter("@GeneralName",txtMendorCode.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@f1",DBNull.Value),
                        new SqlParameter("@f2",txtMendorName.Text),
                        new SqlParameter("@CompanyId",1),
                        new SqlParameter("@f3",DBNull.Value),
                        new SqlParameter("@Tag",Tag),
                        new SqlParameter("@Uid",Uid)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", parameters, conn);
                    MessageBox.Show("Record has been saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMendorCode.Text = string.Empty;
                    txtMendorCode.Tag = string.Empty;
                    txtMendorName.Text = string.Empty;
                    txtMendorName.Tag = string.Empty;
                    btnSave.Text = "Save";
                }
                else
                {
                    MessageBox.Show("Mendor and Name can't empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMendorCode.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridMendor.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridMendor.Rows[Index].Cells[0].Value.ToString());
                txtMendorCode.Tag = Uid;
                txtMendorCode.Text = DataGridMendor.Rows[Index].Cells[2].Value.ToString();
                txtMendorName.Text = DataGridMendor.Rows[Index].Cells[4].Value.ToString();
                string t = DataGridMendor.Rows[Index].Cells[3].Value.ToString();
                if (t == "True")
                {
                    chckActive.Checked = true;
                }
                else
                {
                    chckActive.Checked = false;
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
