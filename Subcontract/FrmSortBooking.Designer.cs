﻿namespace Naalwar
{
    partial class FrmSortBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSortBooking));
            this.grFront = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chckCompleted = new System.Windows.Forms.CheckBox();
            this.dtpdtae = new System.Windows.Forms.DateTimePicker();
            this.DataGridSortBooking = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCompletedOn = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtJobCard = new System.Windows.Forms.TextBox();
            this.txtBeamNo = new System.Windows.Forms.TextBox();
            this.txtLoomNo = new System.Windows.Forms.TextBox();
            this.txtSortNo = new System.Windows.Forms.TextBox();
            this.grImport = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbShift = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.DataGridImport = new System.Windows.Forms.DataGridView();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortBooking)).BeginInit();
            this.panadd.SuspendLayout();
            this.grBack.SuspendLayout();
            this.grImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.label1);
            this.grFront.Controls.Add(this.chckCompleted);
            this.grFront.Controls.Add(this.dtpdtae);
            this.grFront.Controls.Add(this.DataGridSortBooking);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(4, -4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(779, 440);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(167, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Date";
            // 
            // chckCompleted
            // 
            this.chckCompleted.AutoSize = true;
            this.chckCompleted.Location = new System.Drawing.Point(514, 21);
            this.chckCompleted.Name = "chckCompleted";
            this.chckCompleted.Size = new System.Drawing.Size(117, 22);
            this.chckCompleted.TabIndex = 2;
            this.chckCompleted.Text = "Completed On";
            this.chckCompleted.UseVisualStyleBackColor = true;
            this.chckCompleted.CheckedChanged += new System.EventHandler(this.chckCompleted_CheckedChanged);
            // 
            // dtpdtae
            // 
            this.dtpdtae.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdtae.Location = new System.Drawing.Point(230, 19);
            this.dtpdtae.Name = "dtpdtae";
            this.dtpdtae.Size = new System.Drawing.Size(120, 26);
            this.dtpdtae.TabIndex = 1;
            // 
            // DataGridSortBooking
            // 
            this.DataGridSortBooking.AllowUserToAddRows = false;
            this.DataGridSortBooking.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSortBooking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortBooking.Location = new System.Drawing.Point(6, 51);
            this.DataGridSortBooking.Name = "DataGridSortBooking";
            this.DataGridSortBooking.RowHeadersVisible = false;
            this.DataGridSortBooking.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortBooking.Size = new System.Drawing.Size(766, 380);
            this.DataGridSortBooking.TabIndex = 0;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnImport);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button5);
            this.panadd.Location = new System.Drawing.Point(4, 442);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(781, 36);
            this.panadd.TabIndex = 208;
            // 
            // btnImport
            // 
            this.btnImport.BackColor = System.Drawing.Color.White;
            this.btnImport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Image = global::Naalwar.Properties.Resources.if_import_export_63126;
            this.btnImport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImport.Location = new System.Drawing.Point(6, 2);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(87, 32);
            this.btnImport.TabIndex = 210;
            this.btnImport.Text = "Import";
            this.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Visible = false;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(717, 2);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(58, 32);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(654, 2);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(64, 32);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "View";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(654, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(64, 30);
            this.button5.TabIndex = 209;
            this.button5.Text = "Back";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.txtCompletedOn);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtJobCard);
            this.grBack.Controls.Add(this.txtBeamNo);
            this.grBack.Controls.Add(this.txtLoomNo);
            this.grBack.Controls.Add(this.txtSortNo);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(2, -3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(781, 437);
            this.grBack.TabIndex = 4;
            this.grBack.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(194, 289);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "Completed On";
            // 
            // txtCompletedOn
            // 
            this.txtCompletedOn.Location = new System.Drawing.Point(305, 286);
            this.txtCompletedOn.Name = "txtCompletedOn";
            this.txtCompletedOn.Size = new System.Drawing.Size(226, 26);
            this.txtCompletedOn.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(211, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 18);
            this.label5.TabIndex = 7;
            this.label5.Text = "Job Card No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(228, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Beam No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Loom No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Sort No";
            // 
            // txtJobCard
            // 
            this.txtJobCard.Location = new System.Drawing.Point(305, 233);
            this.txtJobCard.Name = "txtJobCard";
            this.txtJobCard.Size = new System.Drawing.Size(226, 26);
            this.txtJobCard.TabIndex = 3;
            // 
            // txtBeamNo
            // 
            this.txtBeamNo.Location = new System.Drawing.Point(305, 180);
            this.txtBeamNo.Name = "txtBeamNo";
            this.txtBeamNo.Size = new System.Drawing.Size(226, 26);
            this.txtBeamNo.TabIndex = 2;
            // 
            // txtLoomNo
            // 
            this.txtLoomNo.Location = new System.Drawing.Point(305, 129);
            this.txtLoomNo.Name = "txtLoomNo";
            this.txtLoomNo.Size = new System.Drawing.Size(226, 26);
            this.txtLoomNo.TabIndex = 1;
            // 
            // txtSortNo
            // 
            this.txtSortNo.Location = new System.Drawing.Point(305, 79);
            this.txtSortNo.Name = "txtSortNo";
            this.txtSortNo.Size = new System.Drawing.Size(226, 26);
            this.txtSortNo.TabIndex = 0;
            // 
            // grImport
            // 
            this.grImport.Controls.Add(this.btnClose);
            this.grImport.Controls.Add(this.btnSave);
            this.grImport.Controls.Add(this.btnBrowse);
            this.grImport.Controls.Add(this.lblPath);
            this.grImport.Controls.Add(this.label9);
            this.grImport.Controls.Add(this.cmbShift);
            this.grImport.Controls.Add(this.label8);
            this.grImport.Controls.Add(this.dtpDate);
            this.grImport.Controls.Add(this.label7);
            this.grImport.Controls.Add(this.DataGridImport);
            this.grImport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grImport.Location = new System.Drawing.Point(1, 0);
            this.grImport.Name = "grImport";
            this.grImport.Size = new System.Drawing.Size(780, 440);
            this.grImport.TabIndex = 4;
            this.grImport.TabStop = false;
            this.grImport.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = global::Naalwar.Properties.Resources.eee;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(696, 406);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 32);
            this.btnClose.TabIndex = 212;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = global::Naalwar.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(598, 406);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(89, 32);
            this.btnSave.TabIndex = 211;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.Color.White;
            this.btnBrowse.Image = global::Naalwar.Properties.Resources.folder;
            this.btnBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBrowse.Location = new System.Drawing.Point(684, 23);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(90, 34);
            this.btnBrowse.TabIndex = 7;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(368, 41);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(13, 18);
            this.lblPath.TabIndex = 6;
            this.lblPath.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(367, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 18);
            this.label9.TabIndex = 5;
            this.label9.Text = "Path";
            // 
            // cmbShift
            // 
            this.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShift.FormattingEnabled = true;
            this.cmbShift.Location = new System.Drawing.Point(173, 37);
            this.cmbShift.Name = "cmbShift";
            this.cmbShift.Size = new System.Drawing.Size(174, 26);
            this.cmbShift.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(173, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "Shift";
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(11, 37);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(138, 26);
            this.dtpDate.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 18);
            this.label7.TabIndex = 1;
            this.label7.Text = "Date";
            // 
            // DataGridImport
            // 
            this.DataGridImport.AllowUserToAddRows = false;
            this.DataGridImport.BackgroundColor = System.Drawing.Color.White;
            this.DataGridImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridImport.Location = new System.Drawing.Point(11, 72);
            this.DataGridImport.Name = "DataGridImport";
            this.DataGridImport.ReadOnly = true;
            this.DataGridImport.RowHeadersVisible = false;
            this.DataGridImport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridImport.Size = new System.Drawing.Size(763, 333);
            this.DataGridImport.TabIndex = 0;
            // 
            // FrmSortBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(788, 492);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grImport);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.MaximizeBox = false;
            this.Name = "FrmSortBooking";
            this.Text = "Sort Booking";
            this.Load += new System.EventHandler(this.FrmSortBooking_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortBooking)).EndInit();
            this.panadd.ResumeLayout(false);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grImport.ResumeLayout(false);
            this.grImport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.DataGridView DataGridSortBooking;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chckCompleted;
        private System.Windows.Forms.DateTimePicker dtpdtae;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCompletedOn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtJobCard;
        private System.Windows.Forms.TextBox txtBeamNo;
        private System.Windows.Forms.TextBox txtLoomNo;
        private System.Windows.Forms.TextBox txtSortNo;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.GroupBox grImport;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView DataGridImport;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbShift;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
    }
}