﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Naalwar
{
    public partial class FrmGRN : Form
    {
        public FrmGRN()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        //int uid = 0;
        int mode = 0;
        //string tpuid = "";
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmGRN_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;

            HFGP.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            chkgrd.RowHeadersVisible = false;

            Genpan.Visible = true;
            Editpan.Visible = false;
            Genclass.Dtype = 10;

            chkact.Checked = true;
            Chkedtact.Checked = true;
            Titlep();
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();

            //Loadgrid();

        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.ColumnCount = 7;
            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "Qty";
            HFIT.Columns[2].Name = "DcQty";
            HFIT.Columns[3].Name = "Addnotes";
            HFIT.Columns[4].Name = "Itid";
            HFIT.Columns[6].Name = "refuid";
            //HFIT.Columns[7].Name = "";

            HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;
            HFIT.Columns[1].ReadOnly = false;
            HFIT.Columns[2].ReadOnly = false;

            HFIT.Columns[0].Width = 450;
            HFIT.Columns[1].Width = 75;
            HFIT.Columns[2].Width = 75;
            HFIT.Columns[3].Width = 250;
            HFIT.Columns[4].Visible = false;
            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Visible = false;
            //HFIT.Columns[7].Visible = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            Editpan.Visible = true;
            Genclass.Module.ClearTextBox(this, Editpan);
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Titlep1();
            Titlep2();
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd MMM yyyy";
            Dtpdt.Format = DateTimePickerFormat.Custom;
            Dtpdt.CustomFormat = "dd MMM yyyy";
            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            txtdcno.Focus();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {

            Genpan.Visible = true;
            Loadgrid();
        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "jjno";
                Genclass.FSSQLSortStr5 = "Name";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }
                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }

                }


                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }



                if (chkact.Checked == true)
                {
                    string quy = "select a.UId,DocNo as GRNNO,convert(nvarchar,docdate,106) as  Date,DcNo as InvNo, convert(nvarchar,DcDate,106) as InvDate,jjno,Name as Party,b.uid as puid,narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid and a.companyid=" + Genclass.data1 + " where  a.doctypeid=10  and a.active=1 and " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = "select a.UId,DocNo as GRNNO, convert(nvarchar,docdate,106) as Date,DcNo as InvNo,convert(nvarchar,DcDate,106) as InvDate,jjno,Name as Party,b.uid as puid,narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid and a.companyid=" + Genclass.data1 + " where  a.doctypeid=10  and a.active=0 and " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);



                HFGP.AutoGenerateColumns = true;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }




                HFGP.Columns[0].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[1].Width = 88;
                HFGP.Columns[2].Width = 89;
                HFGP.Columns[3].Width = 180;
                HFGP.Columns[4].Width = 89;
                HFGP.Columns[5].Visible = false;

                HFGP.Columns[6].Width = 420;

                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                HFGP.DataSource = tap;


                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }



        private void loadput()


        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.fieldSix = "";

            conn.Open();

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                //Genclass.strsql = "select distinct uid,name  from ( select m.uid as Puid,m.name,m.address1,Docno, convert(varchar,Docdate,105)  as Docdate,c.hsnid,c.itemname,c.uid AS itemid,a.uid as UID, b.uid as listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,  b.BasicValue,i.f1  as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval from   sTransactionsP a   inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid  and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and m.companyid=" + Genclass.data1 + " group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0)tab "; 
                Genclass.strsql = "select distinct top 25 uid,Name  from Partym where active=1 and ptype<>1  and  companyid=" + Genclass.data1 + " and type='Supplier' order by name";
                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 2)
            {

                Genclass.Module.Partylistviewcont1("uid", "Itemname", "qty", "Docno", "listuid", Genclass.strsql, this, txtititd, txtitem, txtqty, txtpono, txtpoid, Editpan);
                Genclass.strsql = " select c.uid,c.itemname,b.pqty-isnull(sum(z.pqty),0) as qty,Docno, b.uid as listuid from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + " group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0 ";

                Genclass.FSSQLSortStr = "Itemname";

            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            //Frmlookup contc = new Frmlookup() { Width = 400, Height = 300 };
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;

            if (Genclass.type == 2)
            {
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 600;
                dt.Columns[2].Width = 100;
                dt.Columns[3].Visible = false;
                dt.Columns[4].Visible = false;
            }
            else
            {
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 700;
            }
            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;

            //contc.StartPosition = FormStartPosition.CenterParent;
            //contc.MdiParent = this.ParentForm;
            contc.Show();

            //contc.Show();
            conn.Close();
        }

        private void loadput1()
        {
            conn.Open();
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.fieldSix = "";

            if (Genclass.type == 2)
            {

                //Genclass.Module.Partylistviewcont3("uid", "Item","Itemcode", Genclass.strsql, this, txttitemid, txtitemname,txtdcid, Editpan);

                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                //if (Genclass.data1 == 1)
                //{
                Genclass.Module.Partylistviewcont1("uid", "Itemname", "Docno", "listuid", "qty", Genclass.strsql, this, txtititd, txtitem, txtrecqty, txtpoid, txtqty, Editpan);
                Genclass.strsql = "select c.uid,c.itemname,a.docno, b.uid as listuid,isnull(b.pqty,0)-isnull(sum(z.pqty),0) as qty from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + "  and a.partyuid=" + txtpuid.Text + "  group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0 ";

                Genclass.FSSQLSortStr = "Itemname";


                //Genclass.FSSQLSortStr1 = "qty";

                //}
                //else
                //{
                //    Genclass.Module.Partylistviewcont1("uid", "Itemname", "Docno", "listuid", "qty", Genclass.strsql, this, txtititd, txtitem, txtrecqty, txtpoid, txtqty, Editpan);
                //    Genclass.strsql = "select c.uid,c.itemname,a.docno, b.uid as listuid,isnull(b.pqty,0)-isnull(sum(z.pqty),0) as qty from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + "  and a.partyuid=" + txtpuid.Text + "  group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0 ";

                //    Genclass.FSSQLSortStr = "Itemname"; ;

                //}
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                //if (Genclass.data1 == 1)
                //{
                if (tap.Rows.Count > 0)
                {
                    txtqty.Text = tap.Rows[0]["qty"].ToString();
                }
                //}


                FrmLookup1 contc = new FrmLookup1();
                TabControl tab = (TabControl)contc.Controls["tabC"];


                TabPage tab1 = (TabPage)tab.Controls["tabPage1"];
                tab1.Text = "From PO";
                DataGridView grid = (DataGridView)tab1.Controls["HFGP4"];
                grid.Refresh();
                grid.ColumnCount = tap.Columns.Count;
                grid.Columns[0].Visible = false;
                grid.Columns[1].Width = 380;
                grid.Columns[2].Width = 150;
                grid.Columns[3].Visible = false;
                grid.Columns[4].Width = 100;



                grid.DefaultCellStyle.Font = new Font("Arial", 10);

                grid.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    grid.Columns[Genclass.i].Name = column.ColumnName;
                    grid.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                grid.DataSource = tap;





                if (Genclass.data1 == 1)
                {

                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "id", "lidtoid", Genclass.strsql, this, txtititd, txtitem, txtrecqty, txtpoid, txtqty, Editpan);

                    Genclass.strsql = "select c.uid,c.itemname,c.itemcode,a.price,0 as id,0 as lidtoid from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }
                else
                {
                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "id", "lidtoid", Genclass.strsql, this, txtititd, txtitem, txtrecqty, txtpoid, txtqty, Editpan);

                    Genclass.strsql = "select c.uid,c.itemcode,c.itemname,a.price,0 as id,0 as lidtoid from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + "";
                    Genclass.FSSQLSortStr = "itemcode";
                    Genclass.FSSQLSortStr1 = "itemname";
                }
                txtqty.Text = "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);

                TabPage tab3 = (TabPage)tab.Controls["tabPage2"];
                DataGridView grid2 = (DataGridView)tab3.Controls["HFGP2"];
                grid2.Refresh();
                grid2.ColumnCount = tap2.Columns.Count;
                grid2.Columns[0].Visible = false;
                grid2.Columns[1].Width = 380;
                grid2.Columns[2].Width = 250;
                grid2.Columns[3].Visible = false;
                grid2.Columns[4].Visible = false;





                grid2.DefaultCellStyle.Font = new Font("Arial", 10);

                grid2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid2.AutoGenerateColumns = false;

                Genclass.i = 0;



                foreach (DataColumn column in tap2.Columns)
                {
                    grid2.Columns[Genclass.i].Name = column.ColumnName;
                    grid2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                grid2.DataSource = tap2;

                if (Genclass.data1 == 1)
                {

                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "id", "lidtoid", Genclass.strsql, this, txtititd, txtitem, txtrecqty, txtpoid, txtqty, Editpan);

                    Genclass.strsql = "select distinct uid,itemname,itemcode,0 as id,0 as lidtoid  FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }
                else
                {

                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "id", "lidtoid", Genclass.strsql, this, txtititd, txtitem, txtrecqty, txtpoid, txtqty, Editpan);

                    Genclass.strsql = "select distinct uid,itemcode,itemname,0 as id,0 as lidtoid  FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemcode";
                    Genclass.FSSQLSortStr1 = "itemname";
                }

                txtqty.Text = "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                TabPage tab4 = (TabPage)tab.Controls["tabPage3"];
                DataGridView grid3 = (DataGridView)tab4.Controls["HFGP3"];
                grid3.Refresh();
                grid3.ColumnCount = tap3.Columns.Count;
                grid3.Columns[0].Visible = false;
                grid3.Columns[1].Width = 380;
                grid3.Columns[2].Width = 230;
                grid3.Columns[3].Visible = false;
                grid3.Columns[4].Visible = false;




                grid3.DefaultCellStyle.Font = new Font("Arial", 10);

                grid3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid3.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap3.Columns)
                {
                    grid3.Columns[Genclass.i].Name = column.ColumnName;
                    grid3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                grid3.DataSource = tap3;



                contc.Show();
                conn.Close();
            }
        }

        private void Titlep1()
        {
            RQGR.ColumnCount = 5;
            RQGR.Columns[0].Name = "Itemname";
            RQGR.Columns[1].Name = "Reqdate";
            RQGR.Columns[2].Name = "Qty";
            RQGR.Columns[3].Name = "Uid";
            //HFIT.Columns[4].Name = "Itid";
            RQGR.Columns[0].Width = 350;
            RQGR.Columns[1].Width = 100;
            RQGR.Columns[2].Width = 50;
            RQGR.Columns[3].Visible = false;
            RQGR.Columns[4].Visible = false;
        }
        private void Titlep2()
        {
            chkgrd.ColumnCount = 2;
            chkgrd.Columns[0].Name = "Itemname";

            chkgrd.Columns[1].Name = "Uid";


        }


        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtitem.Text == "")
            {
                MessageBox.Show("Select the Item");
                txtitem.Focus();
                return;
            }
            if (txtqty.Text == "")
            {
                MessageBox.Show("Enter the Quantity");
                txtqty.Focus();
                return;
            }
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Dc Quantity");
                txtdcqty.Focus();
                return;
            }
            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                if (txtititd.Text == HFIT.Rows[i].Cells[4].Value.ToString())
                {
                    MessageBox.Show("Item Already Added");
                    txtitem.Text = "";

                    txtititd.Text = "";
                    txtitem.Focus();
                    return;
                }
            }

            HFIT.AutoGenerateColumns = false;

            var index = HFIT.Rows.Add();
            HFIT.Rows[index].Cells[0].Value = txtitem.Text;
            HFIT.Rows[index].Cells[1].Value = txtqty.Text;
            HFIT.Rows[index].Cells[2].Value = txtdcqty.Text;
            HFIT.Rows[index].Cells[3].Value = txtaddnotes.Text;
            HFIT.Rows[index].Cells[4].Value = txtititd.Text;
            HFIT.Rows[index].Cells[5].Value = 0;
            HFIT.Rows[index].Cells[6].Value = txtpoid.Text;
           
            foreach (DataGridViewColumn dc in HFIT.Columns)
            {
                if (dc.Name == "Pqty")
                {
                    dc.ReadOnly = false;
                }
                else
                {
                    dc.ReadOnly = true;
                }
            }


            txtitem.Text = "";
            txtqty.Text = "";
            txtdcqty.Text = "";
            txtaddnotes.Text = "";
            txtititd.Text = "";
            //txtpono.Text = "";
            //txtpoid.Text = "";
            Genclass.type = 2;
            loadput1();
        }

        private void buttrqok_Click_1(object sender, EventArgs e)
        {
            if (txtreqqty.Text == "")
            {
                MessageBox.Show("Enter the Quantity");
                return;
            }

            int rowscount = HFIT.Rows.Count - 1;
            int rowscount1 = RQGR.Rows.Count - 1;

            txtqty2.Text = "";
            txtqty1.Text = "";

            for (int i = 0; i < rowscount; i++)
            {
                if (Cboit.Text == HFIT.Rows[i].Cells[0].Value.ToString())
                {
                    txtqty2.Text = HFIT.Rows[i].Cells[1].Value.ToString();

                    Genclass.sum = Convert.ToInt16(txtreqqty.Text);

                    for (int j = 0; j < rowscount1; j++)
                    {
                        if (Cboit.Text == RQGR.Rows[j].Cells[0].Value.ToString())
                        {
                            Genclass.sum = Genclass.sum + Convert.ToInt16(RQGR.Rows[j].Cells[2].Value);

                            txtqty1.Text = Genclass.sum.ToString();
                            if (Convert.ToInt16(txtqty1.Text) > Convert.ToInt16(txtqty2.Text))
                            {
                                MessageBox.Show("Qty Exceeds");
                                txtqty.Text = "";
                                txtqty.Focus();
                                return;
                            }
                        }
                    }

                }
            }

            Boolean strboo = false;

            if (chkgrd.RowCount - 1 == 0)
            {
                var index1 = chkgrd.Rows.Add();
                chkgrd.Rows[index1].Cells[0].Value = Cboit.Text;
                chkgrd.Rows[index1].Cells[1].Value = Cboit.SelectedValue;

            }
            else
            {
                for (int i = 0; i < chkgrd.RowCount - 1; i++)
                {
                    if (chkgrd.Rows[i].Cells[0].Value.ToString() == Cboit.Text)
                    {
                        strboo = true;
                    }
                }
                if (strboo == false)
                {
                    Titlep2();
                    var index1 = chkgrd.Rows.Add();
                    chkgrd.Rows[index1].Cells[0].Value = Cboit.Text;
                    chkgrd.Rows[index1].Cells[1].Value = Cboit.SelectedValue;
                }
            }
            Titlep1();
            var index = RQGR.Rows.Add();
            RQGR.Rows[index].Cells[0].Value = Cboit.Text;
            RQGR.Rows[index].Cells[1].Value = Dtpreq.Value;
            RQGR.Columns[1].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
            RQGR.Rows[index].Cells[2].Value = txtreqqty.Text;
            RQGR.Rows[index].Cells[3].Value = Cboit.SelectedValue;
            RQGR.Rows[index].Cells[4].Value = index;


            foreach (DataGridViewColumn dc in RQGR.Columns)
            {
                if (dc.Name == "Reqdate" || dc.Name == "Oty")
                {
                    dc.ReadOnly = false;
                }
                else
                {
                    dc.ReadOnly = true;
                }
            }


            txtreqqty.Text = "";
        }

        private void Reqbk_Click_1(object sender, EventArgs e)
        {

            if (HFIT.RowCount != chkgrd.RowCount)
            {
                MessageBox.Show("Item not matched");
                return;
            }

            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                Genclass.sum = 0;
                for (int j = 0; j < RQGR.RowCount - 1; j++)
                {
                    if (Convert.ToInt16(RQGR.Rows[j].Cells[3].Value) == Convert.ToInt16(HFIT.Rows[i].Cells[4].Value))

                    {
                        Genclass.sum = Genclass.sum + Convert.ToInt16(RQGR.Rows[j].Cells[2].Value);
                    }
                }

                if (Genclass.sum != Convert.ToInt16(HFIT.Rows[i].Cells[1].Value))
                {
                    MessageBox.Show("Qty not matched");
                    return;
                }
            }
            Reqpan.Visible = false;

        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }

            conn.Open();
            if (mode == 1)
                //{

                qur.CommandText = "";



            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {
                if (mode == 1)
                {
                    qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + dtpgrndt.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + Chkedtact.Checked + "',0,0,'" + txtjjno.Text + "'," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[4].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'" + HFIT.Rows[i].Cells[3].Value + "'," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[6].Value + ",0,0,0 ,0,0,0,0,0,0," + i + "," + mode + ",'N'";
                    qur.ExecuteNonQuery();

                }
                else
                {
                    qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + dtpgrndt.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + Chkedtact.Checked + "',0,0,'" + txtjjno.Text + "'," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[4].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'" + HFIT.Rows[i].Cells[3].Value + "'," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[6].Value + ",0,0,0,0,0,0,0," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + i + "," + mode + ",'N'";
                    qur.ExecuteNonQuery();
                }
            }

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + "";
                qur.ExecuteNonQuery();
            }
            MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            //}


            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
            Editpan.Visible = false;
            panadd.Visible = true;
        }

        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                Genclass.type = 1;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }


        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void label8_Click_1(object sender, EventArgs e)
        {

            if (HFIT.Rows[0].Cells[0].Value.ToString() == "" || HFIT.Rows[0].Cells[0].Value == null)
            {
                MessageBox.Show("No Item For Required Date");
                return;
            }
            else
            {
                //Titlep1();
                //Titlep2();

                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("Uid");
                dt.Columns.Add("Item");

                int rowscount = HFIT.Rows.Count - 1;

                for (int i = 0; i < rowscount; i++)
                {
                    dr = dt.NewRow();

                    dr["Uid"] = HFIT.Rows[i].Cells[4].Value;
                    dr["Item"] = HFIT.Rows[i].Cells[0].Value;

                    dt.Rows.Add(dr);
                }


                Cboit.DataSource = null;
                Cboit.DataSource = dt;
                Cboit.DisplayMember = "Item";
                Cboit.ValueMember = "uid";
                //Cboit.Text = dt.Rows[0]["Item"].ToString();
                //Cboit.SelectedIndex = -1;
                Reqpan.Visible = true;


            }
        }



        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            chkact.Checked = true;
            panadd.Visible = true;
        }

        private void btnedit_Click(object sender, EventArgs e)
        {



            try
            {

                conn.Open();
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                mode = 2;
                Genpan.Visible = false;
                Editpan.Visible = true;
                int i = HFGP.SelectedCells[0].RowIndex;
                txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
                txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                txtjjno.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                txtname.Text = HFGP.Rows[i].Cells[6].Value.ToString();
                txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
                txtnar.Text = HFGP.Rows[i].Cells[8].Value.ToString();
                if (HFGP.Rows[i].Cells[9].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;
                }
                else
                {
                    Chkedtact.Checked = false;
                }
                string quy3 = "select * from TransactionsP a inner join transactionsplist b on a.uid=b.transactionspuid and a.doctypeid=10 inner join  transactionsplist c on b.uid=c.refuid and  c.doctypeid=20   where a.uid=" + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy3, conn);

                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                if (tap3.Rows.Count > 0)
                {
                    MessageBox.Show("This GRN No already converted to DCwithout Process");
                    Editpan.Visible = false;
                    Genpan.Visible = true;

                }
                else
                {

                    string quy = "select itemname as Item,Pqty,Dcqty,Addnotes,itemuid,a.uid from TransactionsPlist a inner join itemm b on a.itemuid=b.uid where a.transactionspuid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                    //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                    this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    HFIT.AutoGenerateColumns = false;
                    HFIT.Refresh();
                    HFIT.DataSource = null;
                    HFIT.Rows.Clear();


                    HFIT.ColumnCount = tap.Columns.Count;
                    Genclass.i = 0;
                    foreach (DataColumn column in tap.Columns)
                    {
                        HFIT.Columns[Genclass.i].Name = column.ColumnName;
                        HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                        HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }




                    HFIT.Columns[0].Width = 450;
                    HFIT.Columns[1].Width = 50;
                    HFIT.Columns[2].Width = 50;
                    HFIT.Columns[3].Width = 250;
                    HFIT.Columns[4].Visible = false;
                    HFIT.Columns[5].Visible = false;
                    HFIT.DataSource = tap;



                    foreach (DataGridViewColumn dc in HFIT.Columns)
                    {
                        if (dc.Name == "Pqty")
                        {
                            dc.ReadOnly = false;
                        }
                        else
                        {
                            dc.ReadOnly = true;
                        }
                    }


                    string quy1 = "select itemname as Item,Reqdate,Qty,itemid from grnreqdate a inner join itemm b on a.itemid=b.uid where a.headid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy1, conn);

                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);


                    RQGR.AutoGenerateColumns = false;
                    RQGR.Refresh();
                    RQGR.DataSource = null;
                    RQGR.Rows.Clear();


                    RQGR.ColumnCount = tap1.Columns.Count;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap1.Columns)
                    {
                        RQGR.Columns[Genclass.i].Name = column.ColumnName;
                        RQGR.Columns[Genclass.i].HeaderText = column.ColumnName;
                        RQGR.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }



                    RQGR.Columns[0].Width = 350;
                    RQGR.Columns[1].Width = 100;
                    RQGR.Columns[2].Width = 50;
                    RQGR.Columns[3].Visible = false;

                    RQGR.Columns[1].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                    RQGR.DataSource = tap1;

                    string quy2 = "select Distinct itemname as Item,itemid from grnreqdate a inner join itemm b on a.itemid=b.uid where a.headid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy1, conn);

                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);


                    chkgrd.AutoGenerateColumns = false;
                    chkgrd.Refresh();
                    chkgrd.DataSource = null;
                    chkgrd.Rows.Clear();


                    chkgrd.ColumnCount = tap2.Columns.Count;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap2.Columns)
                    {
                        chkgrd.Columns[Genclass.i].Name = column.ColumnName;
                        chkgrd.Columns[Genclass.i].HeaderText = column.ColumnName;
                        chkgrd.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }



                    chkgrd.Columns[0].Width = 350;
                    chkgrd.Columns[1].Width = 100;

                    chkgrd.DataSource = tap2;

                }
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }



        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void HFGP_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void RQGR_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void HFIT_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (HFIT.CurrentRow.Cells[1].Value != null)
            {
                HFIT.CurrentRow.Cells[1].ReadOnly = false;
                HFIT.Update();
            }


        }


        private void cmdprt_Click(object sender, EventArgs e)
        {
            conn.Open();

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            Crviewer crv = new Crviewer();
            crv.Show();
            conn.Close();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            mode = 3;
            qur.CommandText = "";
            conn.Open();
            int i = HFGP.SelectedCells[0].RowIndex;
            qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + dtpgrndt.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "',0,'" + txtnar.Text + "',0,0,0,0,0,0,'" + txtjjno.Text + "'," + Genclass.data1 + "," + Genclass.Yearid + ",0,0,0,0,'0',0,0,0,0,0,0,0,0,0," + HFGP.Rows[i].Cells[0].Value + ",0," + i + "," + mode + "";
            qur.ExecuteNonQuery();


            string quy = "select * from Msgdes";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);
            conn.Close();
            Loadgrid();
        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            Editpan.Visible = true;
            panadd.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Titlep1();
            Titlep2();
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd MMM yyyy";
            Dtpdt.Format = DateTimePickerFormat.Custom;
            Dtpdt.CustomFormat = "dd MMM yyyy";
            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            txtdcno.Focus();
        }

        private void butedit_Click(object sender, EventArgs e)
        {

            try
            {

                conn.Open();
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                mode = 2;
                Genpan.Visible = false;
                Editpan.Visible = true;
                panadd.Visible = false;
                int i = HFGP.SelectedCells[0].RowIndex;
                txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
                txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                txtjjno.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                txtname.Text = HFGP.Rows[i].Cells[6].Value.ToString();

                txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
                txtnar.Text = HFGP.Rows[i].Cells[8].Value.ToString();
                if (HFGP.Rows[i].Cells[9].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;
                }
                else
                {
                    Chkedtact.Checked = false;
                }
                string quy3 = "select * from TransactionsP a inner join transactionsplist b on a.uid=b.transactionspuid and a.doctypeid=10 inner join  transactionsplist c on b.uid=c.refuid and  c.doctypeid=20   where a.uid=" + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy3, conn);

                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                if (tap3.Rows.Count > 0)
                {
                    MessageBox.Show("This GRN No already converted to DCwithout Process");
                    Editpan.Visible = false;
                    Genpan.Visible = true;

                }
                else
                {

                    string quy = "select itemname as Item,Pqty,Dcqty,Addnotes,itemuid,a.uid,refuid from TransactionsPlist a inner join itemm b on a.itemuid=b.uid where a.transactionspuid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                    //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                    this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    HFIT.AutoGenerateColumns = false;
                    HFIT.Refresh();
                    HFIT.DataSource = null;
                    HFIT.Rows.Clear();


                    HFIT.ColumnCount = tap.Columns.Count;
                    Genclass.i = 0;
                    foreach (DataColumn column in tap.Columns)
                    {
                        HFIT.Columns[Genclass.i].Name = column.ColumnName;
                        HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                        HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }




                    HFIT.Columns[0].Width = 450;
                    HFIT.Columns[1].Width = 80;
                    HFIT.Columns[2].Width = 80;
                    HFIT.Columns[3].Width = 250;
                    HFIT.Columns[4].Visible = false;
                    HFIT.Columns[5].Visible = false;
                    HFIT.Columns[6].Visible = false;
                    HFIT.DataSource = tap;



                    foreach (DataGridViewColumn dc in HFIT.Columns)
                    {
                        if (dc.Name == "Pqty")
                        {
                            dc.ReadOnly = false;
                        }
                        else
                        {
                            dc.ReadOnly = true;
                        }
                    }


                    //string quy1 = "select itemname as Item,Reqdate,Qty,itemid from grnreqdate a inner join itemm b on a.itemid=b.uid where a.headid=" + txtgrnid.Text + "";
                    //Genclass.cmd = new SqlCommand(quy1, conn);

                    //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    //DataTable tap1 = new DataTable();
                    //aptr1.Fill(tap1);


                    //RQGR.AutoGenerateColumns = false;
                    //RQGR.Refresh();
                    //RQGR.DataSource = null;
                    //RQGR.Rows.Clear();


                    //RQGR.ColumnCount = tap1.Columns.Count;

                    //Genclass.i = 0;
                    //foreach (DataColumn column in tap1.Columns)
                    //{
                    //    RQGR.Columns[Genclass.i].Name = column.ColumnName;
                    //    RQGR.Columns[Genclass.i].HeaderText = column.ColumnName;
                    //    RQGR.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    //    Genclass.i = Genclass.i + 1;
                    //}



                    //RQGR.Columns[0].Width = 350;
                    //RQGR.Columns[1].Width = 100;
                    //RQGR.Columns[2].Width = 50;
                    //RQGR.Columns[3].Visible = false;

                    //RQGR.Columns[1].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                    //RQGR.DataSource = tap1;

                    //string quy2 = "select Distinct itemname as Item,itemid from grnreqdate a inner join itemm b on a.itemid=b.uid where a.headid=" + txtgrnid.Text + "";
                    //Genclass.cmd = new SqlCommand(quy1, conn);

                    //SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    //DataTable tap2 = new DataTable();
                    //aptr2.Fill(tap2);


                    //chkgrd.AutoGenerateColumns = false;
                    //chkgrd.Refresh();
                    //chkgrd.DataSource = null;
                    //chkgrd.Rows.Clear();


                    //chkgrd.ColumnCount = tap2.Columns.Count;

                    //Genclass.i = 0;
                    //foreach (DataColumn column in tap2.Columns)
                    //{
                    //    chkgrd.Columns[Genclass.i].Name = column.ColumnName;
                    //    chkgrd.Columns[Genclass.i].HeaderText = column.ColumnName;
                    //    chkgrd.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    //    Genclass.i = Genclass.i + 1;
                    //}



                    //chkgrd.Columns[0].Width = 350;
                    //chkgrd.Columns[1].Width = 100;

                    //chkgrd.DataSource = tap2;

                }
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }


        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Open();

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            Crviewer crv = new Crviewer();
            crv.Show();
            conn.Close();
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            conn.Open();

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            Crviewer crv = new Crviewer();
            crv.Show();
            conn.Close();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_Click(object sender, EventArgs e)
        {

            Genclass.type = 1;
            loadput();
        }

        private void txtitem_Click(object sender, EventArgs e)
        {
            if (txtpuid.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            Genclass.type = 2;
            Genclass.FSSQLSortStr = "Itemname";
            loadput1();
        }

        private void txtitem_MouseClick(object sender, MouseEventArgs e)
        {
        }
    }
}