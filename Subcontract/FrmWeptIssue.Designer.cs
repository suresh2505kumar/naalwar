﻿namespace Naalwar
{
    partial class FrmWeptIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWeptIssue));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grPkBack = new System.Windows.Forms.GroupBox();
            this.panBulkEntry = new System.Windows.Forms.Panel();
            this.txtBGrossWght = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.txtBTareWght = new System.Windows.Forms.TextBox();
            this.txtBNofBags = new System.Windows.Forms.TextBox();
            this.txtFromBagNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbPackType = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.chckNofBags = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMillName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNoogBags = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotalWeight = new System.Windows.Forms.TextBox();
            this.DataGridWeight = new System.Windows.Forms.DataGridView();
            this.dtpSlipDae = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSlipNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBagNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtItem = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGrossWght = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTarWght = new System.Windows.Forms.TextBox();
            this.cmbManulaPackType = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.panNumbers = new System.Windows.Forms.Panel();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFstBack = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnFstNext = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.grPkFront = new System.Windows.Forms.GroupBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.DataGridWeptIssue = new System.Windows.Forms.DataGridView();
            this.grPkBack.SuspendLayout();
            this.panBulkEntry.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeight)).BeginInit();
            this.panadd.SuspendLayout();
            this.panNumbers.SuspendLayout();
            this.grPkFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeptIssue)).BeginInit();
            this.SuspendLayout();
            // 
            // grPkBack
            // 
            this.grPkBack.Controls.Add(this.panBulkEntry);
            this.grPkBack.Controls.Add(this.grSearch);
            this.grPkBack.Controls.Add(this.chckNofBags);
            this.grPkBack.Controls.Add(this.label9);
            this.grPkBack.Controls.Add(this.txtMillName);
            this.grPkBack.Controls.Add(this.label8);
            this.grPkBack.Controls.Add(this.txtNoogBags);
            this.grPkBack.Controls.Add(this.label7);
            this.grPkBack.Controls.Add(this.txtTotalWeight);
            this.grPkBack.Controls.Add(this.DataGridWeight);
            this.grPkBack.Controls.Add(this.dtpSlipDae);
            this.grPkBack.Controls.Add(this.label5);
            this.grPkBack.Controls.Add(this.label4);
            this.grPkBack.Controls.Add(this.txtSlipNumber);
            this.grPkBack.Controls.Add(this.label3);
            this.grPkBack.Controls.Add(this.txtWeight);
            this.grPkBack.Controls.Add(this.label2);
            this.grPkBack.Controls.Add(this.txtBagNo);
            this.grPkBack.Controls.Add(this.label1);
            this.grPkBack.Controls.Add(this.txtItem);
            this.grPkBack.Controls.Add(this.btnOk);
            this.grPkBack.Controls.Add(this.label14);
            this.grPkBack.Controls.Add(this.txtGrossWght);
            this.grPkBack.Controls.Add(this.label13);
            this.grPkBack.Controls.Add(this.txtTarWght);
            this.grPkBack.Controls.Add(this.cmbManulaPackType);
            this.grPkBack.Controls.Add(this.label17);
            this.grPkBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grPkBack.Location = new System.Drawing.Point(8, -2);
            this.grPkBack.Name = "grPkBack";
            this.grPkBack.Size = new System.Drawing.Size(760, 496);
            this.grPkBack.TabIndex = 1;
            this.grPkBack.TabStop = false;
            // 
            // panBulkEntry
            // 
            this.panBulkEntry.Controls.Add(this.txtBGrossWght);
            this.panBulkEntry.Controls.Add(this.label15);
            this.panBulkEntry.Controls.Add(this.btnLoad);
            this.panBulkEntry.Controls.Add(this.txtBTareWght);
            this.panBulkEntry.Controls.Add(this.txtBNofBags);
            this.panBulkEntry.Controls.Add(this.txtFromBagNo);
            this.panBulkEntry.Controls.Add(this.label12);
            this.panBulkEntry.Controls.Add(this.label11);
            this.panBulkEntry.Controls.Add(this.label10);
            this.panBulkEntry.Controls.Add(this.cmbPackType);
            this.panBulkEntry.Controls.Add(this.label16);
            this.panBulkEntry.Location = new System.Drawing.Point(539, 16);
            this.panBulkEntry.Name = "panBulkEntry";
            this.panBulkEntry.Size = new System.Drawing.Size(214, 201);
            this.panBulkEntry.TabIndex = 395;
            this.panBulkEntry.Visible = false;
            // 
            // txtBGrossWght
            // 
            this.txtBGrossWght.Location = new System.Drawing.Point(95, 68);
            this.txtBGrossWght.Name = "txtBGrossWght";
            this.txtBGrossWght.Size = new System.Drawing.Size(110, 26);
            this.txtBGrossWght.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 106);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 18);
            this.label15.TabIndex = 399;
            this.label15.Text = "Tare Weight";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(117, 170);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(86, 28);
            this.btnLoad.TabIndex = 400;
            this.btnLoad.Text = "Generate";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // txtBTareWght
            // 
            this.txtBTareWght.Location = new System.Drawing.Point(95, 104);
            this.txtBTareWght.Name = "txtBTareWght";
            this.txtBTareWght.Size = new System.Drawing.Size(110, 26);
            this.txtBTareWght.TabIndex = 12;
            // 
            // txtBNofBags
            // 
            this.txtBNofBags.Location = new System.Drawing.Point(95, 36);
            this.txtBNofBags.Name = "txtBNofBags";
            this.txtBNofBags.Size = new System.Drawing.Size(110, 26);
            this.txtBNofBags.TabIndex = 10;
            // 
            // txtFromBagNo
            // 
            this.txtFromBagNo.Location = new System.Drawing.Point(95, 5);
            this.txtFromBagNo.Name = "txtFromBagNo";
            this.txtFromBagNo.Size = new System.Drawing.Size(110, 26);
            this.txtFromBagNo.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 72);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 18);
            this.label12.TabIndex = 22;
            this.label12.Text = "Gross Weight";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 18);
            this.label11.TabIndex = 21;
            this.label11.Text = "No of Bags";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 18);
            this.label10.TabIndex = 0;
            this.label10.Text = "From BagNo";
            // 
            // cmbPackType
            // 
            this.cmbPackType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPackType.FormattingEnabled = true;
            this.cmbPackType.Items.AddRange(new object[] {
            "Bag",
            "C Box"});
            this.cmbPackType.Location = new System.Drawing.Point(95, 136);
            this.cmbPackType.Name = "cmbPackType";
            this.cmbPackType.Size = new System.Drawing.Size(108, 26);
            this.cmbPackType.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 140);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 18);
            this.label16.TabIndex = 401;
            this.label16.Text = "Type of Pack";
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button2);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(164, 220);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(382, 300);
            this.grSearch.TabIndex = 393;
            this.grSearch.Visible = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::Naalwar.Properties.Resources.ok;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.Location = new System.Drawing.Point(174, 268);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 394;
            this.button2.Text = "Select (F2)";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(277, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(375, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // chckNofBags
            // 
            this.chckNofBags.AutoSize = true;
            this.chckNofBags.Location = new System.Drawing.Point(453, 16);
            this.chckNofBags.Name = "chckNofBags";
            this.chckNofBags.Size = new System.Drawing.Size(89, 22);
            this.chckNofBags.TabIndex = 394;
            this.chckNofBags.Text = "Bulk Entry";
            this.chckNofBags.UseVisualStyleBackColor = true;
            this.chckNofBags.CheckedChanged += new System.EventHandler(this.chckNofBags_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(42, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 18);
            this.label9.TabIndex = 20;
            this.label9.Text = "Mill Name";
            // 
            // txtMillName
            // 
            this.txtMillName.Location = new System.Drawing.Point(120, 124);
            this.txtMillName.Name = "txtMillName";
            this.txtMillName.Size = new System.Drawing.Size(346, 26);
            this.txtMillName.TabIndex = 2;
            this.txtMillName.Click += new System.EventHandler(this.txtMillName_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(379, 470);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 18);
            this.label8.TabIndex = 16;
            this.label8.Text = "No of Bags";
            // 
            // txtNoogBags
            // 
            this.txtNoogBags.Location = new System.Drawing.Point(458, 466);
            this.txtNoogBags.Name = "txtNoogBags";
            this.txtNoogBags.Size = new System.Drawing.Size(88, 26);
            this.txtNoogBags.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(565, 470);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 18);
            this.label7.TabIndex = 14;
            this.label7.Text = "Total Weight";
            // 
            // txtTotalWeight
            // 
            this.txtTotalWeight.Location = new System.Drawing.Point(656, 466);
            this.txtTotalWeight.Name = "txtTotalWeight";
            this.txtTotalWeight.Size = new System.Drawing.Size(88, 26);
            this.txtTotalWeight.TabIndex = 13;
            // 
            // DataGridWeight
            // 
            this.DataGridWeight.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridWeight.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.DataGridWeight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridWeight.EnableHeadersVisualStyles = false;
            this.DataGridWeight.Location = new System.Drawing.Point(10, 220);
            this.DataGridWeight.Name = "DataGridWeight";
            this.DataGridWeight.RowHeadersVisible = false;
            this.DataGridWeight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridWeight.Size = new System.Drawing.Size(737, 240);
            this.DataGridWeight.TabIndex = 12;
            // 
            // dtpSlipDae
            // 
            this.dtpSlipDae.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSlipDae.Location = new System.Drawing.Point(120, 52);
            this.dtpSlipDae.Name = "dtpSlipDae";
            this.dtpSlipDae.Size = new System.Drawing.Size(104, 26);
            this.dtpSlipDae.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Slip Number";
            // 
            // txtSlipNumber
            // 
            this.txtSlipNumber.Enabled = false;
            this.txtSlipNumber.Location = new System.Drawing.Point(120, 14);
            this.txtSlipNumber.Name = "txtSlipNumber";
            this.txtSlipNumber.Size = new System.Drawing.Size(130, 26);
            this.txtSlipNumber.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Net Weight";
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(288, 190);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(93, 26);
            this.txtWeight.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Bag No";
            // 
            // txtBagNo
            // 
            this.txtBagNo.Location = new System.Drawing.Point(7, 190);
            this.txtBagNo.Name = "txtBagNo";
            this.txtBagNo.Size = new System.Drawing.Size(106, 26);
            this.txtBagNo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Count Name";
            // 
            // txtItem
            // 
            this.txtItem.Location = new System.Drawing.Point(120, 88);
            this.txtItem.Name = "txtItem";
            this.txtItem.Size = new System.Drawing.Size(346, 26);
            this.txtItem.TabIndex = 1;
            this.txtItem.Click += new System.EventHandler(this.txtItem_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(498, 188);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(40, 28);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(113, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 18);
            this.label14.TabIndex = 399;
            this.label14.Text = "Gross";
            // 
            // txtGrossWght
            // 
            this.txtGrossWght.Location = new System.Drawing.Point(114, 190);
            this.txtGrossWght.Name = "txtGrossWght";
            this.txtGrossWght.Size = new System.Drawing.Size(88, 26);
            this.txtGrossWght.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(201, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 18);
            this.label13.TabIndex = 396;
            this.label13.Text = "Tare";
            // 
            // txtTarWght
            // 
            this.txtTarWght.Location = new System.Drawing.Point(202, 190);
            this.txtTarWght.Name = "txtTarWght";
            this.txtTarWght.Size = new System.Drawing.Size(86, 26);
            this.txtTarWght.TabIndex = 5;
            this.txtTarWght.TextChanged += new System.EventHandler(this.txtTarWght_TextChanged);
            // 
            // cmbManulaPackType
            // 
            this.cmbManulaPackType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManulaPackType.FormattingEnabled = true;
            this.cmbManulaPackType.Items.AddRange(new object[] {
            "Bag",
            "C Box"});
            this.cmbManulaPackType.Location = new System.Drawing.Point(383, 189);
            this.cmbManulaPackType.Name = "cmbManulaPackType";
            this.cmbManulaPackType.Size = new System.Drawing.Size(113, 26);
            this.cmbManulaPackType.TabIndex = 7;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(388, 167);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 18);
            this.label17.TabIndex = 403;
            this.label17.Text = "Type of Pack";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.panNumbers);
            this.panadd.Controls.Add(this.btnFstBack);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnFstNext);
            this.panadd.Controls.Add(this.btnNext);
            this.panadd.Controls.Add(this.chckActive);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnDelete);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Location = new System.Drawing.Point(4, 496);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(764, 34);
            this.panadd.TabIndex = 211;
            // 
            // panNumbers
            // 
            this.panNumbers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panNumbers.Controls.Add(this.lblFrom);
            this.panNumbers.Controls.Add(this.lblCount);
            this.panNumbers.Controls.Add(this.flowLayoutPanel3);
            this.panNumbers.Controls.Add(this.flowLayoutPanel2);
            this.panNumbers.Controls.Add(this.flowLayoutPanel1);
            this.panNumbers.Location = new System.Drawing.Point(66, 5);
            this.panNumbers.Name = "panNumbers";
            this.panNumbers.Size = new System.Drawing.Size(74, 24);
            this.panNumbers.TabIndex = 214;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.ForeColor = System.Drawing.Color.Black;
            this.lblFrom.Location = new System.Drawing.Point(4, 3);
            this.lblFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(15, 16);
            this.lblFrom.TabIndex = 163;
            this.lblFrom.Text = "1";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(27, 3);
            this.lblCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(29, 16);
            this.lblCount.TabIndex = 162;
            this.lblCount.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFstBack
            // 
            this.btnFstBack.BackColor = System.Drawing.Color.White;
            this.btnFstBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFstBack.FlatAppearance.BorderSize = 0;
            this.btnFstBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFstBack.Image = ((System.Drawing.Image)(resources.GetObject("btnFstBack.Image")));
            this.btnFstBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFstBack.Location = new System.Drawing.Point(9, 5);
            this.btnFstBack.Name = "btnFstBack";
            this.btnFstBack.Size = new System.Drawing.Size(19, 26);
            this.btnFstBack.TabIndex = 213;
            this.btnFstBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFstBack.UseVisualStyleBackColor = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(38, 5);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 26);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            // 
            // btnFstNext
            // 
            this.btnFstNext.BackColor = System.Drawing.Color.White;
            this.btnFstNext.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFstNext.FlatAppearance.BorderSize = 0;
            this.btnFstNext.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFstNext.Image = ((System.Drawing.Image)(resources.GetObject("btnFstNext.Image")));
            this.btnFstNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFstNext.Location = new System.Drawing.Point(178, 5);
            this.btnFstNext.Name = "btnFstNext";
            this.btnFstNext.Size = new System.Drawing.Size(19, 26);
            this.btnFstNext.TabIndex = 211;
            this.btnFstNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFstNext.UseVisualStyleBackColor = false;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.White;
            this.btnNext.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNext.Location = new System.Drawing.Point(150, 5);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(18, 26);
            this.btnNext.TabIndex = 210;
            this.btnNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext.UseVisualStyleBackColor = false;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.BackColor = System.Drawing.Color.White;
            this.chckActive.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckActive.Location = new System.Drawing.Point(302, 6);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 187;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(487, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(87, 30);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(573, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(58, 30);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(698, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 30);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(695, 3);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 29);
            this.btnaddrcan.TabIndex = 213;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(630, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(69, 30);
            this.btnDelete.TabIndex = 186;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(628, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(71, 30);
            this.btnsave.TabIndex = 212;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // grPkFront
            // 
            this.grPkFront.Controls.Add(this.textBox8);
            this.grPkFront.Controls.Add(this.DataGridWeptIssue);
            this.grPkFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grPkFront.Location = new System.Drawing.Point(7, -1);
            this.grPkFront.Name = "grPkFront";
            this.grPkFront.Size = new System.Drawing.Size(760, 497);
            this.grPkFront.TabIndex = 404;
            this.grPkFront.TabStop = false;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(10, 16);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(743, 26);
            this.textBox8.TabIndex = 1;
            // 
            // DataGridWeptIssue
            // 
            this.DataGridWeptIssue.AllowUserToAddRows = false;
            this.DataGridWeptIssue.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridWeptIssue.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.DataGridWeptIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridWeptIssue.EnableHeadersVisualStyles = false;
            this.DataGridWeptIssue.Location = new System.Drawing.Point(10, 45);
            this.DataGridWeptIssue.Name = "DataGridWeptIssue";
            this.DataGridWeptIssue.RowHeadersVisible = false;
            this.DataGridWeptIssue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridWeptIssue.Size = new System.Drawing.Size(743, 441);
            this.DataGridWeptIssue.TabIndex = 0;
            // 
            // FrmWeptIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(774, 533);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grPkBack);
            this.Controls.Add(this.grPkFront);
            this.Name = "FrmWeptIssue";
            this.Text = "Wept Issue";
            this.Load += new System.EventHandler(this.FrmWeptIssue_Load);
            this.grPkBack.ResumeLayout(false);
            this.grPkBack.PerformLayout();
            this.panBulkEntry.ResumeLayout(false);
            this.panBulkEntry.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeight)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panNumbers.ResumeLayout(false);
            this.panNumbers.PerformLayout();
            this.grPkFront.ResumeLayout(false);
            this.grPkFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeptIssue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grPkBack;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNoogBags;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotalWeight;
        private System.Windows.Forms.DataGridView DataGridWeight;
        private System.Windows.Forms.DateTimePicker dtpSlipDae;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSlipNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBagNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtItem;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtGrossWght;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTarWght;
        private System.Windows.Forms.ComboBox cmbManulaPackType;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panNumbers;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFstBack;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnFstNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Panel panBulkEntry;
        private System.Windows.Forms.TextBox txtBGrossWght;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox txtBTareWght;
        private System.Windows.Forms.TextBox txtBNofBags;
        private System.Windows.Forms.TextBox txtFromBagNo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbPackType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chckNofBags;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMillName;
        private System.Windows.Forms.GroupBox grPkFront;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.DataGridView DataGridWeptIssue;
    }
}