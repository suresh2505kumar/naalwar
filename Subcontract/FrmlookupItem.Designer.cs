﻿namespace Naalwar
{
    partial class FrmlookupItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmlookupItem));
            this.tabC = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.Hfgp = new System.Windows.Forms.DataGridView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cbogrp = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.HFGP4 = new System.Windows.Forms.DataGridView();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.txtscr3 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.HFGP2 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.HFGP3 = new System.Windows.Forms.DataGridView();
            this.tabC.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP4)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabC
            // 
            this.tabC.Controls.Add(this.tabPage1);
            this.tabC.Controls.Add(this.tabPage2);
            this.tabC.Controls.Add(this.tabPage3);
            this.tabC.Controls.Add(this.tabPage4);
            this.tabC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabC.Location = new System.Drawing.Point(12, 12);
            this.tabC.Name = "tabC";
            this.tabC.SelectedIndex = 0;
            this.tabC.Size = new System.Drawing.Size(797, 428);
            this.tabC.TabIndex = 0;
            this.tabC.SelectedIndexChanged += new System.EventHandler(this.tabC_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnadd);
            this.tabPage1.Controls.Add(this.txtscr2);
            this.tabPage1.Controls.Add(this.btnaddrcan);
            this.tabPage1.Controls.Add(this.txtscr1);
            this.tabPage1.Controls.Add(this.Hfgp);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.cbogrp);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(789, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "From DC";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(347, 360);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(61, 32);
            this.btnadd.TabIndex = 90;
            this.btnadd.Text = "Ok";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtscr2
            // 
            this.txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(344, 7);
            this.txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(338, 22);
            this.txtscr2.TabIndex = 91;
            this.txtscr2.TextChanged += new System.EventHandler(this.txtscr2_TextChanged);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(418, 360);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 32);
            this.btnaddrcan.TabIndex = 89;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(7, 7);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(338, 22);
            this.txtscr1.TabIndex = 90;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // Hfgp
            // 
            this.Hfgp.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Hfgp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Hfgp.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Hfgp.Location = new System.Drawing.Point(6, 36);
            this.Hfgp.Name = "Hfgp";
            this.Hfgp.Size = new System.Drawing.Size(781, 321);
            this.Hfgp.TabIndex = 1;
            this.Hfgp.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellContentClick);
            this.Hfgp.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellEnter);
            this.Hfgp.Click += new System.EventHandler(this.Hfgp_Click);
            this.Hfgp.DoubleClick += new System.EventHandler(this.Hfgp_DoubleClick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(312, 174);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 93;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // cbogrp
            // 
            this.cbogrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbogrp.FormattingEnabled = true;
            this.cbogrp.Location = new System.Drawing.Point(333, 171);
            this.cbogrp.Name = "cbogrp";
            this.cbogrp.Size = new System.Drawing.Size(234, 24);
            this.cbogrp.TabIndex = 92;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.HFGP4);
            this.tabPage2.Controls.Add(this.chkact);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.txtscr4);
            this.tabPage2.Controls.Add(this.txtscr3);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(789, 399);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "FromSales Order";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // HFGP4
            // 
            this.HFGP4.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP4.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP4.Location = new System.Drawing.Point(3, 39);
            this.HFGP4.Name = "HFGP4";
            this.HFGP4.Size = new System.Drawing.Size(783, 316);
            this.HFGP4.TabIndex = 189;
            this.HFGP4.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP4_CellClick_2);
            this.HFGP4.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP4_CellContentClick);
            this.HFGP4.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP4_CellValueChanged_3);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(688, 16);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(75, 19);
            this.chkact.TabIndex = 188;
            this.chkact.Text = "Select All";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.Visible = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(320, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 32);
            this.button1.TabIndex = 97;
            this.button1.Text = "Ok";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(391, 361);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(66, 32);
            this.button2.TabIndex = 96;
            this.button2.Text = "Back";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(343, 12);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(338, 22);
            this.txtscr4.TabIndex = 93;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged);
            // 
            // txtscr3
            // 
            this.txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr3.Location = new System.Drawing.Point(6, 12);
            this.txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr3.Name = "txtscr3";
            this.txtscr3.Size = new System.Drawing.Size(338, 22);
            this.txtscr3.TabIndex = 92;
            this.txtscr3.TextChanged += new System.EventHandler(this.txtscr3_TextChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.txtscr6);
            this.tabPage3.Controls.Add(this.txtscr5);
            this.tabPage3.Controls.Add(this.HFGP2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(789, 399);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Mapped Items";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button3.Location = new System.Drawing.Point(310, 364);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 32);
            this.button3.TabIndex = 95;
            this.button3.Text = "Ok";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(381, 364);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(66, 32);
            this.button4.TabIndex = 94;
            this.button4.Text = "Back";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(343, 15);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(338, 22);
            this.txtscr6.TabIndex = 93;
            this.txtscr6.TextChanged += new System.EventHandler(this.txtscr6_TextChanged);
            // 
            // txtscr5
            // 
            this.txtscr5.AcceptsReturn = true;
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(6, 15);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(338, 22);
            this.txtscr5.TabIndex = 92;
            this.txtscr5.TextChanged += new System.EventHandler(this.txtscr5_TextChanged);
            // 
            // HFGP2
            // 
            this.HFGP2.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP2.Location = new System.Drawing.Point(6, 44);
            this.HFGP2.Name = "HFGP2";
            this.HFGP2.Size = new System.Drawing.Size(780, 316);
            this.HFGP2.TabIndex = 2;
            this.HFGP2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellClick);
            this.HFGP2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellContentClick);
            this.HFGP2.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellEnter);
            this.HFGP2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellValueChanged);
            this.HFGP2.DoubleClick += new System.EventHandler(this.HFGP2_DoubleClick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.txtscr8);
            this.tabPage4.Controls.Add(this.txtscr7);
            this.tabPage4.Controls.Add(this.HFGP3);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(789, 399);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Items List";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.Location = new System.Drawing.Point(321, 366);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(61, 32);
            this.button5.TabIndex = 95;
            this.button5.Text = "Ok";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(392, 366);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(66, 32);
            this.button6.TabIndex = 94;
            this.button6.Text = "Back";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(344, 15);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(415, 22);
            this.txtscr8.TabIndex = 93;
            this.txtscr8.TextChanged += new System.EventHandler(this.txtscr8_TextChanged);
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(7, 15);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(752, 22);
            this.txtscr7.TabIndex = 92;
            this.txtscr7.TextChanged += new System.EventHandler(this.txtscr7_TextChanged);
            // 
            // HFGP3
            // 
            this.HFGP3.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP3.Location = new System.Drawing.Point(6, 44);
            this.HFGP3.Name = "HFGP3";
            this.HFGP3.Size = new System.Drawing.Size(780, 316);
            this.HFGP3.TabIndex = 2;
            this.HFGP3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellClick);
            this.HFGP3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellContentClick);
            this.HFGP3.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellEnter);
            this.HFGP3.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP3_CellValueChanged);
            this.HFGP3.Click += new System.EventHandler(this.HFGP3_Click);
            // 
            // FrmlookupItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 438);
            this.Controls.Add(this.tabC);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmlookupItem";
            this.Text = "LookupItems";
            this.Load += new System.EventHandler(this.FrmlookupItem_Load);
            this.tabC.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP4)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabC;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView Hfgp;
        private System.Windows.Forms.DataGridView HFGP2;
        private System.Windows.Forms.DataGridView HFGP3;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.TextBox txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox txtscr3;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox cbogrp;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkact;
        internal System.Windows.Forms.Button button3;
        internal System.Windows.Forms.Button button5;
        internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView HFGP4;
    }
}