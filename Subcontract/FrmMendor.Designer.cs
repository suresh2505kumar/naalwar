﻿namespace Naalwar
{
    partial class FrmMendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMendor));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.txtMendorName = new System.Windows.Forms.TextBox();
            this.txtMendorCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridMendor = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.panadd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMendor)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.txtMendorName);
            this.grBack.Controls.Add(this.txtMendorCode);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(10, 8);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(506, 361);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(196, 186);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 4;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // txtMendorName
            // 
            this.txtMendorName.Location = new System.Drawing.Point(196, 140);
            this.txtMendorName.Name = "txtMendorName";
            this.txtMendorName.Size = new System.Drawing.Size(196, 26);
            this.txtMendorName.TabIndex = 3;
            // 
            // txtMendorCode
            // 
            this.txtMendorCode.Location = new System.Drawing.Point(196, 94);
            this.txtMendorCode.Name = "txtMendorCode";
            this.txtMendorCode.Size = new System.Drawing.Size(196, 26);
            this.txtMendorCode.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mendor Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mendor Code";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Location = new System.Drawing.Point(10, 375);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(510, 36);
            this.panadd.TabIndex = 238;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(303, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 31);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(449, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(390, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(355, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(435, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.btnAddCancel_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridMendor);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(10, 3);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(508, 366);
            this.grFront.TabIndex = 5;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 13);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(496, 26);
            this.txtSearch.TabIndex = 1;
            // 
            // DataGridMendor
            // 
            this.DataGridMendor.AllowUserToAddRows = false;
            this.DataGridMendor.BackgroundColor = System.Drawing.Color.White;
            this.DataGridMendor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridMendor.Location = new System.Drawing.Point(6, 40);
            this.DataGridMendor.Name = "DataGridMendor";
            this.DataGridMendor.RowHeadersVisible = false;
            this.DataGridMendor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridMendor.Size = new System.Drawing.Size(495, 321);
            this.DataGridMendor.TabIndex = 0;
            // 
            // FrmMendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(523, 416);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.MaximizeBox = false;
            this.Name = "FrmMendor";
            this.Text = "Mendor";
            this.Load += new System.EventHandler(this.FrmMendor_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMendor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.TextBox txtMendorName;
        private System.Windows.Forms.TextBox txtMendorCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridMendor;
    }
}