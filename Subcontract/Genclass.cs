﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Naalwar
{
    class Genclass
    {
        public static DateTime FromDate;
        public static DateTime ToDate;
        public static object fieldone;
        public static object fieldtwo;
        public static object fieldthree;
        public static object fieldFour;
        public static object fieldFive;
        public static object fieldSix;
        public static object fieldSeven;
        public static object fieldeight;
        public static object fieldgrnone;
        public static object fieldgrntwo;
        public static object fieldgrnthree;
        public static string frmwhat = "";
        public static string frmname = "";
        public static string sql = "";
        public static string nextform = "";
        public static string EC = "";
        public static string strsql = "";
        public static string strsql1 = "";
        public static string strsql2 = "";
        public static string strsql3 = "";
        public static string strsqltmp = "";
        public static string Genlbl = "";
        public static string Genlbl1 = "";
        public static string Genlbl2 = "";
        public static string Genlbl3 = "";
        public static Boolean canclclik;
        public static SqlCommand cmd;
        public static int i = 0;
        public static int h = 0;
        public static int k = 0;
        public static int Prtid = 0;
        public static int tuid = 0;
        public static string StrSrch = "";
        public static string StrSrch1 = "";
        public static string StrSrch2 = "";
        public static string FSSQLSortStr = "";
        public static string FSSQLSortStr1 = "";
        public static string FSSQLSortStr2 = "";
        public static string FSSQLSortStr3 = "";
        public static string FSSQLSortStr4 = "";
        public static string FSSQLSortStr5 = "";
        public static string FSSQLSortStr6 = "";
        public static string FSSQLSortStr7 = "";
        public static string FSSQLSortStr8 = "";
        public static string FSSQLSortStr9 = "";
        public static string FSSQLSortStrtmp = "";
        public static string FSSQLSortStrtmp1 = "";
        public static int data1 = 0;
        public static string data2 = "";
        public static string data3 = "";
        public static string data4 = "";
        public static string data5 = "";
        public static string data6 = "";
        public static int Gbtxtid = 0;
        public static int Dtype = 0;
        public static int type = 0;
        public static string ST = "";
        public static string STR = "";
        public static string bomstr = "";
        public static string address = "";
        public static Boolean okclick;
        public static Control Parent;
        public static int Qty1 = 0;
        public static int Qty2 = 0;
        public static int Qty3 = 0;
        public static int sum = 0;
        public static string stockid = "";
        public static string strfin;
        public static int compid = 0;
        public static int Yearid = 0;
        public static double sum1;
        public static double sum2;
        public static double sum3;
        public static double sum4;
        public static double sum5;
        public static int ty;
        public static int ty1;
        public static int ty2;
        public static int ty3;
        public static int slno = 0;
        public static int cat = 0;
        public static string Str5;
        public static DateTime prtfrmdate;
        public static DateTime prttodate;
        public static decimal CGSTPer;
        public static decimal CGSTValue;
        public static decimal SGSTPer;
        public static decimal SGSTValue;
        public static decimal IGSTPer = 0;
        public static decimal IGSTValue = 0;
        public static string CompanyName = string.Empty;
        public static string Barcode = string.Empty;
        public static string SortNo = string.Empty;
        public static string ReportType = string.Empty;
        public static DateTime ReortDate;
        public static int RpeortId;
        public static string parameter = string.Empty;
        public static string parameter1 = string.Empty;
        SqlCommand Sqlcmd = new SqlCommand();
        public static int Print = 0;
        public static DataTable dtLoom = new DataTable();

        internal static void ScreenClose()
        {
            throw new NotImplementedException();
        }

        public static Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }


        public static class Module
        {
            public static void Partylistviewcont(String name, String uid, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, Panel whatfldthree)
            {
                Genclass.fieldone = whatfldone.Name;
                Genclass.fieldtwo = whatfldtwo.Name;
                Genclass.Parent = whatfldthree;
                Genclass.frmwhat = whatform.Name;

            }
            public static void PartylistviewcontT(String name, String uid, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TabControl whatfldthree)
            {
                Genclass.fieldone = whatfldone.Name;
                Genclass.fieldtwo = whatfldtwo.Name;
                Genclass.Parent = whatfldthree;
                Genclass.frmwhat = whatform.Name;

            }


            public static void Partylistviewcont1(String name, String uid, String str1, String str2, String str3, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TextBox whatfldFour, TextBox whatfldFive, Panel whatfldSix)
            {
                Genclass.fieldone = whatfldone.Name;
                Genclass.fieldtwo = whatfldtwo.Name;
                Genclass.fieldthree = whatfldthree.Name;
                Genclass.fieldFour = whatfldFour.Name;
                Genclass.fieldFive = whatfldFive.Name;
                Genclass.Parent = whatfldSix;
                Genclass.frmwhat = whatform.Name;
            }
            public static void Partylistviewcont2(String name, String uid, String str1, String str2, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TextBox whatfldFour, Panel whatfldFive)
            {
                Genclass.fieldone = whatfldone.Name;
                Genclass.fieldtwo = whatfldtwo.Name;
                Genclass.fieldthree = whatfldthree.Name;
                Genclass.fieldFour = whatfldFour.Name;
                Genclass.Parent = whatfldFive;
                Genclass.frmwhat = whatform.Name;

            }
            public static void Partylistviewcont2A(String name, String uid, String str1, String str2, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TextBox whatfldFour, Panel whatfldFive)
            {
                Genclass.fieldone = whatfldone.Name;
                Genclass.fieldtwo = whatfldtwo.Name;
                Genclass.fieldthree = whatfldthree.Name;
                Genclass.fieldeight = whatfldFour.Name;
                Genclass.Parent = whatfldFive;
                Genclass.frmwhat = whatform.Name;
            }

            public static void Partylistviewcont3(String name, String uid, String str1, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, Panel whatfldFive)
            {
                Genclass.fieldone = whatfldone.Name;
                Genclass.fieldtwo = whatfldtwo.Name;
                Genclass.fieldthree = whatfldthree.Name;

                Genclass.Parent = whatfldFive;
                Genclass.frmwhat = whatform.Name;

            }
            public static void Partylistviewcont3A(String name, String uid, String str1, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, Panel whatfldFive)
            {
                fieldgrnone = whatfldone.Name;
                fieldgrntwo = whatfldtwo.Name;
                fieldgrnthree = whatfldthree.Name;
                Parent = whatfldFive;
                frmwhat = whatform.Name;
            }



            public static void Partylistviewcont3T(String name, String uid, String str1, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TabControl whatfldFive)
            {
                fieldone = whatfldone.Name;
                fieldtwo = whatfldtwo.Name;
                fieldthree = whatfldthree.Name;
                Parent = whatfldFive;
                frmwhat = whatform.Name;

            }


            public static void Partylistviewcont6(String name, String uid, String str1, String str2, String str3, String str, string str4, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TextBox whatfldFour, TextBox whatfldFive, TextBox whatfldsix, Panel whatfldSeven)
            {
                fieldone = whatfldone.Name;
                fieldtwo = whatfldtwo.Name;
                fieldthree = whatfldthree.Name;
                fieldFour = whatfldFour.Name;
                fieldFive = whatfldFive.Name;
                fieldSix = whatfldsix.Name;
                Parent = whatfldSeven;
                frmwhat = whatform.Name;
            }

            public static void Partylistviewcont7(String name, String uid, String str1, String str2, String str3, String str, string str4, string str5, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TextBox whatfldFour, TextBox whatfldFive, TextBox whatfldsix, TextBox whatfldseven, Panel whatfldeight)
            {
                fieldone = whatfldone.Name;
                fieldtwo = whatfldtwo.Name;
                fieldthree = whatfldthree.Name;
                fieldFour = whatfldFour.Name;
                fieldFive = whatfldFive.Name;
                fieldSix = whatfldsix.Name;
                fieldSeven = whatfldseven.Name;
                Parent = whatfldeight;
                frmwhat = whatform.Name;
            }
           
            public static void ClearTextBox(Form Frmname, Panel whatfldone)
            {
                Parent = whatfldone;
                foreach (Control ccontrol in Frmname.Controls)
                {
                    if (ccontrol is Panel)
                    {
                        foreach (Control c in Genclass.Parent.Controls)
                        {
                            if (c is TextBox || c is RichTextBox)
                            {
                                c.Text = string.Empty;
                            }
                        }
                    }
                }
            }

            public static void ClearTextBox(Form Frmname, GroupBox whatfldone)
            {
                Parent = whatfldone;
                foreach (Control ccontrol in Frmname.Controls)
                {
                    if (ccontrol is Panel)
                    {
                        foreach (Control c in Genclass.Parent.Controls)
                        {
                            if (c is TextBox || c is RichTextBox)
                            {
                                c.Text = "";
                            }
                        }
                    }
                }
            }

            public static void ScreenClosefrm(Form Frmname)
            {
                foreach (Control ccontrol in Frmname.Controls)
                {
                    Frmname.Close();
                }
            }

            public static void buttonstyleform(Form Frmname)
            {
                foreach (Control ct in Frmname.Controls)
                {
                    if (ct is Button)
                    {
                        ct.TabStop = false;
                        (ct as Button).FlatStyle = FlatStyle.Flat;
                        (ct as Button).FlatAppearance.BorderSize = 0;
                        (ct as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                    }
                }
            }

            public static void buttonstylepanel(Panel whatfldone)
            {
                Parent = whatfldone;
                foreach (Control bt in Parent.Controls)
                {
                    if (bt is Button)
                    {
                        bt.TabStop = false;
                        (bt as Button).FlatStyle = FlatStyle.Flat;
                        (bt as Button).FlatAppearance.BorderSize = 0;
                        (bt as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                    }
                }
            }

            public static void Gendocno()
            {
                SqlConnection conn = new SqlConnection("Data Source=" + data2 + "; Initial Catalog=" + data5 + ";User id=" + data3 + ";Password=" + Genclass.data4 + "");
                Genclass.strsql = "select Lastno,prfix from doctypem where doctypeId =" + Genclass.Dtype + " and companyid=" + Genclass.data1 + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                Genclass.ST = tap.Rows[0]["Lastno"].ToString();
                int EL = 0;
                int a = 0;
                EL = Genclass.ST.Length;
                int b = 5 - EL;
                if (EL < 5)
                {
                    for (a = 1; a < b; a++)
                    {
                        ST = "0" + ST;
                    }
                }
                Str5 = ST;
                ST = tap.Rows[0]["prfix"].ToString() + "/" + Genclass.ST;
            }
        }
    }
}
