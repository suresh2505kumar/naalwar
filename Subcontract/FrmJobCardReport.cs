﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmJobCardReport : Form
    {
        public FrmJobCardReport()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (CmbRptType.Text == "Jobcard Live")
                {
                    Genclass.Dtype = 12;
                    Genclass.RpeortId = 0;
                    Genclass.ReportType = "LIVE JOBCARD REPORT";
                }
                else if (CmbRptType.Text == "Jobcard Closed")
                {
                    Genclass.Dtype = 12;
                    Genclass.RpeortId = 1;
                    Genclass.ReportType = "CLOSED JOBCARD REPORT";
                }
                else if (CmbRptType.Text == "Fabric Roll Stock")
                {
                    Genclass.Dtype = 13;
                    Genclass.RpeortId = Convert.ToInt32(CmbShift.SelectedValue);
                    Genclass.ReortDate = Convert.ToDateTime(DtpToDate.Text);
                    Genclass.ReportType = "FABRIC STOCK LIST SUMMARY REPORT";
                    Genclass.parameter = "1";
                }
                else if (CmbRptType.Text == "Fabric Roll Stock Detial")
                {
                    Genclass.Dtype = 13;
                    Genclass.RpeortId = Convert.ToInt32(CmbShift.SelectedValue);
                    Genclass.ReortDate = Convert.ToDateTime(DtpToDate.Text);
                    Genclass.ReportType = "FABRIC STOCK LIST DETAIL REPORT";
                    Genclass.parameter = "2";
                }
                FrmReprtViwer frmReprtViwer = new FrmReprtViwer();
                frmReprtViwer.MdiParent = this.MdiParent;
                frmReprtViwer.StartPosition = FormStartPosition.CenterScreen;
                frmReprtViwer.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw ex;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadShift()
        {
            try
            {
                string Query = "Select Uid,GeneralName as ShiftName from GeneralM Where TypeM_Uid = 27";
                SqlDataAdapter da = new SqlDataAdapter(Query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                CmbShift.DataSource = null;
                CmbShift.DisplayMember = "ShiftName";
                CmbShift.ValueMember = "Uid";
                CmbShift.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CmbRptType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Jobcard Live
                //Jobcard Closed
                //Fabric Roll Stock
                //QC Fabric Roll Stock
                if (CmbRptType.Text == "Fabric Roll Stock")
                {
                    Panrpt.Visible = true;
                    label1.Visible = false;
                    label2.Text = "Date";
                    DtpFromDate.Visible = false;
                }
                else if(CmbRptType.Text == "Jobcard Live")
                {
                    Panrpt.Visible = false;
                }
                else if(CmbRptType.Text == "Jobcard Closed")
                {
                    Panrpt.Visible = false;
                }
                else if(CmbRptType.Text == "Fabric Roll Stock Detial")
                {
                    Panrpt.Visible = true;
                    label1.Visible = false;
                    label2.Text = "Date";
                    DtpFromDate.Visible = false;
                }
                else
                {
                    Panrpt.Visible = true;
                    label1.Visible = true;
                    label2.Text = "To Date";
                    DtpFromDate.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw ex;
            }
        }

        private void FrmJobCardReport_Load(object sender, EventArgs e)
        {
            LoadShift();
        }
    }
}
