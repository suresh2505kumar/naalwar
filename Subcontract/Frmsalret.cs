﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Naalwar
{
    public partial class Frmsalret : Form
    {
        public Frmsalret()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        //private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;
        ReportDocument doc = new ReportDocument();
        string uid = "";
        int mode = 0;
        //double dis9 = 0;
        double dis3 = 0;
        double dis4 = 0;
        double dd1 = 0;
        double dd2 = 0;
        //double dd3 = 0;
        double hg = 0;
        //private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        //private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        //private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        //private static Microsoft.Office.Interop.Excel.Application oXL;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void Frmsalret_Load(object sender, EventArgs e)
        {
            Genclass.Dtype = 200;
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            Editpan.Visible = false;
            Genpan.Visible = true;
            chkact.Checked = true;
            Titlep();
            Titlegst();
            dtpfnt.Value = DateTime.Now;
            this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            Loadgrid();
            TotalAmount();
        }
        private void Titlep()
        {
            HFIT.ColumnCount = 15;
            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "UoM";
            HFIT.Columns[2].Name = "Price";
            HFIT.Columns[3].Name = "Qty";
            HFIT.Columns[4].Name = "Value";
            HFIT.Columns[5].Name = "Pono";
            HFIT.Columns[6].Name = "Itemuid";
            if (txtpluid.Text == "" || txtpluid.Text == "24")
            {
                HFIT.Columns[7].Name = "CGST%";
                HFIT.Columns[8].Name = "CGST";
                HFIT.Columns[13].Name = "SGST%";
                HFIT.Columns[14].Name = "SGST";
            }
            else
            {
                HFIT.Columns[7].Name = "IGST%";
                HFIT.Columns[8].Name = "IGST";
            }
            HFIT.Columns[9].Name = "Invno";
            HFIT.Columns[10].Name = "Refuid";
            HFIT.Columns[11].Name = "Hsnuid";
            HFIT.Columns[12].Name = "Total";
            HFIT.Columns[0].Width = 450;
            HFIT.Columns[1].Width = 50;
            HFIT.Columns[2].Width = 80;
            HFIT.Columns[3].Width = 80;
            HFIT.Columns[4].Width = 100;
            HFIT.Columns[6].Visible = false;
            HFIT.Columns[5].Visible = false;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
        }
        private void loadput1()
        {
            conn.Open();
            txtprice.Text = string.Empty;
            if (Genclass.type == 5)
            {
                if (Genclass.data1 == 1)
                {
                    Genclass.strsql = "select g.Uid,g.itemname,g.itemcode,b.uid as listid ,b.pqty as invqty,'' as RetQty  from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=40 and  a.companyid=" + Genclass.data1 + "  left join   stransactionsplist e on  b.uid=e.refuid  and e.doctypeid=200   left join itemm g on b.ItemUId=g.Uid left join  tmpsalesrtn bb on b.Uid=bb.listid  where   a.companyid=" + Genclass.data1 + "     and a.docno='" + txtgen1.Text + "'  group by g.Uid,g.itemname,g.itemcode,b.uid,b.pqty    having isnull(b.PQty,0)-isnull(sum(e.PQty),0)>0  ";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }
                else
                {
                    Genclass.Module.Partylistviewcont2("uid", "Itemcode", "itemname", "listid", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, txtlisid, Editpan);
                    Genclass.strsql = "select  g.Uid,g.itemcode,g.itemname,b.uid as listid ,b.pqty as invqty,'' as RetQty from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=40 and  a.companyid=" + Genclass.data1 + "  left join   stransactionsplist e on  b.uid=e.refuid  and e.doctypeid=200   left join itemm g on b.ItemUId=g.Uid left join  tmpsalevenk bb on b.Uid=bb.listid  where   a.companyid=" + Genclass.data1 + "     and a.docno='" + txtgen1.Text + "'    group by g.Uid,g.itemname,g.itemcode,b.uid,b.pqty   having isnull(b.PQty,0)-isnull(sum(e.PQty),0)>0 ";
                    Genclass.FSSQLSortStr = "itemcode";
                    Genclass.FSSQLSortStr1 = "itemname";
                }
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                Frmbilllookup contc = new Frmbilllookup();
                TabControl tab = (TabControl)contc.Controls["tabC"];
                TabPage tab1 = (TabPage)tab.Controls["tabPage1"];
                DataGridView grid = (DataGridView)tab1.Controls["hfgp"];
                grid.Refresh();
                grid.ColumnCount = tap.Columns.Count;
                if (Genclass.data1 == 1)
                {
                    grid.Columns[0].Visible = false;
                    grid.Columns[1].Width = 350;
                    grid.Columns[2].Width = 250;
                    grid.Columns[3].Visible = false;
                    grid.Columns[4].Width = 100;
                    grid.Columns[5].Width = 100;
                }
                else
                {
                    grid.Columns[0].Visible = false;
                    grid.Columns[1].Width = 350;
                    grid.Columns[2].Width = 250;
                    grid.Columns[3].Visible = false;
                    grid.Columns[4].Width = 100;
                    grid.Columns[5].Width = 100;
                }
                grid.DefaultCellStyle.Font = new Font("Arial", 10);
                grid.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid.AutoGenerateColumns = false;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    grid.Columns[Genclass.i].Name = column.ColumnName;
                    grid.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                grid.DataSource = tap;
                TabPage tab4 = (TabPage)tab.Controls["tabPage4"];
                tab4.Visible = false;
                TabPage tab3 = (TabPage)tab.Controls["tabPage3"];
                tab3.Visible = false;
                TabPage tab2 = (TabPage)tab.Controls["tabPage2"];
                tab2.Visible = false;
                contc.StartPosition = FormStartPosition.CenterScreen;
                contc.MdiParent = this.MdiParent;
                contc.Show();
                conn.Close();
            }
        }

        private void loadput()
        {
            conn.Open();
            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont3("uid", "Name", "Address1", Genclass.strsql, this, txtpuid, txtname, txttempadd1, Editpan);
                Genclass.strsql = " select distinct   c.uid,c.Name as Party,Address1 from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join   stransactionsplist e on  b.uid=e.refuid and e.doctypeid=200 ";
                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont3("uid", "Docno", "Docdate", Genclass.strsql, this, txtdcid, txtgen1, txtdcdate, Editpan);
                Genclass.strsql = "select distinct a.uid,a.docno,a.docdate from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join   stransactionsplist e on  b.uid=e.refuid and e.doctypeid=200  where a.partyuid=" + txtpuid.Text + "";
                Genclass.FSSQLSortStr = "docno";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            contc.width = 750;
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 400;
            if (Genclass.type == 1 || Genclass.type == 7 || Genclass.type == 5)
            {
                dt.Columns[2].Width = 280;
            }
            dt.DefaultCellStyle.Font = new Font("Arial", 10);
            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            dt.DataSource = tap;
            contc.width = 750;
            contc.Show();
            conn.Close();
        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Invno";
                Genclass.FSSQLSortStr3 = "Invdate";
                Genclass.FSSQLSortStr4 = "Name";
                Genclass.FSSQLSortStr5 = "Basicvalue";
                Genclass.FSSQLSortStr6 = "Charges";
                Genclass.FSSQLSortStr7 = "Taxvalue";
                Genclass.FSSQLSortStr8 = "Netvalue";
                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                }
                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                }
                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                }
                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                }
                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                }
                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }
                }
                if (txtscr7.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    }
                }
                if (txtscr8.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                    }
                }

                if (txtscr9.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr8 + " like '%" + txtscr9.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr8 + " like '%" + txtscr9.Text + "%'";
                    }
                }
                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr7.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr8.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr9.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
                if (Genclass.Dtype == 200)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select distinct a.Uid,DocNo,convert(nvarchar,docdate,106) as  DocDate,Dcno as InvNo, convert(nvarchar,dcdate,106) as  Invdate ,b.Name,isnull(SUM(d.basicvalue),0)-isnull(d.disval,0) as Basicvalue,isnull(e.totalcharges,0) as Charges,case cgstid when 0 then igstval else cgstval end as Taxvalue,a.Netvalue, a.partyuid,a.placeuid,isnull(a.remarks,'') as remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join    stransactionsplist d on a.uid=d.transactionspuid left join STransactionsPCharges e on a.uid=e.STransactionsPUid  inner join  itemm g on d.ItemUId=g.uid left join generalm h on g.uom_uid=h.uid left join ItemGroup x on g.itemgroup_Uid=x.uid left join hsndet f  on x.hsnid=f.uid  left join generalm i on f.Sgid=i.uid  where  a.active=1 and a.doctypeid=200 and  a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by a.Uid,DocNo,docdate,Dcno,dcdate,b.Name,Netvalue,igstval,a.partyuid,a.placeuid,i.f1,a.remarks,e.totalcharges,disval,cgstid,cgstval ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "Select distinct a.Uid,DocNo,convert(nvarchar,docdate,106) as  DocDate,Dcno as InvNo, convert(nvarchar,dcdate,106) as  Invdate ,b.Name,isnull(SUM(d.basicvalue),0)-isnull(d.disval,0) as Basicvalue,isnull(e.totalcharges,0) as Charges,case cgstid when 0 then igstval else cgstval end as Taxvalue,a.Netvalue, a.partyuid,a.placeuid,isnull(a.remarks,'') as remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join    stransactionsplist d on a.uid=d.transactionspuid left join STransactionsPCharges e on a.uid=e.STransactionsPUid  inner join  itemm g on d.ItemUId=g.uid left join generalm h on g.uom_uid=h.uid left join ItemGroup x on g.itemgroup_Uid=x.uid left join hsndet f  on x.hsnid=f.uid  left join generalm i on f.Sgid=i.uid  where  a.active=1 and a.doctypeid=200 and  a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by a.Uid,DocNo,docdate,Dcno,dcdate,b.Name,Netvalue,igstval,a.partyuid,a.placeuid,i.f1,a.remarks,e.totalcharges,disval,cgstid,cgstval ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 89;
                HFGP.Columns[3].Width = 134;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Width = 441;
                HFGP.Columns[6].Width = 98;
                HFGP.Columns[7].Width = 60;
                HFGP.Columns[8].Width = 103;
                HFGP.Columns[9].Width = 112;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            txtitemname.Visible = true;
            txtprice.Visible = true;
            txtqty.Visible = true;
            txtbval.Visible = true;
            Editpan.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            Genclass.sum = 1;
            panadd.Visible = false;
        }

        private void txtname_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }

        private void txtname_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.type = 1;
            loadput();
        }

        private void txtpadd1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtgen1_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            if (txtname.Text != "")
            {
                Genclass.type = 2;
                loadput();
            }
            else
            {
                MessageBox.Show("Enter the Party name");
                txtname.Focus();
            }
        }

        private void txtgen1_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtitemname_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            if (txtgen1.Text != "")
            {
                Genclass.type = 5;
                loadput1();
            }
            else
            {
                MessageBox.Show("Enter the Invno");
                txtgen1.Focus();
            }
        }
        private void Titlegst()
        {
            if (txtpluid.Text != "")
            {
                if (txtpluid.Text == "24")
                {
                    HFGST.ColumnCount = 4;
                    HFGST.Columns[0].Name = "CGST%";
                    HFGST.Columns[1].Name = "CGST";
                    HFGST.Columns[2].Name = "SGST%";
                    HFGST.Columns[3].Name = "SGST";
                    HFGST.Columns[0].Width = 70;
                    HFGST.Columns[1].Width = 100;
                    HFGST.Columns[2].Width = 70;
                    HFGST.Columns[3].Width = 100;
                }
                else
                {
                    HFGST.ColumnCount = 2;
                    HFGST.Columns[0].Name = "IGST%";
                    HFGST.Columns[1].Name = "IGST";
                    HFGST.Columns[0].Width = 100;
                    HFGST.Columns[1].Width = 150;
                }
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            if (txtdcid.Text == "")
            {
                txtdcid.Text = "0";
            }
            if (Genclass.ty == 1)
            {
                if (Genclass.data1 == 1)
                {
                    Genclass.strsql = "select * from tmpsalesrtn ";
                }
                else
                {
                    Genclass.strsql = "select * from tmpsalevenk  ";
                }
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                Genclass.sum4 = 0;
                for (int i = 0; i < tap1.Rows.Count; i++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["uom"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["price"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["invqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["value"].ToString();
                    txtbval.Text = tap1.Rows[i]["value"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["itemid"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["listid"].ToString();
                    txttgstp.Text = tap1.Rows[i]["gstper"].ToString();
                    HFIT.Rows[index].Cells[7].Value = txttgstp.Text;
                    double srty = (Convert.ToDouble(HFIT.Rows[index].Cells[4].Value) * Convert.ToDouble(HFIT.Rows[index].Cells[7].Value)) / 100;
                    HFIT.Rows[index].Cells[9].Value = srty;
                    HFIT.Rows[index].Cells[13].Value = Convert.ToDouble(tap1.Rows[i]["gstper"].ToString()) / 2;
                    HFIT.Rows[index].Cells[14].Value = Convert.ToDouble(HFIT.Rows[index].Cells[9].Value) / 2;
                    Titlegst();
                    if (txtpluid.Text == "24")
                    {
                        int boo = 1;
                        if (HFGST.Rows.Count - 1 == 0)
                        {
                            var index1 = HFGST.Rows.Add();
                            HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                            HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                            HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                            HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                        }
                        else
                        {
                            for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                            {
                                if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                {
                                    double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                    HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");
                                    HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                    boo = 1;
                                    break;
                                }
                                else
                                {
                                    boo = 2;
                                }
                            }
                        }
                        if (boo == 2)
                        {
                            var index1 = HFGST.Rows.Add();
                            HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                            HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                            HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                            HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                        }
                        if (txtigval.Text == "")
                        {
                            txtigval.Text = "0";
                        }
                        double df = Convert.ToDouble(txtigval.Text) + (dis3 * 2);
                        txtigval.Text = df.ToString("0.00");
                    }
                    else
                    {
                        var index1 = HFGST.Rows.Add();
                        HFGST.Rows[index1].Cells[0].Value = HFIT.Rows[0].Cells[7].Value;
                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                        if (txtigval.Text == "")
                        {
                            txtigval.Text = "0";
                        }
                        double df = Convert.ToDouble(txtigval.Text) + dis3;
                        txtigval.Text = df.ToString();
                    }
                }
            }

            else
            {
                if (txtqty.Text == "" || txtqty.Text == "0" || txtprice.Text == "" || txtprice.Text == "0" || txtitemname.Text == "" || txtitemname.Text == "0" || txtname.Text == "" || txtname.Text == "0" || txttitemid.Text == "0")
                {
                    MessageBox.Show("Enter the Party or Item or Rate or Qty to Proceed");
                    return;
                }
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = txtitemname.Text;
                HFIT.Rows[index].Cells[1].Value = txtuom.Text;
                HFIT.Rows[index].Cells[2].Value = txtprice.Text;
                HFIT.Rows[index].Cells[3].Value = txtqty.Text;
                HFIT.Rows[index].Cells[4].Value = txtbval.Text;
                Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value);
                Txttot.Text = Genclass.sum4.ToString();
                HFIT.Rows[index].Cells[5].Value = txtgen1.Text;
                HFIT.Rows[index].Cells[6].Value = txttitemid.Text;
                HFIT.Rows[index].Cells[7].Value = txttgstp.Text;
                HFIT.Rows[index].Cells[8].Value = txttgstval.Text;
                HFIT.Rows[index].Cells[10].Value = txtdcid.Text;
                HFIT.Rows[index].Cells[13].Value = txtgen1.Text;
                HFIT.Rows[index].Cells[14].Value = dcdate.Text;
                Titlegst();
                if (txtpluid.Text == "24")
                {
                    int boo = 1;
                    if (HFGST.Rows.Count - 1 == 0)
                    {
                        var index1 = HFGST.Rows.Add();
                        HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                        HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                        HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                    }
                    else
                    {
                        for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                        {
                            if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                            {
                                double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");
                                HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                boo = 1;
                                break;
                            }
                            else
                            {
                                boo = 2;
                            }
                        }
                    }

                    if (boo == 2)
                    {
                        var index1 = HFGST.Rows.Add();
                        HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                        HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                        HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                    }
                    if (txtigval.Text == "")
                    {
                        txtigval.Text = "0";
                    }
                    double df = Convert.ToDouble(txtigval.Text) + (dis3 * 2);
                    txtigval.Text = df.ToString("0.00");
                }
                else
                {
                    var index1 = HFGST.Rows.Add();
                    HFGST.Rows[index1].Cells[0].Value = HFIT.Rows[0].Cells[7].Value;
                    HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                    if (txtigval.Text == "")
                    {
                        txtigval.Text = "0";
                    }
                    double df = Convert.ToDouble(txtigval.Text) + dis3;
                    txtigval.Text = df.ToString();
                }
            }

            for (int m = 0; m < HFIT.RowCount - 1; m++)
            {
                Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[m].Cells[4].Value);
                Txttot.Text = Genclass.sum4.ToString();
            }
            txttbval.Text = Txttot.Text;
            splittax();
            txtitemname.Text = "";
            txtuom.Text = "";
            txtprice.Text = "";
            txtqty.Text = "";
            txtbval.Text = "";
            txtnotes.Text = "";
            txttitemid.Text = "";
            txtdcid.Text = "";
            conn.Close();
            conn.Open();
            if (Genclass.data1 == 1)
            {
                qur.CommandText = "delete from tmpsalesrtn";
                qur.ExecuteNonQuery();
            }
            else
            {
                qur.CommandText = "delete from tmpsalevenk";
                qur.ExecuteNonQuery();
            }
            conn.Close();
        }

        private void splittax()
        {
            if (txttdis.Text == "" || txttdis.Text == null || txttdis.Text == "0" || txtpuid.Text == "")
            {
                txttprdval.Text = txttbval.Text;
            }
            else
            {
                if (txttbval.Text != "")
                {
                    double dis6 = Convert.ToDouble(txttbval.Text) / 100 * Convert.ToDouble(txttdis.Text);
                    txttdisc.Text = dis6.ToString("0.00");
                    dis4 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                    txttprdval.Text = dis4.ToString("#,0.00");
                }
            }
            if (txtcharges.Text == "" || txtcharges.Text == null)
            {
                txtexcise.Text = txttprdval.Text;
            }
            else
            {
                dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                txtexcise.Text = dis4.ToString("#,0.00");
            }
            if (txtpuid.Text == "")
            {
                return;
            }
            Genclass.strsql = "select generalname from partym a inner join generalm b on a.stateuid=b.uid where a.companyid=" + Genclass.data1 + " and a.uid=" + txtpuid.Text + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (txtexcise.Text != "" && txttgstp.Text != "")
            {
                if (tap1.Rows[0]["generalname"].ToString() == "Tamil Nadu 33")
                {
                    dis4 = Convert.ToDouble(txttgstp.Text) / 2;
                    txttcgstp.Text = dis4.ToString("0.00");
                    txtsgstp.Text = dis4.ToString("0.00");
                    if (Genclass.ty == 1)
                    {
                        dis3 = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));
                        txttgstval.Text = dis3.ToString("0.00");
                        dis3 = dis3 / 2;
                        txttcgval.Text = dis3.ToString("0.00");
                        txttsgval.Text = dis3.ToString("0.00");
                        if (txtigval.Text != "")
                        {
                            double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                        else
                        {
                            double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                    }
                    else
                    {

                        if (HFIT.CurrentRow.Cells[13].Value.ToString() == "" || HFIT.CurrentRow.Cells[13].Value == null)
                        {
                            dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));
                            txttgstval.Text = dis3.ToString("0.00");
                            dis3 = dis3 / 2;
                            txttcgval.Text = dis3.ToString("0.00");
                            txttsgval.Text = dis3.ToString("0.00");
                            if (txtigval.Text != "")
                            {
                                double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                txtttot.Text = dis5.ToString("0.00");
                            }
                            else
                            {
                                double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                txtttot.Text = dis5.ToString("0.00");
                            }
                        }
                    }
                }
            }
            else
            {
                if (txtexcise.Text != "")
                {
                    txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    dis4 = Convert.ToDouble(HFIT.Rows[0].Cells[7].Value.ToString());
                    txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();
                    txtigstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));
                    txttgstval.Text = dis3.ToString("0.00");
                    txtigval.Text = dis3.ToString("0.00");
                    if (txtigval.Text != "")
                    {
                        double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3;
                        txtttot.Text = dis5.ToString("0.00");
                    }
                    else
                    {
                        double dis5 = Convert.ToDouble(txtexcise.Text) + dis3;
                        txtttot.Text = dis5.ToString("0.00");
                    }
                }
            }
            Double net1 = Convert.ToDouble(txtttot.Text);
            TxtNetAmt.Text = net1.ToString("0.00");
            double someInt = (int)net1;

            double rof = Math.Round(net1 - someInt, 2);
            TxtRoff.Text = rof.ToString("0.00");
            if (Convert.ToDouble(TxtRoff.Text) < 0.49)
            {
                Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                TxtRoff.Text = rof1.ToString("0.00");
            }
            else
            {
                Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                TxtRoff.Text = rof2.ToString("0.00");
            }
            Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
            TxtNetAmt.Text = net.ToString("0.00");
        }
        private void txtpuid_TextChanged(object sender, EventArgs e)
        {
            if (txtpuid.Text != "")
            {
                Genclass.strsql = "Select address1, address2 ,city,stateuid  from partym where uid=" + txtpuid.Text + " and companyid=" + Genclass.data1 + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                if (tap1.Rows.Count > 0)
                {
                    txtpadd1.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    txtpluid.Text = txtpuid.Text;
                    txtpluid.Text = tap1.Rows[0]["stateuid"].ToString();
                }
            }
        }

        private void txtpluid_TextChanged(object sender, EventArgs e)
        {
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            btnsave.Visible = true;
            buttnfinbk.Visible = true;
            txtexcise.Text = Txttot.Text;
            conn.Close();
            conn.Open();
            Txttot.Text = "0";
            double ee1 = 0;
            if (HFIT.Rows[0].Cells[7].Value != null && HFIT.Rows[0].Cells[8].Value != null)
            {
                HFIT.CurrentRow.Cells[4].Value = Convert.ToDouble(HFIT.CurrentRow.Cells[2].Value) * Convert.ToDouble(HFIT.CurrentRow.Cells[3].Value);
                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    ee1 = ee1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                }
                Txttot.Text = ee1.ToString();
                txtexcise.Text = Txttot.Text;
                txttbval.Text = Txttot.Text;
                splittax();
            }
            conn.Close();
        }

        private void txttdis_TextChanged(object sender, EventArgs e)
        {
            if (txttdis.Text != "")
            {
                if (txttdis.Text == "" || txttdis.Text == "0")
                {
                    txttdisc.Text = "0";
                }
                double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                txttdisc.Text = dis.ToString();
                double yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = yt7.ToString();
                if (txtcharges.Text == "" || txtcharges.Text == null)
                {
                    txtexcise.Text = txttprdval.Text;
                }
                else
                {
                    dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                    txtexcise.Text = dis4.ToString("0.00");
                }
                Genclass.sum5 = 0;
                hg = 0;
                for (int k = 0; k < HFGST.RowCount - 1; k++)
                {
                    Genclass.sum5 = (Convert.ToDouble(txtexcise.Text) / 100) * Convert.ToDouble(HFGST.Rows[k].Cells[0].Value);
                    HFGST.Rows[k].Cells[1].Value = Genclass.sum5.ToString("0.00");
                    HFGST.Rows[k].Cells[3].Value = Genclass.sum5.ToString("0.00");
                    hg = hg + (Genclass.sum5 * 2);
                }
                txtigval.Text = hg.ToString("0.00");
                double tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                txtttot.Text = tot.ToString("0.00");
                Double net1 = Convert.ToDouble(txtttot.Text);
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;
                double rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");
                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }
                Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                TxtNetAmt.Text = net.ToString("0.00");
            }
        }

        private void txtcharges_TextChanged(object sender, EventArgs e)
        {
            if (txtcharges.Text != "")
            {
                if (txtcharges.Text == "" || txtcharges.Text == "0")
                {
                    txtcharges.Text = "0";
                }
                double yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = yt7.ToString();

                if (txtcharges.Text == "" || txtcharges.Text == null)
                {
                    txtexcise.Text = txttprdval.Text;
                }
                else
                {
                    dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                    txtexcise.Text = dis4.ToString("0.00");
                }
                Genclass.sum5 = 0;
                hg = 0;
                for (int k = 0; k < HFGST.RowCount - 1; k++)
                {

                    Genclass.sum5 = (Convert.ToDouble(txtexcise.Text) / 100) * Convert.ToDouble(HFGST.Rows[k].Cells[0].Value);
                    HFGST.Rows[k].Cells[1].Value = Genclass.sum5.ToString("0.00");
                    HFGST.Rows[k].Cells[3].Value = Genclass.sum5.ToString("0.00");
                    hg = hg + (Genclass.sum5 * 2);
                }
                txtigval.Text = hg.ToString("0.00");
                double tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                txtttot.Text = tot.ToString("0.00");
                Double net1 = Convert.ToDouble(txtttot.Text);
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;
                double rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");
                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }
                Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                TxtNetAmt.Text = net.ToString("0.00");
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;
            Genclass.sum5 = 0;
            TxtNetAmt.Text = Txttot.Text;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
                return;
            }
            if (Genclass.ty != 1)
            {
                if (txtgen1.Text == "")
                {
                    MessageBox.Show("Enter the Invoiceno");
                    txtname.Focus();
                    return;
                }
            }
            conn.Open();
            Genclass.Dtype = 200;
            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
                txttdisc.Text = "0";
            }
            if (mode == 2)
            {

                qur.CommandText = "delete from Stransactionsplist where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
            }
            txtexcise.Text = txtexcise.Text.Replace(",", "");
            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                if (mode == 1)
                {
                    if (i == 0)
                    {
                        qur.CommandText = "insert into Stransactionsp values(" + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtgen1.Text + "','" + dcdate.Text + "'," + txtpuid.Text + ",'" + txtrem.Text + "',1," + Genclass.data1 + "," + TxtNetAmt.Text + "," + Genclass.Yearid + "," + TxtRoff.Text + ",'" + Dtppre.Value + "','" + Dtprem.Value + "','" + txttrans.Text + "','" + txtlisid.Text + "'," + txtpluid.Text + ",0)";
                        qur.ExecuteNonQuery();
                    }
                    string quy = "select uid from STransactionsP where doctypeid=" + Genclass.Dtype + "  and  docno='" + txtgrn.Text + "' and companyid=" + Genclass.data1 + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    txtgrnid.Text = tap.Rows[0]["uid"].ToString();
                    string quyt = "select isnull(c.sname,0) as f1,z.generalname as uom from itemm b  inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid where b.uid=" + HFIT.Rows[i].Cells[6].Value + "";
                    Genclass.cmd = new SqlCommand(quyt, conn);
                    SqlDataAdapter aptrt = new SqlDataAdapter(Genclass.cmd);
                    DataTable tapt = new DataTable();
                    aptrt.Fill(tapt);
                    if (tapt.Rows.Count > 0)
                    {
                        if (txtpluid.Text == "24")
                        {
                            dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString()) / 2;
                            txttcgstp.Text = dd1.ToString("0.00");
                            dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString())) / 2;
                            txttcgval.Text = dd2.ToString("0.00");
                            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[4].Value + ", " + txtexcise.Text + " , " + HFIT.Rows[i].Cells[10].Value + "," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,  " + txttcgstp.Text + ", " + txttcgval.Text + ", " + txttcgstp.Text + ", " + txttcgval.Text + ",0,0," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                            qur.ExecuteNonQuery();
                        }
                        else
                        {
                            dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString());
                            txttcgstp.Text = dd1.ToString("0.00");
                            dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()));
                            txttcgval.Text = dd2.ToString("0.00");
                            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtexcise.Text + " , " + HFIT.Rows[i].Cells[10].Value + "," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,0, 0 ,0, 0,  " + txttcgstp.Text + ", " + txttcgval.Text + "," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                            qur.ExecuteNonQuery();
                        }
                    }
                    if (mode == 2)
                    {
                        qur.CommandText = "update Stransactionsp set docdate='" + DTPDOCDT.Value + "',DcNo='" + txtgen1.Text + "',dcdate='" + dcdate.Value + "',PartyUid=" + txtpuid.Text + ",remarks='" + txtrem.Text + "',active=1,companyid=" + Genclass.data1 + ",netvalue=" + TxtNetAmt.Text + ",placeuid=" + txtpluid.Text + ",transp='" + txttrans.Text + "',vehno='" + txtlisid.Text + "',dtpre='" + Dtppre.Value + "',dtrem='" + Dtprem.Text + "',roff=" + TxtRoff.Text + " where UId=" + uid + "";
                        qur.ExecuteNonQuery();
                        string quyq = "select isnull(c.sname,0) as f1,z.generalname as uom from itemm b  inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid where b.uid=" + HFIT.Rows[i].Cells[6].Value + "";
                        Genclass.cmd = new SqlCommand(quyq, conn);
                        SqlDataAdapter aptrq = new SqlDataAdapter(Genclass.cmd);
                        DataTable tapq = new DataTable();
                        aptrt.Fill(tapt);
                        if (tapq.Rows.Count > 0)
                        {
                            if (txtpluid.Text == "24")
                            {
                                dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2;

                                txttcgstp.Text = dd1.ToString("0.00");
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString())) / 2;
                                txttcgval.Text = dd2.ToString("0.00");
                                qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtexcise.Text + " , " + HFIT.Rows[i].Cells[10].Value + "," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,  " + txttcgstp.Text + ", " + txttcgval.Text + ", " + txttcgstp.Text + ", " + txttcgval.Text + ",0,0," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                                qur.ExecuteNonQuery();
                            }
                            else
                            {
                                dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString());
                                txttcgstp.Text = dd1.ToString("0.00");
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()));
                                txttcgval.Text = dd2.ToString("0.00");
                                qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtexcise.Text + " , " + HFIT.Rows[i].Cells[10].Value + "," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,0, 0 ,0, 0,  " + txttcgstp.Text + ", " + txttcgval.Text + "," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                                qur.ExecuteNonQuery();
                            }
                        }
                    }
                }
                if (txttdis.Text == "" || txttdis.Text == null)
                {
                    txttdis.Text = "0";
                    txttdisc.Text = "0";
                }
                if (txtcharges.Text != "")
                {
                    qur.CommandText = "insert into Stransactionspcharges  values ( " + txtgrnid.Text + "," + Genclass.Dtype + ",13," + txtcharges.Text + "," + txtcharges.Text + ",'0'," + Genclass.Yearid + ")";
                    qur.ExecuteNonQuery();
                }
                if (mode == 1)
                {
                    qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + "";
                    qur.ExecuteNonQuery();
                }
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                conn.Close();
                Loadgrid();
                Editpan.Visible = false;
                panadd.Visible = true;
                Genpan.Visible = true;
                Genclass.sum5 = 0;
            }
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }
            mode = 2;
            HFIT.Columns[0].ReadOnly = true;
            HFIT.Columns[1].ReadOnly = true;
            HFIT.Columns[5].ReadOnly = true;
            HFIT.Columns[6].ReadOnly = true;
            HFIT.Columns[7].ReadOnly = true;
            HFIT.Columns[8].ReadOnly = true;
            HFIT.Columns[9].ReadOnly = true;
            HFIT.Columns[10].ReadOnly = true;
            HFIT.Columns[11].ReadOnly = true;
            HFIT.Columns[12].ReadOnly = true;
            HFIT.Columns[12].ReadOnly = true;
            HFIT.Columns[13].ReadOnly = true;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpan.Visible = true;
            Genclass.sum5 = 0;
            txtitemname.Visible = true;
            txtprice.Visible = true;
            txtqty.Visible = true;
            txtbval.Visible = true;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            dcdate.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtgen1.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtrem.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            conn.Open();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            Genclass.strsql = "select distinct b.uid,c.ItemName,d.generalname as uom,b.PRate,b.pqty,b.BasicValue,b.ItemUId, b.refuid,b.disp,b.disval,b.Taxableval,b.igstid,b.igstval,b.totvalue,b.addnotes,a.transp from stransactionsp a inner join Stransactionsplist b   on a.uid=b.transactionspuid  left join ItemM c on b.ItemUId=c.uid left join generalm d on c.uom_uid=d.uid   where b.transactionspuid=" + uid + "   group by b.uid,c.ItemName,generalname,b.PRate,b.pqty,  b.BasicValue,b.ItemUId,b.refuid,b.disp,b.disval,b.Taxableval,b.igstid,b.igstval,b.totvalue,b.addnotes,a.transp";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            Genclass.sum1 = 0;
            Txttot.Text = "";
            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                double sump = Convert.ToDouble(tap1.Rows[k]["PRate"].ToString());
                HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                double sumq = Convert.ToDouble(tap1.Rows[k]["pqty"].ToString());
                HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                double sumb = Convert.ToDouble(tap1.Rows[k]["BasicValue"].ToString());
                HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");
                txtbval.Text = sumb.ToString("0.00");
                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["addnotes"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["ItemUId"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["igstid"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["igstval"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["transp"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["refuid"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["totvalue"].ToString();
                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value.ToString());
                Txttot.Text = Genclass.sum1.ToString();
            }
            Txttot.Text = Genclass.sum1.ToString();
            if (tap1.Rows.Count > 0)
            {
                txttbval.Text = Txttot.Text;
                txttdis.Text = tap1.Rows[0]["disp"].ToString();
                txttdisc.Text = tap1.Rows[0]["disval"].ToString();
            }
            Titlep();
            Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            if (tap2.Rows.Count > 0)
            {
                txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();
            }
            conn.Close();
            TxtNetAmt.Text = HFGP.CurrentRow.Cells[9].Value.ToString();
        }

        private void HFGST_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        private void TotalAmount()
        {
            conn.Close();
            conn.Open();
            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
            txttotamt.Text = "0";
            txtbasicval.Text = "0";
            txttax.Text = "0";
            Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=1 and a.doctypeid=200 and a.companyid=" + Genclass.data1 + "   and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            txttotamt.Text = tap2.Rows[0]["totamt"].ToString();
            Genclass.strsql = " select SUM(Basicvalue) as Basicvalue from(select Basicvalue-sum(disval) as Basicvalue from (Select  isnull(SUM(d.basicvalue),0) as Basicvalue,transactionspuid from stransactionsp a  left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=200 and a.companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by transactionspuid) aa inner join (select distinct disval,transactionspuid from  stransactionsplist where  doctypeid=100)bb on  aa.transactionspuid=bb.transactionspuid group by Basicvalue) tab ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            txtbasicval.Text = tap3.Rows[0]["Basicvalue"].ToString();
            Genclass.strsql = "select sum(igstval) as Taxval from (select distinct igstval from stransactionsplist a inner join stransactionsp b on a.transactionspuid=b.uid where b.active=1 and a.doctypeid=200 and companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + ") tab ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap4 = new DataTable();
            aptr4.Fill(tap4);
            txttax.Text = tap4.Rows[0]["Taxval"].ToString();
            Genclass.strsql = "Select  isnull(SUM(e.totalcharges),0) as charges from stransactionsp a   left join  STransactionsPCharges e on a.uid=e.STransactionsPUid   where a.active=1 and a.doctypeid=200 and a.companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap5 = new DataTable();
            aptr5.Fill(tap5);
            if (tap5.Rows.Count > 0)
            {
                txtchargessum.Text = tap5.Rows[0]["charges"].ToString();
            }
            else
            {
                txtchargessum.Text = "0.00";
            }
            conn.Close();
        }

        private void dtpfnt_ValueChanged(object sender, EventArgs e)
        {

            Loadgrid();
            TotalAmount();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcid_TextChanged(object sender, EventArgs e)
        {
            if (txtdcid.Text != "")
            {
                Genclass.strsql = "select distinct a.uid,a.docno,Convert(nvarchar,a.docdate,106) as docdate from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join   stransactionsplist e on  b.uid=e.refuid and e.doctypeid=200  where a.partyuid=" + txtpuid.Text + " and a.uid=" + txtdcid.Text + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count > 0)
                {
                    txtdcdate.Text = tap2.Rows[0]["docdate"].ToString();
                }
            }
        }

        private void Editpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}