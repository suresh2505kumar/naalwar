﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmyarnIssue : Form
    {
        public FrmyarnIssue()
        {
            InitializeComponent();
            txtSupplerName.GotFocus += TxtSupplerName_GotFocus;
        }

        private void TxtSupplerName_GotFocus(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getParty();
                bsParty.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = FindLocation(txtSupplerName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Party Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public int FillId = 0;
        public int Fillid = 0;
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource bsParty = new BindingSource();
        BindingSource bsItem = new BindingSource();
        BindingSource bsBeam = new BindingSource();
        BindingSource bsIssue = new BindingSource();
        BindingSource bsPkSlip = new BindingSource();
        BindingSource bsDispatchTo = new BindingSource();
        public int SelectId = 0;
        int DocTypeId = 0;

        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnPrint.Visible = true;
                    chckActive.Visible = true;
                    btnFstBack.Visible = true;
                    btnBack.Visible = true;
                    btnNext.Visible = true;
                    btnFstNext.Visible = true;
                    panNumbers.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnsave.Text = "Save";
                    btnPackingSlipPrint.Visible = true;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    chckActive.Visible = false;
                    btnFstBack.Visible = false;
                    btnBack.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    panNumbers.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnPackingSlipPrint.Visible = false;
                    btnsave.Text = "Save";
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    chckActive.Visible = false;
                    btnFstBack.Visible = false;
                    btnBack.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    panNumbers.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnPackingSlipPrint.Visible = false;
                    btnsave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void FrmyarnIssue_Load(object sender, EventArgs e)
        {
            this.BringToFront();
            this.Focus();
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(FrmyarnIssue_KeyDown);
            DocTypeId = 210;

            Genclass.Dtype = 210;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            DataTable dt = new DataTable();
            LoadDataGridItem(dt);
            LoadTax();
            LoadTaxGrid();
            //LoadReceivedQyrtGrid();
            getJoborderIssue();
            LoadBeam();
            LoadButtons(0);
        }
        protected void LoadTax()
        {
            try
            {
                DataTable dt = getTax();
                cboTax.DataSource = null;
                cboTax.DisplayMember = "GeneralName";
                cboTax.ValueMember = "TaxPer";
                cboTax.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtGridSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grSearch.Visible = false;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtSupplerName_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getParty();
                bsParty.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = FindLocation(txtSupplerName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Party Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.Columns[1].Width = 260;
                    DataGridCommon.DataSource = bsParty;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "ItemCode";
                    DataGridCommon.Columns[1].Width = 260;
                    DataGridCommon.DataSource = bsItem;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[2].Name = "Weight";
                    DataGridCommon.Columns[2].HeaderText = "Weight";
                    DataGridCommon.Columns[2].DataPropertyName = "BWght";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.DataSource = bsBeam;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 4)
                {
                    Fillid = 4;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "PkSId";
                    DataGridCommon.Columns[1].Name = "PkSlipNo";
                    DataGridCommon.Columns[1].HeaderText = "Pk SlipNo";
                    DataGridCommon.Columns[1].DataPropertyName = "PkSlipNo";
                    DataGridCommon.Columns[2].Name = "PkSlipDate";
                    DataGridCommon.Columns[2].HeaderText = "PkSlipDate";
                    DataGridCommon.Columns[2].DataPropertyName = "PkSlipDate";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.DataSource = bsPkSlip;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 5)
                {
                    Fillid = 5;
                    bsDispatchTo.DataSource = dt;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";

                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsDispatchTo;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected DataTable getTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetPendingPackingSlip", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtItem_MouseClick(object sender, MouseEventArgs e)
        {

        }

        protected void LoadDataGridItem(DataTable dt)
        {
            try
            {
                DataGridItem.DataSource = null;
                DataGridItem.AutoGenerateColumns = false;
                DataGridItem.ColumnCount = 18;

                DataGridItem.Columns[0].Name = "YarnName";
                DataGridItem.Columns[0].HeaderText = "Yarn Name";
                DataGridItem.Columns[0].DataPropertyName = "YarnName";
                DataGridItem.Columns[0].Width = 150;

                DataGridItem.Columns[1].Name = "MillName";
                DataGridItem.Columns[1].HeaderText = "Mill Name";
                DataGridItem.Columns[1].DataPropertyName = "MillName";
                DataGridItem.Columns[1].Width = 150;

                DataGridItem.Columns[2].Name = "HSNCode";
                DataGridItem.Columns[2].HeaderText = "HSNCode";
                DataGridItem.Columns[2].DataPropertyName = "HSNCode";
                DataGridItem.Columns[2].Width = 70;

                DataGridItem.Columns[3].Name = "Weight";
                DataGridItem.Columns[3].HeaderText = "Weight";
                DataGridItem.Columns[3].DataPropertyName = "TotalWeight";
                DataGridItem.Columns[3].Width = 80;

                DataGridItem.Columns[4].Name = "NoofBags";
                DataGridItem.Columns[4].HeaderText = "NoofBags";
                DataGridItem.Columns[4].DataPropertyName = "NoOfBags";
                DataGridItem.Columns[4].Width = 80;

                DataGridItem.Columns[5].Name = "PackType";
                DataGridItem.Columns[5].HeaderText = "Pack Type";
                DataGridItem.Columns[5].DataPropertyName = "PackType";
                DataGridItem.Columns[5].Width = 80;

                DataGridItem.Columns[6].Name = "Price";
                DataGridItem.Columns[6].HeaderText = "Price";
                DataGridItem.Columns[6].DataPropertyName = "Price";
                DataGridItem.Columns[6].Width = 80;
                DataGridItem.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridItem.Columns[7].Name = "BasicValue";
                DataGridItem.Columns[7].HeaderText = "BasicValue";
                DataGridItem.Columns[7].DataPropertyName = "BasicValue";
                DataGridItem.Columns[7].Width = 100;
                DataGridItem.Columns[7].DefaultCellStyle.Format = "N2";
                DataGridItem.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridItem.Columns[8].Name = "GSTPercentage";
                DataGridItem.Columns[8].HeaderText = "GST%";
                DataGridItem.Columns[8].DataPropertyName = "Tax";
                DataGridItem.Columns[8].Width = 70;

                DataGridItem.Columns[9].Name = "SGST";
                DataGridItem.Columns[9].HeaderText = "SGST";
                DataGridItem.Columns[9].DataPropertyName = "SGST";
                DataGridItem.Columns[9].Width = 80;
                DataGridItem.Columns[9].DefaultCellStyle.Format = "N2";
                DataGridItem.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridItem.Columns[9].Visible = false;

                DataGridItem.Columns[10].Name = "CGST";
                DataGridItem.Columns[10].HeaderText = "CGST";
                DataGridItem.Columns[10].DataPropertyName = "CGST";
                DataGridItem.Columns[10].Width = 80;
                DataGridItem.Columns[10].DefaultCellStyle.Format = "N2";
                DataGridItem.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridItem.Columns[10].Visible = false;

                DataGridItem.Columns[11].Name = "IGST";
                DataGridItem.Columns[11].HeaderText = "IGST";
                DataGridItem.Columns[11].DataPropertyName = "IGST";
                DataGridItem.Columns[11].Width = 80;
                DataGridItem.Columns[11].DefaultCellStyle.Format = "N2";
                DataGridItem.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridItem.Columns[11].Visible = false;

                DataGridItem.Columns[12].Name = "TaxValue";
                DataGridItem.Columns[12].HeaderText = "TaxValue";
                DataGridItem.Columns[12].DataPropertyName = "TaxValue";
                DataGridItem.Columns[12].Width = 80;
                DataGridItem.Columns[12].DefaultCellStyle.Format = "N2";
                DataGridItem.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridItem.Columns[13].Name = "NetValue";
                DataGridItem.Columns[13].HeaderText = "NetValue";
                DataGridItem.Columns[13].DataPropertyName = "NetValue";
                DataGridItem.Columns[13].DefaultCellStyle.Format = "N2";
                DataGridItem.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridItem.Columns[14].Name = "ID";
                DataGridItem.Columns[14].HeaderText = "ID";
                DataGridItem.Columns[14].DataPropertyName = "PkSId";
                DataGridItem.Columns[14].Visible = false;

                DataGridItem.Columns[15].Name = "MillUid";
                DataGridItem.Columns[15].HeaderText = "MillUid";
                DataGridItem.Columns[15].DataPropertyName = "MillUid";
                DataGridItem.Columns[15].Visible = false;

                DataGridItem.Columns[16].Name = "UOM";
                DataGridItem.Columns[16].HeaderText = "UOM";
                DataGridItem.Columns[16].DataPropertyName = "UOM";
                DataGridItem.Columns[16].Visible = false;

                DataGridItem.Columns[17].Name = "ItemUid";
                DataGridItem.Columns[17].HeaderText = "ItemUid";
                DataGridItem.Columns[17].DataPropertyName = "ItemUid";
                DataGridItem.Columns[17].Visible = false;
                DataGridItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Genclass.Module.Gendocno();
                txtGrn.Text = Genclass.ST;
                Genclass.Module.ClearTextBox(this, grBack);
                DataGridItemReceived.Rows.Clear();
                DataGridItem.DataSource = null;
                DataGridBeem.Rows.Clear();
                btnsave.Text = "Save";
                LoadButtons(1);
                TxtpackingSlipNumber.Text = "";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, "Select PrFix from DocTypeM Where DocTypeId = 210", conn);
                txtGrn.Text = dt.Rows[0]["PrFix"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (Fillid == 1)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtSupplerName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtSupplerName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    string PkSlipNo = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (Fillid == 2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtoutputitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtoutputitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (Fillid == 3)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (DataGridItem.Rows.Count > 1)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridBeem.Rows[0].Clone();
                        row.Cells[0].Value = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        row.Cells[1].Value = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        row.Cells[2].Value = txtGrn.Text;
                        DataGridBeem.Rows.Add(row);
                        grSearch.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show("Select Item", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    SelectId = 0;
                }
                else if (Fillid == 4)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    TxtpackingSlipNumber.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtpackingSlipNumber.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    SqlParameter[] parameters = { new SqlParameter("@PkSlipUid", TxtpackingSlipNumber.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPkSlipdetForIssue", parameters, conn);
                    LoadDataGridItem(dt);
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (Fillid == 5)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtDispatchTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDispatchTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridItem_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridItem_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButtons(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTaxDetails()
        {
            try
            {
                if (DataGridItem.Rows.Count - 1 > 0)
                {
                    decimal BasicValue = 0;
                    for (int i = 0; i < DataGridItem.Rows.Count - 1; i++)
                    {
                        if (DataGridItem.Rows[i].Cells[7].Value != null)
                        {
                            BasicValue += Convert.ToDecimal(DataGridItem.Rows[i].Cells[7].Value.ToString());
                        }
                    }
                    txtBasic.Text = BasicValue.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtoutputitem_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getItemsRec();
                bsItem.DataSource = dt;
                FillGrid(dt, 2);
                Point p = FindLocation(txtoutputitem);
                grSearch.Location = new Point(p.X, p.Y - 300);
                grSearch.Visible = true;
                grSearch.Text = "Item Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected DataTable getItemsRec()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItem", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void btnRecOk_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = (DataGridViewRow)DataGridItemReceived.Rows[0].Clone();
                row.Cells[0].Value = txtoutputitem.Tag;
                row.Cells[1].Value = txtoutputitem.Text;
                row.Cells[2].Value = "";
                row.Cells[3].Value = txtoutoutqty.Text;
                row.Cells[4].Value = 0;
                DataGridItemReceived.Rows.Add(row);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadReceivedQyrtGrid()
        {
            DataGridItemReceived.DataSource = null;
            DataGridItemReceived.AutoGenerateColumns = false;
            DataGridItemReceived.ColumnCount = 5;
            DataGridItemReceived.Columns[0].Name = "ItemUid";
            DataGridItemReceived.Columns[0].HeaderText = "ItemUid";
            DataGridItemReceived.Columns[0].Visible = false;

            DataGridItemReceived.Columns[1].Name = "ItemName";
            DataGridItemReceived.Columns[1].HeaderText = "ItemName";
            DataGridItemReceived.Columns[1].Width = 460;
            DataGridItemReceived.Columns[2].Name = "Uom";
            DataGridItemReceived.Columns[2].HeaderText = "Uom";

            DataGridItemReceived.Columns[3].Name = "Quantity";
            DataGridItemReceived.Columns[3].HeaderText = "Quantity";

            DataGridItemReceived.Columns[4].Name = "OuputUid";
            DataGridItemReceived.Columns[4].HeaderText = "OuputUid";
            DataGridItemReceived.Columns[4].Visible = false;
        }

        private void txtoutputitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("ItemName LIKE '%{0}%' ", txtoutputitem.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSupplerName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtSupplerName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void txtSupplerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grSearch.Visible = false;
            }
        }

        private void cboTax_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TaxPercentage = 0;
                decimal BasicAmount = 0;
                if (txtBasic.Text != string.Empty)
                {
                    TaxPercentage = Convert.ToDecimal(cboTax.SelectedValue);
                    BasicAmount = Convert.ToDecimal(txtBasic.Text);
                    txtTaxValue.Text = ((BasicAmount * TaxPercentage) / 100).ToString("0.00");
                    txttotalValue.Text = (BasicAmount + Convert.ToDecimal(txtTaxValue.Text)).ToString("0.00");
                    Genclass.CGSTPer = TaxPercentage / 2;
                    Genclass.SGSTPer = TaxPercentage / 2;
                    Genclass.CGSTValue = (BasicAmount * Genclass.CGSTPer) / 100;
                    Genclass.SGSTValue = (BasicAmount * Genclass.SGSTPer) / 100;
                    txtRoundOff.Text = "0";
                    txtNetValue.Text = txttotalValue.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private DataTable getBeam()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetBeamNo", conn);
                bsBeam.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtSupplerName.Text == string.Empty && txtDispatchTo.Text == string.Empty || TxtpackingSlipNumber.Text == string.Empty || txtGrn.Text == string.Empty)
                {
                    MessageBox.Show("Select packing Slip number and Supplier Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSupplerName.Focus();
                    return;
                }

                if (btnsave.Text == "Save")
                {
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, "Select * from Joi Where DocNo ='" + txtGrn.Text + "'", conn);
                    if (dataTable.Rows.Count == 0)
                    {
                        int Mode = 1;
                        SqlParameter[] para = {
                        new SqlParameter("@Docno",txtGrn.Text),
                        new SqlParameter("@Docdate",Convert.ToDateTime(dtpGrnDate.Text)),
                        new SqlParameter("@Doctypeid",DocTypeId),
                        new SqlParameter("@Supplierid",txtSupplerName.Tag),
                        new SqlParameter("@Remarks",txtVehicle.Text),
                        new SqlParameter("@Status",txtSupplerName.Tag),
                        new SqlParameter("@roff",Convert.ToDecimal(txtRoundOff.Text)),
                        new SqlParameter("@taxablealue",Convert.ToDecimal(txtTaxValue.Text)),
                        new SqlParameter("@totalValue",Convert.ToDecimal(txtNetValue.Text)),
                        new SqlParameter("@PkSlipNo",TxtpackingSlipNumber.Tag),
                        new SqlParameter("@mode",Mode),
                        new SqlParameter("@ReturnUid",SqlDbType.Int),
                        new SqlParameter("@tax",cboTax.Text),
                        new SqlParameter("@DispatchTo",txtDispatchTo.Tag),
                        new SqlParameter("@Transpoter",txtTranspoter.Text),
                        new SqlParameter("@LRNo",txtLRNo.Text),
                        new SqlParameter("@EwayBill",txtEwayBill.Text),
                        new SqlParameter("@ForReason",cmbFor.Text)
                    };
                        para[11].Direction = ParameterDirection.Output;
                        int ReurnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Joorderiss", para, conn, 11);
                        for (int i = 0; i < DataGridItem.Rows.Count - 1; i++)
                        {
                            int ItemUid = Convert.ToInt32(DataGridItem.Rows[i].Cells[17].Value.ToString());
                            decimal Qty = Convert.ToDecimal(DataGridItem.Rows[i].Cells[3].Value.ToString());
                            int MillUid = Convert.ToInt32(DataGridItem.Rows[i].Cells[15].Value.ToString());
                            int NofBags = Convert.ToInt32(DataGridItem.Rows[i].Cells[4].Value.ToString());
                            decimal Price = Convert.ToDecimal(DataGridItem.Rows[i].Cells[6].Value.ToString());
                            decimal basicvalue = Convert.ToDecimal(DataGridItem.Rows[i].Cells[7].Value.ToString());
                            decimal taxValue = Convert.ToDecimal(DataGridItem.Rows[i].Cells[12].Value.ToString());
                            decimal CGSTID = (Convert.ToDecimal(DataGridItem.Rows[i].Cells[8].Value) / 2);
                            decimal CGSTVal = Convert.ToDecimal(DataGridItem.Rows[i].Cells[9].Value.ToString());
                            decimal SGSTID = (Convert.ToDecimal(DataGridItem.Rows[i].Cells[8].Value) / 2);
                            decimal SGSTVal = Convert.ToDecimal(DataGridItem.Rows[i].Cells[10].Value);
                            decimal totalValue = Convert.ToDecimal(DataGridItem.Rows[i].Cells[13].Value);
                            string UOM = DataGridItem.Rows[i].Cells[17].Value.ToString();
                            string Remarks = DataGridItem.Rows[i].Cells[5].Value.ToString();
                            SqlParameter[] paraDet = {
                                new SqlParameter("@JoIuid",ReurnId),
                                new SqlParameter("@Itemuid",ItemUid),
                                new SqlParameter("@Qty",Qty),
                                new SqlParameter("@Remarks",Remarks),
                                new SqlParameter("@MillId",MillUid),
                                new SqlParameter("@Noofbags",NofBags),
                                new SqlParameter("@mode",Mode),
                                new SqlParameter("@price",Price),
                                new SqlParameter("@basicvalue",basicvalue),
                                new SqlParameter("@taxableValue",taxValue),
                                new SqlParameter("@cgstid",CGSTID),
                                new SqlParameter("@cgstval",CGSTVal),
                                new SqlParameter("@sgstid",SGSTID),
                                new SqlParameter("@sgstval",SGSTVal),
                                new SqlParameter("@igstid","0"),
                                new SqlParameter("@igstval","0"),
                                new SqlParameter("@totalvalue",totalValue),
                                new SqlParameter("@UOM",UOM)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Joorderisslist", paraDet, conn);
                        }
                        for (int j = 0; j < DataGridBeem.Rows.Count - 1; j++)
                        {
                            SqlParameter[] paraBeam = {
                                new SqlParameter("@JoIuid",ReurnId),
                                new SqlParameter("@BeamUid",Convert.ToInt32(DataGridBeem.Rows[j].Cells[0].Value)),
                                new SqlParameter("@ItemUid","0"),
                                new SqlParameter("@JoRListUid",Convert.ToInt32(0)),
                                new SqlParameter("@mode",Mode)
                        };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JoorderisslistBeam", paraBeam, conn);
                        }
                        //for (int j = 0; j < DataGridItemReceived.Rows.Count - 1; j++)
                        //{
                        //    SqlParameter[] paraRecId = {
                        //            new SqlParameter("@JoIuid",ReurnId),
                        //            new SqlParameter("@MatchUid",Convert.ToInt32(0)),
                        //            new SqlParameter("@Itemuid",Convert.ToInt32(DataGridItemReceived.Rows[j].Cells[0].Value)),
                        //            new SqlParameter("@mode",Mode),
                        //            new SqlParameter("@Qty",Convert.ToDecimal(DataGridItemReceived.Rows[j].Cells[3].Value)),
                        //            new SqlParameter("@RBalQty",Convert.ToDecimal(0))
                        //    };
                        //    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Joorderisslistoutput", paraRecId, conn);
                        //}
                        MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ClearTextBox();
                        DataGridItem.DataSource = null;
                        DataGridTax.Rows.Clear();
                        DataGridBeem.Rows.Clear();
                    }
                    else
                    {
                        MessageBox.Show(txtGrn.Text + " Document Number Exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtGrn.Focus();
                    }
                }
                else
                {
                    int Mode = 2;
                    SqlParameter[] para = {
                        new SqlParameter("@Docno",txtGrn.Text),
                        new SqlParameter("@Docdate",Convert.ToDateTime(dtpGrnDate.Text)),
                        new SqlParameter("@Doctypeid",DocTypeId),
                        new SqlParameter("@Supplierid",txtSupplerName.Tag),
                        new SqlParameter("@Remarks",txtVehicle.Text),
                        new SqlParameter("@Status",txtSupplerName.Tag),
                        new SqlParameter("@roff",txtSupplerName.Tag),
                        new SqlParameter("@taxablealue",Convert.ToDecimal(txtBasic.Text).ToString("0.000")),
                        new SqlParameter("@totalValue",Convert.ToDecimal(txtNetValue.Text)),
                        new SqlParameter("@PkSlipNo",TxtpackingSlipNumber.Tag),
                        new SqlParameter("@mode",Mode),
                        new SqlParameter("@ReturnUid",SqlDbType.Int),
                        new SqlParameter("@tax",cboTax.Text),
                        new SqlParameter("@Uid",txtGrn.Tag),
                        new SqlParameter("@DispatchTo",txtDispatchTo.Tag),
                        new SqlParameter("@Transpoter",txtTranspoter.Text),
                        new SqlParameter("@LRNo",txtLRNo.Text),
                        new SqlParameter("@EwayBill",txtEwayBill.Text),
                        new SqlParameter("@ForReason",cmbFor.Text)
                    };
                    para[11].Direction = ParameterDirection.Output;
                    int ReurnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Joorderiss", para, conn, 11);
                    for (int i = 0; i < DataGridItem.Rows.Count - 1; i++)
                    {
                        int ItemUid = Convert.ToInt32(DataGridItem.Rows[i].Cells[17].Value.ToString());
                        decimal Qty = Convert.ToDecimal(DataGridItem.Rows[i].Cells[3].Value.ToString());
                        int MillUid = Convert.ToInt32(DataGridItem.Rows[i].Cells[15].Value.ToString());
                        int NofBags = Convert.ToInt32(DataGridItem.Rows[i].Cells[4].Value.ToString());
                        decimal Price = Convert.ToDecimal(DataGridItem.Rows[i].Cells[6].Value.ToString());
                        decimal basicvalue = Convert.ToDecimal(DataGridItem.Rows[i].Cells[7].Value.ToString());
                        decimal taxValue = Convert.ToInt32(DataGridItem.Rows[i].Cells[12].Value.ToString());
                        decimal CGSTID = (Convert.ToDecimal(DataGridItem.Rows[i].Cells[8].Value) / 2);
                        decimal CGSTVal = Convert.ToDecimal(DataGridItem.Rows[i].Cells[9].Value.ToString());
                        decimal SGSTID = (Convert.ToDecimal(DataGridItem.Rows[i].Cells[8].Value) / 2);
                        decimal SGSTVal = Convert.ToDecimal(DataGridItem.Rows[i].Cells[10].Value);
                        decimal totalValue = Convert.ToDecimal(DataGridItem.Rows[i].Cells[13].Value);
                        string UOM = DataGridItem.Rows[i].Cells[17].Value.ToString();
                        string Remarks = DataGridItem.Rows[i].Cells[5].Value.ToString();
                        SqlParameter[] paraDet = {
                                new SqlParameter("@JoIuid",ReurnId),
                                new SqlParameter("@Itemuid",ItemUid),
                                new SqlParameter("@Qty",Qty),
                                new SqlParameter("@Remarks",Remarks),
                                new SqlParameter("@MillId",MillUid),
                                new SqlParameter("@Noofbags",NofBags),
                                new SqlParameter("@mode",Mode),
                                new SqlParameter("@price",Price),
                                new SqlParameter("@basicvalue",basicvalue),
                                new SqlParameter("@taxableValue",taxValue),
                                new SqlParameter("@cgstid",CGSTID),
                                new SqlParameter("@cgstval",CGSTVal),
                                new SqlParameter("@sgstid",SGSTID),
                                new SqlParameter("@sgstval",SGSTVal),
                                new SqlParameter("@igstid","0"),
                                new SqlParameter("@igstval","0"),
                                new SqlParameter("@totalvalue",totalValue),
                                new SqlParameter("@UOM",UOM)
                            };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Joorderisslist", paraDet, conn);
                    }
                    for (int j = 0; j < DataGridBeem.Rows.Count - 1; j++)
                    {
                        SqlParameter[] paraBeam = {
                                new SqlParameter("@JoIuid",ReurnId),
                                new SqlParameter("@BeamUid",Convert.ToInt32(DataGridBeem.Rows[j].Cells[0].Value)),
                                new SqlParameter("@ItemUid","0"),
                                new SqlParameter("@JoRListUid",Convert.ToInt32(0)),
                                new SqlParameter("@mode",Mode)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JoorderisslistBeam", paraBeam, conn);
                    }
                    //for (int j = 0; j < DataGridItemReceived.Rows.Count - 1; j++)
                    //{
                    //    SqlParameter[] paraRecId = {
                    //            new SqlParameter("@JoIuid",ReurnId),
                    //            new SqlParameter("@MatchUid",Convert.ToInt32(0)),
                    //            new SqlParameter("@Itemuid",Convert.ToInt32(DataGridItemReceived.Rows[j].Cells[0].Value)),
                    //            new SqlParameter("@mode",Mode),
                    //            new SqlParameter("@Qty",Convert.ToDecimal(DataGridItemReceived.Rows[j].Cells[3].Value)),
                    //            new SqlParameter("@RBalQty",Convert.ToDecimal(0))
                    //    };
                    //    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Joorderisslistoutput", paraRecId, conn);
                    //}
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnsave.Text = "Save";
                }
                getJoborderIssue();
                LoadButtons(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ClearTextBox()
        {
            try
            {
                txtGrn.Text = string.Empty;
                TxtpackingSlipNumber.Tag = string.Empty;
                txtSupplerName.Text = string.Empty;
                txtSupplerName.Tag = string.Empty;
                txtDispatchTo.Text = string.Empty;
                txtDispatchTo.Tag = string.Empty;
                txtVehicle.Text = string.Empty;
                txtTranspoter.Text = string.Empty;
                txtLRNo.Text = string.Empty;
                txtEwayBill.Text = string.Empty;
                txtRemarks.Text = string.Empty;
                txtBasic.Text = string.Empty;
                txtTaxValue.Text = string.Empty;
                txttotalValue.Text = string.Empty;
                txtNetValue.Text = string.Empty;
                txtRoundOff.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadBeam()
        {
            try
            {
                DataGridBeem.DataSource = null;
                DataGridBeem.AutoGenerateColumns = false;
                DataGridBeem.ColumnCount = 3;
                DataGridBeem.Columns[0].Name = "Beamid";
                DataGridBeem.Columns[0].Visible = false;
                DataGridBeem.Columns[1].Name = "Beam";
                DataGridBeem.Columns[1].Width = 120;
                DataGridBeem.Columns[2].Name = "itemid";
                DataGridBeem.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void btnBeamOk_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            DataGridViewRow row = (DataGridViewRow)DataGridBeem.Rows[0].Clone();
            row.Cells[0].Value = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
            row.Cells[1].Value = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            row.Cells[2].Value = txtGrn.Text;
            DataGridBeem.Rows.Add(row);
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void txtBeam_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getBeam();
                bsBeam.DataSource = dt;
                FillGrid(dt, 3);
                Point p = FindLocation(txtBeam);
                grSearch.Location = new Point(p.X - 270, p.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F7)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F6)
                {
                    if (Fillid == 1)
                    {
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        txtSupplerName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtSupplerName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        string PkSlipNo = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                        grSearch.Visible = false;
                        SelectId = 0;
                    }
                    else if (Fillid == 2)
                    {
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        txtoutputitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtoutputitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        grSearch.Visible = false;
                        SelectId = 0;
                    }
                    else if (Fillid == 3)
                    {
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        if (DataGridItem.Rows.Count > 1)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridBeem.Rows[0].Clone();
                            row.Cells[0].Value = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                            row.Cells[1].Value = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            row.Cells[2].Value = txtGrn.Text;
                            DataGridBeem.Rows.Add(row);
                            grSearch.Visible = false;
                        }
                        else
                        {
                            MessageBox.Show("Select Item", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        SelectId = 0;
                    }
                    else if (Fillid == 4)
                    {
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        TxtpackingSlipNumber.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        TxtpackingSlipNumber.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        SqlParameter[] parameters = { new SqlParameter("@PkSlipUid", TxtpackingSlipNumber.Tag) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPkSlipdetForIssue", parameters, conn);
                        LoadDataGridItem(dt);
                        grSearch.Visible = false;
                        SelectId = 0;
                    }
                    else if (Fillid == 5)
                    {
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        txtDispatchTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDispatchTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        grSearch.Visible = false;
                        SelectId = 0;
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (Fillid == 1)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtSupplerName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtSupplerName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    string PkSlipNo = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (Fillid == 2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtoutputitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtoutputitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (Fillid == 3)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (DataGridItem.Rows.Count > 1)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridBeem.Rows[0].Clone();
                        row.Cells[0].Value = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        row.Cells[1].Value = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        row.Cells[2].Value = txtGrn.Text;
                        DataGridBeem.Rows.Add(row);
                        grSearch.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show("Select Item", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    SelectId = 0;
                }
                else if (Fillid == 4)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    TxtpackingSlipNumber.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtpackingSlipNumber.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    SqlParameter[] parameters = { new SqlParameter("@PkSlipUid", TxtpackingSlipNumber.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPkSlipdetForIssue", parameters, conn);
                    LoadDataGridItem(dt);
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (Fillid == 5)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtDispatchTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDispatchTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void getJoborderIssue()
        {
            try
            {
                int mode = 1;
                SqlParameter[] para = { new SqlParameter("@Mode", mode) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetJoIssue", para, conn);
                bsIssue.DataSource = dt;
                LoadYarnIssue(bsIssue);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadYarnIssue(BindingSource dt)
        {
            try
            {
                DataGridYarnIssue.DataSource = null;
                DataGridYarnIssue.AutoGenerateColumns = false;
                DataGridYarnIssue.ColumnCount = 10;
                DataGridYarnIssue.Columns[0].Name = "Uid";
                DataGridYarnIssue.Columns[0].HeaderText = "Uid";
                DataGridYarnIssue.Columns[0].DataPropertyName = "Uid";
                DataGridYarnIssue.Columns[0].Visible = false;
                DataGridYarnIssue.Columns[1].Name = "SupplierUid";
                DataGridYarnIssue.Columns[1].HeaderText = "SupplierUid";
                DataGridYarnIssue.Columns[1].DataPropertyName = "SupplierUid";
                DataGridYarnIssue.Columns[1].Visible = false;

                DataGridYarnIssue.Columns[2].Name = "DC No";
                DataGridYarnIssue.Columns[2].HeaderText = "DC No";
                DataGridYarnIssue.Columns[2].DataPropertyName = "Docno";

                DataGridYarnIssue.Columns[3].Name = "Dc Date";
                DataGridYarnIssue.Columns[3].HeaderText = "Dc Date";
                DataGridYarnIssue.Columns[3].DataPropertyName = "Docdate";

                DataGridYarnIssue.Columns[4].Name = "Name";
                DataGridYarnIssue.Columns[4].HeaderText = "Name";
                DataGridYarnIssue.Columns[4].DataPropertyName = "Name";
                DataGridYarnIssue.Columns[4].Width = 490;

                DataGridYarnIssue.Columns[5].Name = "NoofBags";
                DataGridYarnIssue.Columns[5].HeaderText = "NoofBags";
                DataGridYarnIssue.Columns[5].DataPropertyName = "NoofBags";

                DataGridYarnIssue.Columns[6].Name = "Qty";
                DataGridYarnIssue.Columns[6].HeaderText = "Weight";
                DataGridYarnIssue.Columns[6].DataPropertyName = "Qty";

                DataGridYarnIssue.Columns[7].Name = "BasicValue";
                DataGridYarnIssue.Columns[7].HeaderText = "BasicValue";
                DataGridYarnIssue.Columns[7].DataPropertyName = "BaseValue";
                DataGridYarnIssue.Columns[7].DefaultCellStyle.Format = "N2";

                DataGridYarnIssue.Columns[8].Name = "TaxValue";
                DataGridYarnIssue.Columns[8].HeaderText = "TaxValue";
                DataGridYarnIssue.Columns[8].DataPropertyName = "Tax";
                DataGridYarnIssue.Columns[8].DefaultCellStyle.Format = "N2";

                DataGridYarnIssue.Columns[9].Name = "totalValue";
                DataGridYarnIssue.Columns[9].HeaderText = "TotalValue";
                DataGridYarnIssue.Columns[9].DataPropertyName = "totalValue";
                DataGridYarnIssue.Columns[9].DefaultCellStyle.Format = "N4";
                DataGridYarnIssue.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

        }

        private void btnHide_Click_1(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Genclass.Dtype = 10;
                int Index = DataGridYarnIssue.SelectedCells[0].RowIndex;
                Genclass.RpeortId = Convert.ToInt32(DataGridYarnIssue.Rows[Index].Cells[0].Value);
                FrmReprtViwer crv = new FrmReprtViwer();
                crv.MdiParent = this.MdiParent;
                crv.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReceivedQyrtGrid();
                LoadBeam();
                DataGridBeem.Rows.Clear();
                DataGridItem.DataSource = null;
                DataGridItemReceived.Rows.Clear();
                int Index = DataGridYarnIssue.SelectedCells[0].RowIndex;
                int JoUid = Convert.ToInt32(DataGridYarnIssue.Rows[Index].Cells[0].Value.ToString());
                int Mode = 2;
                SqlParameter[] para = { new SqlParameter("@Mode", Mode), new SqlParameter("@JoIUid", JoUid) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetJoIssue", para, conn);
                DataTable dtJoIssue = ds.Tables[0];
                DataTable dtItem = ds.Tables[1];
                DataTable dtBeam = ds.Tables[2];
                DataTable dtOutput = ds.Tables[3];
                txtGrn.Text = dtJoIssue.Rows[0]["Docno"].ToString();
                txtGrn.Tag = dtJoIssue.Rows[0]["Uid"].ToString();
                dtpGrnDate.Text = dtJoIssue.Rows[0]["Docdate"].ToString();
                txtSupplerName.Text = dtJoIssue.Rows[0]["Name"].ToString();
                txtSupplerName.Tag = dtJoIssue.Rows[0]["SupplierUid"].ToString();
                txtVehicle.Text = dtJoIssue.Rows[0]["Remarks"].ToString();
                txtBasic.Text = dtJoIssue.Rows[0]["taxablealue"].ToString();
                cboTax.Text = dtJoIssue.Rows[0]["tax"].ToString();
                decimal CGSTv = Convert.ToDecimal(dtJoIssue.Rows[0]["CGSTValue"].ToString());
                decimal SGSTv = Convert.ToDecimal(dtJoIssue.Rows[0]["SGSTValue"].ToString());
                txtTaxValue.Text = (CGSTv + SGSTv).ToString();
                txttotalValue.Text = dtJoIssue.Rows[0]["totalValue"].ToString();
                txtRoundOff.Text = dtJoIssue.Rows[0]["roff"].ToString();
                txtNetValue.Text = dtJoIssue.Rows[0]["totalValue"].ToString();
                for (int i = 0; i < dtOutput.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridItemReceived.Rows[0].Clone();
                    row.Cells[0].Value = dtOutput.Rows[i]["ItemUid"].ToString();
                    row.Cells[1].Value = dtOutput.Rows[i]["ItemName"].ToString();
                    row.Cells[2].Value = "";
                    row.Cells[3].Value = dtOutput.Rows[i]["Qty"].ToString();
                    row.Cells[4].Value = dtOutput.Rows[i]["Uid"].ToString();
                    DataGridItemReceived.Rows.Add(row);
                }
                LoadDataGridItem(dtItem);
                for (int i = 0; i < dtBeam.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridBeem.Rows[0].Clone();
                    row.Cells[0].Value = dtBeam.Rows[i]["BeamUid"].ToString();
                    row.Cells[1].Value = dtBeam.Rows[i]["Beam"].ToString();
                    row.Cells[2].Value = dtBeam.Rows[i]["ItemUid"].ToString();
                    DataGridBeem.Rows.Add(row);
                }
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmyarnIssue_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F2)
                {
                    btnAdd_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F3)
                {
                    btnEdit_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F9)
                {
                    btnaddrcan_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F5)
                {
                    btnsave_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {

        }

        private void TxtpackingSlipNumber_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = GetPackingSlip();
                bsPkSlip.DataSource = dt;
                FillGrid(dt, 4);
                Point p = FindLocation(TxtpackingSlipNumber);
                grSearch.Location = new Point(p.X - 10, p.Y + 25);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private DataTable GetPackingSlip()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetPackingSlipNo", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void TxtpackingSlipNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("PkSlipNo LIKE '%{0}%' ", TxtpackingSlipNumber.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDispatchTo_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = LoadCommonGrid(0);
                FillGrid(dt, 5);
                Point loc = FindLocation(txtDispatchTo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 25);
                grSearch.Visible = true;
                grSearch.Text = "Dispatch Search";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable LoadCommonGrid(int v)
        {
            DataTable dt = new DataTable();
            DataGridCommon.AutoGenerateColumns = false;
            dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
            return dt;
        }

        private void txtSupplerName_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getParty();
                bsParty.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = FindLocation(txtSupplerName);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 25);
                grSearch.Visible = true;
                grSearch.Text = "Party Search";
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void DataGridItem_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 6)
                {
                    int index = DataGridItem.SelectedCells[0].RowIndex;
                    decimal Price = Convert.ToDecimal(DataGridItem.Rows[index].Cells[6].Value.ToString());
                    decimal Weight = Convert.ToDecimal(DataGridItem.Rows[index].Cells[3].Value.ToString());
                    decimal TaxPercentage = Convert.ToDecimal(DataGridItem.Rows[index].Cells[8].Value.ToString());
                    decimal TaxAmount = 0;
                    decimal Amount = Weight * Price;
                    TaxAmount = (Amount * 5) / 100;
                    DataGridItem.Rows[index].Cells[7].Value = Amount.ToString("0.00");
                    DataGridItem.Rows[index].Cells[9].Value = (TaxAmount / 2).ToString("0.00");
                    DataGridItem.Rows[index].Cells[10].Value = (TaxAmount / 2).ToString("0.00");
                    DataGridItem.Rows[index].Cells[11].Value = 0;
                    DataGridItem.Rows[index].Cells[12].Value = TaxAmount.ToString("0.00");
                    DataGridItem.Rows[index].Cells[13].Value = (Amount + TaxAmount).ToString("0.00");
                    CalculateText();
                    FilltaxGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CalculateText()
        {
            try
            {
                decimal TotalBasicvalue = 0, TaxValue = 0;
                for (int i = 0; i < DataGridItem.Rows.Count - 1; i++)
                {
                    if (DataGridItem.Rows[i].Cells[7].Value != null)
                    {
                        TotalBasicvalue += Convert.ToDecimal(DataGridItem.Rows[i].Cells[7].Value.ToString());
                    }
                    if (DataGridItem.Rows[i].Cells[12].Value != null)
                    {
                        TaxValue += Convert.ToDecimal(DataGridItem.Rows[i].Cells[12].Value.ToString());
                    }
                }
                txtBasic.Text = TotalBasicvalue.ToString("0.00");
                txtTaxValue.Text = TaxValue.ToString("0.00");
                txttotalValue.Text = (TotalBasicvalue + TaxValue).ToString("0.00");
                double a = Convert.ToDouble(txttotalValue.Text);
                string[] str = a.ToString("0.00").Split('.');
                double num1 = Convert.ToDouble("0." + str[1]);
                double res;
                if (num1 < 0.51)
                {
                    res = Math.Floor(a);
                }
                else
                {
                    res = Math.Round(a);
                }
                txtRoundOff.Text = (res - a).ToString("0.00");
                txtNetValue.Text = res.ToString("0.00");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadTaxGrid()
        {
            try
            {
                DataGridTax.DataSource = null;
                DataGridTax.AutoGenerateColumns = false;
                DataGridTax.ColumnCount = 6;
                DataGridTax.Columns[0].Name = "HSNCode";
                DataGridTax.Columns[0].Name = "HSNCode";

                DataGridTax.Columns[1].Name = "GST%";
                DataGridTax.Columns[1].Name = "GST%";
                DataGridTax.Columns[1].Width = 80;
                DataGridTax.Columns[1].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[2].Name = "CGST";
                DataGridTax.Columns[2].Name = "CGST";
                DataGridTax.Columns[2].Width = 80;
                DataGridTax.Columns[2].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[3].Name = "SGST";
                DataGridTax.Columns[3].Name = "SGST";
                DataGridTax.Columns[3].Width = 80;
                DataGridTax.Columns[3].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[4].Name = "IGST";
                DataGridTax.Columns[4].Name = "IGST";
                DataGridTax.Columns[4].Width = 80;
                DataGridTax.Columns[4].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[5].Name = "TotalTax";
                DataGridTax.Columns[5].Name = "TotalTax";
                DataGridTax.Columns[5].Width = 80;
                DataGridTax.Columns[5].DefaultCellStyle.Format = "N2";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FilltaxGrid()
        {
            try
            {
                DataGridTax.Rows.Clear();
                DataTable dt = new DataTable();
                dt.Columns.Add("HSNCode", typeof(string));
                dt.Columns.Add("GSTPer", typeof(decimal));
                dt.Columns.Add("CGST", typeof(decimal));
                dt.Columns.Add("SGST", typeof(decimal));
                dt.Columns.Add("IGST", typeof(decimal));
                dt.Columns.Add("TaxValue", typeof(decimal));
                for (int i = 0; i < DataGridItem.Rows.Count - 1; i++)
                {
                    DataRow dr = dt.NewRow();
                    if (DataGridItem.Rows[i].Cells[9].Value != null)
                    {
                        dr[0] = DataGridItem.Rows[i].Cells[2].Value;
                        dr[1] = DataGridItem.Rows[i].Cells[8].Value;
                        dr[2] = DataGridItem.Rows[i].Cells[9].Value;
                        dr[3] = DataGridItem.Rows[i].Cells[10].Value;
                        dr[4] = DataGridItem.Rows[i].Cells[11].Value;
                        dr[5] = DataGridItem.Rows[i].Cells[12].Value;
                    }
                    else
                    {
                        dr[0] = DataGridItem.Rows[i].Cells[2].Value;
                        dr[1] = 0;
                        dr[2] = 0;
                        dr[3] = 0;
                        dr[4] = 0;
                        dr[5] = 0;
                    }
                    dt.Rows.Add(dr);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        int Index = DataGridTax.Rows.Add();
                        DataGridViewRow row = DataGridTax.Rows[Index];
                        row.Cells[0].Value = dt.Rows[i]["HSNCode"].ToString();
                        row.Cells[1].Value = dt.Rows[i]["GSTPer"].ToString();
                        row.Cells[2].Value = dt.Rows[i]["CGST"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["SGST"].ToString();
                        row.Cells[4].Value = 0;
                        row.Cells[5].Value = dt.Rows[i]["TaxValue"].ToString();
                    }
                    else
                    {
                        decimal gstper = 0;
                        if (dt.Rows[i]["GSTPer"].ToString() == "" || dt.Rows[i]["GSTPer"] == null)
                        {
                            gstper = 0;
                        }
                        else
                        {
                            gstper = Convert.ToDecimal(dt.Rows[i]["GSTPer"].ToString());
                        }
                        bool entryfound = CheckGridValue(dt.Rows[i]["HSNCode"].ToString(), gstper);
                        if (entryfound == true)
                        {
                            string HSNCode = dt.Rows[i]["HSNCode"].ToString();
                            decimal TaxPer = Convert.ToDecimal(dt.Rows[i]["GSTPer"].ToString());
                            decimal SGST = Convert.ToDecimal(dt.Rows[i]["CGST"].ToString());
                            decimal CGST = Convert.ToDecimal(dt.Rows[i]["SGST"].ToString());
                            decimal IGST = Convert.ToDecimal(dt.Rows[i]["IGST"].ToString());
                            decimal taValue = Convert.ToDecimal(dt.Rows[i]["TaxValue"].ToString());
                            for (int j = 0; j < DataGridTax.Rows.Count; j++)
                            {
                                string HSNCodeGrid = DataGridTax.Rows[j].Cells[0].Value.ToString();
                                decimal SGSTGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[3].Value.ToString());
                                decimal CGSTGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[2].Value.ToString());
                                decimal IGSTGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[4].Value.ToString());
                                decimal taValueGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[5].Value.ToString());
                                decimal taxPerGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[1].Value.ToString());
                                if (HSNCode == HSNCodeGrid && TaxPer == taxPerGrid)
                                {
                                    DataGridTax.Rows[j].Cells[2].Value = (CGST + CGSTGrid).ToString("0.00");
                                    DataGridTax.Rows[j].Cells[3].Value = (SGST + SGSTGrid).ToString("0.00");
                                    DataGridTax.Rows[j].Cells[4].Value = (IGST + IGSTGrid).ToString("0.00");
                                    DataGridTax.Rows[j].Cells[5].Value = (taValue + taValueGrid).ToString("0.00");
                                }
                            }
                        }
                        else
                        {
                            int Index = DataGridTax.Rows.Add();
                            DataGridViewRow row = DataGridTax.Rows[Index];
                            row.Cells[0].Value = dt.Rows[i]["HSNCode"].ToString();
                            row.Cells[1].Value = dt.Rows[i]["GSTPer"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["CGST"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["SGST"].ToString();
                            row.Cells[4].Value = 0;
                            row.Cells[5].Value = dt.Rows[i]["TaxValue"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected bool CheckGridValue(string HSNCode, decimal taxpercentage)
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridTax.Rows)
                {
                    object val2 = row.Cells[0].Value;
                    object val3 = Convert.ToDecimal(row.Cells[1].Value);
                    if (val2 != null && val2.ToString() == HSNCode && val3 != null && val3.ToString() == taxpercentage.ToString())
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void btnPackingSlipPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridYarnIssue.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridYarnIssue.Rows[Index].Cells[0].Value.ToString());
                Genclass.ReportType = "YarnPackingSlip";
                Genclass.Dtype = 11;
                Genclass.RpeortId = Uid;
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.StartPosition = FormStartPosition.CenterScreen;
                rpt.MdiParent = this.MdiParent;
                rpt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}