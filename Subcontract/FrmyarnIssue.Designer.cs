﻿namespace Naalwar
{
    partial class FrmyarnIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmyarnIssue));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.grSearch = new System.Windows.Forms.Panel();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbFor = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTranspoter = new System.Windows.Forms.TextBox();
            this.txtLRNo = new System.Windows.Forms.TextBox();
            this.txtEwayBill = new System.Windows.Forms.TextBox();
            this.txtDispatchTo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtpackingSlipNumber = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtRoundOff = new System.Windows.Forms.TextBox();
            this.txtNetValue = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txttotalValue = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtBasic = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.DataGridItemReceived = new System.Windows.Forms.DataGridView();
            this.btnRecOk = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtoutoutqty = new System.Windows.Forms.TextBox();
            this.cbosGReturnItem = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtoutputitem = new System.Windows.Forms.TextBox();
            this.DataGridBeem = new System.Windows.Forms.DataGridView();
            this.DataGridItem = new System.Windows.Forms.DataGridView();
            this.btnBeamOk = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtBags = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBeam = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMIllName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtItem = new System.Windows.Forms.TextBox();
            this.txtVehicle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.Label();
            this.txtGrn = new System.Windows.Forms.TextBox();
            this.dtpGrnDate = new System.Windows.Forms.DateTimePicker();
            this.txtSupplerName = new System.Windows.Forms.TextBox();
            this.txtTaxValue = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cboTax = new System.Windows.Forms.ComboBox();
            this.DataGridTax = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnPackingSlipPrint = new System.Windows.Forms.Button();
            this.panNumbers = new System.Windows.Forms.Panel();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFstBack = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnFstNext = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridYarnIssue = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItemReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridBeem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTax)).BeginInit();
            this.panadd.SuspendLayout();
            this.panNumbers.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnIssue)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label26);
            this.grBack.Controls.Add(this.txtRemarks);
            this.grBack.Controls.Add(this.label25);
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.cmbFor);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.txtTranspoter);
            this.grBack.Controls.Add(this.txtLRNo);
            this.grBack.Controls.Add(this.txtEwayBill);
            this.grBack.Controls.Add(this.txtDispatchTo);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.TxtpackingSlipNumber);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.txtRoundOff);
            this.grBack.Controls.Add(this.txtNetValue);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.label24);
            this.grBack.Controls.Add(this.txttotalValue);
            this.grBack.Controls.Add(this.label23);
            this.grBack.Controls.Add(this.txtBasic);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.DataGridItemReceived);
            this.grBack.Controls.Add(this.btnRecOk);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.txtoutoutqty);
            this.grBack.Controls.Add(this.cbosGReturnItem);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtoutputitem);
            this.grBack.Controls.Add(this.DataGridBeem);
            this.grBack.Controls.Add(this.DataGridItem);
            this.grBack.Controls.Add(this.btnBeamOk);
            this.grBack.Controls.Add(this.label19);
            this.grBack.Controls.Add(this.txtPrice);
            this.grBack.Controls.Add(this.label18);
            this.grBack.Controls.Add(this.txtBags);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtBeam);
            this.grBack.Controls.Add(this.btnOk);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.txtMIllName);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtQty);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.txtItem);
            this.grBack.Controls.Add(this.txtVehicle);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.Phone);
            this.grBack.Controls.Add(this.txtGrn);
            this.grBack.Controls.Add(this.dtpGrnDate);
            this.grBack.Controls.Add(this.txtSupplerName);
            this.grBack.Controls.Add(this.txtTaxValue);
            this.grBack.Controls.Add(this.label21);
            this.grBack.Controls.Add(this.cboTax);
            this.grBack.Controls.Add(this.DataGridTax);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(11, 2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1221, 567);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(404, 427);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 18);
            this.label26.TabIndex = 424;
            this.label26.Text = "Tax Structure";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(131, 488);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(265, 66);
            this.txtRemarks.TabIndex = 422;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(50, 491);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(61, 18);
            this.label25.TabIndex = 421;
            this.label25.Text = "Remarks";
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(209, 62);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(382, 300);
            this.grSearch.TabIndex = 393;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::Naalwar.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(174, 268);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(100, 28);
            this.btnSelect.TabIndex = 394;
            this.btnSelect.Text = "Select (F6)";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(277, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F7)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click_1);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(375, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 427);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 18);
            this.label15.TabIndex = 420;
            this.label15.Text = "E-Way Bill Number";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(84, 390);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 18);
            this.label12.TabIndex = 418;
            this.label12.Text = "LRNo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(50, 361);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 18);
            this.label16.TabIndex = 417;
            this.label16.Text = "Transpoter";
            // 
            // cmbFor
            // 
            this.cmbFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFor.FormattingEnabled = true;
            this.cmbFor.Items.AddRange(new object[] {
            "For Sizing Only",
            "For Weaving Only",
            "For Dyeing Only",
            "For Bleaching Only",
            "For Rewinding Only",
            "For  Doubling Purpose",
            "For TFO Purpose",
            "For Waste Yarn Disposal"});
            this.cmbFor.Location = new System.Drawing.Point(131, 456);
            this.cmbFor.Name = "cmbFor";
            this.cmbFor.Size = new System.Drawing.Size(265, 26);
            this.cmbFor.TabIndex = 415;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(96, 460);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 18);
            this.label17.TabIndex = 416;
            this.label17.Text = "For";
            // 
            // txtTranspoter
            // 
            this.txtTranspoter.Location = new System.Drawing.Point(131, 358);
            this.txtTranspoter.Name = "txtTranspoter";
            this.txtTranspoter.Size = new System.Drawing.Size(326, 26);
            this.txtTranspoter.TabIndex = 413;
            // 
            // txtLRNo
            // 
            this.txtLRNo.Location = new System.Drawing.Point(131, 390);
            this.txtLRNo.Name = "txtLRNo";
            this.txtLRNo.Size = new System.Drawing.Size(326, 26);
            this.txtLRNo.TabIndex = 414;
            // 
            // txtEwayBill
            // 
            this.txtEwayBill.Location = new System.Drawing.Point(131, 424);
            this.txtEwayBill.Name = "txtEwayBill";
            this.txtEwayBill.Size = new System.Drawing.Size(265, 26);
            this.txtEwayBill.TabIndex = 419;
            // 
            // txtDispatchTo
            // 
            this.txtDispatchTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDispatchTo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDispatchTo.Location = new System.Drawing.Point(696, 37);
            this.txtDispatchTo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDispatchTo.Name = "txtDispatchTo";
            this.txtDispatchTo.Size = new System.Drawing.Size(257, 26);
            this.txtDispatchTo.TabIndex = 411;
            this.txtDispatchTo.Click += new System.EventHandler(this.txtDispatchTo_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(695, 17);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 18);
            this.label13.TabIndex = 412;
            this.label13.Text = "Dispatch To";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(271, 17);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 18);
            this.label11.TabIndex = 395;
            this.label11.Text = "Packing Slip Number";
            // 
            // TxtpackingSlipNumber
            // 
            this.TxtpackingSlipNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtpackingSlipNumber.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtpackingSlipNumber.Location = new System.Drawing.Point(268, 37);
            this.TxtpackingSlipNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtpackingSlipNumber.Name = "TxtpackingSlipNumber";
            this.TxtpackingSlipNumber.Size = new System.Drawing.Size(156, 26);
            this.TxtpackingSlipNumber.TabIndex = 0;
            this.TxtpackingSlipNumber.Click += new System.EventHandler(this.TxtpackingSlipNumber_Click);
            this.TxtpackingSlipNumber.TextChanged += new System.EventHandler(this.TxtpackingSlipNumber_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1025, 439);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 18);
            this.label14.TabIndex = 388;
            this.label14.Text = "Round Off";
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRoundOff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoundOff.Location = new System.Drawing.Point(1099, 432);
            this.txtRoundOff.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.Size = new System.Drawing.Size(108, 26);
            this.txtRoundOff.TabIndex = 15;
            this.txtRoundOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNetValue
            // 
            this.txtNetValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetValue.Location = new System.Drawing.Point(1099, 463);
            this.txtNetValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNetValue.Name = "txtNetValue";
            this.txtNetValue.Size = new System.Drawing.Size(110, 26);
            this.txtNetValue.TabIndex = 382;
            this.txtNetValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1027, 470);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 18);
            this.label22.TabIndex = 381;
            this.label22.Text = "Net value";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1019, 409);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 18);
            this.label24.TabIndex = 386;
            this.label24.Text = "Total Value";
            // 
            // txttotalValue
            // 
            this.txttotalValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotalValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalValue.Location = new System.Drawing.Point(1099, 402);
            this.txttotalValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txttotalValue.Name = "txttotalValue";
            this.txttotalValue.Size = new System.Drawing.Size(108, 26);
            this.txttotalValue.TabIndex = 385;
            this.txttotalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1029, 379);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 18);
            this.label23.TabIndex = 384;
            this.label23.Text = "Tax Value";
            // 
            // txtBasic
            // 
            this.txtBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBasic.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasic.Location = new System.Drawing.Point(1099, 342);
            this.txtBasic.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBasic.Name = "txtBasic";
            this.txtBasic.Size = new System.Drawing.Size(108, 26);
            this.txtBasic.TabIndex = 379;
            this.txtBasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1023, 349);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 18);
            this.label20.TabIndex = 378;
            this.label20.Text = "Basic Total";
            // 
            // DataGridItemReceived
            // 
            this.DataGridItemReceived.BackgroundColor = System.Drawing.Color.White;
            this.DataGridItemReceived.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItemReceived.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridItemReceived.Location = new System.Drawing.Point(402, 493);
            this.DataGridItemReceived.Name = "DataGridItemReceived";
            this.DataGridItemReceived.RowHeadersVisible = false;
            this.DataGridItemReceived.Size = new System.Drawing.Size(295, 61);
            this.DataGridItemReceived.TabIndex = 377;
            this.DataGridItemReceived.Visible = false;
            // 
            // btnRecOk
            // 
            this.btnRecOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRecOk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecOk.Image = global::Naalwar.Properties.Resources.ok;
            this.btnRecOk.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRecOk.Location = new System.Drawing.Point(610, 514);
            this.btnRecOk.Name = "btnRecOk";
            this.btnRecOk.Size = new System.Drawing.Size(29, 27);
            this.btnRecOk.TabIndex = 13;
            this.btnRecOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRecOk.UseVisualStyleBackColor = false;
            this.btnRecOk.Visible = false;
            this.btnRecOk.Click += new System.EventHandler(this.btnRecOk_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(573, 525);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 18);
            this.label8.TabIndex = 375;
            this.label8.Text = "Qty";
            this.label8.Visible = false;
            // 
            // txtoutoutqty
            // 
            this.txtoutoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutoutqty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutoutqty.Location = new System.Drawing.Point(526, 516);
            this.txtoutoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutoutqty.Name = "txtoutoutqty";
            this.txtoutoutqty.Size = new System.Drawing.Size(71, 26);
            this.txtoutoutqty.TabIndex = 12;
            this.txtoutoutqty.Visible = false;
            // 
            // cbosGReturnItem
            // 
            this.cbosGReturnItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosGReturnItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosGReturnItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosGReturnItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosGReturnItem.FormattingEnabled = true;
            this.cbosGReturnItem.Location = new System.Drawing.Point(551, 495);
            this.cbosGReturnItem.Name = "cbosGReturnItem";
            this.cbosGReturnItem.Size = new System.Drawing.Size(29, 26);
            this.cbosGReturnItem.TabIndex = 10;
            this.cbosGReturnItem.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(480, 503);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 18);
            this.label6.TabIndex = 374;
            this.label6.Text = "Yarn to be received";
            this.label6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(546, 522);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 18);
            this.label5.TabIndex = 373;
            this.label5.Text = "Item";
            this.label5.Visible = false;
            // 
            // txtoutputitem
            // 
            this.txtoutputitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutputitem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutputitem.Location = new System.Drawing.Point(526, 517);
            this.txtoutputitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutputitem.Name = "txtoutputitem";
            this.txtoutputitem.Size = new System.Drawing.Size(38, 26);
            this.txtoutputitem.TabIndex = 11;
            this.txtoutputitem.Visible = false;
            this.txtoutputitem.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtoutputitem_MouseClick);
            this.txtoutputitem.TextChanged += new System.EventHandler(this.txtoutputitem_TextChanged);
            this.txtoutputitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupplerName_KeyDown);
            // 
            // DataGridBeem
            // 
            this.DataGridBeem.BackgroundColor = System.Drawing.Color.White;
            this.DataGridBeem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridBeem.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridBeem.Location = new System.Drawing.Point(1075, 90);
            this.DataGridBeem.Name = "DataGridBeem";
            this.DataGridBeem.RowHeadersVisible = false;
            this.DataGridBeem.Size = new System.Drawing.Size(132, 237);
            this.DataGridBeem.TabIndex = 368;
            // 
            // DataGridItem
            // 
            this.DataGridItem.BackgroundColor = System.Drawing.Color.White;
            this.DataGridItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItem.Location = new System.Drawing.Point(6, 66);
            this.DataGridItem.Name = "DataGridItem";
            this.DataGridItem.RowHeadersVisible = false;
            this.DataGridItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridItem.Size = new System.Drawing.Size(1063, 281);
            this.DataGridItem.TabIndex = 367;
            this.DataGridItem.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridItem_CellMouseDoubleClick);
            this.DataGridItem.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridItem_CellValueChanged);
            // 
            // btnBeamOk
            // 
            this.btnBeamOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBeamOk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBeamOk.Image = global::Naalwar.Properties.Resources.ok;
            this.btnBeamOk.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnBeamOk.Location = new System.Drawing.Point(1178, 62);
            this.btnBeamOk.Name = "btnBeamOk";
            this.btnBeamOk.Size = new System.Drawing.Size(29, 27);
            this.btnBeamOk.TabIndex = 9;
            this.btnBeamOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBeamOk.UseVisualStyleBackColor = false;
            this.btnBeamOk.Click += new System.EventHandler(this.btnBeamOk_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(658, 68);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 18);
            this.label19.TabIndex = 365;
            this.label19.Text = "Price";
            // 
            // txtPrice
            // 
            this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.Location = new System.Drawing.Point(658, 90);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(71, 26);
            this.txtPrice.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(510, 68);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 18);
            this.label18.TabIndex = 363;
            this.label18.Text = "No of Bags";
            // 
            // txtBags
            // 
            this.txtBags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBags.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBags.Location = new System.Drawing.Point(515, 90);
            this.txtBags.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBags.Name = "txtBags";
            this.txtBags.Size = new System.Drawing.Size(76, 26);
            this.txtBags.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1075, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 361;
            this.label4.Text = "Beam No";
            // 
            // txtBeam
            // 
            this.txtBeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBeam.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBeam.Location = new System.Drawing.Point(1076, 63);
            this.txtBeam.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBeam.Name = "txtBeam";
            this.txtBeam.Size = new System.Drawing.Size(102, 26);
            this.txtBeam.TabIndex = 8;
            this.txtBeam.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtBeam_MouseClick);
            this.txtBeam.TextChanged += new System.EventHandler(this.btnSelect_Click);
            this.txtBeam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnOk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Image = global::Naalwar.Properties.Resources.ok;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnOk.Location = new System.Drawing.Point(729, 90);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(29, 27);
            this.btnOk.TabIndex = 7;
            this.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(591, 68);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 18);
            this.label9.TabIndex = 360;
            this.label9.Text = "Qty";
            // 
            // txtMIllName
            // 
            this.txtMIllName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMIllName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMIllName.Location = new System.Drawing.Point(290, 90);
            this.txtMIllName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMIllName.Name = "txtMIllName";
            this.txtMIllName.Size = new System.Drawing.Size(225, 26);
            this.txtMIllName.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(290, 68);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 18);
            this.label10.TabIndex = 356;
            this.label10.Text = "Mill Name";
            // 
            // txtQty
            // 
            this.txtQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQty.Location = new System.Drawing.Point(591, 90);
            this.txtQty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(67, 26);
            this.txtQty.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 68);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 18);
            this.label7.TabIndex = 353;
            this.label7.Text = "Item";
            // 
            // txtItem
            // 
            this.txtItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem.Location = new System.Drawing.Point(5, 90);
            this.txtItem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtItem.Name = "txtItem";
            this.txtItem.Size = new System.Drawing.Size(283, 26);
            this.txtItem.TabIndex = 2;
            this.txtItem.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtItem_MouseClick);
            // 
            // txtVehicle
            // 
            this.txtVehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicle.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicle.Location = new System.Drawing.Point(961, 37);
            this.txtVehicle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.Size = new System.Drawing.Size(110, 26);
            this.txtVehicle.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(961, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 198;
            this.label1.Text = "Vehicle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 196;
            this.label2.Text = "Doc Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(428, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 193;
            this.label3.Text = "Party Name";
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(5, 17);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(49, 18);
            this.Phone.TabIndex = 195;
            this.Phone.Text = "DocNo";
            // 
            // txtGrn
            // 
            this.txtGrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGrn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrn.Location = new System.Drawing.Point(5, 37);
            this.txtGrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtGrn.Name = "txtGrn";
            this.txtGrn.Size = new System.Drawing.Size(140, 26);
            this.txtGrn.TabIndex = 194;
            // 
            // dtpGrnDate
            // 
            this.dtpGrnDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGrnDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGrnDate.Location = new System.Drawing.Point(146, 37);
            this.dtpGrnDate.Name = "dtpGrnDate";
            this.dtpGrnDate.Size = new System.Drawing.Size(122, 26);
            this.dtpGrnDate.TabIndex = 197;
            // 
            // txtSupplerName
            // 
            this.txtSupplerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSupplerName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplerName.Location = new System.Drawing.Point(425, 37);
            this.txtSupplerName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSupplerName.Name = "txtSupplerName";
            this.txtSupplerName.Size = new System.Drawing.Size(270, 26);
            this.txtSupplerName.TabIndex = 1;
            this.txtSupplerName.Click += new System.EventHandler(this.txtSupplerName_Click);
            this.txtSupplerName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtSupplerName_MouseClick);
            this.txtSupplerName.TextChanged += new System.EventHandler(this.txtSupplerName_TextChanged);
            this.txtSupplerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // txtTaxValue
            // 
            this.txtTaxValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxValue.Location = new System.Drawing.Point(1099, 372);
            this.txtTaxValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTaxValue.Name = "txtTaxValue";
            this.txtTaxValue.Size = new System.Drawing.Size(110, 26);
            this.txtTaxValue.TabIndex = 389;
            this.txtTaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(479, 523);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(28, 18);
            this.label21.TabIndex = 380;
            this.label21.Text = "Tax";
            this.label21.Visible = false;
            // 
            // cboTax
            // 
            this.cboTax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTax.FormattingEnabled = true;
            this.cboTax.Location = new System.Drawing.Point(517, 521);
            this.cboTax.Name = "cboTax";
            this.cboTax.Size = new System.Drawing.Size(108, 26);
            this.cboTax.TabIndex = 14;
            this.cboTax.Visible = false;
            this.cboTax.SelectedIndexChanged += new System.EventHandler(this.cboTax_SelectedIndexChanged);
            // 
            // DataGridTax
            // 
            this.DataGridTax.AllowUserToAddRows = false;
            this.DataGridTax.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridTax.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTax.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridTax.EnableHeadersVisualStyles = false;
            this.DataGridTax.Location = new System.Drawing.Point(493, 349);
            this.DataGridTax.Name = "DataGridTax";
            this.DataGridTax.RowHeadersVisible = false;
            this.DataGridTax.Size = new System.Drawing.Size(523, 114);
            this.DataGridTax.TabIndex = 423;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnPackingSlipPrint);
            this.panadd.Controls.Add(this.panNumbers);
            this.panadd.Controls.Add(this.btnFstBack);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnFstNext);
            this.panadd.Controls.Add(this.btnNext);
            this.panadd.Controls.Add(this.chckActive);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnPrint);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Controls.Add(this.btnDelete);
            this.panadd.Location = new System.Drawing.Point(12, 575);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1220, 34);
            this.panadd.TabIndex = 211;
            // 
            // btnPackingSlipPrint
            // 
            this.btnPackingSlipPrint.BackColor = System.Drawing.Color.White;
            this.btnPackingSlipPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPackingSlipPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPackingSlipPrint.Image")));
            this.btnPackingSlipPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPackingSlipPrint.Location = new System.Drawing.Point(1105, 2);
            this.btnPackingSlipPrint.Name = "btnPackingSlipPrint";
            this.btnPackingSlipPrint.Size = new System.Drawing.Size(108, 30);
            this.btnPackingSlipPrint.TabIndex = 425;
            this.btnPackingSlipPrint.Text = "Print Pk Slip";
            this.btnPackingSlipPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPackingSlipPrint.UseVisualStyleBackColor = false;
            this.btnPackingSlipPrint.Click += new System.EventHandler(this.btnPackingSlipPrint_Click);
            // 
            // panNumbers
            // 
            this.panNumbers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panNumbers.Controls.Add(this.lblFrom);
            this.panNumbers.Controls.Add(this.lblCount);
            this.panNumbers.Controls.Add(this.flowLayoutPanel3);
            this.panNumbers.Controls.Add(this.flowLayoutPanel2);
            this.panNumbers.Controls.Add(this.flowLayoutPanel1);
            this.panNumbers.Location = new System.Drawing.Point(66, 5);
            this.panNumbers.Name = "panNumbers";
            this.panNumbers.Size = new System.Drawing.Size(74, 24);
            this.panNumbers.TabIndex = 214;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.ForeColor = System.Drawing.Color.Black;
            this.lblFrom.Location = new System.Drawing.Point(4, 3);
            this.lblFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(15, 16);
            this.lblFrom.TabIndex = 163;
            this.lblFrom.Text = "1";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(27, 3);
            this.lblCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(29, 16);
            this.lblCount.TabIndex = 162;
            this.lblCount.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFstBack
            // 
            this.btnFstBack.BackColor = System.Drawing.Color.White;
            this.btnFstBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFstBack.FlatAppearance.BorderSize = 0;
            this.btnFstBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFstBack.Image = ((System.Drawing.Image)(resources.GetObject("btnFstBack.Image")));
            this.btnFstBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFstBack.Location = new System.Drawing.Point(9, 5);
            this.btnFstBack.Name = "btnFstBack";
            this.btnFstBack.Size = new System.Drawing.Size(19, 26);
            this.btnFstBack.TabIndex = 213;
            this.btnFstBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFstBack.UseVisualStyleBackColor = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(38, 5);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 26);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnFstNext
            // 
            this.btnFstNext.BackColor = System.Drawing.Color.White;
            this.btnFstNext.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFstNext.FlatAppearance.BorderSize = 0;
            this.btnFstNext.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFstNext.Image = ((System.Drawing.Image)(resources.GetObject("btnFstNext.Image")));
            this.btnFstNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFstNext.Location = new System.Drawing.Point(178, 5);
            this.btnFstNext.Name = "btnFstNext";
            this.btnFstNext.Size = new System.Drawing.Size(19, 26);
            this.btnFstNext.TabIndex = 211;
            this.btnFstNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFstNext.UseVisualStyleBackColor = false;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.White;
            this.btnNext.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNext.Location = new System.Drawing.Point(150, 5);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(18, 26);
            this.btnNext.TabIndex = 210;
            this.btnNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext.UseVisualStyleBackColor = false;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.BackColor = System.Drawing.Color.White;
            this.chckActive.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckActive.Location = new System.Drawing.Point(302, 6);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 187;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(756, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(87, 30);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(1023, 2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(83, 30);
            this.btnPrint.TabIndex = 215;
            this.btnPrint.Text = "Print DC";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(967, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 30);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(1025, 3);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 29);
            this.btnaddrcan.TabIndex = 213;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(842, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(58, 30);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(899, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(69, 30);
            this.btnDelete.TabIndex = 186;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(953, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(71, 30);
            this.btnsave.TabIndex = 212;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridYarnIssue);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(11, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1221, 567);
            this.grFront.TabIndex = 390;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(7, 15);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(1208, 26);
            this.txtSearch.TabIndex = 5;
            // 
            // DataGridYarnIssue
            // 
            this.DataGridYarnIssue.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridYarnIssue.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridYarnIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnIssue.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.DataGridYarnIssue.EnableHeadersVisualStyles = false;
            this.DataGridYarnIssue.Location = new System.Drawing.Point(7, 42);
            this.DataGridYarnIssue.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridYarnIssue.Name = "DataGridYarnIssue";
            this.DataGridYarnIssue.ReadOnly = true;
            this.DataGridYarnIssue.RowHeadersVisible = false;
            this.DataGridYarnIssue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridYarnIssue.Size = new System.Drawing.Size(1207, 522);
            this.DataGridYarnIssue.TabIndex = 0;
            // 
            // FrmyarnIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1244, 614);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Name = "FrmyarnIssue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yarn Issue";
            this.Load += new System.EventHandler(this.FrmyarnIssue_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmyarnIssue_KeyDown);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItemReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridBeem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTax)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panNumbers.ResumeLayout(false);
            this.panNumbers.PerformLayout();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnIssue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panNumbers;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFstBack;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnFstNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.DataGridView DataGridItem;
        private System.Windows.Forms.Button btnBeamOk;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtBags;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBeam;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMIllName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtItem;
        private System.Windows.Forms.TextBox txtVehicle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtGrn;
        private System.Windows.Forms.DateTimePicker dtpGrnDate;
        private System.Windows.Forms.TextBox txtSupplerName;
        private System.Windows.Forms.DataGridView DataGridBeem;
        private System.Windows.Forms.Button btnRecOk;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtoutoutqty;
        private System.Windows.Forms.ComboBox cbosGReturnItem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtoutputitem;
        private System.Windows.Forms.DataGridView DataGridItemReceived;
        private System.Windows.Forms.TextBox txtTaxValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtRoundOff;
        private System.Windows.Forms.TextBox txtNetValue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txttotalValue;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cboTax;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtBasic;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DataGridView DataGridYarnIssue;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtpackingSlipNumber;
        private System.Windows.Forms.TextBox txtDispatchTo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbFor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTranspoter;
        private System.Windows.Forms.TextBox txtLRNo;
        private System.Windows.Forms.TextBox txtEwayBill;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridView DataGridTax;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnPackingSlipPrint;
    }
}