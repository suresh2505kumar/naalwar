﻿using Syncfusion.WinForms.DataGrid;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmDCLedger : Form
    {
        public FrmDCLedger()
        {
            InitializeComponent();
            sfDataGrid1.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();

        private void FrmDCLedger_Load(object sender, EventArgs e)
        {
            GetParty();
        }

        protected void GetParty()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                DataView dv = dt.DefaultView;
                dv.Sort = "Name";
                DataTable sortedDT = dv.ToTable();

                CmbCustomer.DataSource = null;
                CmbCustomer.DisplayMember = "Name";
                CmbCustomer.ValueMember = "Uid";
                CmbCustomer.DataSource = sortedDT;
                System.Data.DataRow row = sortedDT.NewRow();
                row[0] = "0";
                row[1] = "ALL";
                row[2] = "ALL";
                sortedDT.Rows.InsertAt(row, 0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsDCSummary = new DataSet();
                string Query = @"Select D.Name CustomerName,C.Name PlaceOfSupply,b.SortNo,SUm(b.Meters) Mtrs,SUM(b.Wght) Wght 
	                            from dispatchM a 
	                            inner join dispatchD b on a.ID = b.DispatchUid 
	                            inner join PartyM C on a.pofSup = C.Uid
	                            inner join PartyM D on a.PartyUid = d.Uid
	                            Where a.PartyUid = " + CmbCustomer.SelectedValue + " and a.DocDate between '" + Convert.ToDateTime(DtpFromDate.Text).ToString("yyyy-MM-dd") + "' and'" + Convert.ToDateTime(DtpToDate.Text).ToString("yyyy-MM-dd") + "' group by D.Name,C.Name,b.SortNo  order by D.Name,C.Name,b.SortNo";


                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                DataTable dt1 = dt.DefaultView.ToTable(true, "PlaceOfSupply");

                dsDCSummary.Tables.Add(dt);
                SqlDataAdapter da = new SqlDataAdapter();
                CmbLocation.Items.Clear();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                }

                string SortNo = string.Empty;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        SortNo = "\'" + dt.Rows[i]["SortNo"].ToString() + "\'";
                    }
                    else
                    {
                        SortNo += "," + "\'" + dt.Rows[i]["SortNo"].ToString() + "\'";
                    }
                }

                string DetQuery = @"Select a.ID,a.DocNo,a.DocDate,b.SortNo,b.Meters,b.Wght,a.DispatchSlipUid 
	                            from dispatchM a 
	                            inner join dispatchD b on a.ID = b.DispatchUid 
	                            inner join PartyM C on a.pofSup = C.Uid
	                            inner join PartyM D on a.PartyUid = d.Uid
	                            Where a.PartyUid = " + CmbCustomer.SelectedValue + " and a.DocDate between '" + Convert.ToDateTime(DtpFromDate.Text).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(DtpToDate.Text).ToString("yyyy-MM-dd") + "' and b.SortNo in(" + SortNo + ") Order by a.DispatchSlipUid";

                DataTable dtDet = db.GetDataWithoutParam(CommandType.Text, DetQuery, conn);
                dsDCSummary.Tables.Add(dtDet);

                string id = string.Empty;

                for (int i = 0; i < dtDet.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        id = dtDet.Rows[i]["DispatchSlipUid"].ToString();
                    }
                    else
                    {
                        id += "," + dtDet.Rows[i]["DispatchSlipUid"].ToString();
                    }
                }

                string DetPacking = @"Select a.Uid DispatchSlipUid,a.DispatchNo,b.SortNo,b.RollNo,b.Meter,b.Weight 
                                    from DispatchSlipM a 
                                    inner join DispatchSlipD b on a.Uid = b.DispatchSlipMuid
                                    where a.uid in(" + id + ")";
                DataTable dtPacking = db.GetDataWithoutParam(CommandType.Text, DetPacking, conn);
                dsDCSummary.Tables.Add(dtPacking);
                dsDCSummary.Relations.Add(new DataRelation("Sort", dsDCSummary.Tables[0].Columns["SortNo"], dsDCSummary.Tables[1].Columns["SortNo"]));
                //dsDCSummary.Relations.Add(new DataRelation("Sort1", dsDCSummary.Tables[1].Columns["SortNo"], dsDCSummary.Tables[1].Columns["SortNo"]));


                //dsDCSummary.Relations.Add(new DataRelation("Packing", dsDCSummary.Tables[1].Columns["DispatchSlipUid"], dsDCSummary.Tables[2].Columns["DispatchSlipUid"]));
                //dsDCSummary.Relations.Add(Relation2);
                //DataSet dsMain = new DataSet();
                //DataRelation newRelation = new DataRelation("processData",
                //    new DataColumn[] { dt.Columns["EmpName"] },
                //    new DataColumn[] { dtDet.Columns["EmpName"] }
                //);
                //dsMain.Relations.Add(newRelation);

                //sfDataGrid1.AutoGenerateRelations = true;
                ////sfDataGrid1.DataSource = dsDCSummary;
                sfDataGrid1.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCellsExceptHeader;
                sfDataGrid1.AutoGenerateRelations = true;
                sfDataGrid1.DataSource = dsDCSummary.Tables[0];

                // GridViewDefinition for DataGrid
                //var gridViewDefinition = new GridViewDefinition();
                //gridViewDefinition.RelationalColumn = "Sort";
                //gridViewDefinition.DataGrid = new SfDataGrid() { Name = "FirstLevelNestedGrid", AutoGenerateColumns = true };
                //sfDataGrid1.DetailsViewDefinitions.Add(gridViewDefinition);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
