﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmSortIdentification : Form
    {
        public FrmSortIdentification()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsSort = new BindingSource();
        SqlConnection connection = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        int Id = 0; int mode = 0;
        int SId = 0;
        int SelectedId = 0;
        BindingSource bsYarnWarp = new BindingSource();
        BindingSource bsYarnWept = new BindingSource();

        private void FrmSortIdentification_Load(object sender, EventArgs e)
        {
            txtSortName.Enabled = true;
            LoadDataGridSortId();
            LoadButtons(0);
            Uom();
            LoadTax();
        }

        protected DataTable GetTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = GetTax();
                cmbPercentage.DataSource = null;
                cmbPercentage.DisplayMember = "GeneralName";
                cmbPercentage.ValueMember = "TaxPer";
                cmbPercentage.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void Uom()
        {
            string qur = "select Generalname as Uom,uid from generalm where typem_uid =1";
            SqlCommand cmd = new SqlCommand(qur, connection);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            CmbUom.DataSource = null;
            CmbUom.DisplayMember = "Uom";
            CmbUom.ValueMember = "uid";
            CmbUom.DataSource = tab;
        }

        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    GrBack.Visible = false;
                    GrFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    btnPrint.Visible = false;
                }
                else if (id == 1)
                {
                    GrBack.Visible = true;
                    GrFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    btnPrint.Visible = false;
                }
                else if (id == 2)
                {
                    GrBack.Visible = true;
                    GrFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    btnPrint.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                LoadButtons(1);
                txtSortName.Enabled = true;
                txtSampleNo.Tag = "0";
                mode = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                decimal SortUid = 0;
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, "Select Uid from SortDet Where SortNo='" + txtSortName.Text + "'", connection);
                if (dataTable.Rows.Count == 0)
                {
                    if (btnSave.Text == "Save")
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@sortno",txtSortName.Text),
                            new SqlParameter("@rollno",DBNull.Value),
                            new SqlParameter("@design",DBNull.Value),
                            new SqlParameter("@WarpCount",DBNull.Value),
                            new SqlParameter("@WeptCount",DBNull.Value),
                            new SqlParameter("@EPI",DBNull.Value),
                            new SqlParameter("@PPI",DBNull.Value),
                            new SqlParameter("@GreyWidth",DBNull.Value),
                            new SqlParameter("@TotalEnds",DBNull.Value),
                            new SqlParameter("@Reed",DBNull.Value),
                            new SqlParameter("@ReedSpace",DBNull.Value),
                            new SqlParameter("@PickWheel",DBNull.Value),
                            new SqlParameter("@CrimpWarp",DBNull.Value),
                            new SqlParameter("@CrimpWeft",DBNull.Value),
                            new SqlParameter("@WTMtr",DBNull.Value),
                            new SqlParameter("@GSM",DBNull.Value),
                            new SqlParameter("@GTLEN",DBNull.Value),
                            new SqlParameter("@GTwei",DBNull.Value),
                            new SqlParameter("@active",1),
                            new SqlParameter("@Width",DBNull.Value),
                            new SqlParameter("@WarpId",DBNull.Value),
                            new SqlParameter("@WeptId",DBNull.Value),
                            new SqlParameter("@mode",1),
                            new SqlParameter("@uid","0"),
                            new SqlParameter("@HSNCode",DBNull.Value),
                            new SqlParameter("@GSTPercentage",DBNull.Value),
                            new SqlParameter("@SellingPrice",DBNull.Value),
                            new SqlParameter("@ItemType",1071),
                            new SqlParameter("@SellingPriceMtr",DBNull.Value),
                            new SqlParameter("@ReturnId",SqlDbType.Int)
                        };
                        para[29].Direction = ParameterDirection.Output;
                        SortUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SORTADD", para, connection, 29);
                        txtPPI.Tag = SortUid;
                    }
                }
                else
                {
                    SortUid = Convert.ToDecimal(dataTable.Rows[0]["Uid"].ToString());
                    txtPPI.Tag = SortUid;
                }
                if (txtSampleNo.Text != string.Empty && txtSortName.Text != string.Empty && txtSize.Text != string.Empty && txtWeight.Text != string.Empty)
                {
                    if(txtNoOfPanel.Text == string.Empty)
                    {
                        txtNoOfPanel.Text = "0";
                    }
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@SampleNo",txtSampleNo.Text),
                        new SqlParameter("@SortName",txtSortName.Text),
                        new SqlParameter("@Size",txtSize.Text),
                        new SqlParameter("@Wght",txtWeight.Text),
                        new SqlParameter("@UOM",CmbUom.Text),
                        new SqlParameter("@Reed",txtReed.Text),
                        new SqlParameter("@ReedSpace",txtReedSpace.Text),
                        new SqlParameter("@PPI", Convert.ToDecimal(txtPPI.Text)),
                        new SqlParameter("@GroundCount",txtGroundCount.Text),
                        new SqlParameter("@GroundEnds",txtGroundEnds.Text),
                        new SqlParameter("@FileCount",txtFileCount.Text),
                        new SqlParameter("@FileEnds",txtFileCountEnds.Text),
                        new SqlParameter("@WeptCount",txtWeptCount.Text),
                        new SqlParameter("@GrayWght", Convert.ToDecimal(txtGrayWeight.Text)),
                        new SqlParameter("@GraySize",txtGraySize.Text),
                        new SqlParameter("@LSovage",txtLSolvage.Text),
                        new SqlParameter("@RSolvage",txtRSolvage.Text),
                        new SqlParameter("@Body",txtBody.Text),
                        new SqlParameter("@Uid",txtSampleNo.Tag),
                        new SqlParameter("@ReturnID",SqlDbType.Decimal),
                        new SqlParameter("@SortId",txtPPI.Tag),
                        new SqlParameter("@NoOfPanels",Convert.ToDecimal(txtNoOfPanel.Text)),
                        new SqlParameter("@HSNCode",txtHSNCode.Text),
                        new SqlParameter("@GSTPercentage",cmbPercentage.SelectedValue),
                        new SqlParameter("@PileReadOpen",txtPileReadOpen.Text),
                        new SqlParameter("@DrawingNo",txtDrawingNo.Text),
                        new SqlParameter("@RatePrMtr",txtSellingPriceMtr.Text),
                        new SqlParameter("@WeptCount1",txtWeptCount1.Text),
                        new SqlParameter("@TotalPicks",Convert.ToDecimal(txtPicks.Text)),
                    };
                    sqlParameters[19].Direction = ParameterDirection.Output;
                    int ReturnUid= db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_NaalwarSort", sqlParameters, connection,19);
                    //string Query = "Delete from NaalwarSortList Where SortUid =" + ReturnUid + " and WpType=" + Id + "";
                    //db.ExecuteNonQuery(CommandType.Text, Query, connection);
                    for (int i = 0; i < DataGridSortIdentification.Rows.Count; i++)
                    {
                        SqlParameter[] Parameters = {
                            new SqlParameter("@SortUid",ReturnUid),
                            new SqlParameter("@WP1",DataGridSortIdentification.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@Wp2",DataGridSortIdentification.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@WpType",DataGridSortIdentification.Rows[i].Cells[2].Value.ToString())
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_NaalwarSortList", Parameters, connection);
                    }
                    ClearControls();
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //this.Close();
                LoadButtons(0);
                LoadDataGridSortId();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void ClearControls()
        {
            try
            {
                txtSampleNo.Text = string.Empty;
                txtSortName.Text = string.Empty;
                txtSize.Text = string.Empty;
                txtWeight.Text = string.Empty;
                CmbUom.SelectedIndex = -1;
                txtReed.Text = string.Empty;
                txtReedSpace.Text = string.Empty;
                txtPPI.Text = string.Empty;
                txtGroundCount.Text = string.Empty;
                txtGroundEnds.Text = string.Empty;
                txtFileCount.Text = string.Empty;
                txtFileCountEnds.Text = string.Empty;
                txtWeptCount.Text = string.Empty;
                txtGrayWeight.Text = string.Empty;
                txtGraySize.Text = string.Empty;
                txtLSolvage.Text = string.Empty;
                txtRSolvage.Text = string.Empty;
                txtBody.Text = string.Empty;
                txtNoOfPanel.Text = string.Empty;
                txtHSNCode.Text = string.Empty;
                txtPileReadOpen.Text = string.Empty;
                txtDrawingNo.Text = string.Empty;
                txtWeptCount1.Text = string.Empty;
                txtSellingPriceMtr.Text = string.Empty;
                txtPicks.Text = string.Empty;
                CmbPattren.SelectedIndex = -1;
                DataGridSortIdentification.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void GetNaalwarSort(decimal SortNo)
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@SortNo", SortNo) };
                DataSet data = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetNaalwarSortEdit", sqlParameters, connection);
                DataTable dt = data.Tables[0];
                DataTable dt1 = data.Tables[1];
                if (dt.Rows.Count > 0)
                {
                    txtSampleNo.Text = dt.Rows[0]["SampleNo"].ToString();
                    txtSortName.Text = dt.Rows[0]["SortName"].ToString();
                    txtSortName.Tag = dt.Rows[0]["Uid"].ToString();
                    txtSize.Text = dt.Rows[0]["Size"].ToString();
                    txtWeight.Text = dt.Rows[0]["Wght"].ToString();
                    CmbUom.Text = dt.Rows[0]["UOM"].ToString();
                    txtReed.Text = dt.Rows[0]["Reed"].ToString();
                    txtReedSpace.Text = dt.Rows[0]["ReedSpace"].ToString();
                    txtPPI.Text = dt.Rows[0]["PPI"].ToString();
                    txtPPI.Tag = dt.Rows[0]["SortId"].ToString();
                    txtGroundCount.Text = dt.Rows[0]["GroundCount"].ToString();
                    txtGroundEnds.Text = dt.Rows[0]["GroundEnds"].ToString();
                    txtFileCount.Text = dt.Rows[0]["FileCount"].ToString();
                    txtFileCountEnds.Text = dt.Rows[0]["FileEnds"].ToString();
                    txtWeptCount.Text = dt.Rows[0]["WeptCount"].ToString();
                    txtGrayWeight.Text = dt.Rows[0]["GrayWght"].ToString();
                    txtGraySize.Text = dt.Rows[0]["GraySize"].ToString();
                    txtLSolvage.Text = dt.Rows[0]["LSovage"].ToString();
                    txtRSolvage.Text = dt.Rows[0]["RSolvage"].ToString();
                    txtBody.Text = dt.Rows[0]["Body"].ToString();
                    txtSampleNo.Tag = dt.Rows[0]["Uid"].ToString();
                    txtWeptCount1.Text = dt.Rows[0]["WeptCount1"].ToString();
                    txtHSNCode.Text = dt.Rows[0]["HSNCode"].ToString();
                    txtPileReadOpen.Text = dt.Rows[0]["PileReadOpen"].ToString();
                    cmbPercentage.SelectedValue = dt.Rows[0]["GSTPercentage"].ToString();
                    txtDrawingNo.Text = dt.Rows[0]["DrawingNo"].ToString();
                    txtSellingPriceMtr.Text = dt.Rows[0]["RatePrMtr"].ToString();
                    txtPicks.Text = dt.Rows[0]["TotalPicks"].ToString();
                    txtNoOfPanel.Text = dt.Rows[0]["NoOfPanels"].ToString();
                }
                else
                {
                    txtSampleNo.Tag = "0";
                }
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                mode = 1;
                int Index = DataGridSortId.SelectedCells[0].RowIndex;
                //txtSampleNo.Text = DataGridSortId.Rows[Index].Cells[1].Value.ToString();
                //txtSortName.Text = DataGridSortId.Rows[Index].Cells[2].Value.ToString();
                GetNaalwarSort(Convert.ToDecimal(DataGridSortId.Rows[Index].Cells[0].Value.ToString()));
                //txtSize.Text = DataGridSortId.Rows[Index].Cells[3].Value.ToString();
                //txtWeight.Text = DataGridSortId.Rows[Index].Cells[4].Value.ToString();
                //CmbUom.Text = DataGridSortId.Rows[Index].Cells[5].Value.ToString();
                //txtReed.Text = DataGridSortId.Rows[Index].Cells[6].Value.ToString();
                //txtReedSpace.Text = DataGridSortId.Rows[Index].Cells[7].Value.ToString();
                //txtPPI.Text = DataGridSortId.Rows[Index].Cells[8].Value.ToString();
                //txtGroundCount.Text = DataGridSortId.Rows[Index].Cells[9].Value.ToString();
                //txtGroundEnds.Text = DataGridSortId.Rows[Index].Cells[10].Value.ToString();
                //txtFileCount.Text = DataGridSortId.Rows[Index].Cells[11].Value.ToString();
                //txtFileCountEnds.Text = DataGridSortId.Rows[Index].Cells[12].Value.ToString();
                //txtWeptCount.Text = DataGridSortId.Rows[Index].Cells[13].Value.ToString();
                //txtGrayWeight.Text = DataGridSortId.Rows[Index].Cells[14].Value.ToString();
                //txtGraySize.Text = DataGridSortId.Rows[Index].Cells[15].Value.ToString();
                //txtLSolvage.Text = DataGridSortId.Rows[Index].Cells[16].Value.ToString();
                //txtRSolvage.Text = DataGridSortId.Rows[Index].Cells[17].Value.ToString();
                //txtBody.Text = DataGridSortId.Rows[Index].Cells[18].Value.ToString();
                CmbPattren.SelectedIndex = 0;
                CmbPattren_SelectedIndexChanged(sender, e);
                LoadButtons(2);
                mode = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbPattren_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(CmbPattren.Text == "WEFT PATTERN")
                {
                    DataGridSortIdentification.DataSource = null;
                    DataGridSortIdentification.AutoGenerateColumns = false;
                    DataGridSortIdentification.ColumnCount = 3;
                    DataGridSortIdentification.Columns[0].Name = "WP1";
                    DataGridSortIdentification.Columns[0].HeaderText = "WP1";
                    DataGridSortIdentification.Columns[0].Width = 120;

                    DataGridSortIdentification.Columns[1].Name = "WP2";
                    DataGridSortIdentification.Columns[1].HeaderText = "WP2";
                    DataGridSortIdentification.Columns[1].Width = 120;

                    DataGridSortIdentification.Columns[2].Name = "WP1";
                    DataGridSortIdentification.Columns[2].HeaderText = "WP1";
                    DataGridSortIdentification.Columns[2].Visible = false;
                    Id = 1;
                }
                else
                {
                    DataGridSortIdentification.DataSource = null;
                    DataGridSortIdentification.AutoGenerateColumns = false;
                    DataGridSortIdentification.ColumnCount = 3;
                    DataGridSortIdentification.Columns[0].Name = "D1";
                    DataGridSortIdentification.Columns[0].HeaderText = "D1";
                    DataGridSortIdentification.Columns[0].Width = 120;

                    DataGridSortIdentification.Columns[1].Name = "D2";
                    DataGridSortIdentification.Columns[1].HeaderText = "D2";
                    DataGridSortIdentification.Columns[1].Width = 120;

                    DataGridSortIdentification.Columns[2].Name = "Type";
                    DataGridSortIdentification.Columns[2].HeaderText = "Type";
                    DataGridSortIdentification.Columns[2].Visible = false;
                    Id = 2;
                }
                if(mode != 1)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@SortNo", txtSampleNo.Tag) };
                    DataSet data = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetNaalwarSortEdit", sqlParameters, connection);
                    DataTable dt1 = data.Tables[1];
                    if (dt1.Rows.Count > 0)
                    {
                        DataGridSortIdentification.Rows.Clear();
                        DataTable dt2 = dt1.Select("WpType = " + Id + "", null).CopyToDataTable();
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            int Index = DataGridSortIdentification.Rows.Add();
                            DataGridViewRow row = DataGridSortIdentification.Rows[Index];
                            row.Cells[0].Value = dt2.Rows[i]["Wp1"].ToString();
                            row.Cells[1].Value = dt2.Rows[i]["Wp2"].ToString();
                            row.Cells[2].Value = dt2.Rows[i]["WpType"].ToString();
                        }
                    }
                }
               
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridSortId()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetNaalwarSort", connection);
                DataGridSortId.DataSource = null;
                DataGridSortId.AutoGenerateColumns = false;
                DataGridSortId.ColumnCount = 6;
                DataGridSortId.Columns[0].Name = "Uid";
                DataGridSortId.Columns[0].HeaderText = "Uid";
                DataGridSortId.Columns[0].DataPropertyName = "Uid";
                DataGridSortId.Columns[0].Visible = false;

                DataGridSortId.Columns[1].Name = "SampleNo";
                DataGridSortId.Columns[1].HeaderText = "SampleNo";
                DataGridSortId.Columns[1].DataPropertyName = "SampleNo";
                DataGridSortId.Columns[1].Width = 150;

                DataGridSortId.Columns[2].Name = "SortName";
                DataGridSortId.Columns[2].HeaderText = "SortName";
                DataGridSortId.Columns[2].DataPropertyName = "SortName";
                DataGridSortId.Columns[2].Width = 200;

                DataGridSortId.Columns[3].Name = "Size";
                DataGridSortId.Columns[3].HeaderText = "ProductName";
                DataGridSortId.Columns[3].DataPropertyName = "Size";
                DataGridSortId.Columns[3].Width = 150;

                DataGridSortId.Columns[4].Name = "GrayWght";
                DataGridSortId.Columns[4].HeaderText = "GrayWght";
                DataGridSortId.Columns[4].DataPropertyName = "GrayWght";

                DataGridSortId.Columns[5].Name = "GraySize";
                DataGridSortId.Columns[5].HeaderText = "GraySize";
                DataGridSortId.Columns[5].DataPropertyName = "GraySize";
                DataGridSortId.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                int type = 0;
                if(CmbPattren.Text == "WEFT PATTERN")
                {
                    type = 1;
                }
                else
                {
                    type = 2;
                }
                DataTable data = new DataTable();
                if (txtWp1.Text != string.Empty && txtWp2.Text != string.Empty)
                {
                    int Index = DataGridSortIdentification.Rows.Add();
                    DataGridViewRow row = DataGridSortIdentification.Rows[Index];
                    row.Cells[0].Value = txtWp1.Text;
                    row.Cells[1].Value = txtWp2.Text;
                    row.Cells[2].Value = type;
                }
                txtWp1.Text = string.Empty;
                txtWp2.Text = string.Empty;
                txtWp1.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortId_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtWeptCount_Click(object sender, EventArgs e)
        {
            SId = 1;
            int Active = 1;
            SqlParameter[] para = { new SqlParameter("@Active", Active) };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetyarnMaster", para, connection);
            bsYarnWarp.DataSource = dt;
            DataGridCommon.DataSource = null;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.ColumnCount = 2;
            DataGridCommon.Columns[0].Name = "Uid";
            DataGridCommon.Columns[0].HeaderText = "Uid";
            DataGridCommon.Columns[0].DataPropertyName = "YarnId";
            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[1].Name = "YarnName";
            DataGridCommon.Columns[1].HeaderText = "YarnName";
            DataGridCommon.Columns[1].DataPropertyName = "YarnShortName";
            DataGridCommon.Columns[1].Width = 350;
            DataGridCommon.DataSource = bsYarnWarp;
            Point p = Genclass.FindLocation(txtWeptCount);
            grSearch.Location = new Point(p.X - 100, p.Y + 25);
            grSearch.Visible = true;
        }

        private void GrBack_Enter(object sender, EventArgs e)
        {

        }

        private void grSearch_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtFileCount_Click(object sender, EventArgs e)
        {
            SId = 2;
            int Active = 1;
            SqlParameter[] para = { new SqlParameter("@Active", Active) };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetyarnMaster", para, connection);
            bsYarnWarp.DataSource = dt;
            DataGridCommon.DataSource = null;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.ColumnCount = 2;
            DataGridCommon.Columns[0].Name = "Uid";
            DataGridCommon.Columns[0].HeaderText = "Uid";
            DataGridCommon.Columns[0].DataPropertyName = "YarnId";
            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[1].Name = "YarnName";
            DataGridCommon.Columns[1].HeaderText = "YarnName";
            DataGridCommon.Columns[1].DataPropertyName = "YarnShortName";
            DataGridCommon.Columns[1].Width = 350;
            DataGridCommon.DataSource = bsYarnWarp;
            Point p = Genclass.FindLocation(txtFileCount);
            grSearch.Location = new Point(p.X -100, p.Y + 25);
            grSearch.Visible = true;
        }

        private void txtGroundCount_Click(object sender, EventArgs e)
        {
            SId = 3;
            int Active = 1;
            SqlParameter[] para = { new SqlParameter("@Active", Active) };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetyarnMaster", para, connection);
            bsYarnWarp.DataSource = dt;
            DataGridCommon.DataSource = null;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.ColumnCount = 2;
            DataGridCommon.Columns[0].Name = "Uid";
            DataGridCommon.Columns[0].HeaderText = "Uid";
            DataGridCommon.Columns[0].DataPropertyName = "YarnId";
            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[1].Name = "YarnName";
            DataGridCommon.Columns[1].HeaderText = "YarnName";
            DataGridCommon.Columns[1].DataPropertyName = "YarnShortName";
            DataGridCommon.Columns[1].Width = 350;
            DataGridCommon.DataSource = bsYarnWarp;
            Point p = Genclass.FindLocation(txtGroundCount);
            grSearch.Location = new Point(p.X, p.Y - 300);
            grSearch.Visible = true;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (SId == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtWeptCount.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtWeptCount.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if(SId == 2)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtFileCount.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFileCount.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (SId == 3)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtGroundCount.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtGroundCount.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtWeptCount1.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtWeptCount1.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtFileCount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    if (SId == 2)
                    {
                        string f = txtFileCount.Text.Replace("[", "");
                        bsYarnWarp.Filter = string.Format("YarnShortName Like '%{0}%'", f);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtGroundCount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    if (SId == 3)
                    {
                        string f = txtGroundCount.Text.Replace("[", "");
                        bsYarnWarp.Filter = string.Format("YarnShortName Like '%{0}%'", f);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeptCount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    if (SId == 1)
                    {
                        string f = txtWeptCount.Text.Replace("[", "");
                        bsYarnWarp.Filter = string.Format("YarnShortName Like '%{0}%'", f);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeptCount1_Click(object sender, EventArgs e)
        {
            SId = 4;
            int Active = 1;
            SqlParameter[] para = { new SqlParameter("@Active", Active) };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetyarnMaster", para, connection);
            bsYarnWarp.DataSource = dt;
            DataGridCommon.DataSource = null;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.ColumnCount = 2;
            DataGridCommon.Columns[0].Name = "Uid";
            DataGridCommon.Columns[0].HeaderText = "Uid";
            DataGridCommon.Columns[0].DataPropertyName = "YarnId";
            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[1].Name = "YarnName";
            DataGridCommon.Columns[1].HeaderText = "YarnName";
            DataGridCommon.Columns[1].DataPropertyName = "YarnShortName";
            DataGridCommon.Columns[1].Width = 350;
            DataGridCommon.DataSource = bsYarnWarp;
            Point p = Genclass.FindLocation(txtWeptCount1);
            grSearch.Location = new Point(p.X - 100, p.Y + 25);
            grSearch.Visible = true;
        }

        private void TxtWeptCount1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    if (SId == 4)
                    {
                        string f = txtWeptCount1.Text.Replace("[", "");
                        bsYarnWarp.Filter = string.Format("YarnShortName Like '%{0}%'", f);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (SId == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtWeptCount.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtWeptCount.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (SId == 2)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtFileCount.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFileCount.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (SId == 3)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtGroundCount.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtGroundCount.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtWeptCount1.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtWeptCount1.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortIdentification_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    DialogResult result = MessageBox.Show("do you want delete this record","Information",MessageBoxButtons.YesNo,MessageBoxIcon.Information,MessageBoxDefaultButton.Button2); ;
                    if(result == DialogResult.Yes)
                    {
                        int Index = DataGridSortIdentification.SelectedCells[0].RowIndex;
                        string Query = "Delete from NaalwarSortList Where SortUid = " + txtSortName.Tag + " and Wp1 ='" + DataGridSortIdentification.Rows[Index].Cells[0].Value.ToString() + "' and Wp2='" + DataGridSortIdentification.Rows[Index].Cells[1].Value.ToString() + "'";
                        db.ExecuteNonQuery(CommandType.Text, Query, connection);
                        DataGridSortIdentification.Rows.RemoveAt(Index);
                        DataGridSortIdentification.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
