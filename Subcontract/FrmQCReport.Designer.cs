﻿namespace Naalwar
{
    partial class FrmQCReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQCReport));
            this.grReport = new System.Windows.Forms.GroupBox();
            this.lblMendorRptType = new System.Windows.Forms.Label();
            this.cmbRT = new System.Windows.Forms.ComboBox();
            this.txtMendor = new System.Windows.Forms.TextBox();
            this.lblMendor = new System.Windows.Forms.Label();
            this.lblShift = new System.Windows.Forms.Label();
            this.cmbShift = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.lblQC = new System.Windows.Forms.Label();
            this.cmbQCSt = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpQCDate = new System.Windows.Forms.DateTimePicker();
            this.grReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // grReport
            // 
            this.grReport.Controls.Add(this.lblMendorRptType);
            this.grReport.Controls.Add(this.cmbRT);
            this.grReport.Controls.Add(this.txtMendor);
            this.grReport.Controls.Add(this.lblMendor);
            this.grReport.Controls.Add(this.lblShift);
            this.grReport.Controls.Add(this.cmbShift);
            this.grReport.Controls.Add(this.label3);
            this.grReport.Controls.Add(this.cmbType);
            this.grReport.Controls.Add(this.lblQC);
            this.grReport.Controls.Add(this.cmbQCSt);
            this.grReport.Controls.Add(this.btnPrint);
            this.grReport.Controls.Add(this.label1);
            this.grReport.Controls.Add(this.dtpQCDate);
            this.grReport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grReport.Location = new System.Drawing.Point(11, 3);
            this.grReport.Name = "grReport";
            this.grReport.Size = new System.Drawing.Size(425, 244);
            this.grReport.TabIndex = 0;
            this.grReport.TabStop = false;
            // 
            // lblMendorRptType
            // 
            this.lblMendorRptType.AutoSize = true;
            this.lblMendorRptType.Location = new System.Drawing.Point(16, 124);
            this.lblMendorRptType.Name = "lblMendorRptType";
            this.lblMendorRptType.Size = new System.Drawing.Size(134, 18);
            this.lblMendorRptType.TabIndex = 226;
            this.lblMendorRptType.Text = "Mendor Report Type";
            // 
            // cmbRT
            // 
            this.cmbRT.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbRT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRT.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRT.FormattingEnabled = true;
            this.cmbRT.Items.AddRange(new object[] {
            "Sort Wise",
            "Date Shift Wise",
            "Mendor Wise"});
            this.cmbRT.Location = new System.Drawing.Point(156, 120);
            this.cmbRT.Name = "cmbRT";
            this.cmbRT.Size = new System.Drawing.Size(179, 26);
            this.cmbRT.TabIndex = 225;
            this.cmbRT.SelectedIndexChanged += new System.EventHandler(this.cmbRT_SelectedIndexChanged);
            // 
            // txtMendor
            // 
            this.txtMendor.Location = new System.Drawing.Point(156, 152);
            this.txtMendor.Name = "txtMendor";
            this.txtMendor.Size = new System.Drawing.Size(191, 26);
            this.txtMendor.TabIndex = 224;
            // 
            // lblMendor
            // 
            this.lblMendor.AutoSize = true;
            this.lblMendor.Location = new System.Drawing.Point(53, 155);
            this.lblMendor.Name = "lblMendor";
            this.lblMendor.Size = new System.Drawing.Size(97, 18);
            this.lblMendor.TabIndex = 223;
            this.lblMendor.Text = "Mendor Name";
            // 
            // lblShift
            // 
            this.lblShift.AutoSize = true;
            this.lblShift.Location = new System.Drawing.Point(114, 92);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(36, 18);
            this.lblShift.TabIndex = 222;
            this.lblShift.Text = "Shift";
            // 
            // cmbShift
            // 
            this.cmbShift.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShift.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbShift.FormattingEnabled = true;
            this.cmbShift.Location = new System.Drawing.Point(156, 88);
            this.cmbShift.Name = "cmbShift";
            this.cmbShift.Size = new System.Drawing.Size(179, 26);
            this.cmbShift.TabIndex = 221;
            this.cmbShift.SelectedIndexChanged += new System.EventHandler(this.cmbShift_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(113, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 18);
            this.label3.TabIndex = 220;
            this.label3.Text = "Type";
            // 
            // cmbType
            // 
            this.cmbType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Qc Type",
            "Mendor Wise",
            "Shift Wise"});
            this.cmbType.Location = new System.Drawing.Point(156, 23);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(179, 26);
            this.cmbType.TabIndex = 219;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // lblQC
            // 
            this.lblQC.AutoSize = true;
            this.lblQC.Location = new System.Drawing.Point(94, 123);
            this.lblQC.Name = "lblQC";
            this.lblQC.Size = new System.Drawing.Size(56, 18);
            this.lblQC.TabIndex = 218;
            this.lblQC.Text = "Qc Type";
            // 
            // cmbQCSt
            // 
            this.cmbQCSt.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbQCSt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQCSt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbQCSt.FormattingEnabled = true;
            this.cmbQCSt.Items.AddRange(new object[] {
            "QC Pass",
            "Mending",
            "MQC Pass",
            "ALL"});
            this.cmbQCSt.Location = new System.Drawing.Point(156, 119);
            this.cmbQCSt.Name = "cmbQCSt";
            this.cmbQCSt.Size = new System.Drawing.Size(179, 26);
            this.cmbQCSt.TabIndex = 217;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(156, 187);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 216;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date";
            // 
            // dtpQCDate
            // 
            this.dtpQCDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpQCDate.Location = new System.Drawing.Point(156, 56);
            this.dtpQCDate.Name = "dtpQCDate";
            this.dtpQCDate.Size = new System.Drawing.Size(130, 26);
            this.dtpQCDate.TabIndex = 0;
            // 
            // FrmQCReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(446, 259);
            this.Controls.Add(this.grReport);
            this.Name = "FrmQCReport";
            this.Text = "QC Report";
            this.Load += new System.EventHandler(this.FrmQCReport_Load);
            this.grReport.ResumeLayout(false);
            this.grReport.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grReport;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpQCDate;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label lblQC;
        private System.Windows.Forms.ComboBox cmbQCSt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.TextBox txtMendor;
        private System.Windows.Forms.Label lblMendor;
        private System.Windows.Forms.Label lblShift;
        private System.Windows.Forms.ComboBox cmbShift;
        private System.Windows.Forms.Label lblMendorRptType;
        private System.Windows.Forms.ComboBox cmbRT;
    }
}