﻿namespace Naalwar
{
    partial class FrmYarnMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmYarnMaster));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.CmbTax = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtHsnCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbUOM = new System.Windows.Forms.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.txtItemSpec = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cmbItemType = new System.Windows.Forms.ComboBox();
            this.cmbBlended = new System.Windows.Forms.ComboBox();
            this.cmbCtnPc = new System.Windows.Forms.ComboBox();
            this.cmbYarnType = new System.Windows.Forms.ComboBox();
            this.cmbCount = new System.Windows.Forms.ComboBox();
            this.cmbPly = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.chckAc = new System.Windows.Forms.CheckBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.PanelgridNos = new System.Windows.Forms.Panel();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnNxt = new System.Windows.Forms.Button();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.DataGridYarn = new System.Windows.Forms.DataGridView();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.grBack.SuspendLayout();
            this.panadd.SuspendLayout();
            this.PanelgridNos.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarn)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.CmbTax);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.TxtHsnCode);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.cmbUOM);
            this.grBack.Controls.Add(this.btnReset);
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.txtItemSpec);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.txtDescription);
            this.grBack.Controls.Add(this.cmbItemType);
            this.grBack.Controls.Add(this.cmbBlended);
            this.grBack.Controls.Add(this.cmbCtnPc);
            this.grBack.Controls.Add(this.cmbYarnType);
            this.grBack.Controls.Add(this.cmbCount);
            this.grBack.Controls.Add(this.cmbPly);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(4, 0);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(545, 471);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(104, 442);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 18);
            this.label11.TabIndex = 22;
            this.label11.Text = "Tax";
            // 
            // CmbTax
            // 
            this.CmbTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTax.FormattingEnabled = true;
            this.CmbTax.Location = new System.Drawing.Point(144, 438);
            this.CmbTax.Name = "CmbTax";
            this.CmbTax.Size = new System.Drawing.Size(173, 26);
            this.CmbTax.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(63, 408);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "HSN Code";
            // 
            // TxtHsnCode
            // 
            this.TxtHsnCode.Location = new System.Drawing.Point(144, 405);
            this.TxtHsnCode.Name = "TxtHsnCode";
            this.TxtHsnCode.Size = new System.Drawing.Size(173, 26);
            this.TxtHsnCode.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(93, 378);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "UOM";
            // 
            // cmbUOM
            // 
            this.cmbUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUOM.FormattingEnabled = true;
            this.cmbUOM.Location = new System.Drawing.Point(144, 373);
            this.cmbUOM.Name = "cmbUOM";
            this.cmbUOM.Size = new System.Drawing.Size(173, 26);
            this.cmbUOM.TabIndex = 17;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(473, 331);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(67, 31);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(340, 378);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 15;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // txtItemSpec
            // 
            this.txtItemSpec.Location = new System.Drawing.Point(144, 331);
            this.txtItemSpec.Name = "txtItemSpec";
            this.txtItemSpec.ReadOnly = true;
            this.txtItemSpec.Size = new System.Drawing.Size(328, 30);
            this.txtItemSpec.TabIndex = 7;
            this.txtItemSpec.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 335);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "SPECIFICATION";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(144, 288);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(328, 26);
            this.txtDescription.TabIndex = 0;
            // 
            // cmbItemType
            // 
            this.cmbItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItemType.FormattingEnabled = true;
            this.cmbItemType.Location = new System.Drawing.Point(144, 247);
            this.cmbItemType.Name = "cmbItemType";
            this.cmbItemType.Size = new System.Drawing.Size(166, 26);
            this.cmbItemType.TabIndex = 5;
            this.cmbItemType.SelectedIndexChanged += new System.EventHandler(this.cmbItemType_SelectedIndexChanged);
            // 
            // cmbBlended
            // 
            this.cmbBlended.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBlended.FormattingEnabled = true;
            this.cmbBlended.Location = new System.Drawing.Point(144, 201);
            this.cmbBlended.Name = "cmbBlended";
            this.cmbBlended.Size = new System.Drawing.Size(166, 26);
            this.cmbBlended.TabIndex = 4;
            this.cmbBlended.SelectedIndexChanged += new System.EventHandler(this.cmbBlended_SelectedIndexChanged);
            // 
            // cmbCtnPc
            // 
            this.cmbCtnPc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCtnPc.FormattingEnabled = true;
            this.cmbCtnPc.Location = new System.Drawing.Point(144, 156);
            this.cmbCtnPc.Name = "cmbCtnPc";
            this.cmbCtnPc.Size = new System.Drawing.Size(166, 26);
            this.cmbCtnPc.TabIndex = 3;
            this.cmbCtnPc.SelectedIndexChanged += new System.EventHandler(this.cmbCtnPc_SelectedIndexChanged);
            // 
            // cmbYarnType
            // 
            this.cmbYarnType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYarnType.FormattingEnabled = true;
            this.cmbYarnType.Location = new System.Drawing.Point(144, 107);
            this.cmbYarnType.Name = "cmbYarnType";
            this.cmbYarnType.Size = new System.Drawing.Size(166, 26);
            this.cmbYarnType.TabIndex = 2;
            this.cmbYarnType.SelectedIndexChanged += new System.EventHandler(this.cmbYarnType_SelectedIndexChanged);
            // 
            // cmbCount
            // 
            this.cmbCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCount.FormattingEnabled = true;
            this.cmbCount.Location = new System.Drawing.Point(144, 62);
            this.cmbCount.Name = "cmbCount";
            this.cmbCount.Size = new System.Drawing.Size(166, 26);
            this.cmbCount.TabIndex = 1;
            this.cmbCount.SelectedIndexChanged += new System.EventHandler(this.cmbCount_SelectedIndexChanged);
            // 
            // cmbPly
            // 
            this.cmbPly.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPly.FormattingEnabled = true;
            this.cmbPly.Location = new System.Drawing.Point(144, 19);
            this.cmbPly.Name = "cmbPly";
            this.cmbPly.Size = new System.Drawing.Size(166, 26);
            this.cmbPly.TabIndex = 6;
            this.cmbPly.SelectedIndexChanged += new System.EventHandler(this.cmbPly_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 292);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "ITEM SHORT NAME";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(67, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "ITEM TYPE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(89, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "BLEND";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "CTN/PC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "YARN TYPE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "COUNT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "PLY";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.chckAc);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.PanelgridNos);
            this.panadd.Controls.Add(this.btnFirst);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnLast);
            this.panadd.Controls.Add(this.btnNxt);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Location = new System.Drawing.Point(6, 475);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(545, 36);
            this.panadd.TabIndex = 238;
            // 
            // chckAc
            // 
            this.chckAc.AutoSize = true;
            this.chckAc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckAc.Location = new System.Drawing.Point(212, 8);
            this.chckAc.Name = "chckAc";
            this.chckAc.Size = new System.Drawing.Size(65, 22);
            this.chckAc.TabIndex = 16;
            this.chckAc.Text = "Active";
            this.chckAc.UseVisualStyleBackColor = true;
            this.chckAc.CheckedChanged += new System.EventHandler(this.chckAc_CheckedChanged);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(482, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(338, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 31);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // PanelgridNos
            // 
            this.PanelgridNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PanelgridNos.Controls.Add(this.lblFrom);
            this.PanelgridNos.Controls.Add(this.lblCount);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel3);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel2);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel1);
            this.PanelgridNos.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PanelgridNos.Location = new System.Drawing.Point(52, 2);
            this.PanelgridNos.Name = "PanelgridNos";
            this.PanelgridNos.Size = new System.Drawing.Size(70, 30);
            this.PanelgridNos.TabIndex = 214;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.ForeColor = System.Drawing.Color.Black;
            this.lblFrom.Location = new System.Drawing.Point(4, 4);
            this.lblFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(15, 18);
            this.lblFrom.TabIndex = 163;
            this.lblFrom.Text = "1";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(27, 4);
            this.lblCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(31, 18);
            this.lblCount.TabIndex = 162;
            this.lblCount.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFirst
            // 
            this.btnFirst.BackColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderSize = 0;
            this.btnFirst.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirst.Image = ((System.Drawing.Image)(resources.GetObject("btnFirst.Image")));
            this.btnFirst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFirst.Location = new System.Drawing.Point(6, 2);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(19, 31);
            this.btnFirst.TabIndex = 213;
            this.btnFirst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFirst.UseVisualStyleBackColor = false;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(29, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 31);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnLast
            // 
            this.btnLast.BackColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderSize = 0;
            this.btnLast.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLast.Image = ((System.Drawing.Image)(resources.GetObject("btnLast.Image")));
            this.btnLast.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLast.Location = new System.Drawing.Point(153, 2);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(19, 31);
            this.btnLast.TabIndex = 211;
            this.btnLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLast.UseVisualStyleBackColor = false;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnNxt
            // 
            this.btnNxt.BackColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderSize = 0;
            this.btnNxt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNxt.Image = ((System.Drawing.Image)(resources.GetObject("btnNxt.Image")));
            this.btnNxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNxt.Location = new System.Drawing.Point(131, 2);
            this.btnNxt.Name = "btnNxt";
            this.btnNxt.Size = new System.Drawing.Size(18, 31);
            this.btnNxt.TabIndex = 210;
            this.btnNxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNxt.UseVisualStyleBackColor = false;
            this.btnNxt.Click += new System.EventHandler(this.btnNxt_Click);
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(468, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.btnAddCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(424, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(389, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.DataGridYarn);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(3, 0);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(548, 472);
            this.grFront.TabIndex = 16;
            this.grFront.TabStop = false;
            // 
            // DataGridYarn
            // 
            this.DataGridYarn.AllowUserToAddRows = false;
            this.DataGridYarn.BackgroundColor = System.Drawing.Color.White;
            this.DataGridYarn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarn.Location = new System.Drawing.Point(3, 42);
            this.DataGridYarn.Name = "DataGridYarn";
            this.DataGridYarn.RowHeadersVisible = false;
            this.DataGridYarn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridYarn.Size = new System.Drawing.Size(539, 423);
            this.DataGridYarn.TabIndex = 1;
            this.DataGridYarn.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridYarn_CellMouseClick);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(3, 14);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(539, 26);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // FrmYarnMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(558, 515);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmYarnMaster";
            this.Text = "Yarn Master";
            this.Load += new System.EventHandler(this.FrmYarnMaster_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.PanelgridNos.ResumeLayout(false);
            this.PanelgridNos.PerformLayout();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Panel PanelgridNos;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnNxt;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cmbItemType;
        private System.Windows.Forms.ComboBox cmbBlended;
        private System.Windows.Forms.ComboBox cmbCtnPc;
        private System.Windows.Forms.ComboBox cmbYarnType;
        private System.Windows.Forms.ComboBox cmbCount;
        private System.Windows.Forms.ComboBox cmbPly;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtItemSpec;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DataGridView DataGridYarn;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.CheckBox chckAc;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbUOM;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtHsnCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CmbTax;
    }
}