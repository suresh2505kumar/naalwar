﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;


namespace Subcontract
{
    public partial class FrmStirring : Form
    {
        public FrmStirring()
        {
            InitializeComponent();
        }

     
        int mode = 0;
        SqlCommand cmd;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdocno_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmStirring_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            mode = 1;
            Frapan.Visible = false;

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtwt.Text == "")
            {
                MessageBox.Show("Plz enter the weight");
                txtwt.Focus();
            }
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "insert into strring (typeid,strrindocno,date,fromtime,totime,shift,bondno,wt,rubbercom,strinslon,strmek,whitrub,yellrub,totwt,actualsoln,evalos,affilt,fillo,fildt,strgivenby,removedby,filterby)values (genclass.lookupid ,'" + txtdocno.Text + "','" + txtdocdate.Text + "','" + Fromtime.Text + "','" + totime.Text + "','" + cboshift.Text + "','" + txtbondno.Text + "','" + txtwt.Text + "','" + txtrubcom.Text + "','" + txtstisoln.Text + "','" + txtmirrmas.Text + "','" + txtwhiterub.Text + "','" + txtyellow.Text + "','" + txttot.Text + "','" + txtactualtot.Text + "','" + txteva.Text + "','" + txtafterfil.Text + "','" + txtfilterloss.Text + "','" + txtfiltime.Text + "','" + txtstigivenby.Text + "','" + txtremovedby.Text + "','" + txtfilby.Text + "')";
            qur.ExecuteNonQuery();
            MessageBox.Show("Record Saved","Save",MessageBoxButtons.OK);
                       
            }
            else
            {
                qur.CommandText = "Update strring set typeid= genclass.lookupid ,strrindocno='" + txtdocno.Text + "',date='" + txtdocdate.Text + "',fromtime='" + Fromtime.Text + "',totime='" + totime.Text + "',shift='" + cboshift.Text + "',bondno='" + txtbondno.Text + "',wt='" + txtwt.Text + "',rubbercom='" + txtrubcom.Text + "',strinslon='" + txtstisoln.Text + "',strmek='" + txtmirrmas.Text + "',whitrub='" + txtwhiterub.Text + "',yellrub='" + txtyellow.Text + "',totwt='" + txttot.Text + "',actualsoln='" + txtactualtot.Text + "',evalos='" + txteva.Text + "',affilt='" + txtafterfil.Text + "',fillo='" + txtfilterloss.Text + "',fildt='" + txtfiltime.Text + "',strgivenby='" + txtstigivenby.Text + "',removedby='" + txtremovedby.Text + "',filterby=,'" + txtfilby.Text + "' where uid=Genclass.editid";
                qur.ExecuteNonQuery();
                MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            }



            conn.Close();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Frapan.Visible = true;
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Frapan.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;
            txttype.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            Genclass.editid = HFGP.Rows[i].Cells[0].Value.ToString();
            Genclass.lookupid = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdocno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtdocdate.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            Fromtime.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            totime.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            cboshift.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtbondno.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txtwt.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtrubcom.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtstisoln.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtmirrmas.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtwhiterub.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtyellow.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            txttot.Text = HFGP.Rows[i].Cells[15].Value.ToString();
            txtactualtot.Text = HFGP.Rows[i].Cells[16].Value.ToString();
            txteva.Text = HFGP.Rows[i].Cells[17].Value.ToString();
            txtafterfil.Text = HFGP.Rows[i].Cells[18].Value.ToString();
            txtfilterloss.Text = HFGP.Rows[i].Cells[19].Value.ToString();
            txtfiltime.Text = HFGP.Rows[i].Cells[20].Value.ToString();
            txtstigivenby.Text = HFGP.Rows[i].Cells[21].Value.ToString();
            txtremovedby.Text = HFGP.Rows[i].Cells[22].Value.ToString();
            txtfilby.Text = HFGP.Rows[i].Cells[23].Value.ToString();
            
        }
    }
}
