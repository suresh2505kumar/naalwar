using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Naalwar
{
    public partial class FrmBill : Form
    {
        public string fo
        {
            get { return txtitemname.Text; }
            set { txtitemname.Text = value; }
        }
        public FrmBill()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
        ReportDocument doc = new ReportDocument();
        string uid = "";
        int mode = 0;
        //double dis9 = 0;
        double dis3 = 0;
        double dis4 = 0;
        //double dd3 = 0;
        //int cell9 = 0;

        DataTable Docno = new DataTable();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        SqlCommand qur = new SqlCommand();
        BindingSource bsMill = new BindingSource();
        BindingSource bsParty = new BindingSource();
        public int SelectId = 0;
        int Fillid = 0;
        private void FrmBill_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            HFGST.RowHeadersVisible = false;
            HFGT.RowHeadersVisible = false;
            dtpfnt.Value = DateTime.Now;
            Genpan.Visible = true;
            Taxpan.Visible = false;
            Editpan.Visible = false;
            Titleterm();
            Titlep();
            Titlegst();
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGST.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGST.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            txtplace.ReadOnly = true;
            mappnl.Visible = false;
            loadtax();
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            Genclass.sum1 = 0;
            chkact.Checked = true;
            LoadTax();
            Loadgrid();
            HFIT.ReadOnly = false;
            cboTax.SelectedIndex = 0;
        }

        public void loadput1()
        {


            txtprice.Text = "";

            if (txtname.Text == "")
            {
                MessageBox.Show("select the party");
                return;
            }
            if (txtpluid.Text == "")
            {
                txtigval.Text = "0";
            }
            if (Genclass.data1 == 1)
            {
                Genclass.strsql = "select itemid,itemname,itemcode,Pono,listid,Qty as Balqty,'' as Invno,isnull(Bqty,0) as Recqty,noofbags from (select a.UId,a.DocNo as Pono,convert(date,a.docdate,102) as Podate,isnull(b.PQty,0)-isnull(sum(e.PQty),0)  as  Qty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid,isnull(bb.qty,0) as bqty,isnull(bb.invno,0) as noofbags from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID = 90 left join Stransactionsplist e on b.Uid=e.refuid and e.doctypeid=100 left join itemm f on b.ItemUId=f.Uid  left join transactionsp  g on e.transactionspuid=g.uid   left join partym h on a.partyuid=h.uid left join PgBatmp bb on b.uid=bb.refid where f.itemname is not null and h.uid=" + txtpuid.Text + " and a.companyid =" + Genclass.data1 + " group  by   a.uid,a.docno,b.pqty,f.ItemName,a.docdate,f.ItemCode,f.uid,b.uid,bb.qty,bb.invno having isnull(b.PQty,0)-isnull(sum(e.PQty),0)>0 ) tab ";
                Genclass.FSSQLSortStr = "itemname";
                Genclass.FSSQLSortStr1 = "itemcode";
                Genclass.FSSQLSortStr2 = "Pono";
            }
            else
            {
                Genclass.strsql = "select itemid,itemcode,itemname,Pono,listid,Qty as Balqty,'' as Invno,isnull(Bqty,0) as Recqty,noofbags from (select a.UId,a.DocNo as Pono,convert(date,a.docdate,102) as Podate,isnull(b.PQty,0)-isnull(sum(e.PQty),0)  as  Qty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid,isnull(bb.qty,0) as bqty,isnull(bb.invno,0) as noofbags  from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID = 90 left join Stransactionsplist e on b.Uid=e.refuid and e.doctypeid=100 left join itemm f on b.ItemUId=f.Uid  left join transactionsp  g on e.transactionspuid=g.uid   left join partym h on a.partyuid=h.uid left join Batmp bb on b.uid=bb.refid where f.itemname is not null and h.uid=" + txtpuid.Text + " and a.companyid =" + Genclass.data1 + " group  by   a.uid,a.docno,b.pqty,f.ItemName,a.docdate,f.ItemCode,f.uid,b.uid,bb.qty,bb.invno having isnull(b.PQty,0)-isnull(sum(e.PQty),0)>0 ) tab ";
                Genclass.FSSQLSortStr = "itemcode";
                Genclass.FSSQLSortStr1 = "itemname";
                Genclass.FSSQLSortStr2 = "Pono";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmbilllookup contc = new Frmbilllookup();
            TabControl tab = (TabControl)contc.Controls["tabC"];
            TabPage tab1 = (TabPage)tab.Controls["tabPage1"];
            DataGridView grid = (DataGridView)tab1.Controls["hfgp"];
            grid.Refresh();
            grid.ColumnCount = tap.Columns.Count;
            grid.Columns[0].Visible = false;
            if (Genclass.data1 == 1)
            {
                grid.Columns[1].Width = 404;
                grid.Columns[2].Width = 218;
                grid.Columns[3].Width = 92;
            }
            else
            {
                grid.Columns[1].Width = 218;
                grid.Columns[2].Width = 404;
                grid.Columns[3].Width = 92;
            }
            grid.Columns[4].Visible = false;
            grid.Columns[5].Width = 80;
            grid.Columns[6].Visible = false;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 80;
            grid.DefaultCellStyle.Font = new Font("calibri", 10);
            grid.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            grid.AutoGenerateColumns = false;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                grid.Columns[Genclass.i].Name = column.ColumnName;
                grid.Columns[Genclass.i].HeaderText = column.ColumnName;
                grid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            grid.DataSource = tap;
            Genclass.Module.Partylistviewcont3A("listid", "GRNNO", "GRNDATE", Genclass.strsql, this, txtdcid, txtgen3, txtgen2, Editpan);
            Genclass.strsql1 = "select distinct uid,Invno,Invdate from (select a.UId,a.DcNo as Invno,convert(date,a.dcdate,102)  as Invdate,isnull(b.PQty,0)-isnull(sum(e.PQty),0) as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from TransactionsP a inner join TransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID = 10 left join Stransactionsplist e on b.Uid=e.refuid and e.doctypeid=100 left join itemm f on b.ItemUId=f.Uid left join partym h on a.partyuid=h.uid  where f.itemname is not null  and h.uid=" + txtpuid.Text + " and a.companyid =" + Genclass.data1 + " group  by   a.uid,a.dcno,b.pqty,e.pqty,f.ItemName,a.dcdate,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(sum(e.PQty),0)>0 ) tab ";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            TabPage tab2 = (TabPage)tab.Controls["tabPage2"];
            DataGridView grid1 = (DataGridView)tab2.Controls["HFGP4"];
            grid1.Refresh();
            grid1.ColumnCount = tap1.Columns.Count;
            grid1.Columns[0].Visible = false;
            if (Genclass.data1 == 1)
            {
                grid1.Columns[1].Width = 560;
                grid1.Columns[2].Width = 315;
            }
            else
            {
                grid1.Columns[1].Width = 315;
                grid1.Columns[2].Width = 560;
            }
            grid1.DefaultCellStyle.Font = new Font("calibri", 10);
            grid1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            grid1.AutoGenerateColumns = false;
            Genclass.i = 0;
            foreach (DataColumn column in tap1.Columns)
            {
                grid1.Columns[Genclass.i].Name = column.ColumnName;
                grid1.Columns[Genclass.i].HeaderText = column.ColumnName;
                grid1.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            grid1.DataSource = tap1;
            if (Genclass.data1 == 1)
            {
                Genclass.strsql2 = "select distinct c.uid,c.itemname,c.itemcode,a.price,isnull(bb.qty,0) as Qty,isnull(bb.invno,0) as noofbangs from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid left join PgBatmp bb on c.uid=bb.itid  where a.eff_to is null and a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and  c.tax<>0 ";
                Genclass.FSSQLSortStr4 = "c.itemname";
                Genclass.FSSQLSortStr5 = "c.itemcode";
            }
            else
            {
                Genclass.strsql2 = "select distinct c.uid,c.itemcode,c.itemname,a.price,isnull(bb.qty,0) As Qty,isnull(bb.invno,0) as noofbangs from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid left join Batmp bb on c.uid=bb.itid  where a.eff_to is null and a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and  c.tax<>0 ";
                Genclass.FSSQLSortStr4 = "c.itemcode";
                Genclass.FSSQLSortStr5 = "c.itemname";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql2, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            TabPage tab3 = (TabPage)tab.Controls["tabPage3"];
            DataGridView grid2 = (DataGridView)tab3.Controls["HFGP2"];
            grid2.Refresh();
            grid2.ColumnCount = tap2.Columns.Count;
            grid2.Columns[0].Visible = false;
            if (Genclass.data1 == 1)
            {
                grid2.Columns[1].Width = 399;
                grid2.Columns[2].Width = 270;
            }
            else
            {
                grid2.Columns[1].Width = 270;
                grid2.Columns[2].Width = 399;
            }
            grid2.Columns[3].Width = 100;
            grid2.Columns[4].Width = 100;
            grid2.Columns[5].Width = 80;
            grid2.DefaultCellStyle.Font = new Font("calibri", 10);
            grid2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            grid2.AutoGenerateColumns = false;
            Genclass.i = 0;
            foreach (DataColumn column in tap2.Columns)
            {
                grid2.Columns[Genclass.i].Name = column.ColumnName;
                grid2.Columns[Genclass.i].HeaderText = column.ColumnName;
                grid2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            grid2.DataSource = tap2;
            if (Genclass.data1 == 1)
            {
                Genclass.strsql3 = "select distinct top 30 uid,c.itemname,c.itemcode,isnull(bb.price,0) as Price,isnull(bb.qty,0) as Qty,isnull(bb.invno,0) as noofbangs FROM itemm c left join PgBatmp bb on c.uid=bb.itid where  active=1 and companyid=" + Genclass.data1 + " and c.tax<>0 ";
                Genclass.FSSQLSortStr6 = "c.itemname";
                Genclass.FSSQLSortStr7 = "c.itemcode";
            }
            else
            {
                Genclass.strsql3 = "select distinct top 30 uid,c.itemcode,c.itemname,isnull(bb.price,0) as Price,isnull(bb.qty,0) as Qty,isnull(bb.invno,0) as noofbangs FROM itemm c left join Batmp bb on c.uid=bb.itid where  active=1 and companyid=" + Genclass.data1 + " and c.tax<>0 ";
                Genclass.FSSQLSortStr6 = "c.itemcode";
                Genclass.FSSQLSortStr7 = "c.itemname";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql3, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            TabPage tab4 = (TabPage)tab.Controls["tabPage4"];
            DataGridView grid3 = (DataGridView)tab4.Controls["HFGP3"];
            grid3.Refresh();
            grid3.ColumnCount = tap3.Columns.Count;
            grid3.Columns[0].Visible = false;
            if (Genclass.data1 == 1)
            {
                grid3.Columns[1].Width = 399;
                grid3.Columns[2].Width = 270;
            }
            else
            {
                grid3.Columns[1].Width = 270;
                grid3.Columns[2].Width = 399;
            }
            grid3.Columns[3].Width = 100;
            grid3.Columns[4].Width = 85;
            grid3.Columns[5].Width = 80;
            grid3.DefaultCellStyle.Font = new Font("calibri", 10);
            grid3.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            grid3.AutoGenerateColumns = false;
            Genclass.i = 0;
            foreach (DataColumn column in tap3.Columns)
            {
                grid3.Columns[Genclass.i].Name = column.ColumnName;
                grid3.Columns[Genclass.i].HeaderText = column.ColumnName;
                grid3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            grid3.DataSource = tap3;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            conn.Close();
        }

        private void loadtax()
        {

            conn.Open();
            string qur = "select a.UId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            //cbotax.DataSource = null;
            //cbotax.DataSource = tab;
            //cbotax.DisplayMember = "GeneralName";
            //cbotax.ValueMember = "uid";
            //cbotax.SelectedIndex = -1;
            conn.Close();



        }
        private void Loadgrid()
        {
            try
            {
                DateTime dte = Convert.ToDateTime(dtpfnt.Text);
                SqlParameter[] parameters = {
                    new SqlParameter("@MonthId",dte.Month),
                    new SqlParameter("@YearID",dte.Year)
                };

                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetBillAccounting", parameters, conn);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = dt.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in dt.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 130;
                HFGP.Columns[2].Width = 130;
                HFGP.Columns[3].Width = 110;
                HFGP.Columns[4].Width = 120;
                HFGP.Columns[5].Width = 580;
                HFGP.Columns[6].Width = 130;
                HFGP.DataSource = dt;
                lblno2.Text = "Of " + dt.Rows.Count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void Titlegst()
        {
            if (txtpluid.Text != "")
            {
                if (txtpluid.Text == "24")
                {
                    HFGST.ColumnCount = 5;

                    HFGST.Columns[0].Name = "CGST%";
                    HFGST.Columns[1].Name = "CGST";

                    HFGST.Columns[2].Name = "SGST%";
                    HFGST.Columns[3].Name = "SGST";
                    HFGST.Columns[4].Name = "Total";


                    HFGST.Columns[0].Width = 60;
                    HFGST.Columns[1].Width = 70;
                    HFGST.Columns[2].Width = 60;
                    HFGST.Columns[3].Width = 70;
                    HFGST.Columns[4].Width = 60;

                }


                else
                {
                    HFGST.ColumnCount = 2;

                    HFGST.Columns[0].Name = "IGST%";
                    HFGST.Columns[1].Name = "IGST";
                    HFGST.Columns[0].Width = 100;
                    HFGST.Columns[1].Width = 150;

                }
            }
        }
        private void Titlep()
        {
            HFIT.ColumnCount = 19;
            HFIT.Columns[0].Name = "Count";
            HFIT.Columns[1].Name = "MillName";
            HFIT.Columns[2].Name = "UoM";
            HFIT.Columns[3].Name = "Price";
            HFIT.Columns[4].Name = "Qty";
            HFIT.Columns[5].Name = "Value";
            HFIT.Columns[6].Name = "NoofBags";
            HFIT.Columns[7].Name = "CGST%";
            HFIT.Columns[8].Name = "CGST";
            HFIT.Columns[9].Name = "SGST%";
            HFIT.Columns[10].Name = "SGST";
            HFIT.Columns[11].Name = "IGST%";
            HFIT.Columns[12].Name = "IGST";
            HFIT.Columns[13].Name = "TaxValue";
            HFIT.Columns[14].Name = "Total";
            HFIT.Columns[15].Name = "HsnCode";
            HFIT.Columns[16].Name = "Pono";
            HFIT.Columns[17].Name = "Itemuid";
            HFIT.Columns[18].Name = "Milluid";


            HFIT.Columns[0].Width = 200;
            HFIT.Columns[1].Width = 170;
            HFIT.Columns[2].Width = 50;
            HFIT.Columns[3].Width = 80;
            HFIT.Columns[4].Width = 80;
            HFIT.Columns[5].Width = 80;
            HFIT.Columns[6].Width = 80;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = true;
            HFIT.Columns[14].Visible = true;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Visible = false;
            HFIT.Columns[17].Visible = false;
            HFIT.Columns[18].Visible = false;
        }

        private void Loadgrid1()
        {
            try
            {
                conn.Open();
                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";
                Genclass.cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFIT.Columns[0].Width = 450;
                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;
                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            Genclass.Module.ClearTextBox(this, Editpan);
            if (Genclass.Dtype == 40)
            {
                Genclass.Module.Gendocno();
                txtgrn.Text = Genclass.ST;
                label15.Visible = false;
                txtitemname.Visible = false;
                label14.Visible = false;
                txtprice.Visible = false;
                label31.Visible = false;
                txtqty.Visible = false;
                label32.Visible = false;
                txtbval.Visible = false;

            }
            else if (Genclass.Dtype == 80)
            {
                Genclass.Module.Gendocno();
                txtgrn.Text = Genclass.ST;
            }
            Editpan.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

        }

        private void loadput()
        {
            DataTable dt = new DataTable();
            dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
            bsParty.DataSource = dt;
            DataGridParty.DataSource = null;
            DataGridParty.AutoGenerateColumns = false;
            DataGridParty.ColumnCount = 3;
            DataGridParty.Columns[0].Name = "Uid";
            DataGridParty.Columns[0].HeaderText = "Uid";
            DataGridParty.Columns[0].DataPropertyName = "Uid";
            DataGridParty.Columns[0].Visible = false;
            DataGridParty.Columns[1].Name = "Name";
            DataGridParty.Columns[1].HeaderText = "Name";
            DataGridParty.Columns[1].DataPropertyName = "Name";
            DataGridParty.Columns[1].Width = 270;
            DataGridParty.Columns[2].Name = "Code";
            DataGridParty.Columns[2].HeaderText = "Code";
            DataGridParty.Columns[2].DataPropertyName = "Code";
            DataGridParty.DataSource = bsParty;
            Point p = FindLocation(txtname);
            PanelSearch.Location = new Point(p.X, p.Y + 25);
            PanelSearch.Visible = true;

        }


        private void btnexit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtper_TextChanged(object sender, System.EventArgs e)
        {
            int val1;
            int val2;
            int val3;
            val1 = Convert.ToInt16(txttaxable.Text);
            val2 = Convert.ToInt16(txtper.Text);
            val3 = (val1 * val2) / 100;
            CalcNetAmt();

        }

        private void CalcNetAmt()
        {
            int totamt;
            totamt = Convert.ToInt16(txttaxable.Text);
            Genclass.strfin = txtNetValue.Text;
        }

        private void Taxduty()
        {
        }

        private void btnsave_Click(object sender, System.EventArgs e)
        {
            try
            {
                int Uid = 0;
                if (txtname.Text != string.Empty || txtSupplyTo.Text != string.Empty || txtgen1.Text != string.Empty)
                {
                    if (HFIT.Rows.Count > 1)
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@Uid","0"),
                            new SqlParameter("@DocTypeid",Genclass.Dtype),
                            new SqlParameter("@Docno",txtgrn.Text),
                            new SqlParameter("@Docdate",Convert.ToDateTime(DTPDOCDT.Text)),
                            new SqlParameter("@Dcno",txtdcno.Text),
                            new SqlParameter("@DcDate",Convert.ToDateTime(dtpdc.Text)),
                            new SqlParameter("@PartyUid",txtname.Tag),
                            new SqlParameter("@Remarks",txtrem.Text),
                            new SqlParameter("@active",1),
                            new SqlParameter("@companyid",1),
                            new SqlParameter("@Netvalue",txtNetValue.Text),
                            new SqlParameter("@YearId","0"),
                            new SqlParameter("@Roff",txtRoundOff.Text),
                            new SqlParameter("@dtpre",dtpdc.Text),
                            new SqlParameter("@dterm",dtpdc.Text),
                            new SqlParameter("@Transp",""),
                            new SqlParameter("@Vehno",""),
                            new SqlParameter("@PlaceUid","0"),
                            new SqlParameter("@OrderDt",dtpdc.Text),
                            new SqlParameter("@ReturnId",SqlDbType.Int),
                            new SqlParameter("@InvNo",txtgen1.Text),
                            new SqlParameter("@SupId",txtSupplyTo.Tag)
                        };
                        parameters[19].Direction = ParameterDirection.Output;
                        Uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BillAccounting", parameters, conn, 19);

                        for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                        {
                            SqlParameter[] sqlparameters = {
                                new SqlParameter("@DocTypeId",Genclass.Dtype),
                                new SqlParameter("@TransactionPuid",Uid),
                                new SqlParameter("@ItemUid",HFIT.Rows[i].Cells[17].Value.ToString()),
                                new SqlParameter("@PQty",HFIT.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@PRate",HFIT.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@BasicValue",HFIT.Rows[i].Cells[5].Value.ToString()),
                                new SqlParameter("@Taxableval",HFIT.Rows[i].Cells[13].Value.ToString()),
                                new SqlParameter("@RefUid","0"),
                                new SqlParameter("@Disp","0"),
                                new SqlParameter("@Disval","0"),
                                new SqlParameter("@Cgstid",HFIT.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@CgstVal",HFIT.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@Sgstid",HFIT.Rows[i].Cells[9].Value.ToString()),
                                new SqlParameter("@Sgstval",HFIT.Rows[i].Cells[10].Value.ToString()),
                                new SqlParameter("@IgstId",HFIT.Rows[i].Cells[11].Value.ToString()),
                                new SqlParameter("@Igstval",HFIT.Rows[i].Cells[12].Value.ToString()),
                                new SqlParameter("@Mode",1),
                                new SqlParameter("@TotalValue",HFIT.Rows[i].Cells[14].Value.ToString()),
                                new SqlParameter("@Addnotes",HFIT.Rows[i].Cells[15].Value.ToString()),
                                new SqlParameter("@NoogBags",HFIT.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@lengthmtr",DBNull.Value),
                                new SqlParameter("@Width",DBNull.Value),
                                new SqlParameter("@UOM",HFIT.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@MillUid",HFIT.Rows[i].Cells[18].Value.ToString())
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BillaccountingDet", sqlparameters, conn);
                        }
                        MessageBox.Show("Record has been saved successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Genclass.Module.ClearTextBox(this, Editpan);
                        HFIT.Rows.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Items are Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtname.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Required Fields can't empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            Genpan.Visible = true;
        }

        protected void ClearControl()
        {
            try
            {
                txtitemname.Text = string.Empty;
                txtprice.Text = string.Empty;
                txtqty.Text = string.Empty;
                txtbasic.Text = string.Empty;
                txtTaxvalue.Text = string.Empty;
                txtHSNCode.Text = string.Empty;
                txtNoBag.Text = string.Empty;
                txtMillName.Text = string.Empty;
                txtbval.Text = string.Empty;
                //txtttot.Text = string.Empty;
                //txtRoundOff.Text = string.Empty;
                //txtNetValue.Text = string.Empty;
                txtigval.Text = string.Empty;
                txtcharges.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ClearControlAll()
        {
            try
            {
                txtitemname.Text = string.Empty;
                txtprice.Text = string.Empty;
                txtqty.Text = string.Empty;
                txtbasic.Text = string.Empty;
                txtTaxvalue.Text = string.Empty;
                txtHSNCode.Text = string.Empty;
                txtNoBag.Text = string.Empty;
                txtname.Text = string.Empty;
                txtrem.Text = string.Empty;
                txtgen1.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cbotax_Click(object sender, System.EventArgs e)
        {
            CalcNetAmt();
        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Taxpan.Visible = true;
            txtexcise.Text = Txttot.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

            addipan.Visible = true;


        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void txtexcise_TextChanged(object sender, EventArgs e)
        {

        }

        private void Taxpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {

        }

        private void Titleterm()
        {

        }

        private void txtterms_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                Genclass.type = 4;
                loadput();
            }
        }

        private void txtterms_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;
        }

        private void txtaddcharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtaddcharge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                Genclass.type = 3;
                loadput();
            }
        }

        private void txttotaddd_TextChanged(object sender, EventArgs e)
        {
            if (txttotaddd.Text == "0" || txttotaddd.Text == "")
            {
                txtNetValue.Text = txtexcise.Text;
            }
            else
            {
                txtNetValue.Text = "0";
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                txtNetValue.Text = val2.ToString();

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();
            mode = 2;

            Genpan.Visible = false;
            Editpan.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            conn.Open();
            {
                Genclass.strsql = " select b.uid,c.ItemName,d.GeneralName as uom,PRate,pqty,BasicValue,ItemUId,b.refuid,disp,disval,Taxableval,Cgstid,Cgstval,Sgstid,Sgstval,totvalue from stransactionsp a inner join Stransactionsplist b on a.uid=b.transactionspuid  left join itemm c  on b.itemuid=c.uid left join generalm d on c.UOM_UId=d.Uid  where b.transactionspuid=" + txtgrnid.Text + " ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                Genclass.sum1 = 0;
                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["Cgstid"].ToString();
                    HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["Cgstval"].ToString();
                    HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["uid"].ToString();
                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[14].Value);
                    Txttot.Text = Genclass.sum1.ToString();
                }
                HFIT.Columns[0].Width = 300;
                HFIT.Columns[1].Visible = false;
                HFIT.Columns[2].Width = 60;
                HFIT.Columns[3].Width = 60;
                HFIT.Columns[4].Width = 100;
                HFIT.Columns[5].Visible = false;
                HFIT.Columns[6].Visible = false;
                HFIT.Columns[9].Visible = false;
                HFIT.Columns[7].Width = 50;
                HFIT.Columns[8].Width = 100;

                HFIT.Columns[10].Width = 50;
                HFIT.Columns[11].Width = 50;
                HFIT.Columns[12].Width = 50;
                HFIT.Columns[13].Width = 50;
                HFIT.Columns[14].Width = 100;
                HFIT.Columns[15].Visible = false;
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                Genclass.sum1 = 0;
                for (int j = 0; j < tap2.Rows.Count; j++)
                {
                    txttotaddd.Text = Genclass.sum1.ToString();
                }
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from STransactionsPTerms a left join generalm b on a.termsuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                for (int g = 0; g < tap3.Rows.Count; g++)
                {
                    var index = HFGT.Rows.Add();
                    HFGT.Rows[index].Cells[0].Value = tap3.Rows[g]["generalname"].ToString();
                    HFGT.Rows[index].Cells[1].Value = tap3.Rows[g]["chargesamount"].ToString();
                    HFGT.Rows[index].Cells[2].Value = tap3.Rows[g]["chargesuid"].ToString();


                }
                conn.Close();

                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                txtNetValue.Text = val2.ToString();
            }

        }



        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            panadd.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            //cell9 = 0;
            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;
            btnBagNoMapping.Visible = true;
            Editpan.Visible = true;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Rows.Clear();
            HFGST.Rows.Clear();
            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            Genclass.sum = 1;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum3 = 0;
            Genclass.sum4 = 0;
            Genclass.sum5 = 0;
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            try
            {
                mode = 2;
                HFIT.Rows.Clear();
                int Index = HFGP.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(HFGP.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] sqlParameters = { new SqlParameter("@Uid", Uid) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_EditBillAccounting", sqlParameters, conn);
                DataTable dtM = ds.Tables[0];
                DataTable dtD = ds.Tables[1];
                if (dtM.Rows.Count > 0)
                {
                    txtname.Text = dtM.Rows[0]["Name"].ToString();
                    txtSupplyTo.Text = dtM.Rows[0]["SupplyTo"].ToString();
                    txtname.Tag = dtM.Rows[0]["PartyUid"].ToString();
                    txtSupplyTo.Tag = dtM.Rows[0]["SupId"].ToString();
                    GetAddress(Convert.ToInt32(txtname.Tag.ToString()), txtname.Name);
                    GetAddress(Convert.ToInt32(txtSupplyTo.Tag.ToString()), txtSupplyTo.Name);
                    txtgrn.Text = dtM.Rows[0]["Docno"].ToString();
                    txtgrn.Tag = dtM.Rows[0]["Uid"].ToString();
                    DTPDOCDT.Text = Convert.ToDateTime(dtM.Rows[0]["Docdate"].ToString()).ToString("dd-MMM-yyyy");
                    txtgen1.Text = dtM.Rows[0]["InvNo"].ToString();
                    dcdate.Text = Convert.ToDateTime(dtM.Rows[0]["dtpre"].ToString()).ToString("dd-MMM-yyyy");
                    txtdcno.Text = dtM.Rows[0]["DcNo"].ToString();
                    txtrem.Text = dtM.Rows[0]["Remarks"].ToString();
                }
                if (dtD.Rows.Count > 0)
                {
                    for (int i = 0; i < dtD.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)HFIT.Rows[0].Clone();
                        row.Cells[0].Value = dtD.Rows[i]["ItemName"].ToString();
                        row.Cells[1].Value = dtD.Rows[i]["MillName"].ToString();
                        row.Cells[2].Value = dtD.Rows[i]["UOM"].ToString();
                        row.Cells[3].Value = dtD.Rows[i]["PRate"].ToString();
                        row.Cells[4].Value = dtD.Rows[i]["PQty"].ToString();
                        row.Cells[5].Value = dtD.Rows[i]["BasicValue"].ToString();
                        row.Cells[6].Value = dtD.Rows[i]["noobags"].ToString();
                        row.Cells[7].Value = dtD.Rows[i]["CgstId"].ToString();
                        row.Cells[8].Value = dtD.Rows[i]["CgstVal"].ToString();
                        row.Cells[9].Value = dtD.Rows[i]["SgstId"].ToString();
                        row.Cells[10].Value = dtD.Rows[i]["SgstVal"].ToString();
                        row.Cells[11].Value = dtD.Rows[i]["IgstId"].ToString();
                        row.Cells[12].Value = dtD.Rows[i]["IgstVal"].ToString();
                        row.Cells[13].Value = dtD.Rows[i]["TaxableVal"].ToString();
                        row.Cells[14].Value = dtD.Rows[i]["TotValue"].ToString();
                        row.Cells[15].Value = dtD.Rows[i]["AddNotes"].ToString();
                        row.Cells[16].Value = dtD.Rows[i]["RefUid"].ToString();
                        row.Cells[17].Value = dtD.Rows[i]["ItemUid"].ToString();
                        row.Cells[18].Value = dtD.Rows[i]["MillUid"].ToString();
                        HFIT.Rows.Add(row);
                    }
                    TextboxValue();
                }
                Genpan.Visible = false;
                panadd.Visible = false;
                label15.Visible = true;
                txtitemname.Visible = true;
                label14.Visible = true;
                txtprice.Visible = true;
                label31.Visible = true;
                txtqty.Visible = true;
                label32.Visible = true;
                txtbval.Visible = true;
                buttcusok.Visible = true;
                btnBagNoMapping.Visible = true;
                Editpan.Visible = true;
                btnsave.Visible = true;
                buttnfinbk.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value != null)
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value != null)
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

        }



        private void button11_Click_2(object sender, EventArgs e)
        {


        }

        private void splittax()
        {


            if (txttdis.Text == "" || txttdis.Text == null || txttdis.Text == "0" || txtpuid.Text == "")
            {
                txttprdval.Text = txttbval.Text;
            }
            else
            {
                if (txttbval.Text != "")
                {
                    double dis6 = Convert.ToDouble(txttbval.Text) / 100 * Convert.ToDouble(txttdis.Text);
                    txttdisc.Text = dis6.ToString("0.00");

                    dis4 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                    txttprdval.Text = dis4.ToString("#,0.00");
                }
            }

            if (txtcharges.Text == "" || txtcharges.Text == null)
            {
                txtexcise.Text = txttprdval.Text;
            }
            else
            {
                dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                txtexcise.Text = dis4.ToString("#,0.00");
            }

            if (txtpuid.Text == "")
            {
                return;
            }
            Genclass.strsql = "select generalname from partym a inner join generalm b on a.stateuid=b.uid where a.companyid=" + Genclass.data1 + " and a.uid=" + txtpuid.Text + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (txtexcise.Text != "")
            {
                if (tap1.Rows[0]["generalname"].ToString() == "Tamil Nadu 33")
                {


                    //txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    //txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();

                    dis4 = Convert.ToDouble(txttgstp.Text) / 2;

                    txttcgstp.Text = dis4.ToString("0.00");
                    txtsgstp.Text = dis4.ToString("0.00");
                    if (mode == 2)
                    {
                        dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));

                        txttgstval.Text = dis3.ToString("0.00");
                        dis3 = dis3 / 2;

                        txttcgval.Text = dis3.ToString("0.00");
                        txttsgval.Text = dis3.ToString("0.00");

                        if (txtigval.Text != "")
                        {
                            double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                        else
                        {
                            double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                            txtttot.Text = dis5.ToString("0.00");

                        }
                    }
                    else
                    {
                        if (Genclass.ty == 1)
                        {
                            dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));

                            txttgstval.Text = dis3.ToString("0.00");
                            dis3 = dis3 / 2;

                            txttcgval.Text = dis3.ToString("0.00");
                            txttsgval.Text = dis3.ToString("0.00");

                            if (txtigval.Text != "")
                            {
                                double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                txtttot.Text = dis5.ToString("0.00");
                            }
                            else
                            {
                                double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                txtttot.Text = dis5.ToString("0.00");

                            }
                        }
                        else
                        {
                            if (HFIT.CurrentRow.Cells[13].Value == null || HFIT.CurrentRow.Cells[13].Value == null)
                            {
                                dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));
                                txttgstval.Text = dis3.ToString("0.00");
                                dis3 = dis3 / 2;
                                txttcgval.Text = dis3.ToString("0.00");
                                txttsgval.Text = dis3.ToString("0.00");
                                if (txtigval.Text != "")
                                {
                                    double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                    txtttot.Text = dis5.ToString("0.00");
                                }
                                else
                                {
                                    double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                    txtttot.Text = dis5.ToString("0.00");
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (txtexcise.Text != "")
                    {
                        txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                        dis4 = Convert.ToDouble(HFIT.Rows[0].Cells[7].Value.ToString());
                        txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();
                        txtigstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                        dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));
                        txttgstval.Text = dis3.ToString("0.00");
                        txtigval.Text = dis3.ToString("0.00");
                        if (txtigval.Text != "")
                        {
                            double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                        else
                        {
                            double dis5 = Convert.ToDouble(txtexcise.Text) + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                    }
                }
                Double net1 = Convert.ToDouble(txtttot.Text);
                txtNetValue.Text = net1.ToString("0.00");
                double someInt = (int)net1;

                double rof = Math.Round(net1 - someInt, 2);
                txtRoundOff.Text = rof.ToString("0.00");

                if (Convert.ToDouble(txtRoundOff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(txtRoundOff.Text);
                    txtRoundOff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(txtRoundOff.Text);
                    txtRoundOff.Text = rof2.ToString("0.00");
                }
                Double net = Convert.ToDouble(txtNetValue.Text) + Convert.ToDouble(txtRoundOff.Text);
                txtNetValue.Text = net.ToString("0.00");
            }
        }

        private void button12_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Loadgrid();
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            addipan.Visible = true;
            Editpan.Visible = false;
            btnaddrcan.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = true;
            buttnnxt.Visible = true;
        }

        private void buttnnxt_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            btnaddrcan.Visible = true;
            btnsave.Visible = true;
            //HFGT.
            if (HFGT.Rows[0].Cells[0].Value == null || HFGT.Rows[0].Cells[0].Value == null)
            {
                var index = HFGT.Rows.Add();
                HFGT.Rows[index].Cells[0].Value = "No.of Cases";
                var index1 = HFGT.Rows.Add();
                HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
                var index2 = HFGT.Rows.Add();
                HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
                var index3 = HFGT.Rows.Add();
                HFGT.Rows[index3].Cells[0].Value = "Carrier";
                var index4 = HFGT.Rows.Add();
                HFGT.Rows[index4].Cells[0].Value = "Destination";
                var index5 = HFGT.Rows.Add();
                HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            }
            HFGT.Columns[0].ReadOnly = true;
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Taxpan.Visible = false;
            buttnnxt.Visible = false;
            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
            btnBagNoMapping.Visible = false;
            Genclass.sum5 = 0;
            txtNetValue.Text = Txttot.Text;
            Docno.Dispose();
            Docno.Clear();
            buttnfinbk.Visible = false;
            Loadgrid();
            TotalAmount();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }

            conn.Close();
            conn.Open();

            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[10].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            for (int i = 1; i < 4; i++)
            {
                Genclass.slno = i;
                Crviewer crv = new Crviewer();

                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and Companyid=" + Genclass.data1 + " and doctypeid=100";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                if (tap3.Rows.Count > 0)
                {
                    if (tap3.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {
                        if (tap3.Rows[0]["tag"].ToString() == "0")
                        {


                            SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 100;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            if (Genclass.data1 == 1)
                            {
                                doc.Load(Application.StartupPath + "\\BillAccounting.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                            }



                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);

                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                            SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                            da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            DataTable ds2 = new DataTable("taxcal");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);

                        }

                        else
                        {
                            SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 100;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            if (Genclass.data1 == 1)
                            {
                                doc.Load(Application.StartupPath + "\\BillAccounting.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                            }
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);

                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                            SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                            da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            DataTable ds2 = new DataTable("taxcal");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);



                        }
                    }




                    doc.PrintToPrinter(1, false, 0, 0);
                }
            }
        }


        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtdcno_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void TxtNetAmt_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttcusok_Click_1(object sender, EventArgs e)
        {


        }

        private void txtitemname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }



        private void txtplace_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcharges_TextChanged(object sender, EventArgs e)
        {
            //splittax();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void TxtRoff_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txttbval_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttdis_TextChanged(object sender, EventArgs e)
        {
            if (txttdis.Text != "")
            {
                if (Convert.ToDouble(txttdis.Text) > 0)
                {
                    double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                    txttdisc.Text = dis.ToString();
                }
                else
                {
                    txttdisc.Text = "0";
                }
                splittax();
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DataTable dt = new DataTable();
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                bsParty.DataSource = dt;
                FillGrid(dt, 2);
                Point p = FindLocation(txtname);
                PanelSearch.Location = new Point(p.X, p.Y + 25);
                PanelSearch.Visible = true;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }

        private void txtplace_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";

                Genclass.type = 7;
                loadput();


            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtplace.Text = "";
                txtplace.Focus();
            }
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtprice.Focus();
            }
        }

        private void buttcusok_Click(object sender, EventArgs e)
        {

            if (Genclass.ty1 == 2)
            {
                if (Genclass.data1 == 1)
                {
                    Genclass.strsql = "select * from pgbatmp";
                }
                else
                {
                    Genclass.strsql = "select * from batmp";
                }
            }
            else
            {
                if (txtqty.Text == "" || txtqty.Text == "0" || txtprice.Text == "" || txtprice.Text == "0" || txtitemname.Text == "" || txtitemname.Text == "0" || txtname.Text == "" || txtname.Text == "0")
                {
                    MessageBox.Show("Enter the Party or Item or Rate or Qty to Proceed");
                    return;
                }
                Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct c.hsnid,c.itemname,d.generalname as uom,c.uid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,0 as disper,0 as Disvalue," + txtbval.Text + " as Taxablevalue,i.f1 as gstper,convert(decimal(18,2),(" + txtbval.Text + " /100 * i.f1),105) as gstval from  itemm c left join generalm d on c.uom_uid=d.uid left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid  where c.Uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,i.f1) tab";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");
                HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["GSTper"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["GSTVAL"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["hsnid"].ToString();
                HFIT.Rows[index].Cells[16].Value = txtnotes.Text;

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("#,0.00");

                Txttot.Text = Txttot.Text.Replace(",", "");
                docno1();
            }



            Titlep();
            txtitemname.Text = "";
            txtprice.Text = "";
            txtqty.Text = "";
            txtnotes.Text = "";
            txtbval.Text = "";
            txtitemname.Focus();

        }


        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this Invoice ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                qur.CommandText = "Update stransactionsp set active=0 where uid=" + uid + "";
                qur.ExecuteNonQuery();
                MessageBox.Show("Invoice Cancelled");
            }
            Loadgrid();
        }

        private void txtpluid_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtplace_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcno_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (txtname.Text == "")
                {
                    MessageBox.Show("Select the Party");
                    return;
                }
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";

                Genclass.type = 8;
                loadput();



            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtdcno.Text = "";
                txtdcid.Text = "";
                txtdcno.Focus();
            }
        }

        private void txtdcid_TextChanged(object sender, EventArgs e)
        {
            if (txtdcid.Text == "" || txtdcid.Text == "0")

            {
                return;
            }
            if (Genclass.ty == 1)
            {

                Genclass.strsql = "select a.docdate,pqty,prate,basicvalue,isnull(e.f1,0) as f1,convert(decimal(18,2),((basicvalue+isnull(f.ChargeAmount,0))/100)*isnull(e.f1,0),102) as taxvalue,z.generalname as uom from stransactionsplist a inner join itemm b on a.itemuid=b.uid inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid inner join hsndet d on c.hsnid=d.uid inner join generalm e on d.sgid=e.uid left join stransactionspcharges f on a.transactionspuid=f.STransactionsPUid left join stransactions  g on a.transactionspuid=g.uid where a.uid=" + txtdcid.Text + "";


                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtprice.Text = tap1.Rows[0]["prate"].ToString();
                    txtqty.Text = tap1.Rows[0]["pqty"].ToString();
                    txtbval.Text = tap1.Rows[0]["basicvalue"].ToString();
                    txttgstp.Text = tap1.Rows[0]["f1"].ToString();
                    txttgstval.Text = tap1.Rows[0]["taxvalue"].ToString();
                    txtuom.Text = tap1.Rows[0]["uom"].ToString();

                }



            }
            if (Genclass.ty == 2)
            {
                if (txtdcid.Text != "")
                {
                    Genclass.strsql = " select Docno,convert(varchar,Docdate,105) as Docdate,a.dcno,a.dcdate,a.narration,c.hsnid,c.itemname,x.generalname as uom,c.uid ,  a.uid as Tuid,  b.Uid as listuid,b.pqty-isnull(sum(z.pqty),0) as qty,isnull(y.prate,m.price) as price, (b.pqty-isnull(sum(z.pqty),0))* isnull(y.prate,m.price) as BasicValue ,i.f1  as gstper, convert(decimal(18,2), ((((b.pqty-isnull(sum(z.pqty),0)))*(isnull(y.prate,m.price))) *  isnull(i.f1,0)/100))   as gstval,  (b.pqty-isnull(sum(z.pqty),0))* isnull(y.prate,m.price)+ ((((b.pqty-isnull(sum(z.pqty),0)))*(isnull(y.prate,m.price))) *  isnull(i.f1,0)/100) as total from   TransactionsP a inner join TransactionsPList b  on a.UId=b.TransactionsPUId LEFT join stransactionsplist z on b.Uid=z.Refuid   and z.doctypeid=100 LEFT join stransactionsplist y on y.Uid=b.Refuid inner join itemm c  on b.ItemUId=c.uid left join  generalm x on c.UOM_UId=x.uid left join   ItemGroup j on c.itemgroup_Uid=j.UId left join  Hsndet f on j.hsnid=f.uid left join generalm i   on f.sgid=i.uid left join pur_price_list m on a.PartyUid=m.Suppuid and b.ItemUId=m.itemuid  where a.uid=" + txtdcid.Text + "   group by Docno,   Docdate,a.dcno,a.dcdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,Price,  a.uid,b.uid,Narration,x.GeneralName,y.prate having b.pqty-isnull(sum(z.pqty),0)>0";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    Genclass.sum5 = 0;
                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {
                        var index = HFIT.Rows.Add();
                        HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();

                        HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                        double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                        HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                        double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                        HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                        double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                        HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                        Genclass.sum5 = Genclass.sum5 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        Txttot.Text = Genclass.sum5.ToString("0.00");

                        HFIT.Rows[index].Cells[5].Value = txtnotes.Text;

                        HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["uid"].ToString();
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();

                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                        HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["docno"].ToString();

                        HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["listuid"].ToString();
                        HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["hsnid"].ToString();
                        HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["total"].ToString();
                    }
                }
            }
        }

        private void fun()
        {
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum4 = 0;
            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("0.00");

                Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(HFIT.Rows[i].Cells[2].Value);
                txtprice.Text = Genclass.sum2.ToString("0.00");

                // Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
                //txtqty.Text = Genclass.sum3.ToString();

                Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                txtbval.Text = Genclass.sum4.ToString("0.00");
            }
        }

        private void txttrans_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttrans_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            Genclass.type = 8;
            loadput();



        }



        private void txttrans_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txttrans_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            Genclass.type = 8;
            loadput();
        }


        private void fun2()
        {
            if (mode == 1)
            {
                txtbval.Text = "0.00";
                Txttot.Text = "0.00";
                Genclass.sum1 = 0;
                Genclass.sum4 = 0;

                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {

                    if (HFIT.Rows[i].Cells[12].Value != null || HFIT.Rows[i].Cells[12].Value.ToString() != "0.00" || HFIT.Rows[i].Cells[4].Value != null || HFIT.Rows[i].Cells[4].Value.ToString() != "0.00")
                    {
                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        Txttot.Text = Genclass.sum1.ToString();
                    }
                }
            }
        }

        private void fun3()
        {
            txtbval.Text = "0.00";
            Txttot.Text = "0.00";
            Genclass.sum1 = 0;
            Genclass.sum4 = 0;

            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {

                if (HFIT.Rows[i].Cells[12].Value != null || HFIT.Rows[i].Cells[12].Value.ToString() != "0.00" || HFIT.Rows[i].Cells[4].Value != null || HFIT.Rows[i].Cells[4].Value.ToString() != "0.00")
                {
                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                    Txttot.Text = Genclass.sum1.ToString();
                }
            }
        }

        private void Txttot_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtdcno_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtname_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                bsParty.DataSource = dt;
                FillGrid(dt, 2);
                Point p = FindLocation(txtname);
                PanelSearch.Location = new Point(p.X, p.Y + 25);
                PanelSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txttrans_Click(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            Genclass.type = 8;
            loadput();

        }

        private void txttdis_TextChanged_1(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }


            Genclass.cat = 4;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[10].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void txtitemname_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.fieldSix = "";

            Genclass.type = 5;
            conn.Close();
            conn.Open();
            if (Genclass.data1 == 1)
            {
                qur.CommandText = "delete from pgbatmp";
                qur.ExecuteNonQuery();
            }
            else
            {
                qur.CommandText = "delete from batmp";
                qur.ExecuteNonQuery();
            }
            Frmbilllookup bill = new Frmbilllookup();
            bill.MdiParent = this.MdiParent;
            bill.Show();
        }



        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (txtqty.Text != "" && txtprice.Text != "")
            {
                double bval = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtprice.Text);
                txtbval.Text = bval.ToString();

                if (Genclass.ty == 3)
                {
                    double bval1 = (bval * Convert.ToDouble(txttgstp.Text)) / 100;
                    txttgstval.Text = bval.ToString();
                }
            }
        }

        private void txtprice_TextChanged_1(object sender, EventArgs e)
        {
            if (txtprice.Text != "" && txtqty.Text != "")
            {
                double add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("0.000");
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = (DataGridViewRow)HFIT.Rows[0].Clone();
                row.Cells[0].Value = txtitemname.Text;
                row.Cells[1].Value = txtMillName.Text;
                row.Cells[2].Value = txtpluid.Tag;
                row.Cells[3].Value = txtprice.Text;
                row.Cells[4].Value = txtqty.Text;
                row.Cells[5].Value = txtbval.Text;
                row.Cells[6].Value = txtNoBag.Text;
                row.Cells[7].Value = (Convert.ToDecimal(cboTax.SelectedValue) / 2).ToString();
                row.Cells[8].Value = Convert.ToDecimal(txtTaxvalue.Text) / 2;
                row.Cells[9].Value = (Convert.ToDecimal(cboTax.SelectedValue) / 2).ToString();
                row.Cells[10].Value = Convert.ToDecimal(txtTaxvalue.Text) / 2;
                row.Cells[11].Value = "0";
                row.Cells[12].Value = "0";
                row.Cells[13].Value = txtTaxvalue.Text;
                row.Cells[14].Value = (Convert.ToDecimal(txtbval.Text) + Convert.ToDecimal(txtTaxvalue.Text)).ToString("0.00");
                row.Cells[15].Value = txtHSNCode.Text;
                row.Cells[16].Value = "0";
                row.Cells[17].Value = txtitemname.Tag;
                row.Cells[18].Value = txtMillName.Tag;
                HFIT.Rows.Add(row);
                TextboxValue();
                ClearControl();
                txtitemname.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void TextboxValue()
        {
            try
            {
                decimal totalBasicValue = 0;
                decimal totaltaxValue = 0;
                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    totalBasicValue += Convert.ToDecimal(HFIT.Rows[i].Cells[5].Value.ToString());
                    totaltaxValue += Convert.ToDecimal(HFIT.Rows[i].Cells[13].Value.ToString());
                }
                Txttot.Text = totalBasicValue.ToString("0.00");
                txttbval.Text = totalBasicValue.ToString("0.00");
                txtigval.Text = totaltaxValue.ToString("0.00");
                txtcharges.Text = "0";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CalculateTaxValues()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            docno1();
        }

        private void txttitemid_TextChanged(object sender, EventArgs e)
        {
            if (txttitemid.Text != "")
            {
                if (Genclass.ty == 3 || Genclass.ty == 4)
                {
                    Genclass.strsql = "select isnull(c.sname,0) as f1,z.generalname as uom from itemm b  inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid where b.uid=" + txttitemid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        txttgstp.Text = tap1.Rows[0]["f1"].ToString();
                        txtuom.Text = tap1.Rows[0]["uom"].ToString();
                    }
                }
            }
            docno1();
        }

        public void docno1()
        {
        }

        private void dtpfnt_ValueChanged(object sender, EventArgs e)
        {
            Loadgrid();
            TotalAmount();

        }
        private void TotalAmount()
        {
            conn.Close();
            conn.Open();
            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
            txttotamt.Text = "0";
            txtbasicval.Text = "0";
            txttax.Text = "0";
            Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=1 and a.doctypeid=100 and a.companyid=" + Genclass.data1 + "   and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            txttotamt.Text = tap2.Rows[0]["totamt"].ToString();
            Genclass.strsql = " select SUM(Basicvalue) as Basicvalue from(select Basicvalue-sum(disval) as Basicvalue from (Select  isnull(SUM(d.basicvalue),0) as Basicvalue,transactionspuid from stransactionsp a  left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=100 and a.companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by transactionspuid) aa inner join (select distinct disval,transactionspuid from  stransactionsplist where  doctypeid=100)bb on  aa.transactionspuid=bb.transactionspuid group by Basicvalue) tab ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            txtbasicval.Text = tap3.Rows[0]["Basicvalue"].ToString();
            Genclass.strsql = "select sum(igstval) as Taxval from (select distinct igstval from stransactionsplist a inner join stransactionsp b on a.transactionspuid=b.uid where b.active=1 and a.doctypeid=100 and companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + ") tab ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap4 = new DataTable();
            aptr4.Fill(tap4);
            txttax.Text = tap4.Rows[0]["Taxval"].ToString();
            Genclass.strsql = "Select  isnull(SUM(e.totalcharges),0) as charges from stransactionsp a   left join  STransactionsPCharges e on a.uid=e.STransactionsPUid   where a.active=1 and a.doctypeid=100 and a.companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap5 = new DataTable();
            aptr5.Fill(tap5);
            if (tap5.Rows.Count > 0)
            {
                txtchargessum.Text = tap5.Rows[0]["charges"].ToString();
            }
            else
            {
                txtchargessum.Text = "0.00";
            }
            conn.Close();
        }

        private void txtprice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtqty.Focus();
            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtnotes.Focus();
            }
        }

        private void txtnotes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button7.Focus();
            }
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {
            docno1();
        }

        protected void LoadAmountDetails()
        {
            try
            {
                if (txtcharges.Text == string.Empty || txttbval.Text == string.Empty || txtigval.Text == string.Empty)
                {
                    txtcharges.Text = "0";
                    txttbval.Text = "0";
                    txtigval.Text = "0";
                }
                else
                {
                    decimal basicAmount = Convert.ToDecimal(txttbval.Text);
                    decimal TaxAmount = Convert.ToDecimal(txtigval.Text);
                    decimal OtherCharges = Convert.ToDecimal(txtcharges.Text);
                    txtttot.Text = (basicAmount + TaxAmount + OtherCharges).ToString("0.00");
                    double a = Convert.ToDouble(txtttot.Text);
                    string[] str = a.ToString("0.00").Split('.');
                    double num1 = Convert.ToDouble("0." + str[1]);
                    double res;
                    if (num1 < 0.51)
                    {
                        res = Math.Floor(a);
                    }
                    else
                    {
                        res = Math.Round(a);
                    }
                    txtRoundOff.Text = (res - a).ToString("0.00");
                    txtNetValue.Text = res.ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtcharges_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                LoadAmountDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void HFIT_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Delete)
            //{
            //    j = HFIT.SelectedCells[0].RowIndex;
            //    double sun = Convert.ToDouble(HFIT.Rows[j].Cells[4].Value.ToString());
            //    if (Txttot.Text != "")
            //    {
            //        Genclass.sum1 = Convert.ToDouble(Txttot.Text);
            //        Genclass.sum1 = Genclass.sum1 - sun;
            //        Txttot.Text = Genclass.sum1.ToString("0.00");
            //        txttbval.Text = Genclass.sum1.ToString("0.00");
            //    }
            //    txttdis_TextChanged_1(sender, e);
            //    Genclass.strsql = "select a.* from [bagnomap] a inner join itemm b on a.itemid=b.uid  where a.docno='" + txtgrn.Text + "' and itemid=" + HFIT.Rows[j].Cells[6].Value.ToString() + " ";
            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap1 = new DataTable();
            //    aptr1.Fill(tap1);
            //    if (tap1.Rows.Count > 0)
            //    {
            //        qur.CommandText = "delete from [bagnomap] where docno='" + txtgrn.Text + "' and itemid=" + HFIT.Rows[j].Cells[6].Value.ToString() + "";
            //        qur.ExecuteNonQuery();
            //    }
            //}
        }

        private void comboload()
        {
            Docno = new DataTable();
            Docno.Columns.Add("ItemName", typeof(string));
            Docno.Columns.Add("Qty", typeof(string));
            Docno.Columns.Add("NoOfBags", typeof(string));
            Docno.Columns.Add("ItemUid", typeof(string));
            Docno.Columns.Add("Doco", typeof(string));
            for (int l = 0; l < HFIT.Rows.Count - 1; l++)
            {
                DataRow dr = Docno.NewRow();
                dr[0] = HFIT.Rows[l].Cells[0].Value;
                dr[1] = HFIT.Rows[l].Cells[4].Value;
                dr[2] = HFIT.Rows[l].Cells[6].Value;
                dr[3] = HFIT.Rows[l].Cells[17].Value;
                dr[4] = txtgrn.Text;
                Docno.Rows.Add(dr);
            }
            cbosGReturnItem.DisplayMember = "ItemName";
            cbosGReturnItem.ValueMember = "ItemUid";
            cbosGReturnItem.DataSource = Docno;
        }

        private void cbosGReturnItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {
                if (HFIT.Rows[k].Cells[0].Value.ToString() == cbosGReturnItem.Text)
                {
                    txtqty1.Text = Convert.ToString(HFIT.Rows[k].Cells[4].Value);
                    txtbags.Text = Convert.ToString(HFIT.Rows[k].Cells[6].Value);
                }
            }
            txtbags.Focus();
        }

        private void cbosGReturnItem_SelectedValueChanged(object sender, EventArgs e)
        {
            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {
                if (HFIT.Rows[k].Cells[0].Value.ToString() == cbosGReturnItem.Text)
                {
                    txtqty1.Text = Convert.ToString(HFIT.Rows[k].Cells[4].Value);
                    txtbags.Text = Convert.ToString(HFIT.Rows[k].Cells[6].Value);
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                for (int y = 0; y < DataGridWeight.Rows.Count - 1; y++)
                {
                    if (DataGridWeight.Rows[y].Cells[1].Value.ToString() == Convert.ToString(txtBagNo.Text))
                    {
                        MessageBox.Show("Bagno alreay Exist");
                        return;
                    }
                }
                if (cbosGReturnItem.Text != string.Empty && txtBagNo.Text != string.Empty && txtWeight.Text != string.Empty)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                    row.Cells[0].Value = DataGridWeight.Rows.Count - 1;
                    row.Cells[1].Value = txtlotno1.Text;
                    row.Cells[2].Value = txtBagNo.Text;
                    row.Cells[3].Value = txtTarWght.Text;
                    row.Cells[4].Value = txtGrossWght.Text;
                    row.Cells[5].Value = Convert.ToDecimal(txtWeight.Text).ToString("0.000");
                    row.Cells[6].Value = cbosGReturnItem.SelectedValue;
                    row.Cells[7].Value = 0;
                    DataGridWeight.Rows.Add(row);
                }
                txtBagNo.Text = "";
                txtWeight.Text = "";
                txtTarWght.Text = "";
                txtGrossWght.Text = "";
                txtBagNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtGrossWght_TextChanged(object sender, EventArgs e)
        {

        }

        protected void LoadWeightrGrid()
        {
            DataGridWeight.DataSource = null;
            DataGridWeight.AutoGenerateColumns = false;
            DataGridWeight.ColumnCount = 8;
            DataGridWeight.Columns[0].Name = "SlNo";
            DataGridWeight.Columns[0].HeaderText = "SlNo";
            DataGridWeight.Columns[0].Width = 50;
            DataGridWeight.Columns[1].Name = "Lot No";
            DataGridWeight.Columns[1].HeaderText = "Lot No";
            DataGridWeight.Columns[1].Width = 100;
            DataGridWeight.Columns[2].Name = "Bag No";
            DataGridWeight.Columns[2].HeaderText = "Bag No";
            DataGridWeight.Columns[2].Width = 80;
            DataGridWeight.Columns[3].Name = "Tare Wght";
            DataGridWeight.Columns[3].HeaderText = "Tare Wght";
            DataGridWeight.Columns[3].Width = 80;
            DataGridWeight.Columns[4].Name = "Gross Wght";
            DataGridWeight.Columns[4].HeaderText = "Gross Wghto";
            DataGridWeight.Columns[4].Width = 80;
            DataGridWeight.Columns[5].Name = "NetWeight";
            DataGridWeight.Columns[5].HeaderText = "NetWeight";
            DataGridWeight.Columns[5].DefaultCellStyle.Format = "N3";
            DataGridWeight.Columns[5].Width = 70;
            DataGridWeight.Columns[6].Name = "ItemUid";
            DataGridWeight.Columns[6].HeaderText = "ItemUid";
            DataGridWeight.Columns[6].Visible = false;
            DataGridWeight.Columns[7].Name = "Refid";
            DataGridWeight.Columns[7].HeaderText = "Refid";
            DataGridWeight.Columns[7].Visible = false;
        }

        private void button10_Click_3(object sender, EventArgs e)
        {
            if (txtstart.Text != string.Empty && txtNoogBags.Text != string.Empty && txtgrossgen.Text != string.Empty && txttarewt.Text != string.Empty)
            {
                Decimal net = Convert.ToDecimal(txtgrossgen.Text) - Convert.ToDecimal(txttarewt.Text);
                int bags = Convert.ToInt16(txtNoogBags.Text);
                int hg = 0;
                int kk = 0;
                int i = 0;
                for (int p = 0; p < bags; p++)
                {
                    kk = Convert.ToInt16(txtstart.Text) + hg;
                    for (int y = 0; y < DataGridWeight.Rows.Count - 1; y++)
                    {
                        if (DataGridWeight.Rows[y].Cells[2].Value.ToString() == Convert.ToString(kk))
                        {
                            MessageBox.Show("Bagno alreay Exist");
                            return;
                        }
                    }
                    i += 1;
                    DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                    row.Cells[0].Value = i;
                    row.Cells[1].Value = txtlotno.Text;
                    row.Cells[2].Value = kk;
                    row.Cells[3].Value = Convert.ToDecimal(txttarewt.Text);
                    row.Cells[4].Value = Convert.ToDecimal(txtgrossgen.Text);
                    row.Cells[5].Value = Convert.ToDecimal(net);
                    row.Cells[6].Value = cbosGReturnItem.SelectedValue;
                    row.Cells[7].Value = txtgrn.Text;
                    DataGridWeight.Rows.Add(row);
                    hg += 1;
                }
            }
        }

        private void dataload()
        {
            Genclass.strsql = "select a.* from [bagnomaptemp] a inner join itemm b on a.itemid=b.uid  where a.docno='" + txtgrn.Text + "' and a.itemid=" + cbosGReturnItem.SelectedValue + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            Genclass.sum1 = 0;
            Txttot.Text = "";
            for (int m = 0; m < tap1.Rows.Count; m++)
            {
                var index = DataGridWeight.Rows.Add();
                DataGridWeight.Rows[index].Cells[0].Value = tap1.Rows[m]["lotno"].ToString();
                DataGridWeight.Rows[index].Cells[1].Value = tap1.Rows[m]["bagno"].ToString();
                DataGridWeight.Rows[index].Cells[2].Value = tap1.Rows[m]["tareweight"].ToString();
                DataGridWeight.Rows[index].Cells[3].Value = tap1.Rows[m]["Gross"].ToString();
                DataGridWeight.Rows[index].Cells[4].Value = tap1.Rows[m]["weight"].ToString();
                DataGridWeight.Rows[index].Cells[5].Value = tap1.Rows[m]["itemid"].ToString();
                DataGridWeight.Rows[index].Cells[6].Value = tap1.Rows[m]["uid"].ToString();
            }
            LoadWeightrGrid();
        }
        private void dataload1()
        {
            if (mappnl.Visible == true && cbosGReturnItem.SelectedValue != null)
            {
                DataGridWeight.Refresh();
                DataGridWeight.DataSource = null;
                DataGridWeight.Rows.Clear();
                LoadWeightrGrid();
                Genclass.strsql = "select a.* from [bagnomap] a inner join GeneralM b on a.itemid=b.uid and b.TypeM_Uid = 26  where a.docno='" + txtgrn.Text + "' and a.itemid=" + cbosGReturnItem.SelectedValue + " ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                Genclass.sum1 = 0;
                Txttot.Text = "";
                int i = 0;
                if (tap1.Rows.Count != 0)
                {
                    for (int m = 0; m < tap1.Rows.Count; m++)
                    {
                        i += 1;
                        var index = DataGridWeight.Rows.Add();
                        DataGridWeight.Rows[index].Cells[0].Value = i;
                        DataGridWeight.Rows[index].Cells[1].Value = tap1.Rows[m]["lotno"].ToString();
                        DataGridWeight.Rows[index].Cells[2].Value = tap1.Rows[m]["bagno"].ToString();
                        DataGridWeight.Rows[index].Cells[3].Value = tap1.Rows[m]["tareweight"].ToString();
                        DataGridWeight.Rows[index].Cells[4].Value = tap1.Rows[m]["Gross"].ToString();
                        DataGridWeight.Rows[index].Cells[5].Value = tap1.Rows[m]["weight"].ToString();
                        DataGridWeight.Rows[index].Cells[6].Value = tap1.Rows[m]["itemid"].ToString();
                        DataGridWeight.Rows[index].Cells[7].Value = tap1.Rows[m]["uid"].ToString();
                    }
                }
            }
        }
        private void button15_Click(object sender, EventArgs e)
        {
            if (HFIT.Rows.Count > 1)
            {
                mappnl.Visible = true;
                Genclass.Module.ClearTextBox(this, mappnl);
                DataGridWeight.Refresh();
                DataGridWeight.DataSource = null;
                DataGridWeight.Rows.Clear();
                LoadWeightrGrid();
                comboload();
                if (mode == 2)
                {
                    dataload1();
                }
                btnBagNoMapping.Visible = false;
                btnsave.Visible = false;
                buttnfinbk.Visible = false;
                LoadWeightrGrid();
                panadd.Visible = false;
                cbosGReturnItem.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Items Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            mappnl.Visible = false;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;
            btnBagNoMapping.Visible = true;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            if (mode == 1)
            {
            }
            else

            {
                qur.CommandText = "delete from [bagnomap] where docno='" + txtgrn.Text + "' and itemid=" + cbosGReturnItem.SelectedValue + "";
                qur.ExecuteNonQuery();
            }
            for (int p = 0; p < DataGridWeight.Rows.Count - 1; p++)
            {
                SqlParameter[] para ={
                    new SqlParameter("@lotno",DataGridWeight.Rows[p].Cells[1].Value),
                    new SqlParameter("@bagno",DataGridWeight.Rows[p].Cells[2].Value),
                    new SqlParameter("@tareweight",DataGridWeight.Rows[p].Cells[3].Value),
                    new SqlParameter("@Gross", DataGridWeight.Rows[p].Cells[4].Value),
                    new SqlParameter("@weight",DataGridWeight.Rows[p].Cells[5].Value),
                    new SqlParameter("@itemid", DataGridWeight.Rows[p].Cells[6].Value),
                    new SqlParameter("@docno ", txtgrn.Text)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_bagMapping", para, conn);
            }
            MessageBox.Show("Bagno Mapped", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void DataGridWeight_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void DataGridWeight_DoubleClick(object sender, EventArgs e)
        {
            for (int k = 0; k < DataGridWeight.Rows.Count - 1; k++)
            {
                if (DataGridWeight.Rows[k].Cells[2].Value == null || DataGridWeight.Rows[k].Cells[2].Value == null || DataGridWeight.Rows[k].Cells[3].Value == null || DataGridWeight.Rows[k].Cells[3].Value.ToString() == "")
                {
                    return;
                }
                else
                {
                    if (DataGridWeight.Rows[k].Cells[2].Value.ToString() != "0.000" || DataGridWeight.Rows[k].Cells[3].Value.ToString() != "0.000")
                    {
                        decimal str2 = Convert.ToDecimal(DataGridWeight.Rows[k].Cells[2].Value.ToString()) - Convert.ToDecimal(DataGridWeight.Rows[k].Cells[3].Value.ToString());
                        DataGridWeight.Rows[k].Cells[4].Value = str2.ToString();
                    }
                }
            }
        }

        private void DataGridWeight_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void DataGridWeight_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void DataGridWeight_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            for (int k = 0; k < DataGridWeight.Rows.Count - 1; k++)
            {
                if (DataGridWeight.Rows[k].Cells[2].Value == null || DataGridWeight.Rows[k].Cells[2].Value == null || DataGridWeight.Rows[k].Cells[3].Value == null || DataGridWeight.Rows[k].Cells[3].Value.ToString() == "")
                {
                    return;
                }
                else
                {

                    if (DataGridWeight.Rows[k].Cells[2].Value.ToString() != "0.000" || DataGridWeight.Rows[k].Cells[3].Value.ToString() != "0.000")
                    {
                        double str2 = Convert.ToDouble(DataGridWeight.Rows[k].Cells[2].Value.ToString()) - Convert.ToDouble(DataGridWeight.Rows[k].Cells[3].Value.ToString());
                        DataGridWeight.Rows[k].Cells[4].Value = str2.ToString();
                    }
                }
            }
        }

        private void mappnl_Paint(object sender, PaintEventArgs e)
        {
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            PanelSearch.Visible = false;
        }

        private void DataGridParty_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                SelectId = 1;
                if (DataGridParty.Rows.Count > 0)
                {
                    int Index = DataGridParty.SelectedCells[0].RowIndex;
                    if (Fillid == 1)
                    {
                        txtMillName.Text = DataGridParty.Rows[Index].Cells[1].Value.ToString();
                        txtMillName.Tag = DataGridParty.Rows[Index].Cells[0].Value.ToString();
                        txtprice.Focus();
                    }
                    else if (Fillid == 2)
                    {
                        txtname.Text = DataGridParty.Rows[Index].Cells[1].Value.ToString();
                        txtname.Tag = DataGridParty.Rows[Index].Cells[0].Value.ToString();
                        GetAddress(Convert.ToInt32(txtname.Tag.ToString()), txtname.Name);
                        txtSupplyTo.Focus();
                    }
                    else if (Fillid == 3)
                    {
                        txtSupplyTo.Text = DataGridParty.Rows[Index].Cells[1].Value.ToString();
                        txtSupplyTo.Tag = DataGridParty.Rows[Index].Cells[0].Value.ToString();
                        GetAddress(Convert.ToInt32(txtSupplyTo.Tag.ToString()), txtSupplyTo.Name);
                        txtitemname.Focus();

                    }
                    PanelSearch.Visible = false;
                }
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if (DataGridParty.Rows.Count > 0)
            {
                int Index = DataGridParty.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtMillName.Text = DataGridParty.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridParty.Rows[Index].Cells[0].Value.ToString();
                    txtprice.Focus();
                }
                else if (Fillid == 2)
                {
                    txtname.Text = DataGridParty.Rows[Index].Cells[1].Value.ToString();
                    txtname.Tag = DataGridParty.Rows[Index].Cells[0].Value.ToString();
                    GetAddress(Convert.ToInt32(txtname.Tag.ToString()), txtname.Name);
                    txtSupplyTo.Focus();
                }
                else if (Fillid == 3)
                {
                    txtSupplyTo.Text = DataGridParty.Rows[Index].Cells[1].Value.ToString();
                    txtSupplyTo.Tag = DataGridParty.Rows[Index].Cells[0].Value.ToString();
                    GetAddress(Convert.ToInt32(txtSupplyTo.Tag.ToString()), txtSupplyTo.Name);
                    txtitemname.Focus();

                }
                PanelSearch.Visible = false;
            }
            SelectId = 0;
        }

        private void GetAddress(int Id, string txtName)
        {
            try
            {
                Genclass.strsql = "Select address1, address2 ,city,stateuid  from partym where uid=" + Id + " and companyid=" + Genclass.data1 + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    if (txtName == "txtname")
                    {
                        txtpadd1.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    }
                    else
                    {
                        txtSupplyToAdd.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    }

                    txtplace.Text = txtname.Text;
                    txtpluid.Text = txtpuid.Text;
                    txtpadd2.Text = txtpadd1.Text;
                    txtpluid.Text = tap1.Rows[0]["stateuid"].ToString();
                    Genclass.cat = Convert.ToInt32(tap1.Rows[0]["stateuid"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        private void TxtRoff_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = getTax();
                cboTax.DataSource = null;
                cboTax.DisplayMember = "GeneralName";
                cboTax.ValueMember = "TaxPer";
                cboTax.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }


        protected DataTable getTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void cboTax_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtbval.Text != string.Empty)
                {
                    decimal BasicValue = Convert.ToDecimal(txtbval.Text);
                    decimal TaxPer = Convert.ToDecimal(cboTax.SelectedValue);
                    txtTaxvalue.Text = ((BasicValue * TaxPer) / 100).ToString("0.00");
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMillName_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getPartyM();
                FillGrid(dt, 1);
                Point loc = FindLocation(txtMillName);
                PanelSearch.Location = new Point(loc.X, loc.Y + 20);
                PanelSearch.Visible = true;
                PanelSearch.Text = "Mill Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getPartyM()
        {
            DataTable dt = new DataTable();
            try
            {
                //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItem", conn);
                int Active = 1;
                SqlParameter[] para = { new SqlParameter("@Type", "Mill"), new SqlParameter("@Active", Active) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPartyM", para, conn);
                bsMill.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void FillGrid(DataTable dt, int id)
        {
            try
            {
                DataGridParty.DataSource = null;
                DataGridParty.AutoGenerateColumns = false;
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridParty.ColumnCount = 3;
                    DataGridParty.Columns[0].Name = "Uid";
                    DataGridParty.Columns[0].HeaderText = "Uid";
                    DataGridParty.Columns[0].DataPropertyName = "Uid";
                    DataGridParty.Columns[1].Name = "Name";
                    DataGridParty.Columns[1].HeaderText = "Name";
                    DataGridParty.Columns[1].DataPropertyName = "Name";
                    DataGridParty.Columns[2].Name = "PartyType";
                    DataGridParty.Columns[2].HeaderText = "PartyType";
                    DataGridParty.Columns[2].DataPropertyName = "PartyType";
                    DataGridParty.Columns[1].Width = 250;
                    DataGridParty.DataSource = bsMill;
                    DataGridParty.Columns[0].Visible = false;
                }
                else if (id == 2)
                {
                    Fillid = 2;
                    DataGridParty.ColumnCount = 3;
                    DataGridParty.Columns[0].Name = "Uid";
                    DataGridParty.Columns[0].HeaderText = "Uid";
                    DataGridParty.Columns[0].DataPropertyName = "Uid";
                    DataGridParty.Columns[0].Visible = false;
                    DataGridParty.Columns[1].Name = "Name";
                    DataGridParty.Columns[1].HeaderText = "Name";
                    DataGridParty.Columns[1].DataPropertyName = "Name";
                    DataGridParty.Columns[1].Width = 270;
                    DataGridParty.Columns[2].Name = "Code";
                    DataGridParty.Columns[2].HeaderText = "Code";
                    DataGridParty.Columns[2].DataPropertyName = "Code";
                    DataGridParty.DataSource = bsParty;
                }
                else if (id == 3)
                {
                    Fillid = 3;
                    DataGridParty.ColumnCount = 3;
                    DataGridParty.Columns[0].Name = "Uid";
                    DataGridParty.Columns[0].HeaderText = "Uid";
                    DataGridParty.Columns[0].DataPropertyName = "Uid";
                    DataGridParty.Columns[0].Visible = false;
                    DataGridParty.Columns[1].Name = "Name";
                    DataGridParty.Columns[1].HeaderText = "Name";
                    DataGridParty.Columns[1].DataPropertyName = "Name";
                    DataGridParty.Columns[1].Width = 270;
                    DataGridParty.Columns[2].Name = "Code";
                    DataGridParty.Columns[2].HeaderText = "Code";
                    DataGridParty.Columns[2].DataPropertyName = "Code";
                    DataGridParty.DataSource = bsParty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSupplyTo_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                bsParty.DataSource = dt;
                FillGrid(dt, 3);
                Point p = FindLocation(txtSupplyTo);
                PanelSearch.Location = new Point(p.X, p.Y + 25);
                PanelSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSupplyTo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                bsParty.DataSource = dt;
                FillGrid(dt, 3);
                Point p = FindLocation(txtSupplyTo);
                PanelSearch.Location = new Point(p.X, p.Y + 25);
                PanelSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSupplyTo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtSupplyTo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMillName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsMill.Filter = string.Format("Name LIKE '%{0}%' ", txtMillName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button7_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    DataGridViewRow row = (DataGridViewRow)HFIT.Rows[0].Clone();
                    row.Cells[0].Value = txtitemname.Text;
                    row.Cells[1].Value = txtMillName.Text;
                    row.Cells[2].Value = txtpluid.Tag;
                    row.Cells[3].Value = txtprice.Text;
                    row.Cells[4].Value = txtqty.Text;
                    row.Cells[5].Value = txtbval.Text;
                    row.Cells[6].Value = txtNoBag.Text;
                    row.Cells[7].Value = (Convert.ToDecimal(cboTax.SelectedValue) / 2).ToString();
                    row.Cells[8].Value = Convert.ToDecimal(txtTaxvalue.Text) / 2;
                    row.Cells[9].Value = (Convert.ToDecimal(cboTax.SelectedValue) / 2).ToString();
                    row.Cells[10].Value = Convert.ToDecimal(txtTaxvalue.Text) / 2;
                    row.Cells[11].Value = "0";
                    row.Cells[12].Value = "0";
                    row.Cells[13].Value = txtTaxvalue.Text;
                    row.Cells[14].Value = (Convert.ToDecimal(txtbval.Text) + Convert.ToDecimal(txtTaxvalue.Text)).ToString("0.00");
                    row.Cells[15].Value = txtHSNCode.Text;
                    row.Cells[16].Value = "0";
                    row.Cells[17].Value = txtitemname.Tag;
                    row.Cells[18].Value = txtMillName.Tag;
                    HFIT.Rows.Add(row);
                    TextboxValue();
                    ClearControl();
                    txtitemname.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtTarWght_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtGrossWght.Text != string.Empty || txtTarWght.Text != string.Empty)
                {
                    double wgt = Convert.ToDouble(txtGrossWght.Text) - Convert.ToDouble(txtTarWght.Text);
                    txtWeight.Text = wgt.ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtbval_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cboTax_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFIT_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
    }
}


