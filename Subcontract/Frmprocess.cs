﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
namespace Subcontract
{
    public partial class Frmprocess : Form
    {
        public Frmprocess()
        {
            {
                CenterToParent();
                this.BackColor = Color.LightSkyBlue;
                InitializeComponent();
            }

        }
        string uid = "";
        int mode = 0;
        int type = 0;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void Frmprocess_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 2;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 2;
     
            Titlep();

        }
        private void Titlep()
        {
            hfg3.ColumnCount =9;
            hfg3.Columns[0].Name = "Processid";
            hfg3.Columns[1].Name = "Process";
            hfg3.Columns[2].Name = "SeqNo";
            hfg3.Columns[3].Name = "Rate/Qty";
            hfg3.Columns[4].Name = "Set.Time";
            hfg3.Columns[5].Name = "Proc.Time";
            hfg3.Columns[6].Name = "Proc. Qty";
            hfg3.Columns[7].Name = "Rate/Hr";
            hfg3.Columns[8].Name = "itemid";
            hfg3.Columns[0].Width = 0;
            hfg3.Columns[1].Width = 250;
            hfg3.Columns[2].Width = 50;
            hfg3.Columns[3].Width = 50;
            hfg3.Columns[4].Width = 50;
            hfg3.Columns[5].Width = 50;
            hfg3.Columns[6].Width = 50;
            hfg3.Columns[7].Width = 50;
         
            hfg3.Columns[8].Visible = false;
        }

        private void buttrqok_Click(object sender, EventArgs e)
        {

            int rowscount = hfg3.Rows.Count - 1;

            for (int i = 0; i < rowscount; i++)
            {

            }

            var index = hfg3.Rows.Add();
            hfg3.Rows[index].Cells[0].Value = txtprocessid.Text;
            hfg3.Rows[index].Cells[1].Value = txtproceesname.Text;
            hfg3.Rows[index].Cells[2].Value = txtsqgno.Text;
            hfg3.Rows[index].Cells[3].Value = txtrate.Text;
            hfg3.Rows[index].Cells[4].Value = txtsettime.Text;
            hfg3.Rows[index].Cells[5].Value = txtprocestime.Text;
            hfg3.Rows[index].Cells[6].Value = txtprocessqty.Text;
            hfg3.Rows[index].Cells[7].Value = txthour.Text;
            hfg3.Rows[index].Cells[8].Value = Genclass.bomstr;


            txtprocessid.Text = "";
            txtproceesname.Text = "";
            txtsqgno.Text = "";
            txtrate.Text = "";
            txtsettime.Text = "";
            txtprocessqty.Text = "";
            txtprocestime.Text = "";
            txthour.Text = "";
        }
        private void loaduom()
        {
            conn.Open();

            //{
            //    Module.Partylistviewcont("uid", "processname", Genclass.strsql, this, txtprocessid, txtproceesname);
            //    Genclass.strsql = "select uid,processname   from processm where active=1";
            //}
    
            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap = new DataTable();
            //aptr.Fill(tap);
            //Frmlookup contc = new Frmlookup();
            //DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            //dt.Refresh();
            //dt.ColumnCount = tap.Columns.Count;
            //dt.Columns[0].Visible = false;
            //dt.Columns[1].Width = 200;


            //dt.DefaultCellStyle.Font = new Font("Arial", 10);

            //dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            //dt.AutoGenerateColumns = false;

            //Genclass.i = 0;
            //foreach (DataColumn column in tap.Columns)
            //{
            //    dt.Columns[Genclass.i].Name = column.ColumnName;
            //    dt.Columns[Genclass.i].HeaderText = column.ColumnName;
            //    dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
            //    Genclass.i = Genclass.i + 1;
            //}

            //dt.DataSource = tap;
            //contc.Show();
            //conn.Close();

        }

        private void txtproceesname_TextChanged(object sender, EventArgs e)
        {
       
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Open();

            {
                for (int i = 0; i < hfg3.Rows.Count - 1; i++)
                {
                    qur.CommandText = "insert into itemsmprocess values ( " + hfg3.Rows[i].Cells[8].Value + "," + hfg3.Rows[i].Cells[2].Value + "," + hfg3.Rows[i].Cells[0].Value + "," + hfg3.Rows[i].Cells[3].Value + "," + hfg3.Rows[i].Cells[4].Value + "," + hfg3.Rows[i].Cells[5].Value + "," + hfg3.Rows[i].Cells[6].Value + "," + hfg3.Rows[i].Cells[7].Value + ",0,1)";
                    qur.ExecuteNonQuery();
                }


                //qur.CommandText = "insert into itemsmbom values ( " + hfg3.Rows[index].Cells[6].Value + "," + txtalterid.Text + "','" + txtusedqty.Text + "','" + txtperqty.Text + "')";
                //qur.ExecuteNonQuery();
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            }
        }

        private void txtproceesname_KeyDown(object sender, KeyEventArgs e)
        {
            loaduom();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

    }
}
