﻿namespace Naalwar
{
    partial class FrmSortIdentification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSortIdentification));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.txtWeptCount1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPileReadOpen = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtHSNCode = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cmbPercentage = new System.Windows.Forms.ComboBox();
            this.txtNoOfPanel = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.CmbPattren = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.DataGridSortIdentification = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.txtBody = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtRSolvage = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLSolvage = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtGraySize = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGrayWeight = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtWeptCount = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFileCountEnds = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFileCount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtGroundEnds = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtGroundCount = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPPI = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtReed = new System.Windows.Forms.TextBox();
            this.txtReedSpace = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CmbUom = new System.Windows.Forms.ComboBox();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.txtSize = new System.Windows.Forms.TextBox();
            this.txtSampleNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnOk = new System.Windows.Forms.Button();
            this.txtWp1 = new System.Windows.Forms.TextBox();
            this.txtWp2 = new System.Windows.Forms.TextBox();
            this.txtSortName = new System.Windows.Forms.TextBox();
            this.txtSellingPriceMtr = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDrawingNo = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPicks = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridSortId = new System.Windows.Forms.DataGridView();
            this.GrBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortIdentification)).BeginInit();
            this.panadd.SuspendLayout();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortId)).BeginInit();
            this.SuspendLayout();
            // 
            // GrBack
            // 
            this.GrBack.Controls.Add(this.grSearch);
            this.GrBack.Controls.Add(this.txtWeptCount1);
            this.GrBack.Controls.Add(this.label26);
            this.GrBack.Controls.Add(this.label24);
            this.GrBack.Controls.Add(this.txtPileReadOpen);
            this.GrBack.Controls.Add(this.label23);
            this.GrBack.Controls.Add(this.txtHSNCode);
            this.GrBack.Controls.Add(this.label21);
            this.GrBack.Controls.Add(this.label22);
            this.GrBack.Controls.Add(this.cmbPercentage);
            this.GrBack.Controls.Add(this.txtNoOfPanel);
            this.GrBack.Controls.Add(this.label20);
            this.GrBack.Controls.Add(this.CmbPattren);
            this.GrBack.Controls.Add(this.label19);
            this.GrBack.Controls.Add(this.DataGridSortIdentification);
            this.GrBack.Controls.Add(this.label18);
            this.GrBack.Controls.Add(this.txtBody);
            this.GrBack.Controls.Add(this.label17);
            this.GrBack.Controls.Add(this.txtRSolvage);
            this.GrBack.Controls.Add(this.label16);
            this.GrBack.Controls.Add(this.txtLSolvage);
            this.GrBack.Controls.Add(this.label15);
            this.GrBack.Controls.Add(this.txtGraySize);
            this.GrBack.Controls.Add(this.label14);
            this.GrBack.Controls.Add(this.txtGrayWeight);
            this.GrBack.Controls.Add(this.label13);
            this.GrBack.Controls.Add(this.txtWeptCount);
            this.GrBack.Controls.Add(this.label12);
            this.GrBack.Controls.Add(this.txtFileCountEnds);
            this.GrBack.Controls.Add(this.label10);
            this.GrBack.Controls.Add(this.txtFileCount);
            this.GrBack.Controls.Add(this.label11);
            this.GrBack.Controls.Add(this.txtGroundEnds);
            this.GrBack.Controls.Add(this.label9);
            this.GrBack.Controls.Add(this.txtGroundCount);
            this.GrBack.Controls.Add(this.label8);
            this.GrBack.Controls.Add(this.txtPPI);
            this.GrBack.Controls.Add(this.label7);
            this.GrBack.Controls.Add(this.txtReed);
            this.GrBack.Controls.Add(this.txtReedSpace);
            this.GrBack.Controls.Add(this.label6);
            this.GrBack.Controls.Add(this.label5);
            this.GrBack.Controls.Add(this.CmbUom);
            this.GrBack.Controls.Add(this.txtWeight);
            this.GrBack.Controls.Add(this.txtSize);
            this.GrBack.Controls.Add(this.txtSampleNo);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.label2);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Controls.Add(this.BtnOk);
            this.GrBack.Controls.Add(this.txtWp1);
            this.GrBack.Controls.Add(this.txtWp2);
            this.GrBack.Controls.Add(this.txtSortName);
            this.GrBack.Controls.Add(this.txtSellingPriceMtr);
            this.GrBack.Controls.Add(this.label25);
            this.GrBack.Controls.Add(this.txtDrawingNo);
            this.GrBack.Controls.Add(this.label27);
            this.GrBack.Controls.Add(this.txtPicks);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(7, -3);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(734, 506);
            this.GrBack.TabIndex = 0;
            this.GrBack.TabStop = false;
            this.GrBack.Enter += new System.EventHandler(this.GrBack_Enter);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(92, 165);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(383, 263);
            this.grSearch.TabIndex = 395;
            this.grSearch.Visible = false;
            this.grSearch.Paint += new System.Windows.Forms.PaintEventHandler(this.grSearch_Paint);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::Naalwar.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(182, 233);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(100, 28);
            this.btnSelect.TabIndex = 394;
            this.btnSelect.Text = "Select (F6)";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(284, 234);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(95, 28);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F7)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 7);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(375, 222);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // txtWeptCount1
            // 
            this.txtWeptCount1.Location = new System.Drawing.Point(432, 123);
            this.txtWeptCount1.Name = "txtWeptCount1";
            this.txtWeptCount1.Size = new System.Drawing.Size(273, 26);
            this.txtWeptCount1.TabIndex = 17;
            this.txtWeptCount1.Click += new System.EventHandler(this.txtWeptCount1_Click);
            this.txtWeptCount1.TextChanged += new System.EventHandler(this.TxtWeptCount1_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(346, 127);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(81, 18);
            this.label26.TabIndex = 415;
            this.label26.Text = "WeftCount1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(2, 447);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(119, 18);
            this.label24.TabIndex = 410;
            this.label24.Text = "Drawing Order No";
            // 
            // txtPileReadOpen
            // 
            this.txtPileReadOpen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPileReadOpen.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPileReadOpen.Location = new System.Drawing.Point(121, 409);
            this.txtPileReadOpen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPileReadOpen.Name = "txtPileReadOpen";
            this.txtPileReadOpen.Size = new System.Drawing.Size(222, 26);
            this.txtPileReadOpen.TabIndex = 12;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(15, 413);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 18);
            this.label23.TabIndex = 408;
            this.label23.Text = "Pile Read Open";
            // 
            // txtHSNCode
            // 
            this.txtHSNCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSNCode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHSNCode.Location = new System.Drawing.Point(121, 342);
            this.txtHSNCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtHSNCode.Name = "txtHSNCode";
            this.txtHSNCode.Size = new System.Drawing.Size(222, 26);
            this.txtHSNCode.TabIndex = 10;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(13, 379);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(104, 18);
            this.label21.TabIndex = 406;
            this.label21.Text = "GST Precentage";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(48, 346);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 18);
            this.label22.TabIndex = 404;
            this.label22.Text = "HSN Code";
            // 
            // cmbPercentage
            // 
            this.cmbPercentage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPercentage.FormattingEnabled = true;
            this.cmbPercentage.Location = new System.Drawing.Point(121, 375);
            this.cmbPercentage.Name = "cmbPercentage";
            this.cmbPercentage.Size = new System.Drawing.Size(222, 26);
            this.cmbPercentage.TabIndex = 11;
            // 
            // txtNoOfPanel
            // 
            this.txtNoOfPanel.Location = new System.Drawing.Point(626, 155);
            this.txtNoOfPanel.Name = "txtNoOfPanel";
            this.txtNoOfPanel.Size = new System.Drawing.Size(79, 26);
            this.txtNoOfPanel.TabIndex = 19;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(549, 159);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 18);
            this.label20.TabIndex = 40;
            this.label20.Text = "No of Panel";
            // 
            // CmbPattren
            // 
            this.CmbPattren.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPattren.FormattingEnabled = true;
            this.CmbPattren.Items.AddRange(new object[] {
            "WEFT PATTERN",
            "DENSITY"});
            this.CmbPattren.Location = new System.Drawing.Point(432, 324);
            this.CmbPattren.Name = "CmbPattren";
            this.CmbPattren.Size = new System.Drawing.Size(272, 26);
            this.CmbPattren.TabIndex = 26;
            this.CmbPattren.SelectedIndexChanged += new System.EventHandler(this.CmbPattren_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(373, 328);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 18);
            this.label19.TabIndex = 37;
            this.label19.Text = "Pattren";
            // 
            // DataGridSortIdentification
            // 
            this.DataGridSortIdentification.AllowUserToAddRows = false;
            this.DataGridSortIdentification.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSortIdentification.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortIdentification.Location = new System.Drawing.Point(432, 380);
            this.DataGridSortIdentification.Name = "DataGridSortIdentification";
            this.DataGridSortIdentification.RowHeadersVisible = false;
            this.DataGridSortIdentification.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortIdentification.Size = new System.Drawing.Size(273, 120);
            this.DataGridSortIdentification.TabIndex = 36;
            this.DataGridSortIdentification.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridSortIdentification_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(211, 131);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 18);
            this.label18.TabIndex = 35;
            this.label18.Text = "UOM";
            // 
            // txtBody
            // 
            this.txtBody.Location = new System.Drawing.Point(432, 294);
            this.txtBody.Name = "txtBody";
            this.txtBody.Size = new System.Drawing.Size(130, 26);
            this.txtBody.TabIndex = 24;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(388, 298);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 18);
            this.label17.TabIndex = 33;
            this.label17.Text = "Body";
            // 
            // txtRSolvage
            // 
            this.txtRSolvage.Location = new System.Drawing.Point(614, 258);
            this.txtRSolvage.Name = "txtRSolvage";
            this.txtRSolvage.Size = new System.Drawing.Size(99, 26);
            this.txtRSolvage.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(531, 262);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 18);
            this.label16.TabIndex = 31;
            this.label16.Text = "RSelvedge";
            // 
            // txtLSolvage
            // 
            this.txtLSolvage.Location = new System.Drawing.Point(432, 258);
            this.txtLSolvage.Name = "txtLSolvage";
            this.txtLSolvage.Size = new System.Drawing.Size(93, 26);
            this.txtLSolvage.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(356, 262);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 18);
            this.label15.TabIndex = 29;
            this.label15.Text = "LSelvedge";
            // 
            // txtGraySize
            // 
            this.txtGraySize.Location = new System.Drawing.Point(432, 223);
            this.txtGraySize.Name = "txtGraySize";
            this.txtGraySize.Size = new System.Drawing.Size(271, 26);
            this.txtGraySize.TabIndex = 21;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(365, 227);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 18);
            this.label14.TabIndex = 27;
            this.label14.Text = "GreySize";
            // 
            // txtGrayWeight
            // 
            this.txtGrayWeight.Location = new System.Drawing.Point(432, 155);
            this.txtGrayWeight.Name = "txtGrayWeight";
            this.txtGrayWeight.Size = new System.Drawing.Size(114, 26);
            this.txtGrayWeight.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(346, 159);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 18);
            this.label13.TabIndex = 25;
            this.label13.Text = "GreyWeight";
            // 
            // txtWeptCount
            // 
            this.txtWeptCount.Location = new System.Drawing.Point(432, 89);
            this.txtWeptCount.Name = "txtWeptCount";
            this.txtWeptCount.Size = new System.Drawing.Size(273, 26);
            this.txtWeptCount.TabIndex = 16;
            this.txtWeptCount.Click += new System.EventHandler(this.txtWeptCount_Click);
            this.txtWeptCount.TextChanged += new System.EventHandler(this.txtWeptCount_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(353, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 18);
            this.label12.TabIndex = 23;
            this.label12.Text = "WeftCount";
            // 
            // txtFileCountEnds
            // 
            this.txtFileCountEnds.Location = new System.Drawing.Point(432, 52);
            this.txtFileCountEnds.Name = "txtFileCountEnds";
            this.txtFileCountEnds.Size = new System.Drawing.Size(87, 26);
            this.txtFileCountEnds.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(390, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Ends";
            // 
            // txtFileCount
            // 
            this.txtFileCount.Location = new System.Drawing.Point(432, 19);
            this.txtFileCount.Name = "txtFileCount";
            this.txtFileCount.Size = new System.Drawing.Size(282, 26);
            this.txtFileCount.TabIndex = 14;
            this.txtFileCount.Click += new System.EventHandler(this.txtFileCount_Click);
            this.txtFileCount.TextChanged += new System.EventHandler(this.txtFileCount_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(355, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "Pile Count";
            // 
            // txtGroundEnds
            // 
            this.txtGroundEnds.Location = new System.Drawing.Point(121, 308);
            this.txtGroundEnds.Name = "txtGroundEnds";
            this.txtGroundEnds.Size = new System.Drawing.Size(216, 26);
            this.txtGroundEnds.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(80, 312);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 18);
            this.label9.TabIndex = 17;
            this.label9.Text = "Ends";
            // 
            // txtGroundCount
            // 
            this.txtGroundCount.Location = new System.Drawing.Point(121, 272);
            this.txtGroundCount.Name = "txtGroundCount";
            this.txtGroundCount.Size = new System.Drawing.Size(216, 26);
            this.txtGroundCount.TabIndex = 8;
            this.txtGroundCount.Click += new System.EventHandler(this.txtGroundCount_Click);
            this.txtGroundCount.TextChanged += new System.EventHandler(this.txtGroundCount_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 276);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 18);
            this.label8.TabIndex = 15;
            this.label8.Text = "GroundCount";
            // 
            // txtPPI
            // 
            this.txtPPI.Location = new System.Drawing.Point(121, 232);
            this.txtPPI.Name = "txtPPI";
            this.txtPPI.Size = new System.Drawing.Size(217, 26);
            this.txtPPI.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(89, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "PPI";
            // 
            // txtReed
            // 
            this.txtReed.Location = new System.Drawing.Point(121, 161);
            this.txtReed.Name = "txtReed";
            this.txtReed.Size = new System.Drawing.Size(217, 26);
            this.txtReed.TabIndex = 5;
            // 
            // txtReedSpace
            // 
            this.txtReedSpace.Location = new System.Drawing.Point(121, 196);
            this.txtReedSpace.Name = "txtReedSpace";
            this.txtReedSpace.Size = new System.Drawing.Size(217, 26);
            this.txtReedSpace.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Reed Space";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Reed";
            // 
            // CmbUom
            // 
            this.CmbUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbUom.FormattingEnabled = true;
            this.CmbUom.Location = new System.Drawing.Point(254, 127);
            this.CmbUom.Name = "CmbUom";
            this.CmbUom.Size = new System.Drawing.Size(73, 26);
            this.CmbUom.TabIndex = 4;
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(121, 125);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(87, 26);
            this.txtWeight.TabIndex = 3;
            // 
            // txtSize
            // 
            this.txtSize.Location = new System.Drawing.Point(121, 89);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(217, 26);
            this.txtSize.TabIndex = 2;
            // 
            // txtSampleNo
            // 
            this.txtSampleNo.Location = new System.Drawing.Point(121, 19);
            this.txtSampleNo.Name = "txtSampleNo";
            this.txtSampleNo.Size = new System.Drawing.Size(217, 26);
            this.txtSampleNo.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Weight";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Product Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = " Size Sort Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sample No";
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(660, 353);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(51, 26);
            this.BtnOk.TabIndex = 29;
            this.BtnOk.Text = "OK";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // txtWp1
            // 
            this.txtWp1.Location = new System.Drawing.Point(432, 353);
            this.txtWp1.Name = "txtWp1";
            this.txtWp1.Size = new System.Drawing.Size(134, 26);
            this.txtWp1.TabIndex = 27;
            // 
            // txtWp2
            // 
            this.txtWp2.Location = new System.Drawing.Point(566, 353);
            this.txtWp2.Name = "txtWp2";
            this.txtWp2.Size = new System.Drawing.Size(92, 26);
            this.txtWp2.TabIndex = 28;
            // 
            // txtSortName
            // 
            this.txtSortName.Location = new System.Drawing.Point(121, 52);
            this.txtSortName.Name = "txtSortName";
            this.txtSortName.Size = new System.Drawing.Size(217, 26);
            this.txtSortName.TabIndex = 1;
            // 
            // txtSellingPriceMtr
            // 
            this.txtSellingPriceMtr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSellingPriceMtr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSellingPriceMtr.Location = new System.Drawing.Point(432, 190);
            this.txtSellingPriceMtr.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSellingPriceMtr.Name = "txtSellingPriceMtr";
            this.txtSellingPriceMtr.Size = new System.Drawing.Size(197, 26);
            this.txtSellingPriceMtr.TabIndex = 20;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(342, 196);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 18);
            this.label25.TabIndex = 412;
            this.label25.Text = "Rate Per Pcs";
            // 
            // txtDrawingNo
            // 
            this.txtDrawingNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDrawingNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDrawingNo.Location = new System.Drawing.Point(121, 443);
            this.txtDrawingNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDrawingNo.Name = "txtDrawingNo";
            this.txtDrawingNo.Size = new System.Drawing.Size(222, 26);
            this.txtDrawingNo.TabIndex = 13;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(563, 298);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 18);
            this.label27.TabIndex = 417;
            this.label27.Text = "Towel Picks";
            // 
            // txtPicks
            // 
            this.txtPicks.Location = new System.Drawing.Point(641, 293);
            this.txtPicks.Name = "txtPicks";
            this.txtPicks.Size = new System.Drawing.Size(75, 26);
            this.txtPicks.TabIndex = 25;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnPrint);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Location = new System.Drawing.Point(4, 508);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(737, 36);
            this.panadd.TabIndex = 238;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(4, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 216;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(676, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(618, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Visible = false;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(532, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 31);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(589, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(72, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(662, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.BtnAddCancel_Click);
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.txtSearch);
            this.GrFront.Controls.Add(this.DataGridSortId);
            this.GrFront.Location = new System.Drawing.Point(6, 0);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(735, 505);
            this.GrFront.TabIndex = 40;
            this.GrFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(6, 9);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(723, 26);
            this.txtSearch.TabIndex = 1;
            // 
            // DataGridSortId
            // 
            this.DataGridSortId.AllowUserToAddRows = false;
            this.DataGridSortId.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSortId.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortId.Location = new System.Drawing.Point(6, 37);
            this.DataGridSortId.Name = "DataGridSortId";
            this.DataGridSortId.RowHeadersVisible = false;
            this.DataGridSortId.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortId.Size = new System.Drawing.Size(723, 458);
            this.DataGridSortId.TabIndex = 0;
            this.DataGridSortId.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridSortId_CellContentClick);
            // 
            // FrmSortIdentification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(745, 548);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GrBack);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmSortIdentification";
            this.Text = "Sort Identification";
            this.Load += new System.EventHandler(this.FrmSortIdentification_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortIdentification)).EndInit();
            this.panadd.ResumeLayout(false);
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortId)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.TextBox txtReed;
        private System.Windows.Forms.TextBox txtReedSpace;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CmbUom;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.TextBox txtSize;
        private System.Windows.Forms.TextBox txtSortName;
        private System.Windows.Forms.TextBox txtSampleNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFileCountEnds;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFileCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtGroundEnds;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtGroundCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPPI;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtWeptCount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBody;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtRSolvage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtLSolvage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtGraySize;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtGrayWeight;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView DataGridSortIdentification;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.TextBox txtWp2;
        private System.Windows.Forms.TextBox txtWp1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridSortId;
        private System.Windows.Forms.ComboBox CmbPattren;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtNoOfPanel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.TextBox txtHSNCode;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cmbPercentage;
        private System.Windows.Forms.TextBox txtDrawingNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtPileReadOpen;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSellingPriceMtr;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtWeptCount1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtPicks;
        private System.Windows.Forms.Label label27;
    }
}