﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace Naalwar
{
    public partial class FrmDCSales : Form
    {
        public FrmDCSales()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;

        //SqlCommand cmd;
        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmDCSales_Load(object sender, EventArgs e)
        {
                     
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;


            this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;


            //DTPDOCDT = DateTime.Now;
            Genpan.Visible = true;
            Editpan.Visible = false;
            panadd.Visible = true;
            Titlep();
            chkact.Checked = true;
            chkedtact.Checked = true;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            if (Genclass.Dtype == 150)
            {
                label1.Text = "Returnable DC";
                txtcusorderno.Visible = false;
                label9.Visible = false;

            }
            else if (Genclass.Dtype == 160)
            {
                label1.Text = " Non Returnable DC";
                txtcusorderno.Visible = false;
                label9.Visible = false;

            }
            else if (Genclass.Dtype == 170)
            {
                label1.Text = " Non Returnable Receipt";
                label9.Text = "DC NO";
            }
            Loadgrid();
        }

        private void loadput1()
        {
            conn.Open();
            //txttitemid.Text = "";

            if (Genclass.type == 4)
            {

                //Genclass.Module.Partylistviewcont3("uid", "Item","Itemcode", Genclass.strsql, this, txttitemid, txtitemname,txtdcid, Editpan);

                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                if (Genclass.data1 == 1)
                {
                    Genclass.Module.Partylistviewcont3("uid", "itemname", "itemcode", Genclass.strsql, this, txtnrid, txtnar, txtitemcode, Editpan);

                    //Genclass.strsql = "select uid,itemname ,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";
                    Genclass.strsql = "select itemid,itemname,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join partym h on a.partyuid=h.uid  where a.partyuid=" + txtpuid.Text + " group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                }
                else
                {
                    Genclass.Module.Partylistviewcont3("uid", "Itemcode", "itemname", Genclass.strsql, this, txtnrid, txtitemcode, txtnar, Editpan);
                    Genclass.strsql = "select distinct uid,itemcode,itemname FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";

                    Genclass.FSSQLSortStr = "itemcode";

                }
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                FrmLookup1 contc = new FrmLookup1();
                TabControl tab = (TabControl)contc.Controls["tabC"];


                TabPage tab1 = (TabPage)tab.Controls["tabPage1"];
                DataGridView grid = (DataGridView)tab1.Controls["HFGP4"];
                grid.Refresh();
                grid.ColumnCount = tap.Columns.Count;
                grid.Columns[0].Visible = false;
                grid.Columns[1].Width = 380;
                grid.Columns[2].Width = 250;



                grid.DefaultCellStyle.Font = new Font("Arial", 10);

                grid.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    grid.Columns[Genclass.i].Name = column.ColumnName;
                    grid.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                grid.DataSource = tap;





                if (Genclass.data1 == 1)
                {

                    Genclass.Module.Partylistviewcont3("uid", "itemname", "itemcode", Genclass.strsql, this, txtnrid, txtnar, txtitemcode, Editpan);

                    Genclass.strsql = "select c.uid,c.itemname,c.itemcode from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);

                    TabPage tab3 = (TabPage)tab.Controls["tabPage2"];
                    DataGridView grid2 = (DataGridView)tab3.Controls["HFGP2"];
                    grid2.Refresh();
                    grid2.ColumnCount = tap2.Columns.Count;
                    grid2.Columns[0].Visible = false;
                    grid2.Columns[1].Width = 380;
                    grid2.Columns[2].Width = 250;



                    grid2.DefaultCellStyle.Font = new Font("Arial", 10);

                    grid2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    grid2.AutoGenerateColumns = false;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap2.Columns)
                    {
                        grid2.Columns[Genclass.i].Name = column.ColumnName;
                        grid2.Columns[Genclass.i].HeaderText = column.ColumnName;
                        grid2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }


                    grid2.DataSource = tap2;


                    Genclass.Module.Partylistviewcont3("uid", "itemname", "itemcode", Genclass.strsql, this, txtnrid, txtnar, txtitemcode, Editpan);

                    Genclass.strsql = "select distinct uid,itemname,itemcode FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap3 = new DataTable();
                    aptr3.Fill(tap3);

                    TabPage tab4 = (TabPage)tab.Controls["tabPage3"];
                    DataGridView grid3 = (DataGridView)tab4.Controls["HFGP3"];
                    grid3.Refresh();
                    grid3.ColumnCount = tap3.Columns.Count;
                    grid3.Columns[0].Visible = false;
                    grid3.Columns[1].Width = 380;
                    grid3.Columns[2].Width = 250;



                    grid3.DefaultCellStyle.Font = new Font("Arial", 10);

                    grid3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    grid3.AutoGenerateColumns = false;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap3.Columns)
                    {
                        grid3.Columns[Genclass.i].Name = column.ColumnName;
                        grid3.Columns[Genclass.i].HeaderText = column.ColumnName;
                        grid3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }

                    grid3.DataSource = tap3;
                }


                contc.Show();
                conn.Close();
            }
        }

        private void Titlep()
        {

            //DataTable dt = new DataTable();
            //dt.Columns.Add("Item");
            //dt.Columns.Add("Qty");
            //dt.Columns.Add("itid");

            if (Genclass.Dtype == 110)
            {
                HFIT.ColumnCount = 5;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";

                HFIT.Columns[2].Name = "itemid";
                HFIT.Columns[3].Name = "Refid";

                HFIT.Columns[0].Width = 450;
                HFIT.Columns[1].Width = 75;

                HFIT.Columns[2].Visible = false;
                HFIT.Columns[4].Name = "Orderno";
                HFIT.Columns[4].Width = 120;
                HFIT.Columns[3].Visible = false;
            }
            else if (Genclass.Dtype == 170)
            {
                HFIT.ColumnCount = 5;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";

                HFIT.Columns[2].Name = "itemid";
                HFIT.Columns[3].Name = "Refid";

                HFIT.Columns[0].Width = 450;
                HFIT.Columns[1].Width = 75;

                HFIT.Columns[2].Visible = false;
                HFIT.Columns[4].Name = "DC NO";
                HFIT.Columns[4].Width = 120;
                HFIT.Columns[3].Visible = false;
            }

            else if (Genclass.Dtype == 150)
            {
                HFIT.ColumnCount = 3;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";

                HFIT.Columns[2].Name = "itemid";


                HFIT.Columns[0].Width = 550;
                HFIT.Columns[1].Width = 80;

                HFIT.Columns[2].Visible = false;
            }
            else if (Genclass.Dtype == 160)
            {
                HFIT.ColumnCount = 3;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";

                HFIT.Columns[2].Name = "itemid";


                HFIT.Columns[0].Width = 550;
                HFIT.Columns[1].Width = 80;

                HFIT.Columns[2].Visible = false;


            }





            //HFIT.DataSource = dt.DefaultView.Table;

        }



        private void loadput()
        {
            conn.Open();

            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                //Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + "";
                //Genclass.strsql = " select distinct c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0";
                Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + " order by name";
                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 3)
            {
                Genclass.Module.Partylistviewcont2("uid", "Docno", "Qty", "Qty", Genclass.strsql, this, txtgrnid, txtgrnno, txtfqty, txtqty, Editpan);
                //Genclass.strsql = "select distinct a.uid,a.docno as grnno from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join   transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join  transactionsplist   f on  b.uid=f.refuid   and f.doctypeid=30  left join itemsmbom g on b.itemuid=g.itemm_uid inner join itemm e on g.useditem_uid=e.uid    where a.uid=1 group by a.uid,a.docno,b.pqty having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(f.pqty),0) >0 ";
                Genclass.strsql = "select uid,docno,isnull(sum(a.dcqty),0)-isnull(sum(b.PQty),0) as qty,isnull(sum(a.dcqty),0)-isnull(sum(b.PQty),0) as qty from (select distinct a.uid,a.docno,b.DCQty,c.Uid as puid,c.name from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid where c.uid=" + txtpuid.Text + " and d.uid=" + txtnrid.Text + ") a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b on a.uid=b.Refuid group by uid,docno";
                Genclass.FSSQLSortStr = "docno";
            }
            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont2("uid", "Docno", "Qty", "Qty", Genclass.strsql, this, txtgrnid, txtgrnno, txtfqty, txtqty, Editpan);
                Genclass.strsql = "select distinct  b.uid,a.docno as grnno,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) as qty,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=50 where a.partyuid=" + txtpuid.Text + " and b.itemuid=" + txtnrid.Text + " group by b.uid,a.docno,b.pqty having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) >0";
                Genclass.FSSQLSortStr = "docno";
            }
            else if (Genclass.type == 4)
            {
                Genclass.Module.Partylistviewcont3("uid", "itemcode", "itemname", Genclass.strsql, this, txtnrid,txtititd,txtnar,Editpan);
                //Genclass.strsql = "select distinct  Z.uid,Z.itemname from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 and a.companyid=" + Genclass.data1 + " inner join itemm z on b.itemuid=z.uid inner join partym c on a.partyuid=c.uid left join transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=30 where a.partyuid=" + txtpuid.Text + " group by Z.uid,Z.itemname,b.pqty having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) >0";
                Genclass.strsql = " select uid,itemcode,itemname from itemm where active=1 and companyid=" + Genclass.data1 + " ";

                Genclass.FSSQLSortStr = "itemcode";
                Genclass.FSSQLSortStr1 = "itemname";

            }
            else if (Genclass.type == 5)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtpuid, txtname, Editpan);
                Genclass.strsql = " select distinct uid,name from (select  c.Uid,c.name from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid) a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b  on a.uid=b.Refuid";

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 6)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtnrid, txtnar, Editpan);
                Genclass.strsql = " select distinct uid,itemname from (select  d.Uid,d.itemname  from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid) a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b  on a.uid=b.Refuid";

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 7)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtnrid, txtnar, Editpan);
                Genclass.strsql = " select uid,itemname from itemm where active=1 and companyid=" + Genclass.data1 + "";

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 8)
            {
                Genclass.Module.Partylistviewcont("uid", "docno", Genclass.strsql, this, txtcusid, txtcusorderno, Editpan);
                Genclass.strsql = "select a.uid, a.docno  from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=80 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join transactionsplist e on     b.uid=e.refuid and e.doctypeid=30 group by  a.uid, a.docno, b.pqty  having     b.pqty-isnull(sum(e.pqty),0) >0";

                Genclass.FSSQLSortStr = "docno";
            }
            else if (Genclass.type == 9)
            {
                Genclass.Module.Partylistviewcont("uid", "docno", Genclass.strsql, this, txtcusid, txtcusorderno, Editpan);
                Genclass.strsql = "select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0) as  qty ,c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid  where c.uid=" + txtpuid.Text + " group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0   ";

                Genclass.FSSQLSortStr = "docno";
            }
            else if (Genclass.type == 10)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                //Genclass.strsql = "select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0) as  qty ,c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid  where c.uid=" + txtpuid.Text + " group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0   ";
                Genclass.strsql = "select puid,name from (select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0) as  qty ,c.Uid as puid,c.name    from transactionsp a inner join  transactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=150 left join     transactionsplist f on b.uid=f.refuid   and f.DocTypeID=170      inner join  partym c  on a.partyuid=c.uid   group by a.uid,docno,    b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)>0 )tab ";
                Genclass.FSSQLSortStr = "Name";
            }
            else if (Genclass.type == 11)
            {
                Genclass.Module.Partylistviewcont2("uid", "ItemName", "qty", "listid", Genclass.strsql, this, txtnrid, txtnar, txtfqty, txtbuid, Editpan);
                //Genclass.strsql = "select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0) as  qty ,c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid  where c.uid=" + txtpuid.Text + " group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0   ";
                Genclass.strsql = "select itemid,ItemName,qty,listid from  (select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0) as  qty ,c.Uid as puid,c.name , b.uid as listid,g.Uid as itemid,g.ItemName    from transactionsp a inner join  transactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=150 left join         transactionsplist f on b.uid=f.refuid   and f.DocTypeID=170      inner join  partym c  on a.partyuid=c.uid left join       itemm g on b.itemuid=g.uid  where c.Uid=" + txtpuid.Text + " group by a.uid,docno,    b.pqty,c.Uid,c.name ,g.uid,g.itemname, b.uid  having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)>0 )tab ";
                Genclass.FSSQLSortStr = "docno";
            }

            else if (Genclass.type == 12)
            {
                Genclass.Module.Partylistviewcont("uid", "docno", Genclass.strsql, this, txtcusid, txtcusorderno, Editpan);
                //Genclass.strsql = "select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0) as  qty ,c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid  where c.uid=" + txtpuid.Text + " group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0   ";
                Genclass.strsql = "select  uid,docno from  (select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0) as  qty ,c.Uid as puid,c.name , b.uid as listid,g.Uid as itemid,g.ItemName    from transactionsp a inner join  transactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=150 left join         transactionsplist f on b.uid=f.refuid   and f.DocTypeID=170      inner join  partym c  on a.partyuid=c.uid left join       itemm g on b.itemuid=g.uid  where c.Uid=" + txtpuid.Text + " group by a.uid,docno,    b.pqty,c.Uid,c.name ,g.uid,g.itemname, b.uid  having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)>0 )tab ";
                Genclass.FSSQLSortStr = "ItemName";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Genclass.type == 4)
            {
                dt.Columns[1].Width = 340;
            }
            else
            {
                dt.Columns[1].Width = 400;
            }

            if (Genclass.type == 2 || Genclass.type == 3 || Genclass.type == 11)
            {
                dt.Columns[2].Width = 100;
                dt.Columns[3].Visible = false;
            }
            else if(Genclass.type == 4)
            {
                dt.Columns[2].Width = 350;
            }

            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();
            conn.Close();


        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "Name";

                if (Genclass.Dtype == 110)
                {
                    Genclass.FSSQLSortStr4 = "narration";
                }





                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    if (Genclass.Dtype == 110 || Genclass.Dtype == 150 || Genclass.Dtype == 160 || Genclass.Dtype == 170)
                    {
                        Genclass.StrSrch = "a.uid <> 0";
                    }


                }

                if (chkact.Checked == true)
                {
                    if (Genclass.Dtype == 110)
                    {
                        if (Genclass.Dtype == 110)
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " ";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                        else
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                    }
                    else if (Genclass.Dtype == 150)
                    {
                        if (Genclass.Dtype == 150)
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " ";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                        else
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                    }
                    else if (Genclass.Dtype == 160)
                    {
                        if (Genclass.Dtype == 160)
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " ";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                        else
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                    }
                    else if (Genclass.Dtype == 170)
                    {
                        if (Genclass.Dtype == 170)
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " ";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                        else
                        {
                            string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                            Genclass.cmd = new SqlCommand(quy, conn);
                        }
                    }
                }


                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 88;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 90;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Width = 460;
                HFGP.Columns[6].Visible = false;



                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;






                HFGP.DataSource = tap;
                //int ct = tap.Rows.Count;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                //int index = HFGP.Rows.Count - 1;

                //if (HFGP.Rows[0].Cells[1].Value == "" || HFGP.Rows[0].Cells[1].Value == null)
                //{
                //    lblno1.Text = "0";
                //}
                //else
                //{
                //    lblno1.Text = "1";
                //}


                //lblno2.Text = "of " + index.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }




        private void button1_Click(object sender, EventArgs e)
        {



        }


        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            //Genclass.Module.ClearTextBox(this);
            Genpan.Visible = true;
            //Loadgrid();
        }
        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = " select b.uid,e.itemname,b.pqty-isnull(sum(d.pqty),0) as qty ,b.addnotes,b.itemuid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=125 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=130 inner join itemm e on b.itemuid=e.uid  group by e.itemname,b.pqty, b.addnotes,b.itemuid  having b.pqty-isnull(sum(d.pqty),0) >0";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Visible = false;

                HFIT.Columns[1].Width = 120;
                HFIT.Columns[2].Width = 120;
                HFIT.Columns[3].Width = 120;
                HFIT.Columns[4].Visible = false;
                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }


        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }



        private void txtgrnno_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (Genclass.Dtype == 20)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Genclass.type = 2;
                    loadput();

                    foreach (DataGridViewColumn dc in HFIT.Columns)
                    {
                        if (dc.Name == "Qty")
                        {
                            dc.ReadOnly = false;
                        }
                        else
                        {
                            dc.ReadOnly = true;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    txtgrnno.Text = "";
                    txtgrnno.Focus();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Genclass.type = 3;
                    loadput();

                    foreach (DataGridViewColumn dc in HFIT.Columns)
                    {
                        if (dc.Name == "Qty")
                        {
                            dc.ReadOnly = false;
                        }
                        else
                        {
                            dc.ReadOnly = true;
                        }
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    txtgrnno.Text = "";
                    txtgrnno.Focus();
                }
            }
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Genclass.Module.ClearTextBox(this, Editpan);
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            chkact.Checked = true;
            Loadgrid();
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {

        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Editpan.Visible = true;
            Genpan.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;

            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();

            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtnar.Text = HFGP.Rows[i].Cells[7].Value.ToString();

            if (HFGP.Rows[i].Cells[8].Value.ToString() == "True")
            {
                chkedtact.Checked = true;
            }
            else
            {
                chkedtact.Checked = false;
            }


            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            Loadgrid2();
        }


        private void Loadgrid2()
        {
            try
            {
                conn.Open();


                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";
                Genclass.cmd = new SqlCommand(quy, conn);





                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;

                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {


        }


        private void cmdprt_Click(object sender, EventArgs e)
        {
            conn.Open();


            Genclass.Gbtxtid = 101;

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            //Crviewer crv = new Crviewer();
            //crv.Show();
            //conn.Close();
        }


        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            conn.Open();
            mode = 3;
            int i = HFGP.SelectedCells[0].RowIndex;
            qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "',0,'" + txtnar.Text + "',0,0,0,0,0,0,''," + Genclass.data1 + "," + Genclass.Yearid + ",0,0,0,0,'',0,0,0,0,0,0,0,0,0," + HFGP.Rows[i].Cells[0].Value + ",0," + i + "," + mode + ",null";
            qur.ExecuteNonQuery();
            conn.Close();
            Loadgrid();
        }


        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "myla123";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;

        }

        //public static string Decrypt(string cipherText)
        //{
        //    string EncryptionKey = "myla123";
        //    cipherText = cipherText.Replace(" ", "+");
        //    byte[] cipherBytes = Convert.FromBase64String(cipherText);
        //    using (Aes encryptor = Aes.Create())
        //    {
        //        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
        //        encryptor.Key = pdb.GetBytes(32);
        //        encryptor.IV = pdb.GetBytes(16);
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
        //            {
        //                cs.Write(cipherBytes, 0, cipherBytes.Length);
        //                cs.Close();
        //            }
        //            cipherText = Encoding.Unicode.GetString(ms.ToArray());
        //        }
        //    }
        //    return cipherText;
        //}

        //private void ClearText_TextChanged(object sender, EventArgs e)
        //{

        //}

        //private void btnEncrypt_Click(object sender, EventArgs e)
        //{
        //    txtCipherText.Text = Encrypt(ClearText.Text);
        //}

        //private void btnDecrypt_Click(object sender, EventArgs e)
        //{
        //    txtDecryptedText.Text = Decrypt(txtCipherText.Text);
        //}


        private void txtnar_KeyDown(object sender, KeyEventArgs e)
        {



        }

        //private void txtgrnid_TextChanged_1(object sender, EventArgs e)
        //{

        //    if (Genclass.Dtype == 20)
        //    {
        //        if (txtgrnid.Text == "")
        //        {
        //            return;

        //        }
        //        else
        //        {
        //            //Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0) as pqty,b.itemuid,b.uid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid where a.uid=" + txtgrnid.Text + " group by itemname,b.pqty,b.itemuid,b.uid  having b.pqty-isnull(sum(d.pqty),0) >0 ";
        //            Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(f.pqty),0) as pqty,b.itemuid,b.uid,a.docno from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join  transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid left join transactionsplist f on  b.uid=f.refuid and f.doctypeid=50 where a.uid=" + txtgrnid.Text + " and b.itemuid=" + txtnrid.Text + " group by itemname,b.pqty,b.itemuid,b.uid,a.docno  having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(f.pqty),0) >0 ";
        //            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //            DataTable tap1 = new DataTable();
        //            aptr1.Fill(tap1);
        //            //for (int i = 0; i < tap1.Rows.Count; i++)
        //            //{
        //            var index = HFIT.Rows.Add();
        //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[0]["itemname"].ToString();
        //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[0]["pqty"].ToString();
        //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[0]["itemuid"].ToString();
        //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[0]["uid"].ToString();
        //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[0]["docno"].ToString();
        //            //}

        //        }
        //    }
        //    else if (Genclass.Dtype == 30)
        //    {
        //        if (txtgrnid.Text == "")
        //        {
        //            return;

        //        }
        //        else
        //        {
        //            //Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0) as pqty,b.itemuid,b.uid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid where a.uid=" + txtgrnid.Text + " group by itemname,b.pqty,b.itemuid,b.uid  having b.pqty-isnull(sum(d.pqty),0) >0 ";
        //            Genclass.strsql = "Select itemname,DCQty-isnull(sum(pqty),0) as pqty,uid,itid,docno from (select distinct a.uid,a.docno,b.DCQty,c.Uid as puid,c.name,d.itemname,d.uid as itid from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid) a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b on a.uid=b.Refuid group by DCQty,itemname,uid,itid,docno";
        //            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //            DataTable tap1 = new DataTable();
        //            aptr1.Fill(tap1);
        //            //for (int i = 0; i < tap1.Rows.Count; i++)
        //            //{
        //            var index = HFIT.Rows.Add();
        //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[0]["itemname"].ToString();
        //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[0]["pqty"].ToString();
        //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[0]["itid"].ToString();
        //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[0]["uid"].ToString();
        //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[0]["docno"].ToString();
        //            //}

        //        }
        //    }

        //    txtnar.Text = "";
        //    //txtgrnno.Text = "";
        //    //txtgrnid.Text = "";
        //    txtnar.Focus();

        //}


        private void buttrqok_Click(object sender, EventArgs e)
        {


        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtgrnno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void butedit_Click(object sender, EventArgs e)
        {

        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Open();


            Genclass.Gbtxtid = 101;

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            //Crviewer crv = new Crviewer();
            //crv.Show();
            //conn.Close();
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtcusorderno_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcusorderno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcusid_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            mode = 1;

            Genpan.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            Genclass.Module.Gendocno();
            Genpan.Visible = false;
            Editpan.Visible = true;

            txtgrn.Text = Genclass.ST;
            DateTime d = new DateTime();
            d = DateTime.Now;
            txtgrndt.Text = d.ToString("dd.MM.yyyy");
            txtdcno.Focus();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            mode = 1;

            Genpan.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            Genclass.Module.Gendocno();
            Genpan.Visible = false;
            Editpan.Visible = true;
            panadd.Visible = false;
            txtgrn.Text = Genclass.ST;
            DateTime d = new DateTime();
            d = DateTime.Now;
            txtgrndt.Text = d.ToString("dd.MM.yyyy");
            txtdcno.Focus();
        }

        private void buttnext1_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void butedit_Click_1(object sender, EventArgs e)
        {
            mode = 2;
            Editpan.Visible = true;
            Genpan.Visible = false;
            panadd.Visible = false;

            int i = HFGP.SelectedCells[0].RowIndex;

            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();


            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtnar.Text = HFGP.Rows[i].Cells[7].Value.ToString();

            if (HFGP.Rows[i].Cells[8].Value.ToString() == "True")
            {
                chkedtact.Checked = true;
            }
            else
            {
                chkedtact.Checked = false;
            }



            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            Loadgrid2();
        }

        private void txtname_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtcusorderno_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtcusorderno_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }
                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }
            }
        }

        private void txtcusid_TextChanged_1(object sender, EventArgs e)
        {
            //if (txtcusid.Text != "")
            //{


            //    //Genclass.strsql = "select Docno,convert(varchar,Docdate,105) as Docdate,a.transp,c.hsnid,c.itemname,uom,c.uid,a.uid as Tuid,b.uid as Listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1 as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval,b.width,b.length,b.nos from orderp a inner join orderplist b on a.uid=b.TransactionsPUId left join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid where a.uid=" + txtdcid.Text + " group by Docno,Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,b.width,b.length,b.nos,uom,a.uid,a.transp,b.uid having b.pqty-isnull(sum(z.pqty),0)>0";
            //    Genclass.strsql = "select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0) as  qty ,c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join stransactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0  ";


            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap1 = new DataTable();
            //    aptr1.Fill(tap1);

            //    txtpuid.Text = tap1.Rows[0]["puid"].ToString();
            //    txtname.Text = tap1.Rows[0]["name"].ToString();
            //}

        }

        private void txtnar_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnrid_TextChanged(object sender, EventArgs e)
        {
            //if (txtnrid.Text != "")
            //{


            //    //Genclass.strsql = "select Docno,convert(varchar,Docdate,105) as Docdate,a.transp,c.hsnid,c.itemname,uom,c.uid,a.uid as Tuid,b.uid as Listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1 as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval,b.width,b.length,b.nos from orderp a inner join orderplist b on a.uid=b.TransactionsPUId left join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid where a.uid=" + txtdcid.Text + " group by Docno,Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,b.width,b.length,b.nos,uom,a.uid,a.transp,b.uid having b.pqty-isnull(sum(z.pqty),0)>0";
            //    Genclass.strsql = "  select distinct a.uid,b.uid as listid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0) as  qty ,c.Uid as puid,c.name,  h.Uid,h.ItemName  from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid left join ItemM h on b.ItemUId=h.Uid    where a.uid =" + txtcusid.Text + "  group by a.uid,docno,b.pqty,c.Uid,c.name     ,h.Uid,h.ItemName,b.uid      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0";

            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap1 = new DataTable();
            //    aptr1.Fill(tap1);

            //    txtfqty.Text = tap1.Rows[0]["qty"].ToString();
            //    txtbuid.Text = tap1.Rows[0]["listid"].ToString();

            //}

            if (txtnrid.Text != "")
            {
                //Genclass.strsql = "select Docno,convert(varchar,Docdate,105) as Docdate,a.transp,c.hsnid,c.itemname,uom,c.uid,a.uid as Tuid,b.uid as Listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1 as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval,b.width,b.length,b.nos from orderp a inner join orderplist b on a.uid=b.TransactionsPUId left join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid where a.uid=" + txtdcid.Text + " group by Docno,Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,b.width,b.length,b.nos,uom,a.uid,a.transp,b.uid having b.pqty-isnull(sum(z.pqty),0)>0";

                FrmlookupItem contc1 = new FrmlookupItem();
                TabControl tab = (TabControl)contc1.Controls["TabC"];

                if (Genclass.ty1 == 2)
                {
                    TabPage tab1 = (TabPage)tab.Controls["TabPage1"];
                    DataGridView grid = (DataGridView)tab1.Controls["HFGP4"];

                    Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid     and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid where b.itemuid=" + txtnrid.Text + "  and a.partyuid=" + txtpuid.Text + " and itemname is not null   group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,b.prate,b.uid having       isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                        txtfqty.Text = tap1.Rows[0]["balqty"].ToString();
                        txtbuid.Text = tap1.Rows[0]["listid"].ToString();
                        txtcusorderno.Text = tap1.Rows[0]["DocNo"].ToString();
                    }
                }


                else if (Genclass.ty2 == 3)
                {
                    TabPage tab1 = (TabPage)tab.Controls["TabPage2"];
                    DataGridView grid = (DataGridView)tab1.Controls["HFGP2"];

                    Genclass.strsql = "select c.uid,c.itemname,c.itemcode,isnull(a.price,0) as price from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        //txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                        //txtprice.Text = tap1.Rows[0]["price"].ToString();
                        //txtbuid.Text = tap1.Rows[0]["listid"].ToString();
                    }
                }

                else if (Genclass.ty3 == 4)
                {
                    TabPage tab1 = (TabPage)tab.Controls["TabPage3"];
                    DataGridView grid = (DataGridView)tab1.Controls["HFGP3"];
                    Genclass.strsql = "select distinct uid,itemname,itemcode FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        //txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                        //txtprice.Text = tap1.Rows[0]["price"].ToString();
                        //txtbuid.Text = tap1.Rows[0]["listid"].ToString();
                    }
                }


            }

        }

        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }
        }

        private void txtnar_KeyDown_1(object sender, KeyEventArgs e)
        {
        }

        private void buttrqok_Click_1(object sender, EventArgs e)
        {
            if (Genclass.Dtype == 110)
            {
                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    if (txtnrid.Text == HFIT.Rows[i].Cells[2].Value.ToString() && txtcusid.Text == HFIT.Rows[i].Cells[3].Value.ToString())
                    {
                        MessageBox.Show("Item for this orderno already Exist");
                        txtnar.Text = "";
                        txtfqty.Text = "";
                        txtnrid.Text = "";
                        txtcusid.Text = "";
                        txtcusorderno.Text = "";
                        txtitem.Focus();
                        return;
                    }
                }
            }

            HFIT.AllowUserToAddRows = true;
            var index = HFIT.Rows.Add();
            if (Genclass.Dtype == 150 || Genclass.Dtype == 160)
            {
                HFIT.Rows[index].Cells[0].Value = txtnar.Text;
                HFIT.Rows[index].Cells[1].Value = txtfqty.Text;
                HFIT.Rows[index].Cells[2].Value = txtnrid.Text;
            }
            else if (Genclass.Dtype == 110 || Genclass.Dtype == 170)
            {
                HFIT.Rows[index].Cells[0].Value = txtnar.Text;
                HFIT.Rows[index].Cells[1].Value = txtfqty.Text;
                HFIT.Rows[index].Cells[2].Value = txtnrid.Text;
                HFIT.Rows[index].Cells[3].Value = txtbuid.Text;
                HFIT.Rows[index].Cells[4].Value = txtcusorderno.Text;
            }

            txtnar.Text = "";
            txtfqty.Text = "";
            txtnrid.Text = "";
            txtgrnid.Text = "";
            txtgrnno.Text = "";
            txtitem.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtgrn.Text == "")
            {
                MessageBox.Show("Enter the Grnno");
                txtname.Focus();
                return;
            }

            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
                return;
            }

            conn.Open();

            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {
                if (mode == 1)
                {
                    if (HFIT.Rows[i].Cells[3].Value.ToString() == "")
                    {
                        HFIT.Rows[i].Cells[3].Value = "0";
                    }
                    if (Genclass.Dtype == 110 || Genclass.Dtype == 170)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[3].Value + ",0,0,0,0,0,0,0,0,0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 150 || Genclass.Dtype == 160)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0,0,0,0,0,0,0,0,0,0,0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                }

                else
                {
                    if (Genclass.Dtype == 110 || Genclass.Dtype == 170)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[3].Value + ",0,0,0,0,0,0,0," + uid + ",0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 150 || Genclass.Dtype == 160)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0,0,0,0,0,0,0,0,0," + uid + ",0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                }
                if (mode == 1)
                {
                    qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + "";
                    qur.ExecuteNonQuery();
                }
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                conn.Close();
                btnaddrcan_Click_1(sender, e);
                Loadgrid();
                Genpan.Visible = true;
                panadd.Visible = true;
            }

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Loadgrid();
        }

        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }



            }
        }

        private void txtname_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }



            }
        }

        private void txtname_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }


            }

            else if (e.Button == MouseButtons.Left)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }


            }
        }





        private void txtname_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }

            }
            else if (e.Button == MouseButtons.Left)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 1;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 10;
                    loadput();
                }
            }
        }


        private void txtname_MouseMove(object sender, MouseEventArgs e)
        {

        }





        private void txtname_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void txtcusorderno_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }
                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }
            }
        }

        private void txtcusorderno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }
                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }
            }
        }

        private void txtcusorderno_MouseClick(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Right)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }
                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }
            }
            else if (e.Button == MouseButtons.Left)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }
                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }
            }
        }

        private void txtcusorderno_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }

                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }

            }
            else if (e.Button == MouseButtons.Left)
            {
                if (Genclass.Dtype == 110)
                {
                    Genclass.type = 9;
                    loadput();
                }

                if (Genclass.Dtype == 170)
                {
                    Genclass.type = 12;
                    loadput();
                }
            }
        }

        private void txtnar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 13)
            //{
            //    if (Genclass.Dtype == 110)
            //    {
            //        Genclass.type = 4;
            //        loadput1();
            //    }

            //    else if (Genclass.Dtype == 150)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }

            //    else if (Genclass.Dtype == 160)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }
            //    else if (Genclass.Dtype == 170)
            //    {
            //        Genclass.type = 11;
            //        loadput();
            //    }

            //}
        }

        private void txtnar_KeyUp(object sender, KeyEventArgs e)
        {
           
        }

        private void txtnar_MouseClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    if (Genclass.Dtype == 110)
            //    {
            //        Genclass.type = 4;
            //        loadput1();
            //    }

            //    else if (Genclass.Dtype == 150)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }

            //    else if (Genclass.Dtype == 160)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }
            //    else if (Genclass.Dtype == 170)
            //    {
            //        Genclass.type = 11;
            //        loadput();
            //    }

            //}
            //else if (e.Button == MouseButtons.Left)
            //{
            //    if (Genclass.Dtype == 110)
            //    {
            //        Genclass.type = 4;
            //        loadput1();
            //    }

            //    else if (Genclass.Dtype == 150)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }

            //    else if (Genclass.Dtype == 160)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }
            //    else if (Genclass.Dtype == 170)
            //    {
            //        Genclass.type = 11;
            //        loadput();
            //    }

            //}
        }

        private void txtnar_MouseDown(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    if (Genclass.Dtype == 110)
            //    {
            //        Genclass.type = 4;
            //        loadput1();
            //    }
            //    else if (Genclass.Dtype == 150)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }

            //    else if (Genclass.Dtype == 160)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }
            //    else if (Genclass.Dtype == 170)
            //    {
            //        Genclass.type = 11;
            //        loadput();
            //    }
            //}
            //else if (e.Button == MouseButtons.Left)
            //{
            //    if (Genclass.Dtype == 110)
            //    {
            //        Genclass.type = 4;
            //        loadput1();
            //    }

            //    else if (Genclass.Dtype == 150)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }

            //    else if (Genclass.Dtype == 160)
            //    {
            //        Genclass.type = 4;
            //        loadput();
            //    }
            //    else if (Genclass.Dtype == 170)
            //    {
            //        Genclass.type = 11;
            //        loadput();
            //    }
            //}
        }

        private void txtfqty_TextChanged(object sender, EventArgs e)
        {
            if (txtfqty.Text != "")
            {
                if (txtfqty.Text != Convert.ToString("0.00"))
                {
                    if (Genclass.Dtype == 110)
                    {
                        FrmlookupItem contc1 = new FrmlookupItem();
                        TabControl tab = (TabControl)contc1.Controls["TabC"];

                        if (Genclass.ty1 == 2)
                        {
                            TabPage tab1 = (TabPage)tab.Controls["TabPage1"];
                            DataGridView grid = (DataGridView)tab1.Controls["HFGP4"];

                            Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid     and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid where  b.itemuid=" + txtnrid.Text + " and a.partyuid="+ txtpuid.Text +"   and itemname is not null   group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,b.prate,b.uid having       isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0";


                            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                            DataTable tap1 = new DataTable();
                            aptr1.Fill(tap1);
                            if (tap1.Rows.Count > 0)
                            {
                                txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                                if (txtfqty.Text != txtqty.Text)
                                {
                                    if (Convert.ToDouble(txtfqty.Text) > Convert.ToDouble(txtqty.Text))
                                    {
                                        MessageBox.Show("Qty Exceeds");
                                        txtfqty.Text = "";
                                        txtfqty.Focus();
                                        return;
                                    }
                                }
                            }

                        }
                    }

                        else if (Genclass.Dtype == 170)
                        {
                            Genclass.strsql = "select itemid,ItemName,qty,listid from  (select distinct a.uid,a.docno,isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0) as  qty ,c.Uid as puid,c.name , b.uid as listid,g.Uid as itemid,g.ItemName    from transactionsp a inner join  transactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=150 left join         transactionsplist f on b.uid=f.refuid   and f.DocTypeID=170      inner join  partym c  on a.partyuid=c.uid left join       itemm g on b.itemuid=g.uid  where c.Uid=" + txtpuid.Text + " group by a.uid,docno,    b.pqty,c.Uid,c.name ,g.uid,g.itemname, b.uid  having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)>0 )tab ";


                            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                            DataTable tap1 = new DataTable();
                            aptr1.Fill(tap1);
                            if (tap1.Rows.Count > 0)
                            {
                                txtqty.Text = tap1.Rows[0]["qty"].ToString();
                                if (txtfqty.Text != txtqty.Text)
                                {
                                    if (Convert.ToDouble(txtfqty.Text) > Convert.ToDouble(txtqty.Text))
                                    {
                                        MessageBox.Show("Qty Exceeds");
                                        txtfqty.Text = "";
                                        txtfqty.Focus();
                                        return;
                                    }
                                }
                            }
                        }

                    }


                }
            }

        private void txtnar_Click(object sender, EventArgs e)
        {
           
                if (Genclass.Dtype == 110 && Genclass.data1==1)
                {
                    Genclass.type = 4;
                    loadput1();
                }
                else if (Genclass.Dtype == 110 && Genclass.data1 == 3)
                {
                    Genclass.type = 4;
                    loadput();
                }

                else if (Genclass.Dtype == 150)
                {
                    Genclass.type = 4;
                    loadput();
                }

                else if (Genclass.Dtype == 160)
                {
                    Genclass.type = 4;
                    loadput();
                }
                else if (Genclass.Dtype == 170)
                {
                    Genclass.type = 11;
                    loadput();
                }

            
        }

        private void txtscr1_TextChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }
        }
    }

