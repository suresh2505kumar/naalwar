﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;


namespace Subcontract
{
    public partial class FrmMix : Form
    {

        public FrmMix()
        {
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtrubber_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmdok_Click(object sender, EventArgs e)
        {
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "insert into  mixing (docdate,fromtime,totime,rubberid,mixing,chewt,rubcom,wtloss,opname) values ('" + txtdcodate.Text + "','" + fromdt.Text + "','" + todt.Text + "','" + txtrubberid.Text + "','" + txtmix.Text + "','" + txtche.Text + "','" + txtrb.Text + "','" + txtwt.Text + "','" + txtopname.Text + "')";
                qur.ExecuteNonQuery();
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            }
            //if (mode == 2)
            //{
            //    qur.CommandText = "update mixing  set docdate='" + txtdcodate.Text + "',fromtime='" + fromdt.Text + "',totime='" + todt.Text + "',rubberid='" + txtrubberid.Text + "',mixing='" + txtmix.Text + "',chewt='" + txtche.Text + "',rubcom='" + txtrb.Text + "',wtloss='" + txtwt.Text + "',opname='" + txtopname.Text + "'";
            //    qur.ExecuteNonQuery(); 
            //    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            //}
            conn.Close();
        }
        private void title()
        {
            conn.Close();
            conn.Open();
            string str = "select a.uid,a.docdate,a.fromtime,a.totime,a.rubberid,b.itemcode,a.mixing,a.chewt,a.rubcom,a.wtloss,a.opname from mixing a inner join recpmast b on a.rubberid=b.uid inner join itemm c on b.itid=c.uid";
            SqlCommand cmd = new SqlCommand(str, conn);
            SqlDataAdapter data = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            data.Fill(tab);
            HFG3.DataSource = null;

            HFG3.AutoGenerateColumns = false;
            HFG3.ColumnCount = 6;
            HFG3.Columns[0].Name = "uid";
            HFG3.Columns[0].HeaderText = "uid";
            HFG3.Columns[0].DataPropertyName = "uid";
            HFG3.Columns[0].Visible = false;

            HFG3.Columns[1].Name = "DocDate";
            HFG3.Columns[1].HeaderText = "docdate";
            HFG3.Columns[1].DataPropertyName = "DocDate";

            HFG3.Columns[2].Name = "fromtime";
            HFG3.Columns[2].HeaderText = "fromtime";
            HFG3.Columns[2].DataPropertyName = "fromtime";

            HFG3.Columns[3].Name = "totime";
            HFG3.Columns[3].HeaderText = "totime";
            HFG3.Columns[3].DataPropertyName = "totime";

            HFG3.Columns[4].Name = "rubberid";
            HFG3.Columns[4].HeaderText = "rubberid";
            HFG3.Columns[4].DataPropertyName = "rubberid";
            HFG3.Columns[4].Visible = false;

            HFG3.Columns[5].Name = "Rubber";
            HFG3.Columns[5].HeaderText = "Rubber";
            HFG3.Columns[5].DataPropertyName = "Rubber";

            HFG3.Columns[6].Name = "mixing";
            HFG3.Columns[6].HeaderText = "mixing";
            HFG3.Columns[6].DataPropertyName = "mixing";


            HFG3.Columns[7].Name = "chewt";
            HFG3.Columns[7].HeaderText = "Chemical Wt";
            HFG3.Columns[7].DataPropertyName = "chewt";

            HFG3.Columns[8].Name = "rubcom";
            HFG3.Columns[8].HeaderText = "rubberwt";
            HFG3.Columns[8].DataPropertyName = "rubcom";

            HFG3.Columns[9].Name = "wtloss";
            HFG3.Columns[9].HeaderText = "wtloss";
            HFG3.Columns[9].DataPropertyName = "wtloss";

            HFG3.Columns[10].Name = "opname";
            HFG3.Columns[10].HeaderText = "OpeartorName";
            HFG3.Columns[10].DataPropertyName = "opname";

            //Adding new col in drid

            HFG3.DataSource = tab;

        }
        private void FrmMix_Load(object sender, EventArgs e)
        {
            mode = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtrubber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                conn.Open();
                                //if (Genclass.gbtxtid==1)
                    
                 Module.Partylistviewcont("uid", "itemname", "total", Genclass.strsql, this, txtrubberid, txtrubber, txtche);
                Genclass.strsql = "select distinct itid as uid,itemname,total from recpmast  a inner join itemm b on a.itid=b.uid";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                Frmlookup contc = new Frmlookup();
                DataGridView dt = (DataGridView)contc.Controls["HFGP"];
                dt.Refresh();
                dt.ColumnCount = tap.Columns.Count;
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 200;
                dt.Columns[2].Width = 200;

                dt.DefaultCellStyle.Font = new Font("Arial", 10);

                dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                dt.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    dt.Columns[Genclass.i].Name = column.ColumnName;
                    dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                    dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                dt.DataSource = tap;
                contc.Show();
                conn.Close();
              
            }
        }

        private void txtrb_TextChanged(object sender, EventArgs e)
        {
          


        }
         
        }

    }
