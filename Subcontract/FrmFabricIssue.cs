﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmFabricIssue : Form
    {
        public FrmFabricIssue()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource bsVechile = new BindingSource();
        BindingSource bsParty = new BindingSource();
        BindingSource bsFabIssue = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int SelectId = 0;
        int FillId = 0;
        int DispatchMuid, DispatchDUid;
        private int EditMode = 0;
        int Active = 0;
        int PrintId = 0;
        private void txtVehicle_Click(object sender, EventArgs e)
        {
            try
            {
                LoadCommonGrid(1);
                Point loc = FindLocation(txtVehicle);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Vehicle Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        private void LoadCommonGrid(int id)
        {
            try
            {
                if (id == 1)
                {
                    FillId = 1;
                    DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetDispatch", conn);
                    bsVechile.DataSource = dt;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "DispatchNo";
                    DataGridCommon.Columns[1].HeaderText = "DispatchNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DispatchNo";

                    DataGridCommon.Columns[2].Name = "VehicleNo";
                    DataGridCommon.Columns[2].HeaderText = "VehicleNo";
                    DataGridCommon.Columns[2].DataPropertyName = "VehicleNo";
                    DataGridCommon.Columns[2].Width = 250;
                    DataGridCommon.DataSource = bsVechile;
                }
                else if (id == 2)
                {
                    FillId = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                    bsParty.DataSource = dt;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";

                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsParty;
                }
                else if (id == 3)
                {
                    FillId = 3;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                    bsParty.DataSource = dt;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";

                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsParty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                if (DataGridCommon.Rows.Count > 0)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (FillId == 1)
                    {
                        txtVehicle.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                        txtVechNo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                        txtVehicle.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDispatchSlip.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtSupplerName.Focus();
                        GetPackingSlipD(Convert.ToInt32(txtVehicle.Tag));
                        PrintId = 1;
                        btnPackingSlipPrint.Visible = true;
                        btnUpdateRoll.Visible = true;
                    }
                    else if (FillId == 2)
                    {
                        txtSupplerName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtSupplerName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtDispatchTo.Focus();
                    }
                    else if (FillId == 3)
                    {
                        txtDispatchTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDispatchTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtRemarks.Focus();
                    }
                    grSearch.Visible = false;
                }
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SelectId = 1;
                    if (DataGridCommon.Rows.Count > 0)
                    {
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        if (FillId == 1)
                        {
                            txtVehicle.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                            txtVehicle.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                            txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtDispatchSlip.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtSupplerName.Focus();
                            GetPackingSlipD(Convert.ToInt32(txtVehicle.Tag));
                        }
                        else if (FillId == 2)
                        {
                            txtSupplerName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtSupplerName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                            txtDispatchTo.Focus();
                        }
                        else if (FillId == 3)
                        {
                            txtDispatchTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                            txtDispatchTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                            txtRemarks.Focus();
                        }
                        grSearch.Visible = false;
                    }
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtSupplerName_Click(object sender, EventArgs e)
        {
            try
            {
                LoadCommonGrid(2);
                Point loc = FindLocation(txtSupplerName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Party Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtVehicle_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsVechile.Filter = string.Format("DispatchNo LIKE '%{0}%' or VehicleNo LIKE '%{1}%' ", txtVehicle.Text, txtVehicle.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GetPackingSlipD(int id)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@DispatchSlipMUID", id) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetDispatchSlipD", parameters, conn);
                DataGridFabricItem.DataSource = null;
                DataGridFabricItem.AutoGenerateColumns = false;
                DataGridFabricItem.ColumnCount = 16;
                DataGridFabricItem.Columns[0].Name = "SortNo";
                DataGridFabricItem.Columns[0].HeaderText = "SortNo";
                DataGridFabricItem.Columns[0].DataPropertyName = "SortNo";
                DataGridFabricItem.Columns[0].Width = 200;

                DataGridFabricItem.Columns[1].Name = "HSNCode";
                DataGridFabricItem.Columns[1].HeaderText = "HSNCode";
                DataGridFabricItem.Columns[1].DataPropertyName = "HSNCode";

                DataGridFabricItem.Columns[2].Name = "Meters";
                DataGridFabricItem.Columns[2].HeaderText = "Meters";
                DataGridFabricItem.Columns[2].DataPropertyName = "Meter";
                DataGridFabricItem.Columns[2].Width = 80;

                DataGridFabricItem.Columns[3].Name = "Weight";
                DataGridFabricItem.Columns[3].HeaderText = "Weight";
                DataGridFabricItem.Columns[3].DataPropertyName = "Wght";
                DataGridFabricItem.Columns[3].Width = 80;

                DataGridFabricItem.Columns[4].Name = "NoofBags";
                DataGridFabricItem.Columns[4].HeaderText = "NoofBags";
                DataGridFabricItem.Columns[4].DataPropertyName = "NoofBags";
                DataGridFabricItem.Columns[4].Width = 90;

                DataGridFabricItem.Columns[5].Name = "Rolltype";
                DataGridFabricItem.Columns[5].HeaderText = "Rolltype";
                DataGridFabricItem.Columns[5].DataPropertyName = "Rolltype";
                DataGridFabricItem.Columns[5].Width = 80;

                DataGridFabricItem.Columns[6].Name = "Price";
                DataGridFabricItem.Columns[6].HeaderText = "Price";
                DataGridFabricItem.Columns[6].DataPropertyName = "Price";
                DataGridFabricItem.Columns[6].Width = 100;
                DataGridFabricItem.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricItem.Columns[7].Name = "BasicValue";
                DataGridFabricItem.Columns[7].HeaderText = "BasicValue";
                DataGridFabricItem.Columns[7].DataPropertyName = "BasicValue";
                DataGridFabricItem.Columns[7].Width = 120;
                DataGridFabricItem.Columns[7].DefaultCellStyle.Format = "N2";
                DataGridFabricItem.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricItem.Columns[8].Name = "ID";
                DataGridFabricItem.Columns[8].HeaderText = "ID";
                DataGridFabricItem.Columns[8].DataPropertyName = "DUid";
                DataGridFabricItem.Columns[8].Visible = false;

                DataGridFabricItem.Columns[9].Name = "GSTPercentage";
                DataGridFabricItem.Columns[9].HeaderText = "GST%";
                DataGridFabricItem.Columns[9].DataPropertyName = "GStPercentage";
                DataGridFabricItem.Columns[9].Width = 80;

                DataGridFabricItem.Columns[10].Name = "SGST";
                DataGridFabricItem.Columns[10].HeaderText = "SGST";
                DataGridFabricItem.Columns[10].DataPropertyName = "SGST";
                DataGridFabricItem.Columns[10].Width = 80;
                DataGridFabricItem.Columns[10].DefaultCellStyle.Format = "N2";
                DataGridFabricItem.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridFabricItem.Columns[10].Visible = false;

                DataGridFabricItem.Columns[11].Name = "CGST";
                DataGridFabricItem.Columns[11].HeaderText = "CGST";
                DataGridFabricItem.Columns[11].DataPropertyName = "CGST";
                DataGridFabricItem.Columns[11].Width = 115;
                DataGridFabricItem.Columns[11].Width = 80;
                DataGridFabricItem.Columns[11].DefaultCellStyle.Format = "N2";
                DataGridFabricItem.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridFabricItem.Columns[11].Visible = false;

                DataGridFabricItem.Columns[12].Name = "IGST";
                DataGridFabricItem.Columns[12].HeaderText = "IGST";
                DataGridFabricItem.Columns[12].DataPropertyName = "IGST";
                DataGridFabricItem.Columns[12].Width = 80;
                DataGridFabricItem.Columns[12].DefaultCellStyle.Format = "N2";
                DataGridFabricItem.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridFabricItem.Columns[12].Visible = false;

                DataGridFabricItem.Columns[13].Name = "TaxValue";
                DataGridFabricItem.Columns[13].HeaderText = "TaxValue";
                DataGridFabricItem.Columns[13].DataPropertyName = "TaxValue";
                DataGridFabricItem.Columns[13].Width = 80;
                DataGridFabricItem.Columns[13].DefaultCellStyle.Format = "N2";
                DataGridFabricItem.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricItem.Columns[14].Name = "NetValue";
                DataGridFabricItem.Columns[14].HeaderText = "NetValue";
                DataGridFabricItem.Columns[14].DataPropertyName = "NetValue";
                DataGridFabricItem.Columns[14].DefaultCellStyle.Format = "N2";
                DataGridFabricItem.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricItem.Columns[15].Name = "UOM";
                DataGridFabricItem.Columns[15].HeaderText = "UOM";
                DataGridFabricItem.Columns[15].DataPropertyName = "UOM";
                DataGridFabricItem.Columns[15].Visible = false;
                DataGridFabricItem.DataSource = dataTable;
                LoadTotal();
                FilltaxGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmFabricIssue_Load(object sender, EventArgs e)
        {
            DtpMonth.Format = DateTimePickerFormat.Custom;
            DtpMonth.CustomFormat = "MMM-yyyy";
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            Active = 1;
            LoadButton(1);
            LoadSlipdet(0, "");
            GetPackingSlipD(0);
            LoadTax();
            LoadFrontGrid();
            LoadProcessing();
            LoadTaxGrid();
            LoadBranch();
            grdetView.Visible = false;
            PrintId = 0;
            btnUpdateRoll.Visible = false;
        }

        protected void LoadBranch()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeM_Uid", 31), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                txtFrom.DataSource = null;
                txtFrom.DisplayMember = "GeneralName";
                txtFrom.ValueMember = "Uid";
                txtFrom.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadTaxGrid()
        {
            try
            {
                DataGridTax.DataSource = null;
                DataGridTax.AutoGenerateColumns = false;
                DataGridTax.ColumnCount = 6;
                DataGridTax.Columns[0].Name = "HSNCode";
                DataGridTax.Columns[0].Name = "HSNCode";

                DataGridTax.Columns[1].Name = "GST%";
                DataGridTax.Columns[1].Name = "GST%";
                DataGridTax.Columns[1].Width = 80;
                DataGridTax.Columns[1].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[2].Name = "CGST";
                DataGridTax.Columns[2].Name = "CGST";
                DataGridTax.Columns[2].Width = 80;
                DataGridTax.Columns[2].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[3].Name = "SGST";
                DataGridTax.Columns[3].Name = "SGST";
                DataGridTax.Columns[3].Width = 80;
                DataGridTax.Columns[3].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[4].Name = "IGST";
                DataGridTax.Columns[4].Name = "IGST";
                DataGridTax.Columns[4].Width = 80;
                DataGridTax.Columns[4].DefaultCellStyle.Format = "N2";

                DataGridTax.Columns[5].Name = "TotalTax";
                DataGridTax.Columns[5].Name = "TotalTax";
                DataGridTax.Columns[5].Width = 80;
                DataGridTax.Columns[5].DefaultCellStyle.Format = "N2";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FilltaxGrid()
        {
            try
            {
                DataGridTax.Rows.Clear();
                DataTable dt = new DataTable();
                dt.Columns.Add("HSNCode", typeof(string));
                dt.Columns.Add("GSTPer", typeof(decimal));
                dt.Columns.Add("CGST", typeof(decimal));
                dt.Columns.Add("SGST", typeof(decimal));
                dt.Columns.Add("IGST", typeof(decimal));
                dt.Columns.Add("TaxValue", typeof(decimal));
                for (int i = 0; i < DataGridFabricItem.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = DataGridFabricItem.Rows[i].Cells[1].Value;
                    dr[1] = DataGridFabricItem.Rows[i].Cells[9].Value;
                    dr[2] = DataGridFabricItem.Rows[i].Cells[11].Value;
                    dr[3] = DataGridFabricItem.Rows[i].Cells[10].Value;
                    dr[4] = DataGridFabricItem.Rows[i].Cells[12].Value;
                    dr[5] = DataGridFabricItem.Rows[i].Cells[13].Value;
                    dt.Rows.Add(dr);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        int Index = DataGridTax.Rows.Add();
                        DataGridViewRow row = DataGridTax.Rows[Index];
                        row.Cells[0].Value = dt.Rows[i]["HSNCode"].ToString();
                        row.Cells[1].Value = dt.Rows[i]["GSTPer"].ToString();
                        row.Cells[2].Value = dt.Rows[i]["CGST"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["SGST"].ToString();
                        row.Cells[4].Value = 0;
                        row.Cells[5].Value = dt.Rows[i]["TaxValue"].ToString();
                    }
                    else
                    {
                        decimal gstper = 0;
                        if (dt.Rows[i]["GSTPer"].ToString() == "" || dt.Rows[i]["GSTPer"] == null)
                        {
                            gstper = 0;
                        }
                        else
                        {
                            gstper = Convert.ToDecimal(dt.Rows[i]["GSTPer"].ToString());
                        }
                        bool entryfound = CheckGridValue(dt.Rows[i]["HSNCode"].ToString(), gstper);
                        if (entryfound == true)
                        {
                            string HSNCode = dt.Rows[i]["HSNCode"].ToString();
                            decimal TaxPer = Convert.ToDecimal(dt.Rows[i]["GSTPer"].ToString());
                            decimal SGST = Convert.ToDecimal(dt.Rows[i]["CGST"].ToString());
                            decimal CGST = Convert.ToDecimal(dt.Rows[i]["SGST"].ToString());
                            decimal IGST = Convert.ToDecimal(dt.Rows[i]["IGST"].ToString());
                            decimal taValue = Convert.ToDecimal(dt.Rows[i]["TaxValue"].ToString());
                            for (int j = 0; j < DataGridTax.Rows.Count; j++)
                            {
                                string HSNCodeGrid = DataGridTax.Rows[j].Cells[0].Value.ToString();
                                decimal SGSTGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[3].Value.ToString());
                                decimal CGSTGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[2].Value.ToString());
                                decimal IGSTGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[4].Value.ToString());
                                decimal taValueGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[5].Value.ToString());
                                decimal taxPerGrid = Convert.ToDecimal(DataGridTax.Rows[j].Cells[1].Value.ToString());
                                if (HSNCode == HSNCodeGrid && TaxPer == taxPerGrid)
                                {
                                    DataGridTax.Rows[j].Cells[2].Value = (CGST + CGSTGrid).ToString("0.00");
                                    DataGridTax.Rows[j].Cells[3].Value = (SGST + SGSTGrid).ToString("0.00");
                                    DataGridTax.Rows[j].Cells[4].Value = (IGST + IGSTGrid).ToString("0.00");
                                    DataGridTax.Rows[j].Cells[5].Value = (taValue + taValueGrid).ToString("0.00");
                                }
                            }
                        }
                        else
                        {
                            int Index = DataGridTax.Rows.Add();
                            DataGridViewRow row = DataGridTax.Rows[Index];
                            row.Cells[0].Value = dt.Rows[i]["HSNCode"].ToString();
                            row.Cells[1].Value = dt.Rows[i]["GSTPer"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["CGST"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["SGST"].ToString();
                            row.Cells[4].Value = 0;
                            row.Cells[5].Value = dt.Rows[i]["TaxValue"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected bool CheckGridValue(string HSNCode, decimal taxpercentage)
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridTax.Rows)
                {
                    object val2 = row.Cells[0].Value;
                    object val3 = Convert.ToDecimal(row.Cells[1].Value);
                    if (val2 != null && val2.ToString() == HSNCode && val3 != null && val3.ToString() == taxpercentage.ToString())
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Load
                {
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    chckActive.Visible = true;
                    panNumbers.Visible = true;
                    btnNext.Visible = true;
                    btnFstNext.Visible = true;
                    btnBack.Visible = true;
                    btnFstBack.Visible = true;
                    btnPrint.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnPackingSlipPrint.Visible = true;
                }
                else if (id == 2)//add
                {
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    chckActive.Visible = false;
                    panNumbers.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    btnBack.Visible = false;
                    btnFstBack.Visible = false;
                    btnPrint.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnPackingSlipPrint.Visible = false;
                }
                else if (id == 3)//Edit
                {
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    chckActive.Visible = false;
                    panNumbers.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    btnBack.Visible = false;
                    btnFstBack.Visible = false;
                    btnPrint.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnPackingSlipPrint.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                if (DataGridCommon.Rows.Count > 0)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (FillId == 1)
                    {
                        txtVehicle.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                        txtVehicle.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDispatchSlip.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtSupplerName.Focus();
                        GetPackingSlipD(Convert.ToInt32(txtVehicle.Tag));
                        PrintId = 1;
                        btnPackingSlipPrint.Visible = true;
                        btnUpdateRoll.Visible = true;
                    }
                    else if (FillId == 2)
                    {
                        txtSupplerName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtSupplerName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtDispatchTo.Focus();
                    }
                    else if (FillId == 3)
                    {
                        txtDispatchTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDispatchTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtRemarks.Focus();
                    }
                    grSearch.Visible = false;
                }
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(2);
                EditMode = 0;
                DispatchDUid = 0;
                DispatchMuid = 0;
                txtVehicle.Enabled = true;
                txtDispatchSlip.Enabled = true;
                DataGridFabricItem.Enabled = true;
                ClearControl();
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, "Select PrFix from DocTypeM Where DocTypeId = 260", conn);
                txtDocNo.Text = dt.Rows[0]["PrFix"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            btnUpdateRoll.Visible = false;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDocNo.TextLength <= 15)
                {
                    MessageBox.Show("Enter DC Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDocNo.Focus();
                    return;
                }
                if (btnsave.Text == "Save")
                {
                    if (txtSupplerName.Text == string.Empty || txtDocNo.Text == string.Empty)
                    {
                        MessageBox.Show("Supplier nae can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSupplerName.Focus();
                        return;
                    }
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, "Select * from DispatchM Where DocNo ='" + txtDocNo.Text + "'", conn);
                    if (dataTable.Rows.Count == 0)
                    {
                        if (txtVehicle.Text != string.Empty)
                        {
                            SqlParameter[] parameters = {
                                new SqlParameter("@ID",DispatchMuid),
                                new SqlParameter("@PartyUid",txtSupplerName.Tag),
                                new SqlParameter("@DispatchSlipUid",txtVehicle.Tag),
                                new SqlParameter("@BasicValue",Convert.ToDecimal(txtBasic.Text)),
                                new SqlParameter("@Transpoter",txtTranspoter.Text),
                                new SqlParameter("@LRNO",txtLRNo.Text),
                                new SqlParameter("@Roff",Convert.ToDecimal(txtRoundOff.Text)),
                                new SqlParameter("@NetValue",Convert.ToDecimal(txtNetValue.Text)),
                                new SqlParameter("@DocNo",txtDocNo.Text),
                                new SqlParameter("@DocDate",Convert.ToDateTime(dtpGrnDate.Text)),
                                new SqlParameter("@ReturnId",SqlDbType.Int),
                                new SqlParameter("@Remarks",txtRemarks.Text),
                                new SqlParameter("@NOfProce",cmbFor.Text),
                                new SqlParameter("@PofSup",txtDispatchTo.Tag),
                                new SqlParameter("@PofSupAdd",DBNull.Value),
                                new SqlParameter("@Active",1),
                                new SqlParameter("@EWaybill",txtEwayBill.Text),
                                new SqlParameter("@Frm",txtFrom.SelectedValue),
                                new SqlParameter("@VchNo",txtVechNo.Text)
                            };
                            parameters[10].Direction = ParameterDirection.Output;
                            int uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DispatchMChngVer1", parameters, conn, 10);

                            for (int i = 0; i < DataGridFabricItem.Rows.Count; i++)
                            {
                                SqlParameter[] paraDet = {
                                    new SqlParameter("@DispatchUid",uid),
                                    new SqlParameter("@SortNo",DataGridFabricItem.Rows[i].Cells[0].Value.ToString()),
                                    new SqlParameter("@NoofBags",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[4].Value.ToString())),
                                    new SqlParameter("@Meters",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[2].Value.ToString())),
                                    new SqlParameter("@Wght",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[3].Value.ToString())),
                                    new SqlParameter("@Price",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[6].Value.ToString())),
                                    new SqlParameter("@BasicValue",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[7].Value.ToString())),
                                    new SqlParameter("@RollType",DataGridFabricItem.Rows[i].Cells[5].Value.ToString()),
                                    new SqlParameter("@Duid",DispatchDUid),
                                    new SqlParameter("@TaxPercentage",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[9].Value.ToString())),
                                    new SqlParameter("@SGST",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[10].Value.ToString())),
                                    new SqlParameter("@CSGT",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[11].Value.ToString())),
                                    new SqlParameter("@IGST",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[12].Value.ToString())),
                                    new SqlParameter("@TaxAmt",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[13].Value.ToString())),
                                    new SqlParameter("@UOM",DataGridFabricItem.Rows[i].Cells[15].Value.ToString()),
                                    new SqlParameter("@HSNCode",DataGridFabricItem.Rows[i].Cells[1].Value.ToString()),
                                    new SqlParameter("TotalValue",Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[14].Value.ToString()))
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DispatchD", paraDet, conn);
                            }
                            MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ClearControl();
                            LoadButton(3);
                        }
                    }
                    else
                    {
                        MessageBox.Show(txtDocNo.Text + " Document Number Exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDocNo.Focus();
                    }
                }
                else
                {
                    DispatchMuid = Convert.ToInt32(txtDocNo.Tag);
                    if (txtVehicle.Text != string.Empty)
                    {
                        SqlParameter[] parameters = {
                                new SqlParameter("@ID",DispatchMuid),
                                new SqlParameter("@PartyUid",txtSupplerName.Tag),
                                new SqlParameter("@DispatchSlipUid",txtDispatchSlip.Tag),
                                new SqlParameter("@BasicValue",Convert.ToDecimal(txtBasic.Text)),
                                new SqlParameter("@Transpoter",txtTranspoter.Text),
                                new SqlParameter("@LRNO",txtLRNo.Text),
                                new SqlParameter("@Roff",Convert.ToDecimal(txtRoundOff.Text)),
                                new SqlParameter("@NetValue",Convert.ToDecimal(txtNetValue.Text)),
                                new SqlParameter("@DocNo",txtDocNo.Text),
                                new SqlParameter("@DocDate",Convert.ToDateTime(dtpGrnDate.Text)),
                                new SqlParameter("@ReturnId",SqlDbType.Int),
                                new SqlParameter("@Remarks",txtRemarks.Text),
                                new SqlParameter("@NOfProce",cmbFor.Text),
                                new SqlParameter("@PofSup",txtDispatchTo.Tag),
                                new SqlParameter("@PofSupAdd",DBNull.Value),
                                new SqlParameter("@Active",1),
                                new SqlParameter("@EWaybill",txtEwayBill.Text),
                                new SqlParameter("@VchNo",txtVechNo.Text),
                                new SqlParameter("@Frm",txtFrom.SelectedValue),
                            };
                        parameters[10].Direction = ParameterDirection.Output;
                        int uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DispatchMChngVer1", parameters, conn, 10);
                        MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ClearControl();
                        LoadButton(3);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ClearControl()
        {
            try
            {
                Genclass.Module.ClearTextBox(this, grBack);
                DataGridFabricItem.DataSource = null;
                DataGridFabricItem.Rows.Clear();
                DataGridDet.DataSource = null;
                DataGridDet.Rows.Clear();
                DataGridTax.Rows.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DataGridFabricItem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F2)
                {
                    int Index = DataGridFabricItem.SelectedCells[0].RowIndex;
                    string SortNo = DataGridFabricItem.Rows[Index].Cells[0].Value.ToString();
                    LoadSlipdet(Convert.ToInt32(txtVehicle.Tag), SortNo);
                    grdetView.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadSlipdet(int id, string SortNo)
        {
            try
            {
                SqlParameter[] parameters = {
                        new SqlParameter("@DispatchSlipMUID",id),new SqlParameter("@SortNo",SortNo)
                    };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetDispatchSlipDet", parameters, conn);
                DataGridDet.DataSource = null;
                DataGridDet.AutoGenerateColumns = false;
                DataGridDet.ColumnCount = 4;
                DataGridDet.Columns[0].Name = "SortNo";
                DataGridDet.Columns[0].HeaderText = "SortNo";
                DataGridDet.Columns[0].DataPropertyName = "SortNo";
                DataGridDet.Columns[0].Width = 280;
                DataGridDet.Columns[1].Name = "RollNo";
                DataGridDet.Columns[1].HeaderText = "RollNo";
                DataGridDet.Columns[1].DataPropertyName = "RollNo";

                DataGridDet.Columns[2].Name = "Meter";
                DataGridDet.Columns[2].HeaderText = "Meter";
                DataGridDet.Columns[2].DataPropertyName = "Meter";

                DataGridDet.Columns[3].Name = "Weight";
                DataGridDet.Columns[3].HeaderText = "Weight";
                DataGridDet.Columns[3].DataPropertyName = "Weight";
                DataGridDet.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DataGridFabricItem_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5 || e.ColumnIndex == 6)
                {
                    decimal price = Convert.ToDecimal(DataGridFabricItem.Rows[e.RowIndex].Cells[6].Value.ToString());
                    decimal Weight = Convert.ToDecimal(DataGridFabricItem.Rows[e.RowIndex].Cells[3].Value.ToString());
                    decimal basicvalue = (Weight * price);
                    string val = basicvalue.ToString("0.00");
                    decimal GSTPercentage = Convert.ToDecimal(DataGridFabricItem.Rows[e.RowIndex].Cells[9].Value.ToString());
                    decimal tax = (basicvalue * GSTPercentage) / 100;

                    DataGridFabricItem.Rows[e.RowIndex].Cells[7].Value = val;
                    DataGridFabricItem.Rows[e.RowIndex].Cells[10].Value = (tax / 2).ToString("0.00");
                    DataGridFabricItem.Rows[e.RowIndex].Cells[11].Value = (tax / 2).ToString("0.00");
                    DataGridFabricItem.Rows[e.RowIndex].Cells[12].Value = 0.0;
                    DataGridFabricItem.Rows[e.RowIndex].Cells[13].Value = tax;
                    DataGridFabricItem.Rows[e.RowIndex].Cells[14].Value = basicvalue + tax;
                }
                LoadTotal();
                FilltaxGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadTotal()
        {
            try
            {
                if (DataGridFabricItem.Rows.Count != 0)
                {
                    decimal Sum = 0;
                    decimal TaxValue = 0;
                    for (int i = 0; i < DataGridFabricItem.Rows.Count; i++)
                    {
                        decimal basicvalue = Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[7].Value);
                        string val = basicvalue.ToString("0.00");
                        decimal GSTPercentage;
                        if (DataGridFabricItem.Rows[i].Cells[9].Value.ToString() == "" || DataGridFabricItem.Rows[i].Cells[9].Value == null)
                        {
                            GSTPercentage = 0;
                        }
                        else
                        {
                            GSTPercentage = Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[9].Value.ToString());
                        }
                        decimal tax = (basicvalue * GSTPercentage) / 100;
                        DataGridFabricItem.Rows[i].Cells[7].Value = val;
                        DataGridFabricItem.Rows[i].Cells[10].Value = (tax / 2).ToString("0.00");
                        DataGridFabricItem.Rows[i].Cells[11].Value = (tax / 2).ToString("0.00");
                        DataGridFabricItem.Rows[i].Cells[12].Value = 0.0;
                        DataGridFabricItem.Rows[i].Cells[13].Value = tax;
                        DataGridFabricItem.Rows[i].Cells[14].Value = basicvalue + tax;
                        Sum += Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[7].Value.ToString());
                        TaxValue += Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[13].Value.ToString());
                    }
                    txtBasic.Text = string.Empty;
                    txtBasic.Text = Sum.ToString("0.00");
                    txttotalValue.Text = TaxValue.ToString("0.00");
                    txtBaseTotal.Text = (Sum + TaxValue).ToString("0.00");
                    double a = Convert.ToDouble(txtBaseTotal.Text);
                    string[] str = a.ToString("0.00").Split('.');
                    double num1 = Convert.ToDouble("0." + str[1]);
                    double res;
                    if (num1 < 0.51)
                    {
                        res = Math.Floor(a);
                    }
                    else
                    {
                        res = Math.Round(a);
                    }
                    txtRoundOff.Text = (res - a).ToString("0.00");
                    txtNetValue.Text = res.ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CellValueChanged()
        {
            try
            {
                for (int i = 0; i < DataGridFabricItem.Rows.Count; i++)
                {
                    decimal price = Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[5].Value.ToString());
                    decimal Weight = Convert.ToDecimal(DataGridFabricItem.Rows[i].Cells[2].Value.ToString());
                    decimal basicvalue = (Weight * price);
                    string val = basicvalue.ToString("0.00");
                    DataGridFabricItem.Rows[i].Cells[6].Value = val;
                }
                LoadTotal();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = getTax();
                //cboTax.DataSource = null;
                //cboTax.DisplayMember = "GeneralName";
                //cboTax.ValueMember = "TaxPer";
                //cboTax.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtSupplerName_Enter(object sender, EventArgs e)
        {
            try
            {
                LoadCommonGrid(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void grBack_Enter(object sender, EventArgs e)
        {

        }

        private DataSet GetData(int Uid, string Tag, int Active)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@DispatchUid",0),
                    new SqlParameter("@Tag",Tag),
                     new SqlParameter("@Active",Active)
                };
                ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetDispatchM", parameters, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        protected void LoadFrontGrid()
        {
            try
            {
                DataSet ds = new DataSet();

                if (chckActive.Checked == true)
                {
                    ds = GetData(0, "SELECT", Active);
                }
                else
                {
                    ds = GetData(0, "SELECT", Active);
                }
                DataTable dtNew = ds.Tables[0];
                DateTime date = Convert.ToDateTime(DtpMonth.Text);

                DateTime now = Convert.ToDateTime(DtpMonth.Text);
                var thisMonthRows = dtNew.AsEnumerable()
                    .Where(r => r.Field<DateTime>("DocDate").Year == now.Year && r.Field<DateTime>("DocDate").Month == now.Month)
                    .OrderByDescending(r => r.Field<DateTime>("DocDate"));

                DataTable dt = thisMonthRows.CopyToDataTable();
                bsFabIssue.DataSource = dt;
                DataGridFabIssue.DataSource = null;
                DataGridFabIssue.AutoGenerateColumns = false;
                DataGridFabIssue.ColumnCount = 9;
                DataGridFabIssue.Columns[0].Name = "DocNo";
                DataGridFabIssue.Columns[0].HeaderText = "DocNo";
                DataGridFabIssue.Columns[0].DataPropertyName = "DocNo";
                DataGridFabIssue.Columns[0].Width = 150;

                DataGridFabIssue.Columns[1].Name = "DocDate";
                DataGridFabIssue.Columns[1].HeaderText = "DocDate";
                DataGridFabIssue.Columns[1].DataPropertyName = "DocDate";
                DataGridFabIssue.Columns[1].Width = 120;

                DataGridFabIssue.Columns[2].Name = "Name";
                DataGridFabIssue.Columns[2].HeaderText = "Name";
                DataGridFabIssue.Columns[2].DataPropertyName = "Name";
                DataGridFabIssue.Columns[2].Width = 300;

                DataGridFabIssue.Columns[3].Name = "VehicleNo";
                DataGridFabIssue.Columns[3].HeaderText = "VehicleNo";
                DataGridFabIssue.Columns[3].DataPropertyName = "VehicleNo";
                DataGridFabIssue.Columns[3].Width = 200;

                DataGridFabIssue.Columns[4].Name = "DispatchSlipNo";
                DataGridFabIssue.Columns[4].HeaderText = "DispatchSlipNo";
                DataGridFabIssue.Columns[4].DataPropertyName = "DispatchNo";
                DataGridFabIssue.Columns[4].Width = 235;


                DataGridFabIssue.Columns[5].Name = "NetVale";
                DataGridFabIssue.Columns[5].HeaderText = "NetValue";
                DataGridFabIssue.Columns[5].DataPropertyName = "NetValue";
                DataGridFabIssue.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabIssue.Columns[6].Name = "PartyUid";
                DataGridFabIssue.Columns[6].HeaderText = "PartyUid";
                DataGridFabIssue.Columns[6].DataPropertyName = "PartyUid";
                DataGridFabIssue.Columns[6].Visible = false;

                DataGridFabIssue.Columns[7].Name = "ID";
                DataGridFabIssue.Columns[7].HeaderText = "ID";
                DataGridFabIssue.Columns[7].DataPropertyName = "ID";
                DataGridFabIssue.Columns[7].Visible = false;

                DataGridFabIssue.Columns[8].Name = "DispatchSlipUid";
                DataGridFabIssue.Columns[8].HeaderText = "DispatchSlipUid";
                DataGridFabIssue.Columns[8].DataPropertyName = "DispatchSlipUid";
                DataGridFabIssue.Columns[8].Visible = false;
                DataGridFabIssue.DataSource = bsFabIssue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridFabIssue.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridFabIssue.Rows[Index].Cells[7].Value.ToString());
                Genclass.ReportType = "Dispatch";
                Genclass.Dtype = 7;
                Genclass.RpeortId = Uid;
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.StartPosition = FormStartPosition.CenterScreen;
                rpt.MdiParent = this.MdiParent;
                rpt.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(1);
                LoadFrontGrid();
                btnUpdateRoll.Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DataGridFabricItem_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridFabricItem.SelectedCells[0].RowIndex;
                string SortNo = DataGridFabricItem.Rows[Index].Cells[0].Value.ToString();
                LoadSlipdet(Convert.ToInt32(txtVehicle.Tag), SortNo);
                grdetView.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            grdetView.Visible = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridFabIssue.SelectedCells[0].RowIndex;
                DialogResult result = MessageBox.Show("Do you want to delete this record", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    int Uid = Convert.ToInt32(DataGridFabIssue.Rows[Index].Cells[7].Value.ToString());
                    string Query = "Update DispatchM Set Active =0 Where ID=" + Uid + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, conn);
                    MessageBox.Show("Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadFrontGrid();
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int Uid;
                if (PrintId == 0)
                {
                    int Index = DataGridFabIssue.SelectedCells[0].RowIndex;
                    Uid = Convert.ToInt32(DataGridFabIssue.Rows[Index].Cells[8].Value.ToString());
                    Genclass.Print = 0;
                }
                else
                {
                    Uid = Convert.ToInt32(txtVehicle.Tag);
                    Genclass.Print = 1;
                }
                Genclass.ReportType = "PackingSlip";
                Genclass.Dtype = 8;
                Genclass.RpeortId = Uid;
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.StartPosition = FormStartPosition.CenterScreen;
                rpt.MdiParent = this.MdiParent;
                rpt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckActive.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                LoadFrontGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtDispatchTo_Click(object sender, EventArgs e)
        {
            try
            {
                LoadCommonGrid(3);
                Point loc = FindLocation(txtDispatchTo);
                grSearch.Location = new Point(loc.X - 140, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Dispatch Search";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtSupplerName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name Like '%{0}%'", txtSupplerName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtDispatchTo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name Like '%{0}%'", txtDispatchTo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtDispatchSlip_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsVechile.Filter = string.Format("DispatchNo LIKE '%{0}%' or VehicleNo LIKE '%{1}%' ", txtDispatchSlip.Text, txtDispatchSlip.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridFabIssue.SelectedRows.Count > 0)
                {
                    ClearControl();
                    int index = DataGridFabIssue.SelectedCells[0].RowIndex;
                    decimal Uid = Convert.ToDecimal(DataGridFabIssue.Rows[index].Cells[7].Value.ToString());
                    SqlParameter[] parameters = { new SqlParameter("@ID", Uid) };
                    DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_DispatchEdit", parameters, conn);
                    DataTable dtDispatch = ds.Tables[0];
                    DataTable dtDispatchD = ds.Tables[1];
                    txtDocNo.Text = dtDispatch.Rows[0]["DocNo"].ToString();
                    txtDocNo.Tag = dtDispatch.Rows[0]["ID"].ToString();
                    dtpGrnDate.Text = dtDispatch.Rows[0]["DocDate"].ToString();
                    txtVehicle.Text = dtDispatch.Rows[0]["VehicleNo"].ToString();
                    txtDispatchSlip.Text = dtDispatch.Rows[0]["DispatchNo"].ToString();
                    txtDispatchSlip.Tag = dtDispatch.Rows[0]["DispatchSlipUid"].ToString();
                    txtSupplerName.Text = dtDispatch.Rows[0]["Name"].ToString();
                    txtSupplerName.Tag = dtDispatch.Rows[0]["PartyUid"].ToString();
                    txtDispatchTo.Text = dtDispatch.Rows[0]["DispatchTO"].ToString();
                    txtRemarks.Text = dtDispatch.Rows[0]["Remarks"].ToString();
                    txtTranspoter.Text = dtDispatch.Rows[0]["Transpoter"].ToString();
                    txtEwayBill.Text = dtDispatch.Rows[0]["EWayBill"].ToString();
                    txtLRNo.Text = dtDispatch.Rows[0]["LRNO"].ToString();
                    cmbFor.Text = dtDispatch.Rows[0]["NOfProce"].ToString();
                    txtVechNo.Text = dtDispatch.Rows[0]["VchNo"].ToString();
                    txtFrom.SelectedValue = dtDispatch.Rows[0]["Frm"];
                    GetPackingSlipD(Convert.ToInt16(txtDispatchSlip.Tag));
                    btnsave.Text = "Update";
                    txtVehicle.Enabled = false;
                    txtDispatchSlip.Enabled = false;
                    DataGridFabricItem.Enabled = false;
                    if (txtFrom.SelectedIndex == -1)
                    {
                        txtFrom.SelectedIndex = 0;
                    }
                    LoadButton(2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DtpMonth_ValueChanged(object sender, EventArgs e)
        {
            LoadFrontGrid();
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsFabIssue.Filter = string.Format("DocNo Like '%{0}%'", txtSearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnUpdateRoll_Click(object sender, EventArgs e)
        {
            try
            {
                GrRollUpdate.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnRollNoSave_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@DispatchSlipUid",txtVehicle.Tag),
                        new SqlParameter("@SortNo",txtSortNo.Text),
                        new SqlParameter("@RollNo",txtRollNo.Text),
                        new SqlParameter("@Meter",txtMeter.Text),
                        new SqlParameter("@Weight",txtWeight.Text)
                    };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DispatchSlipD", sqlParameters, conn);
                MessageBox.Show("Roll Number Inserted successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtRollNo.Text = string.Empty;
                txtSortNo.Text = string.Empty;
                txtMeter.Text = string.Empty;
                txtWeight.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtRollNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtVehicle.Text != string.Empty && txtDispatchSlip.Text != string.Empty)
                    {
                        if (txtRollNo.Text != string.Empty)
                        {
                            SqlParameter[] sqlParameters = { new SqlParameter("@RollNo", txtRollNo.Text) };
                            DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetRollNoNew", sqlParameters, conn);
                            if (data.Rows.Count == 0)
                            {
                                MessageBox.Show("Invalid Roll Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtRollNo.Focus();
                                return;
                            }
                            else
                            {
                                txtSortNo.Text = data.Rows[0]["SortNo"].ToString();
                                txtMeter.Text = data.Rows[0]["Meter"].ToString();
                                txtWeight.Text = data.Rows[0]["Weight"].ToString();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Enter Roll Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtRollNo.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select Dispatch Slip Number and Vechile Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnRollNoClose_Click(object sender, EventArgs e)
        {
            GrRollUpdate.Visible = false;
        }

        protected void LoadProcessing()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeM_Uid", 30), new SqlParameter("@Active", 1) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                cmbFor.DataSource = null;
                cmbFor.DisplayMember = "GeneralName";
                cmbFor.ValueMember = "Uid";
                cmbFor.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
