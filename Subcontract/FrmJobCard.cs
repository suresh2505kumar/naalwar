﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmJobCard : Form
    {
        public FrmJobCard()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource bsIOItem = new BindingSource();
        int SelectId = 0;
        int Fillid = 0;
        int DocTypeMUid = 0;
        BindingSource bsJobCard = new BindingSource();
        private void FrmJobCard_Load(object sender, EventArgs e)
        {
            DocTypeMUid = 240;
            LoadDataGrid();
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            LoadButtons(0);
        }
        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    btnPrint.Visible = false;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    btnPrint.Visible = false;
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    btnPrint.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void txtItemName_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItemForJobCard", conn);
                bsIOItem.DataSource = dt;
                FillGrid(dt, 1);
                Point p = Genclass.FindLocation(txtItemName);
                panelSearch.Location = new Point(p.X, p.Y + 20);
                panelSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsIOItem.Filter = string.Format("SortNo LIKE '%{0}%' ", txtItemName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtQuantity.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtQuantity.Tag = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtQuantity.Focus();
                }
                panelSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtQuantity.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtQuantity.Tag = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtQuantity.Focus();
                }
                panelSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            panelSearch.Visible = false;
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 5;
                    DataGridCommon.Columns[0].Name = "Name";
                    DataGridCommon.Columns[0].HeaderText = "Name";
                    DataGridCommon.Columns[0].DataPropertyName = "SortNo";
                    DataGridCommon.Columns[0].Width = 210;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 80;
                    DataGridCommon.Columns[2].Name = "Qty";
                    DataGridCommon.Columns[2].HeaderText = "Qty";
                    DataGridCommon.Columns[2].DataPropertyName = "Qty";
                    DataGridCommon.Columns[2].Width = 80;
                    DataGridCommon.Columns[3].Name = "ItemUid";
                    DataGridCommon.Columns[3].HeaderText = "ItemUid";
                    DataGridCommon.Columns[3].DataPropertyName = "ItemUid";
                    DataGridCommon.Columns[3].Visible = false;
                    DataGridCommon.Columns[4].Name = "IOLUid";
                    DataGridCommon.Columns[4].HeaderText = "IOLUid";
                    DataGridCommon.Columns[4].DataPropertyName = "IOLUid";
                    DataGridCommon.Columns[4].Visible = false;
                    DataGridCommon.DataSource = bsIOItem;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButtons(1);
            Genclass.Module.ClearTextBox(this, grBack);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtItemName.Text == string.Empty || txtQuantity.Text == string.Empty)
                {
                    MessageBox.Show("Enter Item and Quantity", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if(btnSave.Text == "Save")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Tag","Insert"),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@IOLUid",txtQuantity.Tag),
                        new SqlParameter("@Remarks",txtRemarks.Text),
                        new SqlParameter("@ItemUid",txtItemName.Tag),
                        new SqlParameter("@Qty",Convert.ToDecimal(txtQuantity.Text)),
                        new SqlParameter("@TypeMUid",DocTypeMUid),
                        new SqlParameter("@ReqDate",Convert.ToDateTime(dtpreqDate.Text)),
                        new SqlParameter("@TotalEnds","0")
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JobCard", para, conn);
                }
                else
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Tag","Update"),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@IOLUid",txtQuantity.Tag),
                        new SqlParameter("@ItemUid",txtItemName.Tag),
                        new SqlParameter("@Remarks",txtRemarks.Text),
                        new SqlParameter("@Qty",Convert.ToDecimal(txtQuantity.Text)),
                        new SqlParameter("@TypeMUid",DocTypeMUid),
                        new SqlParameter("@JcUid",txtDocNo.Tag),
                        new SqlParameter("@ReqDate",Convert.ToDateTime(dtpreqDate.Text)),
                        new SqlParameter("@TotalEnds","0")
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JobCard", para, conn);
                }
                MessageBox.Show("Record Has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadDataGrid();
                Genclass.Module.ClearTextBox(this, grBack);
                LoadButtons(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            LoadButtons(0);
        }

        protected void LoadDataGrid()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetJobCard", conn);
                bsJobCard.DataSource = dt;
                DataGridJobCard.DataSource = null;
                DataGridJobCard.AutoGenerateColumns = false;
                DataGridJobCard.ColumnCount = 11;
                DataGridJobCard.Columns[0].Name = "DocNo";
                DataGridJobCard.Columns[0].HeaderText = "DocNo";
                DataGridJobCard.Columns[0].DataPropertyName = "DocNo";

                DataGridJobCard.Columns[1].Name = "DocDate";
                DataGridJobCard.Columns[1].HeaderText = "DocDate";
                DataGridJobCard.Columns[1].DataPropertyName = "DocDate";

                DataGridJobCard.Columns[2].Name = "ItemName";
                DataGridJobCard.Columns[2].HeaderText = "Sort No";
                DataGridJobCard.Columns[2].DataPropertyName = "ItemName";
                DataGridJobCard.Columns[2].Width = 150;
                DataGridJobCard.Columns[3].Name = "Qty";
                DataGridJobCard.Columns[3].HeaderText = "Qty";
                DataGridJobCard.Columns[3].DataPropertyName = "Qty";

                DataGridJobCard.Columns[4].Name = "ReqDate";
                DataGridJobCard.Columns[4].HeaderText = "ReqDate";
                DataGridJobCard.Columns[4].DataPropertyName = "ReqDate";

                DataGridJobCard.Columns[5].Name = "IODocNo";
                DataGridJobCard.Columns[5].HeaderText = "IODocNo";
                DataGridJobCard.Columns[5].DataPropertyName = "IODocNo";

                DataGridJobCard.Columns[6].Name = "ItemUid";
                DataGridJobCard.Columns[6].HeaderText = "ItemUid";
                DataGridJobCard.Columns[6].DataPropertyName = "ItemUid";
                DataGridJobCard.Columns[6].Visible = false;
                DataGridJobCard.Columns[7].Name = "IOLUid";
                DataGridJobCard.Columns[7].HeaderText = "IOLUid";
                DataGridJobCard.Columns[7].DataPropertyName = "IOLUid";
                DataGridJobCard.Columns[7].Visible = false;
                DataGridJobCard.Columns[8].Name = "JcUid";
                DataGridJobCard.Columns[8].HeaderText = "JcUid";
                DataGridJobCard.Columns[8].DataPropertyName = "JcUid";
                DataGridJobCard.Columns[8].Visible = false;
                DataGridJobCard.Columns[9].Name = "Remarks";
                DataGridJobCard.Columns[9].HeaderText = "Remarks";
                DataGridJobCard.Columns[9].DataPropertyName = "Remarks";
                DataGridJobCard.Columns[9].Visible = false;

                DataGridJobCard.Columns[10].Name = "TotalEnds";
                DataGridJobCard.Columns[10].HeaderText = "TotalEnds";
                DataGridJobCard.Columns[10].DataPropertyName = "TotalEnds";
                DataGridJobCard.Columns[10].Visible = false;

                DataGridJobCard.DataSource = bsJobCard;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridJobCard.SelectedCells[0].RowIndex;
                txtItemName.Text = DataGridJobCard.Rows[Index].Cells[2].Value.ToString();
                txtItemName.Tag =  DataGridJobCard.Rows[Index].Cells[6].Value.ToString();
                txtDocNo.Text = DataGridJobCard.Rows[Index].Cells[0].Value.ToString();
                txtDocNo.Tag = DataGridJobCard.Rows[Index].Cells[8].Value.ToString();
                txtQuantity.Text = DataGridJobCard.Rows[Index].Cells[3].Value.ToString();
                txtQuantity.Tag = DataGridJobCard.Rows[Index].Cells[7].Value.ToString();
                dtpDocDate.Text = DataGridJobCard.Rows[Index].Cells[1].Value.ToString();
                dtpreqDate.Text = DataGridJobCard.Rows[Index].Cells[4].Value.ToString();
                txtRemarks.Text = DataGridJobCard.Rows[Index].Cells[9].Value.ToString();
                txtTotalEnds.Text = DataGridJobCard.Rows[Index].Cells[10].Value.ToString();
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridJobCard.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridJobCard.Rows[Index].Cells[6].Value.ToString());
                Genclass.Dtype = 1000;
                Genclass.Prtid = Uid;
                Genclass.SortNo = DataGridJobCard.Rows[Index].Cells[2].Value.ToString();
                Genclass.Barcode = DataGridJobCard.Rows[Index].Cells[0].Value.ToString();
                Crviewer crv = new Crviewer();
                crv.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtjobcardSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsJobCard.Filter = string.Format("DocNo LIKE '%{0}%' or ItemName LIKE '%{1}%'", txtjobcardSearch.Text,txtjobcardSearch.Text);
                    
            }
            catch ( Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
