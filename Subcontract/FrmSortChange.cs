﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmSortChange : Form
    {
        public FrmSortChange()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        int uid = 0;
        BindingSource bsSortNo = new BindingSource();
        BindingSource bsJobCard = new BindingSource();
        int Fillid = 0;
        int SelectId = 0;
        int ItemType = 0;

        private void FrmSortChange_Load(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            Loom();
            Shift();
            DG2();
            LoadGrid();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            CmbLoomNo.SelectedIndex = -1;
            CmbShift.SelectedIndex = -1;
            //CmbJobcard.SelectedIndex = -1;
        }
        public void Loom()
        {
            try
            {
                conn.Open();
                string qur = "select * from generalm where typem_uid=18 ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                CmbLoomNo.DataSource = null;
                CmbLoomNo.DataSource = tab;
                CmbLoomNo.DisplayMember = "GeneralName";
                CmbLoomNo.ValueMember = "Uid";
                CmbLoomNo.SelectedIndex = -1;
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        public void Shift()
        {
            try
            {
                string qur = "select * from generalm where typem_uid=27 ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab3 = new DataTable();
                apt.Fill(tab3);
                CmbShift.DataSource = null;
                CmbShift.DataSource = tab3;
                CmbShift.DisplayMember = "GeneralName";
                CmbShift.ValueMember = "Uid";
                CmbShift.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        public void Jobcard()
        {
            try
            {
                string qur = "Select DocNo,JcUid from JobCard Where IsClosed =0 Order by DocNo desc";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab4 = new DataTable();
                apt.Fill(tab4);
                //CmbJobcard.DataSource = null;
                //CmbJobcard.DataSource = tab4;
                //CmbJobcard.DisplayMember = "DocNo";
                //CmbJobcard.ValueMember = "JcUid";
                //CmbJobcard.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@SDChange",DateTime.Now),
                new SqlParameter("@LoomId",CmbLoomNo.SelectedValue),
                new SqlParameter("@SortNo",TxtSortNo.Text),
                new SqlParameter("@ShiftId",CmbShift.SelectedValue),
                new SqlParameter("@JobCardID",txtJobCard.Tag)
            };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SortChange", parameters, conn);
            CmbLoomNo.SelectedIndex = -1;
            CmbShift.SelectedIndex = -1;
            TxtSortNo.Text = string.Empty;
            txtJobCard.Text = string.Empty;
            lblJobQty.Text = "0";
            lblPQty.Text = "0";
            LoadGrid();
        }
        private void DG2()
        {
            try
            {
                DataGrid2.AutoGenerateColumns = false;
                DataGrid2.DataSource = null;
                DataGrid2.ColumnCount = 9;

                DataGrid2.Columns[0].Name = "ShiftID";
                DataGrid2.Columns[0].HeaderText = "ShiftID";
                DataGrid2.Columns[0].Visible = false;

                DataGrid2.Columns[1].Name = "JobCardUid";
                DataGrid2.Columns[1].HeaderText = "JobCardUid";
                DataGrid2.Columns[1].Visible = false;

                DataGrid2.Columns[2].Name = "LoomUid";
                DataGrid2.Columns[2].HeaderText = "LoomUid";
                DataGrid2.Columns[2].Visible = false;

                DataGrid2.Columns[3].Name = "JobCardNo";
                DataGrid2.Columns[3].HeaderText = "JobCardNo";
                DataGrid2.Columns[3].Width = 155;

                DataGrid2.Columns[4].Name = "SortNo";
                DataGrid2.Columns[4].HeaderText = "SortNo";
                DataGrid2.Columns[4].Width = 200;

                DataGrid2.Columns[5].Name = "Date";
                DataGrid2.Columns[5].HeaderText = "Time";
                DataGrid2.Columns[5].DefaultCellStyle.Format = "HH:mm:ss";
                DataGrid2.Columns[5].Width = 150;

                DataGrid2.Columns[6].Name = "Shift";
                DataGrid2.Columns[6].HeaderText = "Shift";
                DataGrid2.Columns[6].Width = 150;

                DataGrid2.Columns[7].Name = "LoomNo";
                DataGrid2.Columns[7].HeaderText = "LoomNo";
                DataGrid2.Columns[7].Width = 150;

                DataGrid2.Columns[8].Name = "ItemType";
                DataGrid2.Columns[8].HeaderText = "ItemType";
                DataGrid2.Columns[8].Visible = false;
            
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void DataGrid2_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void DataGrid2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (uid == 0)
                {
                    for (int i = 0; i < DataGrid2.Rows.Count - 1; i++)
                    {
                        SqlParameter[] Para = {
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@date",Convert.ToDateTime(DataGrid2.Rows[i].Cells[1].Value.ToString())),
                        new SqlParameter("@loomid",DataGrid2.Rows[i].Cells[1].Value.ToString()),
                        new SqlParameter("@sortid",DataGrid2.Rows[i].Cells[1].Value.ToString()),
                        new SqlParameter("@shiftid",DataGrid2.Rows[i].Cells[1].Value.ToString()),

                    };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SortChange", Para, conn);
                    }

                }
                else
                {
                    for (int i = 0; i < DataGrid2.Rows.Count - 1; i++)
                    {
                        SqlParameter[] Para = {
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@date",Convert.ToDateTime(DataGrid2.Rows[i].Cells[1].Value.ToString())),
                        new SqlParameter("@loomid",DataGrid2.Rows[i].Cells[1].Value.ToString()),
                        new SqlParameter("@sortid",DataGrid2.Rows[i].Cells[1].Value.ToString()),
                        new SqlParameter("@shiftid",DataGrid2.Rows[i].Cells[1].Value.ToString()),

                    };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SortChange", Para, conn);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbJobcard_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void LoadData(string jobcardNo)
        {
            try
            {

                SqlParameter[] para = { new SqlParameter("@JobCardNo", jobcardNo) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSortChangeDetail", para, conn);
                if (dt.Rows.Count > 0)
                {
                    lblJobQty.Text = Convert.ToDecimal(dt.Rows[0]["Qty"].ToString()).ToString("0.000");
                    lblPQty.Text = Convert.ToDecimal(dt.Rows[0]["PQty"].ToString()).ToString("0.000");
                    decimal JobQty = Convert.ToDecimal(lblJobQty.Text);
                    decimal pQty = Convert.ToDecimal(lblPQty.Text);
                    if(JobQty < pQty)
                    {
                        DialogResult res = MessageBox.Show("Job Quantity exceeded Do you want to continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (res == DialogResult.Yes)
                        {
                            btnOK.Visible = true;
                        }
                        else
                        {
                            btnOK.Visible = false;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void CmbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (CmbShift.SelectedIndex != -1)
            //    {
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
        }

        private void BtnView_Click(object sender, EventArgs e)
        {
        }

        private void TxtSortNo_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = getItem();
                FillGrid(dt, 2);
                Point loc = Genclass.FindLocation(TxtSortNo);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "SortNo";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetSorNo", conn);
                bsSortNo.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "SortNo";
                    DataGridCommon.Columns[2].Name = "ItemType";
                    DataGridCommon.Columns[2].HeaderText = "ItemType";
                    DataGridCommon.Columns[2].DataPropertyName = "ItemType";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsSortNo;
                }
                if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "JcUid";
                    DataGridCommon.Columns[0].HeaderText = "JcUid";
                    DataGridCommon.Columns[0].DataPropertyName = "JcUid";
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "Jobcard No";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[2].Name = "DocDate";
                    DataGridCommon.Columns[2].HeaderText = "DocDate";
                    DataGridCommon.Columns[2].DataPropertyName = "DocDate";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsJobCard;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    TxtSortNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtSortNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    ItemType = Convert.ToInt32(DataGridCommon.Rows[Index].Cells[0].Value.ToString());
                }
                else
                {
                    txtJobCard.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtJobCard.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    LoadData(txtJobCard.Text);
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    TxtSortNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtSortNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtJobCard.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtJobCard.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    LoadData(txtJobCard.Text);
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetJobCard(string SortNo)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@SortNo", SortNo) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetJobCardSortChange", parameters, conn);
                bsJobCard.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtJobCard_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = GetJobCard(TxtSortNo.Text);
                FillGrid(dt, 3);
                Point loc = Genclass.FindLocation(txtJobCard);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Jobcard Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtSortNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsSortNo.Filter = string.Format("SortNo LIKE '%{0}%'", TxtSortNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtJobCard_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsJobCard.Filter = string.Format("DocNo LIKE '%{0}%'", txtJobCard.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadGrid()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Date", DateTimePick.Text) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSortChange", parameters, conn);
                DataGrid2.Rows.Clear();
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGrid2.Rows[0].Clone();
                        row.Cells[0].Value = dataTable.Rows[i]["SHID"].ToString(); 
                        row.Cells[1].Value = dataTable.Rows[i]["JobcardId"].ToString();
                        row.Cells[2].Value = dataTable.Rows[i]["LID"].ToString();
                        row.Cells[3].Value = dataTable.Rows[i]["JobCard"].ToString(); 
                        row.Cells[4].Value = dataTable.Rows[i]["SortNo"].ToString();
                        row.Cells[5].Value = dataTable.Rows[i]["SDChange"].ToString();
                        row.Cells[6].Value = dataTable.Rows[i]["Shiftname"].ToString();
                        row.Cells[7].Value = dataTable.Rows[i]["Loom"].ToString();
                        row.Cells[8].Value = dataTable.Rows[i]["ItemType"].ToString();
                        DataGrid2.Rows.Add(row);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGrid2.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGrid2.Rows[Index].Cells[1].Value.ToString());
                Genclass.parameter = DataGrid2.Rows[Index].Cells[7].Value.ToString();
                Genclass.SortNo = DataGrid2.Rows[Index].Cells[4].Value.ToString();
                Genclass.Barcode = DataGrid2.Rows[Index].Cells[3].Value.ToString();
                ItemType = Convert.ToInt32(DataGrid2.Rows[Index].Cells[8].Value.ToString());
                if (ItemType == 1071)
                {
                    Genclass.Dtype = 18;
                }
                else
                {
                    Genclass.Dtype = 15;
                }
                Genclass.Prtid = Uid;
                FrmReprtViwer crv = new FrmReprtViwer
                {
                    MdiParent = this.MdiParent,
                    StartPosition = FormStartPosition.CenterScreen
                };
                crv.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DateTimePick_ValueChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }
    }
}
