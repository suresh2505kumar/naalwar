﻿namespace Naalwar
{
    partial class Frmstkrpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmstkrpt));
            this.label1 = new System.Windows.Forms.Label();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.Txtitem = new System.Windows.Forms.TextBox();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.Fromdt = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.Todt = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.cmdprt = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkact1 = new System.Windows.Forms.CheckBox();
            this.cbogrp = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 20);
            this.label1.TabIndex = 161;
            this.label1.Text = "DC Without Process";
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.Color.White;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(16, 76);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(542, 442);
            this.HFGP.TabIndex = 162;
            this.HFGP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellClick);
            this.HFGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellContentClick);
            // 
            // Txtitem
            // 
            this.Txtitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtitem.Location = new System.Drawing.Point(16, 47);
            this.Txtitem.Margin = new System.Windows.Forms.Padding(4);
            this.Txtitem.Name = "Txtitem";
            this.Txtitem.Size = new System.Drawing.Size(542, 22);
            this.Txtitem.TabIndex = 163;
            this.Txtitem.TextChanged += new System.EventHandler(this.Txtitem_TextChanged);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(576, 203);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(93, 20);
            this.chkact.TabIndex = 164;
            this.chkact.Text = "Select All";
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            this.chkact.Click += new System.EventHandler(this.chkact_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(606, 284);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(39, 39);
            this.button2.TabIndex = 166;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.Color.White;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT.Location = new System.Drawing.Point(675, 76);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(344, 440);
            this.HFIT.TabIndex = 167;
            this.HFIT.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellClick);
            // 
            // Fromdt
            // 
            this.Fromdt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fromdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Fromdt.Location = new System.Drawing.Point(673, 48);
            this.Fromdt.Name = "Fromdt";
            this.Fromdt.Size = new System.Drawing.Size(96, 22);
            this.Fromdt.TabIndex = 223;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(670, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 18);
            this.label2.TabIndex = 222;
            this.label2.Text = "From";
            // 
            // Todt
            // 
            this.Todt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Todt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Todt.Location = new System.Drawing.Point(775, 47);
            this.Todt.Name = "Todt";
            this.Todt.Size = new System.Drawing.Size(96, 22);
            this.Todt.TabIndex = 225;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(772, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 18);
            this.label3.TabIndex = 224;
            this.label3.Text = "To";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button3.Location = new System.Drawing.Point(1053, 207);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 40);
            this.button3.TabIndex = 228;
            this.button3.Text = "Refresh";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // cmdprt
            // 
            this.cmdprt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdprt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt.Image = global::Naalwar.Properties.Resources.printer_35x35;
            this.cmdprt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdprt.Location = new System.Drawing.Point(1053, 262);
            this.cmdprt.Name = "cmdprt";
            this.cmdprt.Size = new System.Drawing.Size(87, 43);
            this.cmdprt.TabIndex = 227;
            this.cmdprt.Text = "Print";
            this.cmdprt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt.UseVisualStyleBackColor = false;
            this.cmdprt.Click += new System.EventHandler(this.cmdprt_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = global::Naalwar.Properties.Resources.eee;
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(1053, 322);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(89, 39);
            this.btnexit.TabIndex = 226;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Naalwar.Properties.Resources.arrow_35x35;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(606, 239);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 39);
            this.button1.TabIndex = 165;
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkact1
            // 
            this.chkact1.AutoSize = true;
            this.chkact1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact1.Location = new System.Drawing.Point(576, 339);
            this.chkact1.Name = "chkact1";
            this.chkact1.Size = new System.Drawing.Size(93, 20);
            this.chkact1.TabIndex = 229;
            this.chkact1.Text = "Select All";
            this.chkact1.Click += new System.EventHandler(this.chkact1_Click);
            // 
            // cbogrp
            // 
            this.cbogrp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbogrp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbogrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbogrp.FormattingEnabled = true;
            this.cbogrp.Items.AddRange(new object[] {
            "Stock Statement",
            "Stock Ledger"});
            this.cbogrp.Location = new System.Drawing.Point(877, 47);
            this.cbogrp.Name = "cbogrp";
            this.cbogrp.Size = new System.Drawing.Size(209, 24);
            this.cbogrp.TabIndex = 230;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(874, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 18);
            this.label4.TabIndex = 231;
            this.label4.Text = "Report Type";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(242, 262);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 22);
            this.textBox1.TabIndex = 232;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(260, 207);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 22);
            this.textBox2.TabIndex = 233;
            // 
            // Frmstkrpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1152, 530);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbogrp);
            this.Controls.Add(this.chkact1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.cmdprt);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.Todt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Fromdt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HFIT);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chkact);
            this.Controls.Add(this.Txtitem);
            this.Controls.Add(this.HFGP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Frmstkrpt";
            this.Text = "Frmstkrpt";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frmstkrpt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox Txtitem;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.DateTimePicker Fromdt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker Todt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cmdprt;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox chkact1;
        private System.Windows.Forms.ComboBox cbogrp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
    }
}