﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmYarnMaster : Form
    {
        public FrmYarnMaster()
        {
            InitializeComponent();
        }
        int SelectId = 0;
        bool EditMode = false;
        BindingSource bsYarn = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void FrmYarnMaster_Load(object sender, EventArgs e)
        {
            chckAc.Checked = true;
            GetData();
            cmbPly.SelectedIndex = -1;
            cmbCount.SelectedIndex = -1;
            cmbYarnType.SelectedIndex = -1;
            cmbCtnPc.SelectedIndex = -1;
            cmbBlended.SelectedIndex = -1;
            cmbItemType.SelectedIndex = -1;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            LoadButton(0);
            uom();
            LoadTax();
        }
        public void uom()
        {
            conn.Open();
            string qur = "select Generalname as Uom,uid from generalm where typem_uid =1";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cmbUOM.DataSource = null;
            cmbUOM.DisplayMember = "Uom";
            cmbUOM.ValueMember = "uid";
            cmbUOM.DataSource = tab;
            conn.Close();
        }


        protected void GetData()
        {
            try
            {
                SelectId = 1;
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GETYARNTYPEMASTER", conn);
                DataTable dtPLY = ds.Tables[0];
                DataTable dtCOUNT = ds.Tables[1];
                DataTable dtYARNTYPE = ds.Tables[2];
                DataTable dtCTNPC = ds.Tables[3];
                DataTable dtBLEND = ds.Tables[4];
                DataTable dtITEMTYPE = ds.Tables[5];
                cmbPly.DataSource = null;
                cmbPly.DisplayMember = "GENERALNAME";
                cmbPly.ValueMember = "UID";
                cmbPly.DataSource = dtPLY;

                cmbCount.DataSource = null;
                cmbCount.DisplayMember = "GENERALNAME";
                cmbCount.ValueMember = "UID";
                cmbCount.DataSource = dtCOUNT;

                cmbYarnType.DataSource = null;
                cmbYarnType.DisplayMember = "GENERALNAME";
                cmbYarnType.ValueMember = "UID";
                cmbYarnType.DataSource = dtYARNTYPE;

                cmbCtnPc.DataSource = null;
                cmbCtnPc.DisplayMember = "GENERALNAME";
                cmbCtnPc.ValueMember = "UID";
                cmbCtnPc.DataSource = dtCTNPC;

                cmbBlended.DataSource = null;
                cmbBlended.DisplayMember = "GENERALNAME";
                cmbBlended.ValueMember = "UID";
                cmbBlended.DataSource = dtBLEND;

                cmbItemType.DataSource = null;
                cmbItemType.DisplayMember = "GENERALNAME";
                cmbItemType.ValueMember = "UID";
                cmbItemType.DataSource = dtITEMTYPE;
                SelectId = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbPly_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (EditMode == false)
                {
                    txtItemSpec.Text = string.Empty;
                    txtItemSpec.Text = cmbPly.Text.Trim();
                }
                else
                {
                    if (SelectId == 0)
                    {
                        string[] txt = txtItemSpec.Text.ToString().Split(' ');
                        txt[0] = cmbPly.Text;
                        txtItemSpec.Text = txt[0] + " " + txt[1] + " " + txt[2] + " " + txt[3] + " " + txt[4] + " " + txt[5];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (EditMode == false)
                {
                    if (cmbPly.SelectedIndex != -1)
                    {
                        string ItemSpec = txtItemSpec.Text;
                        txtItemSpec.Text = ItemSpec + " " + cmbCount.Text.Trim();
                    }
                    else
                    {
                        cmbPly.Focus();
                        return;
                    }
                }
                else
                {
                    if (SelectId == 0)
                    {
                        string[] txt = txtItemSpec.Text.ToString().Split(' ');
                        txt[1] = cmbCount.Text;
                        txtItemSpec.Text = txt[0] + " " + txt[1] + " " + txt[2] + " " + txt[3] + " " + txt[4] + " " + txt[5];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbYarnType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (EditMode == false)
                {
                    if (cmbPly.SelectedIndex != -1 && cmbCount.SelectedIndex != -1)
                    {
                        string ItemSpec = txtItemSpec.Text;
                        txtItemSpec.Text = ItemSpec + " " + cmbYarnType.Text.Trim();
                    }
                    else
                    {
                        cmbCount.Focus();
                        return;
                    }
                }
                else
                {
                    if (SelectId == 0)
                    {
                        string[] txt = txtItemSpec.Text.ToString().Split(' ');
                        txt[2] = cmbYarnType.Text;
                        txtItemSpec.Text = txt[0] + " " + txt[1] + " " + txt[2] + " " + txt[3] + " " + txt[4] + " " + txt[5];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbCtnPc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (EditMode == false)
                {
                    if (cmbPly.SelectedIndex != -1 && cmbCount.SelectedIndex != -1 && cmbYarnType.SelectedIndex != -1)
                    {
                        string ItemSpec = txtItemSpec.Text;
                        txtItemSpec.Text = ItemSpec + " " + cmbCtnPc.Text.Trim();
                    }
                    else
                    {
                        cmbYarnType.Focus();
                        return;
                    }
                }
                else
                {
                    if (SelectId == 0)
                    {
                        string[] txt = txtItemSpec.Text.ToString().Split(' ');
                        txt[3] = cmbCtnPc.Text;
                        txtItemSpec.Text = txt[0] + " " + txt[1] + " " + txt[2] + " " + txt[3] + " " + txt[4] + " " + txt[5];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbBlended_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (EditMode == false)
                {
                    if (cmbPly.SelectedIndex != -1 && cmbCount.SelectedIndex != -1 && cmbYarnType.SelectedIndex != -1 && cmbCtnPc.SelectedIndex != -1)
                    {
                        string ItemSpec = txtItemSpec.Text;
                        txtItemSpec.Text = ItemSpec + " " + cmbBlended.Text.Trim();
                    }
                    else
                    {
                        cmbBlended.Focus();
                        return;
                    }
                }
                else
                {
                    if (SelectId == 0)
                    {
                        string[] txt = txtItemSpec.Text.ToString().Split(' ');
                        txt[4] = cmbBlended.Text;
                        txtItemSpec.Text = txt[0] + " " + txt[1] + " " + txt[2] + " " + txt[3] + " " + txt[4] + " " + txt[5];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (EditMode == false)
                {
                    if (cmbPly.SelectedIndex != -1 && cmbCount.SelectedIndex != -1 && cmbYarnType.SelectedIndex != -1 && cmbCtnPc.SelectedIndex != -1 && cmbBlended.SelectedIndex != -1)
                    {
                        string ItemSpec = txtItemSpec.Text;
                        txtItemSpec.Text = ItemSpec + " " + cmbItemType.Text.Trim();
                    }
                    else
                    {
                        cmbItemType.Focus();
                        return;
                    }
                }
                else
                {
                    if (SelectId == 0)
                    {
                        string[] txt = txtItemSpec.Text.ToString().Split(' ');
                        txt[5] = cmbItemType.Text;
                        txtItemSpec.Text = txt[0] + " " + txt[1] + " " + txt[2] + " " + txt[3] + " " + txt[4] + " " + txt[5];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {

        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckAc.Visible = true;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckAc.Visible = false;
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckAc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(1);
                chckActive.Checked = true;
                cmbPly.SelectedIndex = -1;
                cmbCount.SelectedIndex = -1;
                cmbYarnType.SelectedIndex = -1;
                cmbCtnPc.SelectedIndex = -1;
                cmbBlended.SelectedIndex = -1;
                cmbItemType.SelectedIndex = -1;
                txtDescription.Text = string.Empty;
                txtItemSpec.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDescription.Text != string.Empty)
                {
                    int Active;

                    if (chckActive.Checked == true)
                    {
                        Active = 1;
                    }
                    else
                    {
                        Active = 0;
                    }
                    SqlParameter[] paracheck = { new SqlParameter("@Value1", txtItemSpec.Text), new SqlParameter("@Value2", txtDescription.Text) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_CheckDuplicate", paracheck, conn);

                    if (btnSave.Text == "Save")
                    {
                        if (dt.Rows.Count != 0)
                        {
                            MessageBox.Show("Entry already found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else
                        {
                            SqlParameter[] para = {
                                new SqlParameter("@Tag","Insert"),
                                new SqlParameter("@YarnId","0"),
                                new SqlParameter("@YarnName",txtItemSpec.Text),
                                new SqlParameter("@YarnShortName",txtDescription.Text),
                                new SqlParameter("@Uom",cmbUOM.SelectedValue),
                                new SqlParameter("@HSNCode",TxtHsnCode.Text),
                                new SqlParameter("@Tax",CmbTax.SelectedValue),
                                new SqlParameter("@Active",Active),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_YarnMaster", para, conn);
                        }
                    }
                    else
                    {
                        SqlParameter[] para = {
                                new SqlParameter("@Tag","Update"),
                                new SqlParameter("@YarnId",txtDescription.Tag),
                                new SqlParameter("@YarnName",txtItemSpec.Text),
                                new SqlParameter("@YarnShortName",txtDescription.Text),
                                new SqlParameter("@Uom",cmbUOM.SelectedValue),
                                new SqlParameter("@HSNCode",TxtHsnCode.Text),
                                new SqlParameter("@Tax",CmbTax.SelectedValue),
                                new SqlParameter("@Active",Active),
                            };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_YarnMaster", para, conn);
                        EditMode = false;
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtItemSpec.Text = string.Empty;
                    txtDescription.Text = string.Empty;
                    LoadButton(0);
                    LoadDatatable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDatatable()
        {
            try
            {
                int Active;
                if (chckAc.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                SqlParameter[] para = { new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetYarnMaster", para, conn);
                bsYarn.DataSource = dt;
                DataGridYarn.DataSource = null;
                DataGridYarn.AutoGenerateColumns = false;
                DataGridYarn.ColumnCount = 7;
                DataGridYarn.Columns[0].Name = "Uid";
                DataGridYarn.Columns[0].HeaderText = "Uid";
                DataGridYarn.Columns[0].DataPropertyName = "Yarnid";
                DataGridYarn.Columns[0].Visible = false;
                DataGridYarn.Columns[1].Name = "Name";
                DataGridYarn.Columns[1].HeaderText = "Name";
                DataGridYarn.Columns[1].DataPropertyName = "YarnName";
                DataGridYarn.Columns[1].Width = 280;
                DataGridYarn.Columns[2].Name = "Active";
                DataGridYarn.Columns[2].HeaderText = "Active";
                DataGridYarn.Columns[2].DataPropertyName = "Active";
                DataGridYarn.Columns[2].Visible = false;
                DataGridYarn.Columns[3].Name = "Short Name";
                DataGridYarn.Columns[3].HeaderText = "Short Name";
                DataGridYarn.Columns[3].DataPropertyName = "YarnShortName";
                DataGridYarn.Columns[3].Width = 230;
                
                DataGridYarn.Columns[4].HeaderText = "Uom";
                DataGridYarn.Columns[4].DataPropertyName = "Uom";
                DataGridYarn.Columns[4].DataPropertyName = "Uom";
                DataGridYarn.Columns[4].Visible = false;

                DataGridYarn.Columns[5].HeaderText = "HSNCode";
                DataGridYarn.Columns[5].DataPropertyName = "HSNCode";
                DataGridYarn.Columns[5].DataPropertyName = "HSNCode";
                DataGridYarn.Columns[5].Visible = false;

                DataGridYarn.Columns[6].HeaderText = "Tax";
                DataGridYarn.Columns[6].DataPropertyName = "Tax";
                DataGridYarn.Columns[6].DataPropertyName = "Tax";
                DataGridYarn.Columns[6].Visible = false;
                DataGridYarn.DataSource = bsYarn;
                lblCount.Text = "Of " + dt.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            LoadButton(0);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                EditMode = true;
                SelectId = 1;
                int Index = DataGridYarn.SelectedCells[0].RowIndex;
                string[] text = (DataGridYarn.Rows[Index].Cells[1].Value.ToString()).Split(' ');
                cmbPly.Text = text[0].ToString();
                cmbCount.Text = text[1].ToString();
                cmbYarnType.Text = text[2].ToString();
                cmbCtnPc.Text = text[3].ToString();
                cmbBlended.Text = text[4].ToString();
                cmbItemType.Text = text[5].ToString();
                txtDescription.Text = DataGridYarn.Rows[Index].Cells[3].Value.ToString();
                txtDescription.Tag = DataGridYarn.Rows[Index].Cells[0].Value.ToString();
                cmbUOM.Text = DataGridYarn.Rows[Index].Cells[4].Value.ToString();
                int Active = Convert.ToInt32(DataGridYarn.Rows[Index].Cells[2].Value.ToString());
                txtItemSpec.Text = DataGridYarn.Rows[Index].Cells[1].Value.ToString();
                if (Active == 1)
                {
                    chckActive.Checked = true;
                }
                else
                {
                    chckActive.Checked = false;
                }
                TxtHsnCode.Text = DataGridYarn.Rows[Index].Cells[5].Value.ToString();
                if(DataGridYarn.Rows[Index].Cells[6].Value == null || DataGridYarn.Rows[Index].Cells[6].Value.ToString() == "")
                {

                }
                else
                {
                    CmbTax.SelectedValue = DataGridYarn.Rows[Index].Cells[6].Value.ToString();
                }
                SelectId = 0;
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckAc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckAc.Checked == true)
                {
                    LoadDatatable();
                }
                else
                {
                    LoadDatatable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsYarn.Filter = string.Format("YarnName LIKE '%{0}%' or YarnShortName LIKE '%{1}%'", txtSearch.Text, txtSearch.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridYarn.CurrentCell = DataGridYarn.Rows[0].Cells[1];
                DataGridYarn.Rows[0].Selected = true;
                int i = DataGridYarn.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridYarn_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int i = DataGridYarn.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridYarn.CurrentCell.RowIndex;
                if (Cnt > 0)
                {
                    int i = DataGridYarn.CurrentCell.RowIndex - 1;
                    DataGridYarn.CurrentCell = DataGridYarn.Rows[i].Cells[1];
                    DataGridYarn.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnNxt_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridYarn.CurrentCell.RowIndex + 1;
                if (Cnt >= 0 && Cnt != DataGridYarn.Rows.Count)
                {
                    int i = DataGridYarn.CurrentCell.RowIndex + 1;
                    DataGridYarn.CurrentCell = DataGridYarn.Rows[i].Cells[1];
                    DataGridYarn.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            try
            {
                int i = DataGridYarn.Rows.Count - 1;
                DataGridYarn.CurrentCell = DataGridYarn.Rows[i].Cells[1];
                DataGridYarn.Rows[i].Selected = true;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtItemSpec.Text = string.Empty;
            txtDescription.Text = string.Empty;
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = getTax();
                CmbTax.DataSource = null;
                CmbTax.DisplayMember = "GeneralName";
                CmbTax.ValueMember = "TaxPer";
                CmbTax.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
    }
}
