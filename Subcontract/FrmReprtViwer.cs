﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmReprtViwer : Form
    {
        public FrmReprtViwer()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void FrmReprtViwer_Load(object sender, EventArgs e)
        {
            if (Genclass.Dtype == 1)
            {
                ShifReport(Genclass.ReportType, Genclass.RpeortId, Genclass.ReortDate);
            }
            else if (Genclass.Dtype == 2)
            {
                QCReport();
            }
            else if (Genclass.Dtype == 3)
            {
                QCMendorReport();
            }
            else if (Genclass.Dtype == 4)
            {
                QCShiftReport();
            }
            else if (Genclass.Dtype == 5)
            {
                LoomWiseReport(Genclass.dtLoom);
            }
            else if (Genclass.Dtype == 6)
            {
                ShortShiftWisereport();
            }
            else if (Genclass.Dtype == 7)
            {
                Dispatch();
            }
            else if (Genclass.Dtype == 8)
            {
                PrintPackingSlip();
            }
            else if (Genclass.Dtype == 9)
            {
                RollReport();
            }
            else if (Genclass.Dtype == 10)
            {
                YarnIssue();
            }
            else if (Genclass.Dtype == 11)
            {
                PrintYarnPackingSlip();
            }
            else if (Genclass.Dtype == 12)
            {
                PrintJobCardReport();
            }
            else if (Genclass.Dtype == 13)
            {
                PrintFabricStockListReport();
            }
            else if (Genclass.Dtype == 14)
            {
                PrintFabricStockLedgr();
            }
            else if (Genclass.Dtype == 15)
            {
                PrintJobCardModifiedLoom();
            }
            else if (Genclass.Dtype == 16)
            {
                PrintCustomerDCLedger();
            }
            else if (Genclass.Dtype == 17)
            {
                PrintCustomerDCLedger();
            }
            else if (Genclass.Dtype == 18)
            {
                PrintTerryCard();
            }
        }

        private void PrintTerryCard()
        {
            try
            {
                ReportDocument docBar = new ReportDocument();
                string Barcode = Genclass.Barcode;
                int Uid = Genclass.Prtid;
                string Sort = Genclass.SortNo;
                string LoomNo = Genclass.parameter;
                KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
                br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                br.CodeText = Barcode;
                byte[] bt = br.drawBarcodeAsBytes();

                KeepDynamic.Barcode.CrystalReport.BarCode Loombr = new KeepDynamic.Barcode.CrystalReport.BarCode();
                Loombr.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                Loombr.CodeText = LoomNo;
                byte[] Loombt = Loombr.drawBarcodeAsBytes();

                SqlParameter[] para = { new SqlParameter("@SortNo", Sort) };
                DataTable dtS = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_TerryPrintLoomCard", para, conn);
                DataTable dt = new DataTable();
                dt.Columns.Add("SampleNo", typeof(string));
                dt.Columns.Add("Barcode", typeof(byte[]));
                dt.Columns.Add("SortName", typeof(string));
                dt.Columns.Add("Size", typeof(string));
                dt.Columns.Add("Wght", typeof(string));
                dt.Columns.Add("UOM", typeof(string));
                dt.Columns.Add("Reed", typeof(string));
                dt.Columns.Add("ReedSpace", typeof(string));
                dt.Columns.Add("PPI", typeof(string));
                dt.Columns.Add("GroundCount", typeof(string));
                dt.Columns.Add("GroundEnds", typeof(string));
                dt.Columns.Add("FileCount", typeof(string));
                dt.Columns.Add("FileEnds", typeof(string));
                dt.Columns.Add("WeptCount", typeof(string));
                dt.Columns.Add("GrayWght", typeof(string));
                dt.Columns.Add("GraySize", typeof(string));
                dt.Columns.Add("LSovage", typeof(string));
                dt.Columns.Add("RSolvage", typeof(string));
                dt.Columns.Add("Body", typeof(string));

                dt.Columns.Add("NoOfPanels", typeof(string));
                dt.Columns.Add("PileReadOpen", typeof(string));
                dt.Columns.Add("DrawingNo", typeof(string));
                dt.Columns.Add("TotalPicks", typeof(string));
                dt.Columns.Add("WeptCount1", typeof(string));
                dt.Columns.Add("LoomBarcode", typeof(byte[]));
                DataRow dr = dt.NewRow();
                if (dtS.Rows.Count > 0)
                {
                    string Query = @"select a.* from NaalwarSortList a inner join NaalwarSort b on a.SortUid = b.Uid
                                    Where b.SortName = '" + dtS.Rows[0]["SortName"].ToString() + "'";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);

                    dr["SampleNo"] = dtS.Rows[0]["SampleNo"].ToString();
                    dr["Barcode"] = bt;
                    dr["SortName"] = dtS.Rows[0]["SortName"].ToString();
                    dr["Size"] = dtS.Rows[0]["Size"].ToString();
                    dr["Wght"] = dtS.Rows[0]["Wght"].ToString();
                    dr["UOM"] = dtS.Rows[0]["UOM"].ToString();
                    dr["Reed"] = dtS.Rows[0]["Reed"].ToString();
                    dr["ReedSpace"] = dtS.Rows[0]["ReedSpace"].ToString();
                    dr["PPI"] = dtS.Rows[0]["PPI"].ToString();
                    dr["GroundCount"] = dtS.Rows[0]["GroundCount"].ToString();
                    dr["GroundEnds"] = dtS.Rows[0]["GroundEnds"];
                    dr["FileCount"] = dtS.Rows[0]["FileCount"].ToString();
                    dr["FileEnds"] = dtS.Rows[0]["FileEnds"].ToString();
                    dr["WeptCount"] = dtS.Rows[0]["WeptCount"].ToString();
                    dr["GrayWght"] = dtS.Rows[0]["GrayWght"];
                    dr["GraySize"] = dtS.Rows[0]["GraySize"];
                    dr["LSovage"] = dtS.Rows[0]["LSovage"].ToString();
                    dr["RSolvage"] = dtS.Rows[0]["RSolvage"].ToString();
                    dr["TotalPicks"] = dtS.Rows[0]["TotalPicks"].ToString();
                    dr["Body"] = dtS.Rows[0]["Body"].ToString();
                    dr["NoOfPanels"] = dtS.Rows[0]["NoOfPanels"].ToString();
                    dr["PileReadOpen"] = dtS.Rows[0]["PileReadOpen"].ToString();
                    dr["DrawingNo"] = dtS.Rows[0]["DrawingNo"].ToString();
                    dr["WeptCount1"] = dtS.Rows[0]["WeptCount1"].ToString();

                    dr["LoomBarcode"] = Loombt;
                    dt.Rows.Add(dr);
                    docBar.Load(Application.StartupPath + "\\Reports\\CrySortIdentificationSlip.rpt");
                    decimal j = 10;
                    decimal k = 30;
                    DataRow[] dataRows = dataTable.Select("WpType =1", null);
                    if (dataRows.Length > 0)
                    {
                        DataTable dtWp = dataTable.Select("WpType =1", null).CopyToDataTable();
                        for (int i = 0; i < dtWp.Rows.Count; i++)
                        {
                            if(i <= 10)
                            {
                                string txtName = "txtWP" + j;
                                TextObject txtNoOfBales = (TextObject)docBar.ReportDefinition.ReportObjects[txtName];
                                txtNoOfBales.Text = dtWp.Rows[i]["WP1"].ToString();
                                j++;

                                string txtName1 = "txtWP" + k;
                                TextObject txtNoOfBales1 = (TextObject)docBar.ReportDefinition.ReportObjects[txtName1];
                                txtNoOfBales1.Text = dtWp.Rows[i]["WP2"].ToString();
                                k++;
                            }
                           
                        }
                    }
                    decimal l = 10;
                    decimal m = 20;
                    DataRow[] dataRows1 = dataTable.Select("WpType =2", null);
                    if (dataRows1.Length > 0)
                    {
                        DataTable dtWp = dataTable.Select("WpType =2", null).CopyToDataTable();
                        for (int i = 0; i < dtWp.Rows.Count; i++)
                        {
                            if(i < 9)
                            {
                                string txtName = "txtD" + l;
                                TextObject txtNoOfBales = (TextObject)docBar.ReportDefinition.ReportObjects[txtName];
                                txtNoOfBales.Text = dtWp.Rows[i]["WP1"].ToString();
                                l++;

                                string txtName1 = "txtD" + m;
                                TextObject txtNoOfBales1 = (TextObject)docBar.ReportDefinition.ReportObjects[txtName1];
                                txtNoOfBales1.Text = dtWp.Rows[i]["WP2"].ToString();
                                m++;
                            }
                        }
                    }
                    docBar.SetDataSource(dt);
                    crystalReportViewer.ReportSource = docBar;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void PrintCustomerDCLedger()
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@FromDate",Genclass.FromDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("@TODate",Genclass.ToDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("@PartyUid",Genclass.RpeortId),
                };
                DataTable dt = new DataTable();
                if (Genclass.parameter == "S")
                {
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_DCLedgerSummary", parameters, conn);
                }
                else
                {
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetPartyDCLedger", parameters, conn);
                }
                ReportDocument docBar = new ReportDocument();
                if (Genclass.parameter == "S")
                {
                    docBar.Load(Application.StartupPath + "\\Reports\\CryDCSummary.rpt");
                }
                else
                {
                    docBar.Load(Application.StartupPath + "\\Reports\\CryDCLedger.rpt");
                }
                TextObject txtFromDate = (TextObject)docBar.ReportDefinition.ReportObjects["txtFromDate"];
                txtFromDate.Text = Genclass.FromDate.ToString("dd-MMM-yyyy");
                TextObject txtToDate = (TextObject)docBar.ReportDefinition.ReportObjects["txtToDate"];
                txtToDate.Text = Genclass.ToDate.ToString("dd-MMM-yyyy");
                docBar.SetDataSource(dt);
                crystalReportViewer.ReportSource = docBar;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw;
            }
        }

        private void PrintJobCardModifiedLoom()
        {
            try
            {
                ReportDocument docBar = new ReportDocument();
                string Barcode = Genclass.Barcode;
                int Uid = Genclass.Prtid;
                string Sort = Genclass.SortNo;
                string LoomNo = Genclass.parameter;
                KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
                br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                br.CodeText = Barcode;
                byte[] bt = br.drawBarcodeAsBytes();

                KeepDynamic.Barcode.CrystalReport.BarCode Loombr = new KeepDynamic.Barcode.CrystalReport.BarCode();
                Loombr.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                Loombr.CodeText = LoomNo;
                byte[] Loombt = Loombr.drawBarcodeAsBytes();

                SqlParameter[] para = { new SqlParameter("@uid", Sort) };
                DataTable dtS = db.GetDataWithParam(CommandType.StoredProcedure, "SP_LoomCardPrintSortChange", para, conn);
                DataTable dt = new DataTable();
                dt.Columns.Add("DocNo", typeof(string));
                dt.Columns.Add("Barcode", typeof(byte[]));
                dt.Columns.Add("sortNo", typeof(string));
                dt.Columns.Add("design", typeof(string));
                dt.Columns.Add("rollno", typeof(string));
                dt.Columns.Add("warpcount", typeof(string));
                dt.Columns.Add("WeptCount", typeof(string));
                dt.Columns.Add("epi", typeof(string));
                dt.Columns.Add("ppi", typeof(string));
                dt.Columns.Add("GreyWidth", typeof(string));
                dt.Columns.Add("TotalEnds", typeof(decimal));
                dt.Columns.Add("Reed", typeof(string));
                dt.Columns.Add("ReedSpace", typeof(string));
                dt.Columns.Add("PickWheel", typeof(string));
                dt.Columns.Add("wtmtr", typeof(string));
                dt.Columns.Add("gtlen", typeof(decimal));
                dt.Columns.Add("gtwei", typeof(decimal));
                dt.Columns.Add("LoomBarcode", typeof(byte[]));
                DataRow dr = dt.NewRow();
                dr["DocNo"] = Sort;
                dr["Barcode"] = bt;
                dr["sortNo"] = Sort;
                dr["design"] = dtS.Rows[0]["design"].ToString();
                dr["rollno"] = dtS.Rows[0]["rollno"].ToString();
                dr["warpcount"] = dtS.Rows[0]["warpcount"].ToString();
                dr["WeptCount"] = dtS.Rows[0]["WeptCount"].ToString();
                dr["epi"] = dtS.Rows[0]["epi"].ToString();
                dr["ppi"] = dtS.Rows[0]["ppi"].ToString();
                dr["GreyWidth"] = dtS.Rows[0]["GreyWidth"].ToString();
                dr["TotalEnds"] = dtS.Rows[0]["TotalEnds"];
                dr["Reed"] = dtS.Rows[0]["Reed"].ToString();
                dr["ReedSpace"] = dtS.Rows[0]["ReedSpace"].ToString();
                dr["PickWheel"] = dtS.Rows[0]["PickWheel"].ToString();
                dr["wtmtr"] = dtS.Rows[0]["wtmtr"];
                string str = dtS.Rows[0]["gtlen"].ToString();
                decimal gtLen;
                decimal gtwei;
                if (str == "")
                {
                    gtLen = 0;
                    gtwei = 0;
                }
                else
                {
                    gtLen = (decimal)dtS.Rows[0]["gtlen"];
                    gtwei = (decimal)dtS.Rows[0]["gtwei"];
                }
                if (gtLen == 0)
                {
                    dr["gtlen"] = DBNull.Value;
                }
                else
                {
                    dr["gtlen"] = dtS.Rows[0]["gtlen"];
                }
                if (gtwei == 0)
                {
                    dr["gtwei"] = DBNull.Value;
                }
                else
                {
                    dr["gtwei"] = dtS.Rows[0]["gtwei"];
                }
                dr["LoomBarcode"] = Loombt;
                dt.Rows.Add(dr);
                docBar.Load(Application.StartupPath + "\\Reports\\CryLoomMCard.rpt");

                docBar.SetDataSource(dt);
                crystalReportViewer.ReportSource = docBar;

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void PrintFabricStockLedgr()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@FDT", Genclass.FromDate.ToString("yyyyMMdd")), new SqlParameter("@TDT", Genclass.ToDate.ToString("yyyyMMdd")) };
                DataTable ds = db.GetDataWithParam(CommandType.StoredProcedure, "RPT_FABRICSTKLEDGER", parameters, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryStockLedger.rpt");
                TextObject txtFromDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtFromDate"];
                txtFromDate.Text = Genclass.FromDate.ToString("dd-MMM-yyyy");
                TextObject txtToDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtToDate"];
                txtToDate.Text = Genclass.ToDate.ToString("dd-MMM-yyyy");
                rpt.SetDataSource(ds);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintFabricStockListReport()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@T", Convert.ToInt32(Genclass.parameter)) };
                DataTable ds = db.GetDataWithParam(CommandType.StoredProcedure, "RPT_FABRICSTOCKLIST", parameters, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryStockListSummary.rpt");
                TextObject txtRptType = (TextObject)rpt.ReportDefinition.ReportObjects["txtReportType"];
                txtRptType.Text = Genclass.ReportType;
                TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDate"];
                txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                rpt.SetDataSource(ds);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintJobCardReport()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@IsClosed", Genclass.RpeortId) };
                DataTable ds = db.GetDataWithParam(CommandType.StoredProcedure, "RPT_JOBCARD", parameters, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryJobCardReport.rpt");
                TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtReportType"];
                txtDate.Text = Genclass.ReportType;
                rpt.SetDataSource(ds);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RollReport()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@ShiftID", Genclass.parameter), new SqlParameter("@RollDate", Genclass.ReortDate) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_RollReport", para, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\CryQCReport.rpt");
                TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDate"];
                txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                TextObject txtShift = (TextObject)rpt.ReportDefinition.ReportObjects["txtShift"];
                if (Genclass.parameter == "984")
                {
                    txtShift.Text = "I Shift";
                }
                else if (Genclass.parameter == "985")
                {
                    txtShift.Text = "II Shift";
                }
                else if (Genclass.parameter == "986")
                {
                    txtShift.Text = "III Shift";
                }
                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintYarnPackingSlip()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@PkSid", Genclass.RpeortId) };
                SqlParameter[] para1 = { new SqlParameter("@PkSid", Genclass.RpeortId) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_YarnPackingSlipPrint", para, conn);
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_YarnPackingSlipPrintSummary", para1, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryYarnPKSlip.rpt");
                rpt.Subreports["CryPackingSlipSummary.rpt"].SetDataSource(dataTable);
                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintPackingSlip()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@DispatchSlipMUid", Genclass.RpeortId) };
                SqlParameter[] para1 = { new SqlParameter("@DispatchSlipMUid", Genclass.RpeortId) };
                DataSet ds = new DataSet();
                if (Genclass.Print == 1)
                {
                    ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_PrintOPackingSlipNew", para, conn);
                }
                else
                {
                    ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_PackingSlipPrint", para, conn);
                }
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_PackingSlipPrintSummary", para1, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryPKSlip.rpt");
                rpt.Subreports["CryPackingSlipSummary.rpt"].SetDataSource(dataTable);
                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Dispatch()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@ID", Genclass.RpeortId) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_DispatchPrint", para, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryDispatchDC.rpt");
                SqlParameter[] paratax = { new SqlParameter("@DispatchUid", Genclass.RpeortId) };
                DataSet dt = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GettaxDet", paratax, conn);
                rpt.Subreports["CryDcSubReport.rpt"].SetDataSource(dt.Tables[0]);

                if (dt.Tables[1].Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Tables[1].Rows.Count; i++)
                    {
                        string Roll = dt.Tables[1].Rows[i]["RollType"].ToString();
                        if (Roll == "Rolls")
                        {
                            TextObject txtNoOfBales = (TextObject)rpt.ReportDefinition.ReportObjects["txtNofRolls"];
                            txtNoOfBales.Text = dt.Tables[1].Rows[i]["Cnt"].ToString();
                        }
                        else
                        {
                            TextObject textObject = (TextObject)rpt.ReportDefinition.ReportObjects["txtNoOfBales"];
                            textObject.Text = dt.Tables[1].Rows[i]["Cnt"].ToString();
                        }
                    }
                }
                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void YarnIssue()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@ID", Genclass.RpeortId) };
                SqlParameter[] parameters = { new SqlParameter("@JoIUid", Genclass.RpeortId) };
                DataTable dtBeam = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetYarnDcBeam", parameters, conn);
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_YarnIssuePrint", para, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryYarnDC.rpt");
                SqlParameter[] paratax = { new SqlParameter("@JoIUid", Genclass.RpeortId) };
                DataSet dt = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetYarntaxDet", paratax, conn);
                rpt.Subreports["CryDcSubReport.rpt"].SetDataSource(dt.Tables[0]);
                if (dt.Tables[1].Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Tables[1].Rows.Count; i++)
                    {
                        string s = dt.Tables[1].Rows[i]["remarks"].ToString();
                        if (s == "Bag")
                        {
                            TextObject txtNoOfBales = (TextObject)rpt.ReportDefinition.ReportObjects["txtNofRolls"];
                            txtNoOfBales.Text = dt.Tables[1].Rows[i]["Cnt"].ToString();
                        }
                        else
                        {
                            TextObject textObject = (TextObject)rpt.ReportDefinition.ReportObjects["txtCBoxs"];
                            textObject.Text = dt.Tables[1].Rows[i]["Cnt"].ToString();
                        }
                    }
                }
                TextObject txtBeamNo = (TextObject)rpt.ReportDefinition.ReportObjects["txtBeam"];
                txtBeamNo.Text = dtBeam.Rows[0]["BeamNo"].ToString();

                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShortShiftWisereport()
        {
            try
            {
                //ReportDocument doc = new ReportDocument();
                //SqlDataAdapter da = new SqlDataAdapter("RPT_SORTWISEPrint", conn);
                //da.SelectCommand.CommandType = CommandType.StoredProcedure;
                //da.SelectCommand.Parameters.Add("@DT", SqlDbType.Date).Value = Genclass.ReortDate.ToString("yyyy-MM-dd");
                //da.SelectCommand.Parameters.Add("@SHIFTID", SqlDbType.Int).Value = Genclass.RpeortId;
                //DataSet ds = new DataSet();
                //da.Fill(ds, "SortWiseReport");
                //doc.Load(Application.StartupPath + "\\Reports\\CrySortWise.rpt");
                //TextObject txtDate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                //txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                //TextObject txtShift = (TextObject)doc.ReportDefinition.ReportObjects["txtShift"];
                //if (Genclass.RpeortId == 984)
                //{
                //    txtShift.Text = "I Shift";
                //}
                //else if (Genclass.RpeortId == 985)
                //{
                //    txtShift.Text = "II Shift";
                //}
                //else if (Genclass.RpeortId == 986)
                //{
                //    txtShift.Text = "III Shift";
                //}
                //doc.SetDataSource(ds);
                //crystalReportViewer.ReportSource = doc;
                string PrcedureName = string.Empty;
                PrcedureName = "RPT_SHIFTREPORTSORTWISE";
                ReportDocument doc = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter(PrcedureName, conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@DT", SqlDbType.Date).Value = Genclass.ReortDate.ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@SHIFTID", SqlDbType.Int).Value = Genclass.RpeortId;
                DataSet ds = new DataSet();
                da.Fill(ds, "RollWiseReport");
                doc.Load(Application.StartupPath + "\\Reports\\CrySortwiseGroupNew.rpt");
                TextObject txtDate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                TextObject txtShift = (TextObject)doc.ReportDefinition.ReportObjects["txtShift"];
                if (Genclass.RpeortId == 984)
                {
                    txtShift.Text = "I Shift";
                }
                else if (Genclass.RpeortId == 985)
                {
                    txtShift.Text = "II Shift";
                }
                else if (Genclass.RpeortId == 986)
                {
                    txtShift.Text = "III Shift";
                }
                doc.SetDataSource(ds);
                crystalReportViewer.ReportSource = doc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoomWiseReport(DataTable dt)
        {
            //ReportDocument rpt = new ReportDocument();
            //rpt.Load(Application.StartupPath + "\\CryLoomWise.rpt");
            //rpt.SetDataSource(dt);
            //crystalReportViewer.ReportSource = rpt;
            string PrcedureName = string.Empty;
            PrcedureName = "RPT_SHIFTREPORTLOOMWISE";
            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter(PrcedureName, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@DT", SqlDbType.Date).Value = Genclass.ReortDate.ToString("yyyy-MM-dd");
            da.SelectCommand.Parameters.Add("@SHIFTID", SqlDbType.Int).Value = Genclass.RpeortId;
            DataSet ds = new DataSet();
            da.Fill(ds, "RollWiseReport");
            doc.Load(Application.StartupPath + "\\Reports\\CryLoomwiseNew.rpt");
            TextObject txtDate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
            txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
            TextObject txtShift = (TextObject)doc.ReportDefinition.ReportObjects["txtShift"];
            if (Genclass.RpeortId == 984)
            {
                txtShift.Text = "I Shift";
            }
            else if (Genclass.RpeortId == 985)
            {
                txtShift.Text = "II Shift";
            }
            else if (Genclass.RpeortId == 986)
            {
                txtShift.Text = "III Shift";
            }
            doc.SetDataSource(ds);
            crystalReportViewer.ReportSource = doc;
        }

        private void QCShiftReport()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@shiftId", Genclass.parameter), new SqlParameter("@Date", Genclass.ReortDate) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetQCShiftWise", para, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\CryQCReport.rpt");
                TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDate"];
                txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                TextObject txtShift = (TextObject)rpt.ReportDefinition.ReportObjects["txtShift"];
                if (Genclass.parameter == "984")
                {
                    txtShift.Text = "I Shift";
                }
                else if (Genclass.parameter == "985")
                {
                    txtShift.Text = "II Shift";
                }
                else if (Genclass.parameter == "986")
                {
                    txtShift.Text = "III Shift";
                }
                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void QCMendorReport()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@MendorName", Genclass.parameter), new SqlParameter("@shiftId", Genclass.parameter1), new SqlParameter("@Date", Genclass.ReortDate) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_QCReportMendor", para, conn);
                ReportDocument rpt = new ReportDocument();
                DataTable dt = new DataTable();
                if (Genclass.ReportType == "Sort Wise")
                {
                    dt = ds.Tables[0];
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        rpt.Load(Application.StartupPath + "\\CryQCReportSort.rpt");
                        TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDate"];
                        txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                        rpt.SetDataSource(dt);
                    }
                    else
                    {
                        MessageBox.Show("No data found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
                else if (Genclass.ReportType == "Mendor Wise")
                {
                    if (ds.Tables[2].Rows.Count != 0)
                    {
                        dt = ds.Tables[2];
                        rpt.Load(Application.StartupPath + "\\CryQCReport.rpt");
                        TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDate"];
                        txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                        rpt.SetDataSource(dt);
                    }
                    else
                    {
                        MessageBox.Show("No data found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else //Date Shift and Mendor
                {
                    if (ds.Tables[1].Rows.Count != 0)
                    {
                        dt = ds.Tables[1];
                        rpt.Load(Application.StartupPath + "\\CryQCMendor.rpt");
                        TextObject txtDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDate"];
                        txtDate.Text = Genclass.ReortDate.ToString("dd-MMM-yyyy");
                        rpt.SetDataSource(dt);
                    }
                    else
                    {
                        MessageBox.Show("No data found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void QCReport()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@QCType", Genclass.parameter), new SqlParameter("@Date", Genclass.ReortDate) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetQC_Version1", para, conn);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\CryQCReport.rpt");
                rpt.SetDataSource(ds.Tables[0]);
                crystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ShifReport(string reportType, int ShiftId, DateTime dte)
        {
            try
            {
                string PrcedureName = string.Empty;
                PrcedureName = "RPT_SHIFTREPORTROLLWISE";
                ReportDocument doc = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter(PrcedureName, conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@DT", SqlDbType.Date).Value = dte.ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@SHIFTID", SqlDbType.Int).Value = ShiftId;
                DataSet ds = new DataSet();
                da.Fill(ds, "RollWiseReport");
                doc.Load(Application.StartupPath + "\\Reports\\CryRollwise.rpt");
                TextObject txtDate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                txtDate.Text = dte.ToString("dd-MMM-yyyy");
                TextObject txtShift = (TextObject)doc.ReportDefinition.ReportObjects["txtShift"];
                if (ShiftId == 984)
                {
                    txtShift.Text = "I Shift";
                }
                else if (ShiftId == 985)
                {
                    txtShift.Text = "II Shift";
                }
                else if (ShiftId == 986)
                {
                    txtShift.Text = "III Shift";
                }
                doc.SetDataSource(ds);
                crystalReportViewer.ReportSource = doc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
