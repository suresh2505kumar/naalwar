﻿namespace Naalwar
{
    partial class Frmopstock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmopstock));
            this.Genpan = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttrqok = new System.Windows.Forms.Button();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtitid = new System.Windows.Forms.TextBox();
            this.txtitcode = new System.Windows.Forms.TextBox();
            this.txtscr3 = new System.Windows.Forms.TextBox();
            this.txtop = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label5);
            this.Genpan.Controls.Add(this.label4);
            this.Genpan.Controls.Add(this.label3);
            this.Genpan.Controls.Add(this.label2);
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.buttrqok);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtname);
            this.Genpan.Controls.Add(this.txtitem);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.txtscr2);
            this.Genpan.Controls.Add(this.txtpuid);
            this.Genpan.Controls.Add(this.txtitid);
            this.Genpan.Controls.Add(this.txtitcode);
            this.Genpan.Controls.Add(this.txtscr3);
            this.Genpan.Controls.Add(this.txtop);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Location = new System.Drawing.Point(-1, -1);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1092, 481);
            this.Genpan.TabIndex = 157;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 19);
            this.label5.TabIndex = 136;
            this.label5.Text = "Opening Stock";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(865, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 16);
            this.label4.TabIndex = 135;
            this.label4.Text = "Op.Stock";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(633, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 134;
            this.label3.Text = "Itemcode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(313, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 16);
            this.label2.TabIndex = 133;
            this.label2.Text = "Item";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 132;
            this.label1.Text = "Party";
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttrqok.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.Image = global::Naalwar.Properties.Resources.ok1;
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(950, 62);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(39, 37);
            this.buttrqok.TabIndex = 131;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click);
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(3, 101);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(1087, 380);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(4, 70);
            this.txtname.Margin = new System.Windows.Forms.Padding(4);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(310, 22);
            this.txtname.TabIndex = 1;
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(316, 70);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(318, 22);
            this.txtitem.TabIndex = 88;
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged);
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(7, 70);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(306, 22);
            this.txtscr1.TabIndex = 137;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // txtscr2
            // 
            this.txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(316, 70);
            this.txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(318, 22);
            this.txtscr2.TabIndex = 138;
            this.txtscr2.TextChanged += new System.EventHandler(this.txtscr2_TextChanged);
            // 
            // txtpuid
            // 
            this.txtpuid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpuid.Location = new System.Drawing.Point(274, 70);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(4);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(25, 22);
            this.txtpuid.TabIndex = 87;
            // 
            // txtitid
            // 
            this.txtitid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitid.Location = new System.Drawing.Point(334, 70);
            this.txtitid.Margin = new System.Windows.Forms.Padding(4);
            this.txtitid.Name = "txtitid";
            this.txtitid.Size = new System.Drawing.Size(22, 22);
            this.txtitid.TabIndex = 89;
            // 
            // txtitcode
            // 
            this.txtitcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitcode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitcode.Location = new System.Drawing.Point(636, 70);
            this.txtitcode.Margin = new System.Windows.Forms.Padding(4);
            this.txtitcode.Name = "txtitcode";
            this.txtitcode.Size = new System.Drawing.Size(231, 22);
            this.txtitcode.TabIndex = 90;
            // 
            // txtscr3
            // 
            this.txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr3.Location = new System.Drawing.Point(636, 70);
            this.txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr3.Name = "txtscr3";
            this.txtscr3.Size = new System.Drawing.Size(231, 22);
            this.txtscr3.TabIndex = 139;
            this.txtscr3.TextChanged += new System.EventHandler(this.txtscr3_TextChanged);
            // 
            // txtop
            // 
            this.txtop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtop.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtop.Location = new System.Drawing.Point(868, 70);
            this.txtop.Margin = new System.Windows.Forms.Padding(4);
            this.txtop.Name = "txtop";
            this.txtop.Size = new System.Drawing.Size(76, 22);
            this.txtop.TabIndex = 91;
            this.txtop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtop_KeyDown);
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(868, 70);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(76, 22);
            this.txtscr4.TabIndex = 140;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Location = new System.Drawing.Point(5, 482);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(646, 40);
            this.panadd.TabIndex = 237;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 16);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(29, 16);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(207, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 30);
            this.button1.TabIndex = 141;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 5);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 5);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 5);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 5);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(273, 4);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(54, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // Frmopstock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 522);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.Genpan);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Frmopstock";
            this.Text = "Opening Stock";
            this.Load += new System.EventHandler(this.Frmopstock_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox txtop;
        private System.Windows.Forms.TextBox txtitcode;
        private System.Windows.Forms.TextBox txtitid;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox txtscr3;
        private System.Windows.Forms.TextBox txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
    }
}