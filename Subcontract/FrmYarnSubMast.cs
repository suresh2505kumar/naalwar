﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmYarnSubMast : Form
    {
        public FrmYarnSubMast()
        {
            InitializeComponent();
        }
        int TypeMUid;
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsPLy = new BindingSource();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void FrmYarnSubMast_Load(object sender, EventArgs e)
        {
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            TypeMUid = Genclass.Dtype;
            chckAc.Checked = true;
            LoadButton(0);
        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckAc.Visible = true;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckAc.Visible = false;
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckAc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text != string.Empty)
                {
                    int Active;

                    if (chckActive.Checked == true)
                    {
                        Active = 1;
                    }
                    else
                    {
                        Active = 0;
                    }
                    if (btnSave.Text == "Save")
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@TypeM_Uid",TypeMUid),
                            new SqlParameter("@GeneralName",txtName.Text),
                            new SqlParameter("@Active",Active),
                            new SqlParameter("@f1",DBNull.Value),
                            new SqlParameter("@f2",DBNull.Value),
                            new SqlParameter("@CompanyId","1"),
                            new SqlParameter("@f3",DBNull.Value),
                            new SqlParameter("@Tag","Insert"),
                            new SqlParameter("@Uid","0")
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", para, conn);
                    }
                    else
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@TypeM_Uid",TypeMUid),
                            new SqlParameter("@GeneralName",txtName.Text),
                            new SqlParameter("@Active",Active),
                            new SqlParameter("@f1",DBNull.Value),
                            new SqlParameter("@f2",DBNull.Value),
                            new SqlParameter("@CompanyId","1"),
                            new SqlParameter("@f3",DBNull.Value),
                            new SqlParameter("@Tag","Update"),
                            new SqlParameter("@Uid",txtName.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", para, conn);
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Text = string.Empty;
                    LoadDatatable();
                    LoadButton(0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadDatatable()
        {
            try
            {
                int Active;
                if (chckAc.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                SqlParameter[] para = { new SqlParameter("@TypeM_Uid", TypeMUid), new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", para, conn);
                bsPLy.DataSource = dt;
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[1].Name = "GeneralName";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                DataGridCommon.Columns[1].Width = 500;
                DataGridCommon.Columns[2].Name = "Active";
                DataGridCommon.Columns[2].HeaderText = "Active";
                DataGridCommon.Columns[2].DataPropertyName = "Active";
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.DataSource = bsPLy;
                lblCount.Text = "of " + dt.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckAc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(chckAc.Checked == true)
                {
                    LoadDatatable();
                }
                else
                {
                    LoadDatatable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            txtName.Text = string.Empty;
            chckActive.Checked = true;
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            LoadButton(0);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                txtName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                bool Active = Convert.ToBoolean(DataGridCommon.Rows[Index].Cells[2].Value.ToString());
                if (Active == false)
                {
                    chckActive.Checked = false;
                }
                else
                {
                    chckActive.Checked = true;
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int i = DataGridCommon.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridCommon.CurrentCell = DataGridCommon.Rows[0].Cells[1];
                DataGridCommon.Rows[0].Selected = true;
                int i = DataGridCommon.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridCommon.CurrentCell.RowIndex;
                if (Cnt > 0)
                {
                    int i = DataGridCommon.CurrentCell.RowIndex - 1;
                    DataGridCommon.CurrentCell = DataGridCommon.Rows[i].Cells[1];
                    DataGridCommon.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnNxt_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridCommon.CurrentCell.RowIndex + 1;
                if (Cnt >= 0 && Cnt != DataGridCommon.Rows.Count)
                {
                    int i = DataGridCommon.CurrentCell.RowIndex + 1;
                    DataGridCommon.CurrentCell = DataGridCommon.Rows[i].Cells[1];
                    DataGridCommon.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            try
            {
                int i = DataGridCommon.Rows.Count  -1;
                DataGridCommon.CurrentCell = DataGridCommon.Rows[i].Cells[1];
                DataGridCommon.Rows[i].Selected = true;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
