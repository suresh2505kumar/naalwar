﻿namespace Naalwar
{
    partial class Frmpricelist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmpricelist));
            this.Genpan = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcgta = new System.Windows.Forms.TextBox();
            this.txtstate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboigst = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cboSGST = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbocgst = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttrqok = new System.Windows.Forms.Button();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.txtitid = new System.Windows.Forms.TextBox();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.txtstid = new System.Windows.Forms.TextBox();
            this.txttxid = new System.Windows.Forms.TextBox();
            this.txttaxper = new System.Windows.Forms.TextBox();
            this.txtuomid = new System.Windows.Forms.TextBox();
            this.txtmmid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtdis = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtmm = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtcode = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.updatpan = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtoite = new System.Windows.Forms.TextBox();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtparty = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.HFG2 = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.HFG1 = new System.Windows.Forms.DataGridView();
            this.button12 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.txtpartyuid = new System.Windows.Forms.TextBox();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.updatpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFG1)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label4);
            this.Genpan.Controls.Add(this.txtcgta);
            this.Genpan.Controls.Add(this.txtstate);
            this.Genpan.Controls.Add(this.label9);
            this.Genpan.Controls.Add(this.cboigst);
            this.Genpan.Controls.Add(this.label22);
            this.Genpan.Controls.Add(this.cboSGST);
            this.Genpan.Controls.Add(this.label21);
            this.Genpan.Controls.Add(this.label7);
            this.Genpan.Controls.Add(this.cbocgst);
            this.Genpan.Controls.Add(this.label6);
            this.Genpan.Controls.Add(this.Dtpdt);
            this.Genpan.Controls.Add(this.label5);
            this.Genpan.Controls.Add(this.label3);
            this.Genpan.Controls.Add(this.label2);
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.buttrqok);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtname);
            this.Genpan.Controls.Add(this.txtitem);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.txtpuid);
            this.Genpan.Controls.Add(this.txtuom);
            this.Genpan.Controls.Add(this.txtitid);
            this.Genpan.Controls.Add(this.txtrate);
            this.Genpan.Controls.Add(this.txtstid);
            this.Genpan.Controls.Add(this.txttxid);
            this.Genpan.Controls.Add(this.txttaxper);
            this.Genpan.Controls.Add(this.txtuomid);
            this.Genpan.Controls.Add(this.txtmmid);
            this.Genpan.Controls.Add(this.label8);
            this.Genpan.Controls.Add(this.txtdis);
            this.Genpan.Controls.Add(this.label11);
            this.Genpan.Controls.Add(this.txtmm);
            this.Genpan.Controls.Add(this.label10);
            this.Genpan.Controls.Add(this.txtcode);
            this.Genpan.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Genpan.Location = new System.Drawing.Point(0, -1);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(751, 463);
            this.Genpan.TabIndex = 158;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(459, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 21);
            this.label4.TabIndex = 235;
            this.label4.Text = "IGST %";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtcgta
            // 
            this.txtcgta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcgta.Enabled = false;
            this.txtcgta.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcgta.Location = new System.Drawing.Point(392, 112);
            this.txtcgta.Margin = new System.Windows.Forms.Padding(4);
            this.txtcgta.Name = "txtcgta";
            this.txtcgta.ReadOnly = true;
            this.txtcgta.Size = new System.Drawing.Size(68, 26);
            this.txtcgta.TabIndex = 234;
            this.txtcgta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtstate
            // 
            this.txtstate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstate.Enabled = false;
            this.txtstate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstate.Location = new System.Drawing.Point(0, 159);
            this.txtstate.Margin = new System.Windows.Forms.Padding(4);
            this.txtstate.Name = "txtstate";
            this.txtstate.ReadOnly = true;
            this.txtstate.Size = new System.Drawing.Size(390, 26);
            this.txtstate.TabIndex = 230;
            this.txtstate.TextChanged += new System.EventHandler(this.txtstate_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 189);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 21);
            this.label9.TabIndex = 229;
            this.label9.Text = "Item Search";
            // 
            // cboigst
            // 
            this.cboigst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboigst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboigst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboigst.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboigst.FormattingEnabled = true;
            this.cboigst.Location = new System.Drawing.Point(261, 159);
            this.cboigst.Margin = new System.Windows.Forms.Padding(4);
            this.cboigst.Name = "cboigst";
            this.cboigst.Size = new System.Drawing.Size(78, 22);
            this.cboigst.TabIndex = 7;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(389, 92);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 21);
            this.label22.TabIndex = 225;
            this.label22.Text = "CGST %";
            // 
            // cboSGST
            // 
            this.cboSGST.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSGST.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSGST.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSGST.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSGST.FormattingEnabled = true;
            this.cboSGST.Location = new System.Drawing.Point(160, 159);
            this.cboSGST.Margin = new System.Windows.Forms.Padding(4);
            this.cboSGST.Name = "cboSGST";
            this.cboSGST.Size = new System.Drawing.Size(139, 22);
            this.cboSGST.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(161, 158);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 16);
            this.label21.TabIndex = 222;
            this.label21.Text = "SGST  %";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 21);
            this.label7.TabIndex = 220;
            this.label7.Text = "State";
            // 
            // cbocgst
            // 
            this.cbocgst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocgst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocgst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocgst.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocgst.FormattingEnabled = true;
            this.cbocgst.Location = new System.Drawing.Point(1, 159);
            this.cbocgst.Margin = new System.Windows.Forms.Padding(4);
            this.cbocgst.Name = "cbocgst";
            this.cbocgst.Size = new System.Drawing.Size(151, 22);
            this.cbocgst.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(389, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 21);
            this.label6.TabIndex = 207;
            this.label6.Text = "Rate";
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(462, 64);
            this.Dtpdt.Margin = new System.Windows.Forms.Padding(4);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(113, 26);
            this.Dtpdt.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 21);
            this.label5.TabIndex = 136;
            this.label5.Text = "Customer Price List";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(459, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 21);
            this.label3.TabIndex = 134;
            this.label3.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 21);
            this.label2.TabIndex = 133;
            this.label2.Text = "Item";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 21);
            this.label1.TabIndex = 132;
            this.label1.Text = "Party";
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttrqok.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.Image = global::Naalwar.Properties.Resources.ok1_25x25;
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(469, 153);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(32, 32);
            this.buttrqok.TabIndex = 7;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click);
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(0, 239);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(710, 222);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellContentClick);
            this.HFGP.DoubleClick += new System.EventHandler(this.HFGP_DoubleClick);
            this.HFGP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyDown);
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(1, 64);
            this.txtname.Margin = new System.Windows.Forms.Padding(4);
            this.txtname.Name = "txtname";
            this.txtname.ReadOnly = true;
            this.txtname.Size = new System.Drawing.Size(458, 26);
            this.txtname.TabIndex = 1;
            this.txtname.Click += new System.EventHandler(this.txtname_Click);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(0, 112);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4);
            this.txtitem.Name = "txtitem";
            this.txtitem.ReadOnly = true;
            this.txtitem.Size = new System.Drawing.Size(390, 26);
            this.txtitem.TabIndex = 3;
            this.txtitem.Click += new System.EventHandler(this.txtitem_Click);
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged);
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(2, 210);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(298, 26);
            this.txtscr1.TabIndex = 137;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // txtpuid
            // 
            this.txtpuid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpuid.Location = new System.Drawing.Point(274, 66);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(4);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(25, 22);
            this.txtpuid.TabIndex = 87;
            this.txtpuid.TextChanged += new System.EventHandler(this.txtpuid_TextChanged_1);
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(389, 323);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4);
            this.txtuom.Name = "txtuom";
            this.txtuom.ReadOnly = true;
            this.txtuom.Size = new System.Drawing.Size(68, 22);
            this.txtuom.TabIndex = 4;
            this.txtuom.Click += new System.EventHandler(this.txtuom_Click);
            this.txtuom.TextChanged += new System.EventHandler(this.txtuom_TextChanged);
            this.txtuom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtuom_KeyDown);
            // 
            // txtitid
            // 
            this.txtitid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitid.Location = new System.Drawing.Point(277, 111);
            this.txtitid.Margin = new System.Windows.Forms.Padding(4);
            this.txtitid.Name = "txtitid";
            this.txtitid.Size = new System.Drawing.Size(22, 22);
            this.txtitid.TabIndex = 89;
            this.txtitid.TextChanged += new System.EventHandler(this.txtitid_TextChanged);
            this.txtitid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitid_KeyDown);
            // 
            // txtrate
            // 
            this.txtrate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrate.Location = new System.Drawing.Point(392, 159);
            this.txtrate.Margin = new System.Windows.Forms.Padding(4);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(70, 26);
            this.txtrate.TabIndex = 5;
            this.txtrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtstid
            // 
            this.txtstid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstid.Location = new System.Drawing.Point(164, 159);
            this.txtstid.Margin = new System.Windows.Forms.Padding(4);
            this.txtstid.Name = "txtstid";
            this.txtstid.Size = new System.Drawing.Size(27, 22);
            this.txtstid.TabIndex = 231;
            // 
            // txttxid
            // 
            this.txttxid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttxid.Enabled = false;
            this.txttxid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttxid.Location = new System.Drawing.Point(261, 159);
            this.txttxid.Margin = new System.Windows.Forms.Padding(4);
            this.txttxid.Name = "txttxid";
            this.txttxid.Size = new System.Drawing.Size(26, 22);
            this.txttxid.TabIndex = 233;
            // 
            // txttaxper
            // 
            this.txttaxper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttaxper.Enabled = false;
            this.txttaxper.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttaxper.Location = new System.Drawing.Point(462, 112);
            this.txttaxper.Margin = new System.Windows.Forms.Padding(4);
            this.txttaxper.Name = "txttaxper";
            this.txttaxper.ReadOnly = true;
            this.txttaxper.Size = new System.Drawing.Size(70, 26);
            this.txttaxper.TabIndex = 232;
            this.txttaxper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttaxper.TextChanged += new System.EventHandler(this.txttaxper_TextChanged);
            // 
            // txtuomid
            // 
            this.txtuomid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuomid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuomid.Location = new System.Drawing.Point(432, 277);
            this.txtuomid.Margin = new System.Windows.Forms.Padding(4);
            this.txtuomid.Name = "txtuomid";
            this.txtuomid.Size = new System.Drawing.Size(28, 22);
            this.txtuomid.TabIndex = 238;
            this.txtuomid.TextChanged += new System.EventHandler(this.txtuomid_TextChanged);
            // 
            // txtmmid
            // 
            this.txtmmid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmmid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmmid.Location = new System.Drawing.Point(322, 279);
            this.txtmmid.Margin = new System.Windows.Forms.Padding(4);
            this.txtmmid.Name = "txtmmid";
            this.txtmmid.Size = new System.Drawing.Size(29, 22);
            this.txtmmid.TabIndex = 239;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(381, 285);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 16);
            this.label8.TabIndex = 228;
            this.label8.Text = "Dis %";
            // 
            // txtdis
            // 
            this.txtdis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdis.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdis.Location = new System.Drawing.Point(359, 279);
            this.txtdis.Margin = new System.Windows.Forms.Padding(4);
            this.txtdis.Name = "txtdis";
            this.txtdis.Size = new System.Drawing.Size(38, 22);
            this.txtdis.TabIndex = 6;
            this.txtdis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdis.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdis_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(456, 305);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 16);
            this.label11.TabIndex = 237;
            this.label11.Text = "MM";
            // 
            // txtmm
            // 
            this.txtmm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmm.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmm.Location = new System.Drawing.Point(459, 323);
            this.txtmm.Margin = new System.Windows.Forms.Padding(4);
            this.txtmm.Name = "txtmm";
            this.txtmm.Size = new System.Drawing.Size(53, 22);
            this.txtmm.TabIndex = 236;
            this.txtmm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmm_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(386, 305);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 16);
            this.label10.TabIndex = 207;
            this.label10.Text = "UoM";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtcode
            // 
            this.txtcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcode.Location = new System.Drawing.Point(274, 249);
            this.txtcode.Margin = new System.Windows.Forms.Padding(4);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(53, 22);
            this.txtcode.TabIndex = 240;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(290, 468);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 30);
            this.button1.TabIndex = 240;
            this.button1.Text = "Import Price list";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(506, 468);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(56, 30);
            this.btnexit.TabIndex = 159;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click_1);
            // 
            // updatpan
            // 
            this.updatpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.updatpan.Controls.Add(this.checkBox1);
            this.updatpan.Controls.Add(this.txtoite);
            this.updatpan.Controls.Add(this.btnadd);
            this.updatpan.Controls.Add(this.txtparty);
            this.updatpan.Controls.Add(this.label13);
            this.updatpan.Controls.Add(this.HFG2);
            this.updatpan.Controls.Add(this.label12);
            this.updatpan.Controls.Add(this.HFG1);
            this.updatpan.Controls.Add(this.button12);
            this.updatpan.Controls.Add(this.chkact);
            this.updatpan.Controls.Add(this.txtpartyuid);
            this.updatpan.Location = new System.Drawing.Point(0, 34);
            this.updatpan.Name = "updatpan";
            this.updatpan.Size = new System.Drawing.Size(751, 428);
            this.updatpan.TabIndex = 160;
            this.updatpan.Paint += new System.Windows.Forms.PaintEventHandler(this.updatpan_Paint);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(577, 76);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(92, 25);
            this.checkBox1.TabIndex = 167;
            this.checkBox1.Text = "Select All";
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtoite
            // 
            this.txtoite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoite.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoite.Location = new System.Drawing.Point(11, 76);
            this.txtoite.Margin = new System.Windows.Forms.Padding(4);
            this.txtoite.Name = "txtoite";
            this.txtoite.Size = new System.Drawing.Size(559, 26);
            this.txtoite.TabIndex = 166;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(322, 393);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(106, 30);
            this.btnadd.TabIndex = 168;
            this.btnadd.Text = "Get Price List";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtparty
            // 
            this.txtparty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtparty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtparty.Location = new System.Drawing.Point(56, 27);
            this.txtparty.Margin = new System.Windows.Forms.Padding(4);
            this.txtparty.Name = "txtparty";
            this.txtparty.Size = new System.Drawing.Size(514, 26);
            this.txtparty.TabIndex = 136;
            this.txtparty.Click += new System.EventHandler(this.txtparty_Click);
            this.txtparty.TextChanged += new System.EventHandler(this.txtparty_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 21);
            this.label13.TabIndex = 135;
            this.label13.Text = "Itemname";
            // 
            // HFG2
            // 
            this.HFG2.AllowUserToDeleteRows = false;
            this.HFG2.AllowUserToOrderColumns = true;
            this.HFG2.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFG2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFG2.Location = new System.Drawing.Point(12, 103);
            this.HFG2.Name = "HFG2";
            this.HFG2.Size = new System.Drawing.Size(658, 284);
            this.HFG2.TabIndex = 134;
            this.HFG2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFG2_CellClick);
            this.HFG2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFG2_CellContentClick);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 21);
            this.label12.TabIndex = 133;
            this.label12.Text = "Party";
            // 
            // HFG1
            // 
            this.HFG1.AllowUserToDeleteRows = false;
            this.HFG1.AllowUserToOrderColumns = true;
            this.HFG1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFG1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFG1.Location = new System.Drawing.Point(160, 204);
            this.HFG1.Name = "HFG1";
            this.HFG1.Size = new System.Drawing.Size(148, 109);
            this.HFG1.TabIndex = 4;
            this.HFG1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFG1_CellClick);
            this.HFG1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFG1_CellContentClick);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(435, 393);
            this.button12.Margin = new System.Windows.Forms.Padding(4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 30);
            this.button12.TabIndex = 238;
            this.button12.Text = "Back";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(115, 217);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(93, 20);
            this.chkact.TabIndex = 165;
            this.chkact.Text = "Select All";
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            this.chkact.Click += new System.EventHandler(this.chkact_Click);
            // 
            // txtpartyuid
            // 
            this.txtpartyuid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpartyuid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpartyuid.Location = new System.Drawing.Point(322, 155);
            this.txtpartyuid.Margin = new System.Windows.Forms.Padding(4);
            this.txtpartyuid.Name = "txtpartyuid";
            this.txtpartyuid.Size = new System.Drawing.Size(113, 22);
            this.txtpartyuid.TabIndex = 239;
            this.txtpartyuid.TextChanged += new System.EventHandler(this.txtpartyuid_TextChanged);
            // 
            // Frmpricelist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 499);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.updatpan);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Frmpricelist";
            this.Text = "Price List";
            this.Load += new System.EventHandler(this.Frmpricelist_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.updatpan.ResumeLayout(false);
            this.updatpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFG1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtitid;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbocgst;
        private System.Windows.Forms.ComboBox cboSGST;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtdis;
        private System.Windows.Forms.ComboBox cboigst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtstate;
        private System.Windows.Forms.TextBox txtstid;
        private System.Windows.Forms.TextBox txttaxper;
        private System.Windows.Forms.TextBox txttxid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcgta;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtmm;
        private System.Windows.Forms.TextBox txtuomid;
        private System.Windows.Forms.TextBox txtmmid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel updatpan;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView HFG2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView HFG1;
        private System.Windows.Forms.TextBox txtparty;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtoite;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox txtcode;
        private System.Windows.Forms.TextBox txtpartyuid;

    }
}