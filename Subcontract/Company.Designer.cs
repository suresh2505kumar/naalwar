﻿namespace Naalwar
{
    partial class Company
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Company));
            this.label22 = new System.Windows.Forms.Label();
            this.tngno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpin = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtstate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtcity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtadd2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtadd1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.butnsave = new System.Windows.Forms.Button();
            this.buttnext2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(312, 257);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 21);
            this.label22.TabIndex = 197;
            this.label22.Text = "GSTTIN";
            // 
            // tngno
            // 
            this.tngno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tngno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tngno.Location = new System.Drawing.Point(376, 256);
            this.tngno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tngno.Name = "tngno";
            this.tngno.Size = new System.Drawing.Size(202, 26);
            this.tngno.TabIndex = 187;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(20, 295);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 21);
            this.label11.TabIndex = 196;
            this.label11.Text = "E-mail Id";
            // 
            // txtmail
            // 
            this.txtmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmail.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmail.Location = new System.Drawing.Point(115, 295);
            this.txtmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(463, 26);
            this.txtmail.TabIndex = 188;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(312, 201);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 21);
            this.label12.TabIndex = 195;
            this.label12.Text = "Pin";
            // 
            // txtpin
            // 
            this.txtpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpin.Location = new System.Drawing.Point(376, 201);
            this.txtpin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtpin.Name = "txtpin";
            this.txtpin.Size = new System.Drawing.Size(202, 26);
            this.txtpin.TabIndex = 185;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(20, 201);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 21);
            this.label9.TabIndex = 194;
            this.label9.Text = "State";
            // 
            // txtstate
            // 
            this.txtstate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstate.Location = new System.Drawing.Point(115, 199);
            this.txtstate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtstate.Name = "txtstate";
            this.txtstate.Size = new System.Drawing.Size(189, 26);
            this.txtstate.TabIndex = 184;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 153);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 21);
            this.label10.TabIndex = 193;
            this.label10.Text = "City";
            // 
            // txtcity
            // 
            this.txtcity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcity.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcity.Location = new System.Drawing.Point(115, 152);
            this.txtcity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtcity.Name = "txtcity";
            this.txtcity.Size = new System.Drawing.Size(463, 26);
            this.txtcity.TabIndex = 183;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(20, 113);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 21);
            this.label8.TabIndex = 192;
            this.label8.Text = "Address2";
            // 
            // txtadd2
            // 
            this.txtadd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtadd2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd2.Location = new System.Drawing.Point(115, 113);
            this.txtadd2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtadd2.Name = "txtadd2";
            this.txtadd2.Size = new System.Drawing.Size(463, 26);
            this.txtadd2.TabIndex = 182;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(20, 66);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 21);
            this.label7.TabIndex = 189;
            this.label7.Text = "Address1";
            // 
            // txtadd1
            // 
            this.txtadd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtadd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd1.Location = new System.Drawing.Point(115, 64);
            this.txtadd1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtadd1.Name = "txtadd1";
            this.txtadd1.Size = new System.Drawing.Size(463, 26);
            this.txtadd1.TabIndex = 181;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(20, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 21);
            this.label1.TabIndex = 181;
            this.label1.Text = "Name";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(115, 19);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(463, 26);
            this.txtname.TabIndex = 180;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.ForeColor = System.Drawing.Color.Black;
            this.Phone.Location = new System.Drawing.Point(20, 251);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(54, 21);
            this.Phone.TabIndex = 184;
            this.Phone.Text = "Phone";
            // 
            // txtphone
            // 
            this.txtphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtphone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphone.Location = new System.Drawing.Point(115, 251);
            this.txtphone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(189, 26);
            this.txtphone.TabIndex = 186;
            // 
            // butnsave
            // 
            this.butnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.butnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butnsave.Image = ((System.Drawing.Image)(resources.GetObject("butnsave.Image")));
            this.butnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butnsave.Location = new System.Drawing.Point(224, 350);
            this.butnsave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butnsave.Name = "butnsave";
            this.butnsave.Size = new System.Drawing.Size(65, 30);
            this.butnsave.TabIndex = 189;
            this.butnsave.Text = "Save";
            this.butnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butnsave.UseVisualStyleBackColor = false;
            this.butnsave.Click += new System.EventHandler(this.butnsave_Click);
            // 
            // buttnext2
            // 
            this.buttnext2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext2.Image = ((System.Drawing.Image)(resources.GetObject("buttnext2.Image")));
            this.buttnext2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext2.Location = new System.Drawing.Point(314, 350);
            this.buttnext2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.buttnext2.Name = "buttnext2";
            this.buttnext2.Size = new System.Drawing.Size(60, 30);
            this.buttnext2.TabIndex = 209;
            this.buttnext2.Text = "Exit";
            this.buttnext2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext2.UseVisualStyleBackColor = false;
            this.buttnext2.Click += new System.EventHandler(this.buttnext2_Click);
            // 
            // Company
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 407);
            this.Controls.Add(this.butnsave);
            this.Controls.Add(this.buttnext2);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.tngno);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtmail);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtpin);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtstate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtcity);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtadd2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtadd1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.Phone);
            this.Controls.Add(this.txtphone);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "Company";
            this.Text = "Company";
            this.Load += new System.EventHandler(this.Company_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tngno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtpin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtstate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtcity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtadd2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtadd1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Button butnsave;
        private System.Windows.Forms.Button buttnext2;
    }
}