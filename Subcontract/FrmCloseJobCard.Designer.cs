﻿namespace Naalwar
{
    partial class FrmCloseJobCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCloseJobCard));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.DataGridCloseJobCard = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpreqDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtTotalEnds = new System.Windows.Forms.TextBox();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtjobcardSearch = new System.Windows.Forms.TextBox();
            this.DataGridJobCard = new System.Windows.Forms.DataGridView();
            this.btnNxt = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.PanelgridNos = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txttotalHour = new System.Windows.Forms.TextBox();
            this.txtTotalMeters = new System.Windows.Forms.TextBox();
            this.txtTotalWght = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txttotalRoll = new System.Windows.Forms.TextBox();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCloseJobCard)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridJobCard)).BeginInit();
            this.PanelgridNos.SuspendLayout();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.txttotalRoll);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.txtTotalWght);
            this.grBack.Controls.Add(this.txtTotalMeters);
            this.grBack.Controls.Add(this.txttotalHour);
            this.grBack.Controls.Add(this.DataGridCloseJobCard);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.txtQuantity);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtItemName);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.dtpDocDate);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.dtpreqDate);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtRemarks);
            this.grBack.Controls.Add(this.txtTotalEnds);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, 4);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(687, 446);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // DataGridCloseJobCard
            // 
            this.DataGridCloseJobCard.AllowUserToAddRows = false;
            this.DataGridCloseJobCard.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCloseJobCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCloseJobCard.Location = new System.Drawing.Point(17, 163);
            this.DataGridCloseJobCard.Name = "DataGridCloseJobCard";
            this.DataGridCloseJobCard.ReadOnly = true;
            this.DataGridCloseJobCard.RowHeadersVisible = false;
            this.DataGridCloseJobCard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCloseJobCard.Size = new System.Drawing.Size(659, 246);
            this.DataGridCloseJobCard.TabIndex = 402;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(245, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 18);
            this.label7.TabIndex = 401;
            this.label7.Text = "Total Ends";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(444, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 18);
            this.label6.TabIndex = 399;
            this.label6.Text = "Remarks";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Enabled = false;
            this.txtQuantity.Location = new System.Drawing.Point(116, 115);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(111, 26);
            this.txtQuantity.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 18);
            this.label4.TabIndex = 10;
            this.label4.Text = "Quantity";
            // 
            // txtItemName
            // 
            this.txtItemName.Enabled = false;
            this.txtItemName.Location = new System.Drawing.Point(116, 83);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(324, 26);
            this.txtItemName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Item Name";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Enabled = false;
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(116, 51);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(111, 26);
            this.dtpDocDate.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Doc Date";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Enabled = false;
            this.txtDocNo.Location = new System.Drawing.Point(118, 18);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(109, 26);
            this.txtDocNo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Doc No";
            // 
            // dtpreqDate
            // 
            this.dtpreqDate.Enabled = false;
            this.dtpreqDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpreqDate.Location = new System.Drawing.Point(329, 115);
            this.dtpreqDate.Name = "dtpreqDate";
            this.dtpreqDate.Size = new System.Drawing.Size(111, 26);
            this.dtpreqDate.TabIndex = 396;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(226, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 18);
            this.label5.TabIndex = 397;
            this.label5.Text = "Required Date";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(446, 52);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(230, 91);
            this.txtRemarks.TabIndex = 398;
            // 
            // txtTotalEnds
            // 
            this.txtTotalEnds.Enabled = false;
            this.txtTotalEnds.Location = new System.Drawing.Point(329, 47);
            this.txtTotalEnds.Name = "txtTotalEnds";
            this.txtTotalEnds.Size = new System.Drawing.Size(111, 26);
            this.txtTotalEnds.TabIndex = 400;
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtjobcardSearch);
            this.grFront.Controls.Add(this.DataGridJobCard);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(6, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(687, 447);
            this.grFront.TabIndex = 396;
            this.grFront.TabStop = false;
            // 
            // txtjobcardSearch
            // 
            this.txtjobcardSearch.Location = new System.Drawing.Point(6, 22);
            this.txtjobcardSearch.Name = "txtjobcardSearch";
            this.txtjobcardSearch.Size = new System.Drawing.Size(675, 26);
            this.txtjobcardSearch.TabIndex = 1;
            this.txtjobcardSearch.TextChanged += new System.EventHandler(this.txtjobcardSearch_TextChanged);
            // 
            // DataGridJobCard
            // 
            this.DataGridJobCard.AllowUserToAddRows = false;
            this.DataGridJobCard.BackgroundColor = System.Drawing.Color.White;
            this.DataGridJobCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridJobCard.Location = new System.Drawing.Point(6, 51);
            this.DataGridJobCard.Name = "DataGridJobCard";
            this.DataGridJobCard.ReadOnly = true;
            this.DataGridJobCard.RowHeadersVisible = false;
            this.DataGridJobCard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridJobCard.Size = new System.Drawing.Size(675, 387);
            this.DataGridJobCard.TabIndex = 0;
            // 
            // btnNxt
            // 
            this.btnNxt.BackColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderSize = 0;
            this.btnNxt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNxt.Image = ((System.Drawing.Image)(resources.GetObject("btnNxt.Image")));
            this.btnNxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNxt.Location = new System.Drawing.Point(144, 2);
            this.btnNxt.Name = "btnNxt";
            this.btnNxt.Size = new System.Drawing.Size(18, 31);
            this.btnNxt.TabIndex = 210;
            this.btnNxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNxt.UseVisualStyleBackColor = false;
            // 
            // btnLast
            // 
            this.btnLast.BackColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderSize = 0;
            this.btnLast.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLast.Image = ((System.Drawing.Image)(resources.GetObject("btnLast.Image")));
            this.btnLast.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLast.Location = new System.Drawing.Point(178, 2);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(19, 31);
            this.btnLast.TabIndex = 211;
            this.btnLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLast.UseVisualStyleBackColor = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(40, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 31);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            // 
            // btnFirst
            // 
            this.btnFirst.BackColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderSize = 0;
            this.btnFirst.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirst.Image = ((System.Drawing.Image)(resources.GetObject("btnFirst.Image")));
            this.btnFirst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFirst.Location = new System.Drawing.Point(6, 2);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(19, 31);
            this.btnFirst.TabIndex = 213;
            this.btnFirst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFirst.UseVisualStyleBackColor = false;
            // 
            // PanelgridNos
            // 
            this.PanelgridNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PanelgridNos.Controls.Add(this.lblno1);
            this.PanelgridNos.Controls.Add(this.lblno2);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel3);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel2);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel1);
            this.PanelgridNos.Location = new System.Drawing.Point(64, 2);
            this.PanelgridNos.Name = "PanelgridNos";
            this.PanelgridNos.Size = new System.Drawing.Size(74, 30);
            this.PanelgridNos.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 18);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(31, 18);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(550, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.btnAddCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(471, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(447, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(119, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Close Job Card";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(564, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.checkBox1);
            this.panadd.Controls.Add(this.btnPrint);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Controls.Add(this.PanelgridNos);
            this.panadd.Controls.Add(this.btnFirst);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnLast);
            this.panadd.Controls.Add(this.btnNxt);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Location = new System.Drawing.Point(6, 456);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(687, 36);
            this.panadd.TabIndex = 237;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(621, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 216;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(203, 8);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(124, 22);
            this.checkBox1.TabIndex = 217;
            this.checkBox1.Text = "Closed Job Card";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txttotalHour
            // 
            this.txttotalHour.Location = new System.Drawing.Point(571, 410);
            this.txttotalHour.Name = "txttotalHour";
            this.txttotalHour.Size = new System.Drawing.Size(105, 26);
            this.txttotalHour.TabIndex = 403;
            this.txttotalHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalMeters
            // 
            this.txtTotalMeters.Location = new System.Drawing.Point(163, 410);
            this.txtTotalMeters.Name = "txtTotalMeters";
            this.txtTotalMeters.Size = new System.Drawing.Size(87, 26);
            this.txtTotalMeters.TabIndex = 404;
            this.txtTotalMeters.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalWght
            // 
            this.txtTotalWght.Location = new System.Drawing.Point(73, 410);
            this.txtTotalWght.Name = "txtTotalWght";
            this.txtTotalWght.Size = new System.Drawing.Size(84, 26);
            this.txtTotalWght.TabIndex = 405;
            this.txtTotalWght.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 414);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 18);
            this.label8.TabIndex = 406;
            this.label8.Text = "Total";
            // 
            // txttotalRoll
            // 
            this.txttotalRoll.Location = new System.Drawing.Point(256, 410);
            this.txttotalRoll.Name = "txttotalRoll";
            this.txttotalRoll.Size = new System.Drawing.Size(43, 26);
            this.txttotalRoll.TabIndex = 407;
            this.txttotalRoll.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FrmCloseJobCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(699, 500);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.MaximizeBox = false;
            this.Name = "FrmCloseJobCard";
            this.Text = "Job Card";
            this.Load += new System.EventHandler(this.FrmJobCard_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCloseJobCard)).EndInit();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridJobCard)).EndInit();
            this.PanelgridNos.ResumeLayout(false);
            this.PanelgridNos.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtjobcardSearch;
        private System.Windows.Forms.DataGridView DataGridJobCard;
        private System.Windows.Forms.DateTimePicker dtpreqDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Button btnNxt;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Panel PanelgridNos;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.TextBox txtTotalEnds;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DataGridView DataGridCloseJobCard;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTotalWght;
        private System.Windows.Forms.TextBox txtTotalMeters;
        private System.Windows.Forms.TextBox txttotalHour;
        private System.Windows.Forms.TextBox txttotalRoll;
    }
}