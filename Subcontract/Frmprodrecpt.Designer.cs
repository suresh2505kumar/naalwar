﻿namespace Naalwar
{
    partial class Frmprodrecpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmprodrecpt));
            this.Genpan = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.panrea = new System.Windows.Forms.Panel();
            this.txtrejrea = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtrwrea = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtrew = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtrej = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtacc = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txthqty = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timefrom = new System.Windows.Forms.DateTimePicker();
            this.timeto = new System.Windows.Forms.DateTimePicker();
            this.txtemp = new System.Windows.Forms.TextBox();
            this.Employee = new System.Windows.Forms.Label();
            this.txtop = new System.Windows.Forms.TextBox();
            this.Operation = new System.Windows.Forms.Label();
            this.txtmac = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Item = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtdocno = new System.Windows.Forms.TextBox();
            this.txtititd = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.txtid = new System.Windows.Forms.TextBox();
            this.txtit = new System.Windows.Forms.TextBox();
            this.txtempid = new System.Windows.Forms.TextBox();
            this.txtopid = new System.Windows.Forms.TextBox();
            this.txtmcid = new System.Windows.Forms.TextBox();
            this.txtgqty = new System.Windows.Forms.TextBox();
            this.chkedtact = new System.Windows.Forms.CheckBox();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butexit = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Reqbk = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.Editpnl.SuspendLayout();
            this.panrea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Genpan.Location = new System.Drawing.Point(-2, -1);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(844, 487);
            this.Genpan.TabIndex = 188;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 19);
            this.label1.TabIndex = 188;
            this.label1.Text = "Production Reciept";
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(1, 67);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(841, 418);
            this.HFGP.TabIndex = 103;
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(741, 41);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(101, 22);
            this.txtscr5.TabIndex = 90;
            this.txtscr5.TextChanged += new System.EventHandler(this.txtscr5_TextChanged);
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(461, 41);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(280, 22);
            this.txtscr4.TabIndex = 100;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged);
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(181, 41);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(280, 22);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(91, 41);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 22);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 41);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(90, 22);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.panrea);
            this.Editpnl.Controls.Add(this.label10);
            this.Editpnl.Controls.Add(this.txtrew);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.txtrej);
            this.Editpnl.Controls.Add(this.label8);
            this.Editpnl.Controls.Add(this.txtacc);
            this.Editpnl.Controls.Add(this.button2);
            this.Editpnl.Controls.Add(this.label7);
            this.Editpnl.Controls.Add(this.txthqty);
            this.Editpnl.Controls.Add(this.label5);
            this.Editpnl.Controls.Add(this.label6);
            this.Editpnl.Controls.Add(this.timefrom);
            this.Editpnl.Controls.Add(this.timeto);
            this.Editpnl.Controls.Add(this.txtemp);
            this.Editpnl.Controls.Add(this.Employee);
            this.Editpnl.Controls.Add(this.txtop);
            this.Editpnl.Controls.Add(this.Operation);
            this.Editpnl.Controls.Add(this.txtmac);
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.DTPDOCDT);
            this.Editpnl.Controls.Add(this.HFIT);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.Item);
            this.Editpnl.Controls.Add(this.txtitem);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.txtname);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtdocno);
            this.Editpnl.Controls.Add(this.txtititd);
            this.Editpnl.Controls.Add(this.txtpuid);
            this.Editpnl.Controls.Add(this.txtgrnno);
            this.Editpnl.Controls.Add(this.label11);
            this.Editpnl.Controls.Add(this.txtgrnid);
            this.Editpnl.Controls.Add(this.txtqty);
            this.Editpnl.Controls.Add(this.txtid);
            this.Editpnl.Controls.Add(this.txtit);
            this.Editpnl.Controls.Add(this.txtempid);
            this.Editpnl.Controls.Add(this.txtopid);
            this.Editpnl.Controls.Add(this.txtmcid);
            this.Editpnl.Controls.Add(this.txtgqty);
            this.Editpnl.Location = new System.Drawing.Point(0, 0);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(843, 485);
            this.Editpnl.TabIndex = 195;
            // 
            // panrea
            // 
            this.panrea.Controls.Add(this.txtrejrea);
            this.panrea.Controls.Add(this.label13);
            this.panrea.Controls.Add(this.txtrwrea);
            this.panrea.Controls.Add(this.label12);
            this.panrea.Location = new System.Drawing.Point(1, 120);
            this.panrea.Name = "panrea";
            this.panrea.Size = new System.Drawing.Size(840, 362);
            this.panrea.TabIndex = 250;
            this.panrea.Visible = false;
            // 
            // txtrejrea
            // 
            this.txtrejrea.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrejrea.Location = new System.Drawing.Point(13, 211);
            this.txtrejrea.Name = "txtrejrea";
            this.txtrejrea.Size = new System.Drawing.Size(813, 147);
            this.txtrejrea.TabIndex = 232;
            this.txtrejrea.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 190);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 16);
            this.label13.TabIndex = 231;
            this.label13.Text = "Rejection Reason";
            // 
            // txtrwrea
            // 
            this.txtrwrea.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrwrea.Location = new System.Drawing.Point(13, 36);
            this.txtrwrea.Name = "txtrwrea";
            this.txtrwrea.Size = new System.Drawing.Size(813, 147);
            this.txtrwrea.TabIndex = 230;
            this.txtrwrea.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 15);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 16);
            this.label12.TabIndex = 228;
            this.label12.Text = "Rework Reason";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(611, 122);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 16);
            this.label10.TabIndex = 248;
            this.label10.Text = "Rew.Qty";
            // 
            // txtrew
            // 
            this.txtrew.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrew.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrew.Location = new System.Drawing.Point(612, 143);
            this.txtrew.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrew.Name = "txtrew";
            this.txtrew.Size = new System.Drawing.Size(71, 22);
            this.txtrew.TabIndex = 247;
            this.txtrew.Text = "0";
            this.txtrew.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrew_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(688, 122);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 16);
            this.label9.TabIndex = 246;
            this.label9.Text = "Rej.Qty";
            // 
            // txtrej
            // 
            this.txtrej.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrej.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrej.Location = new System.Drawing.Point(691, 143);
            this.txtrej.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrej.Name = "txtrej";
            this.txtrej.Size = new System.Drawing.Size(73, 22);
            this.txtrej.TabIndex = 245;
            this.txtrej.Text = "0";
            this.txtrej.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrej_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(529, 122);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 244;
            this.label8.Text = "Acc.Qty";
            // 
            // txtacc
            // 
            this.txtacc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtacc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtacc.Location = new System.Drawing.Point(532, 143);
            this.txtacc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtacc.Name = "txtacc";
            this.txtacc.Size = new System.Drawing.Size(72, 22);
            this.txtacc.TabIndex = 243;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::Naalwar.Properties.Resources.ok1_25x25;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(770, 135);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 32);
            this.button2.TabIndex = 242;
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(450, 122);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 16);
            this.label7.TabIndex = 240;
            this.label7.Text = "Qty";
            // 
            // txthqty
            // 
            this.txthqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthqty.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthqty.Location = new System.Drawing.Point(453, 143);
            this.txthqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txthqty.Name = "txthqty";
            this.txthqty.Size = new System.Drawing.Size(71, 22);
            this.txthqty.TabIndex = 239;
            this.txthqty.TextChanged += new System.EventHandler(this.txthqty_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(316, 22);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 16);
            this.label5.TabIndex = 238;
            this.label5.Text = "To";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(216, 22);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 16);
            this.label6.TabIndex = 237;
            this.label6.Text = "From";
            // 
            // timefrom
            // 
            this.timefrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timefrom.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timefrom.Location = new System.Drawing.Point(217, 44);
            this.timefrom.Name = "timefrom";
            this.timefrom.Size = new System.Drawing.Size(96, 22);
            this.timefrom.TabIndex = 236;
            // 
            // timeto
            // 
            this.timeto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeto.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeto.Location = new System.Drawing.Point(319, 44);
            this.timeto.Name = "timeto";
            this.timeto.Size = new System.Drawing.Size(93, 22);
            this.timeto.TabIndex = 235;
            // 
            // txtemp
            // 
            this.txtemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtemp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemp.Location = new System.Drawing.Point(549, 95);
            this.txtemp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtemp.Name = "txtemp";
            this.txtemp.Size = new System.Drawing.Size(269, 22);
            this.txtemp.TabIndex = 231;
            this.txtemp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtemp_KeyDown);
            // 
            // Employee
            // 
            this.Employee.AutoSize = true;
            this.Employee.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Employee.Location = new System.Drawing.Point(546, 72);
            this.Employee.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Employee.Name = "Employee";
            this.Employee.Size = new System.Drawing.Size(71, 16);
            this.Employee.TabIndex = 230;
            this.Employee.Text = "Employee";
            // 
            // txtop
            // 
            this.txtop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtop.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtop.Location = new System.Drawing.Point(278, 95);
            this.txtop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtop.Name = "txtop";
            this.txtop.Size = new System.Drawing.Size(261, 22);
            this.txtop.TabIndex = 229;
            this.txtop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtop_KeyDown);
            // 
            // Operation
            // 
            this.Operation.AutoSize = true;
            this.Operation.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Operation.Location = new System.Drawing.Point(277, 72);
            this.Operation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Operation.Name = "Operation";
            this.Operation.Size = new System.Drawing.Size(71, 16);
            this.Operation.TabIndex = 228;
            this.Operation.Text = "Operation";
            // 
            // txtmac
            // 
            this.txtmac.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmac.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmac.Location = new System.Drawing.Point(9, 95);
            this.txtmac.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmac.Name = "txtmac";
            this.txtmac.Size = new System.Drawing.Size(261, 22);
            this.txtmac.TabIndex = 227;
            this.txtmac.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmac_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 226;
            this.label4.Text = "Machine";
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(109, 45);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(97, 22);
            this.DTPDOCDT.TabIndex = 221;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFIT.Location = new System.Drawing.Point(1, 173);
            this.HFIT.Name = "HFIT";
            this.HFIT.ReadOnly = true;
            this.HFIT.Size = new System.Drawing.Size(840, 311);
            this.HFIT.TabIndex = 214;
            this.HFIT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFIT_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(106, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 201;
            this.label2.Text = "Doc.Date";
            // 
            // Item
            // 
            this.Item.AutoSize = true;
            this.Item.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Item.Location = new System.Drawing.Point(6, 122);
            this.Item.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Item.Name = "Item";
            this.Item.Size = new System.Drawing.Size(36, 16);
            this.Item.TabIndex = 197;
            this.Item.Text = "Item";
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(8, 143);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(442, 22);
            this.txtitem.TabIndex = 196;
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(416, 21);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 16);
            this.label3.TabIndex = 195;
            this.label3.Text = "Party Name";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(419, 44);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(400, 22);
            this.txtname.TabIndex = 194;
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(6, 22);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(53, 16);
            this.Phone.TabIndex = 199;
            this.Phone.Text = "Doc.No";
            // 
            // txtdocno
            // 
            this.txtdocno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdocno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdocno.Location = new System.Drawing.Point(9, 45);
            this.txtdocno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdocno.Name = "txtdocno";
            this.txtdocno.Size = new System.Drawing.Size(93, 22);
            this.txtdocno.TabIndex = 198;
            // 
            // txtititd
            // 
            this.txtititd.Location = new System.Drawing.Point(317, 145);
            this.txtititd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtititd.Name = "txtititd";
            this.txtititd.Size = new System.Drawing.Size(51, 20);
            this.txtititd.TabIndex = 208;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(477, 46);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(66, 20);
            this.txtpuid.TabIndex = 217;
            // 
            // txtgrnno
            // 
            this.txtgrnno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnno.Location = new System.Drawing.Point(33, 197);
            this.txtgrnno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnno.Name = "txtgrnno";
            this.txtgrnno.Size = new System.Drawing.Size(122, 22);
            this.txtgrnno.TabIndex = 219;
            this.txtgrnno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgrnno_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(29, 197);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 18);
            this.label11.TabIndex = 218;
            this.label11.Text = "Grn No";
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(124, 197);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(22, 20);
            this.txtgrnid.TabIndex = 220;
            // 
            // txtqty
            // 
            this.txtqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(323, 262);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(33, 22);
            this.txtqty.TabIndex = 225;
            // 
            // txtid
            // 
            this.txtid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtid.Location = new System.Drawing.Point(282, 262);
            this.txtid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(33, 22);
            this.txtid.TabIndex = 224;
            // 
            // txtit
            // 
            this.txtit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtit.Location = new System.Drawing.Point(241, 262);
            this.txtit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtit.Name = "txtit";
            this.txtit.Size = new System.Drawing.Size(33, 22);
            this.txtit.TabIndex = 223;
            this.txtit.TextChanged += new System.EventHandler(this.txtit_TextChanged);
            // 
            // txtempid
            // 
            this.txtempid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempid.Location = new System.Drawing.Point(456, 263);
            this.txtempid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtempid.Name = "txtempid";
            this.txtempid.Size = new System.Drawing.Size(33, 22);
            this.txtempid.TabIndex = 234;
            // 
            // txtopid
            // 
            this.txtopid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtopid.Location = new System.Drawing.Point(415, 263);
            this.txtopid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtopid.Name = "txtopid";
            this.txtopid.Size = new System.Drawing.Size(33, 22);
            this.txtopid.TabIndex = 233;
            // 
            // txtmcid
            // 
            this.txtmcid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmcid.Location = new System.Drawing.Point(374, 263);
            this.txtmcid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmcid.Name = "txtmcid";
            this.txtmcid.Size = new System.Drawing.Size(33, 22);
            this.txtmcid.TabIndex = 232;
            // 
            // txtgqty
            // 
            this.txtgqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgqty.Location = new System.Drawing.Point(457, 239);
            this.txtgqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgqty.Name = "txtgqty";
            this.txtgqty.Size = new System.Drawing.Size(32, 22);
            this.txtgqty.TabIndex = 241;
            // 
            // chkedtact
            // 
            this.chkedtact.AutoSize = true;
            this.chkedtact.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkedtact.Location = new System.Drawing.Point(45, 493);
            this.chkedtact.Name = "chkedtact";
            this.chkedtact.Size = new System.Drawing.Size(57, 19);
            this.chkedtact.TabIndex = 222;
            this.chkedtact.Text = "Active";
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(170, 487);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 216;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(109, 487);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(60, 30);
            this.button4.TabIndex = 251;
            this.button4.Text = "Save";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Location = new System.Drawing.Point(1, 486);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(682, 34);
            this.panadd.TabIndex = 207;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(495, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 16);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(29, 16);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 0);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 0);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 0);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 0);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(559, 1);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(54, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(227, 7);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(57, 19);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(425, 1);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(69, 30);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.butexit_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(371, 1);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderSize = 0;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(290, 1);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(82, 30);
            this.btnadd.TabIndex = 184;
            this.btnadd.Text = "Add new";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(231, 487);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(54, 30);
            this.button5.TabIndex = 252;
            this.button5.Text = "Exit";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(360, 487);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 31);
            this.button3.TabIndex = 254;
            this.button3.Text = "Ok";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // Reqbk
            // 
            this.Reqbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Reqbk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reqbk.Image = ((System.Drawing.Image)(resources.GetObject("Reqbk.Image")));
            this.Reqbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Reqbk.Location = new System.Drawing.Point(424, 487);
            this.Reqbk.Name = "Reqbk";
            this.Reqbk.Size = new System.Drawing.Size(60, 31);
            this.Reqbk.TabIndex = 253;
            this.Reqbk.Text = "Back";
            this.Reqbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Reqbk.UseVisualStyleBackColor = false;
            this.Reqbk.Click += new System.EventHandler(this.Reqbk_Click_1);
            // 
            // Frmprodrecpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 520);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.chkedtact);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Reqbk);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Editpnl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Frmprodrecpt";
            this.Text = "Production Reciept";
            this.Load += new System.EventHandler(this.Frmprodrecpt_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.panrea.ResumeLayout(false);
            this.panrea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.CheckBox chkedtact;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Item;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtdocno;
        private System.Windows.Forms.TextBox txtititd;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.TextBox txtit;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtop;
        private System.Windows.Forms.Label Operation;
        private System.Windows.Forms.TextBox txtmac;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtemp;
        private System.Windows.Forms.Label Employee;
        private System.Windows.Forms.TextBox txtempid;
        private System.Windows.Forms.TextBox txtopid;
        private System.Windows.Forms.TextBox txtmcid;
        private System.Windows.Forms.DateTimePicker timefrom;
        private System.Windows.Forms.DateTimePicker timeto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txthqty;
        private System.Windows.Forms.TextBox txtgqty;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtrew;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtrej;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtacc;
        private System.Windows.Forms.Panel panrea;
        private System.Windows.Forms.RichTextBox txtrejrea;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox txtrwrea;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button Reqbk;
    }
}