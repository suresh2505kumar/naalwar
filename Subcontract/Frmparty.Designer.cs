﻿namespace Subcontract
{
    partial class Frmparty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmparty));
            this.label1 = new System.Windows.Forms.Label();
            this.Genpan = new System.Windows.Forms.Panel();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmditem = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.fraitem = new System.Windows.Forms.Panel();
            this.HFG3 = new System.Windows.Forms.DataGridView();
            this.cmdcancel = new System.Windows.Forms.Button();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.cbocr = new System.Windows.Forms.ComboBox();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtcrdlmt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtcrddays = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtcrange = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtcereg = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtdiv = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtecc = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcoll = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtcst = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tngno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpin = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtstate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtcity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtadd2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtadd1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txtctper = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Txtstid = new System.Windows.Forms.TextBox();
            this.CmdItem1 = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.Editpnl.SuspendLayout();
            this.fraitem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(27, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 86;
            this.label1.Text = "Party";
            // 
            // Genpan
            // 
            this.Genpan.Controls.Add(this.chkact);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.btnexit);
            this.Genpan.Controls.Add(this.btndelete);
            this.Genpan.Controls.Add(this.btnedit);
            this.Genpan.Controls.Add(this.btnadd);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.label16);
            this.Genpan.Controls.Add(this.panel1);
            this.Genpan.Controls.Add(this.cmditem);
            this.Genpan.Controls.Add(this.label25);
            this.Genpan.Location = new System.Drawing.Point(19, 39);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1073, 503);
            this.Genpan.TabIndex = 95;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(972, 100);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(70, 20);
            this.chkact.TabIndex = 135;
            this.chkact.Text = "Active";
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // Txtscr3
            // 
            this.Txtscr3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(612, 15);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(200, 22);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(412, 15);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(200, 22);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(956, 326);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(90, 39);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btndelete
            // 
            this.btndelete.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btndelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndelete.Image = ((System.Drawing.Image)(resources.GetObject("btndelete.Image")));
            this.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndelete.Location = new System.Drawing.Point(956, 270);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(90, 40);
            this.btndelete.TabIndex = 85;
            this.btndelete.Text = "Delete";
            this.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btndelete.UseVisualStyleBackColor = false;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(956, 206);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(88, 41);
            this.btnedit.TabIndex = 84;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(956, 146);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(87, 40);
            this.btnadd.TabIndex = 83;
            this.btnadd.Text = "Add";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(12, 15);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(400, 22);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.Color.White;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(12, 44);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(906, 432);
            this.HFGP.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(99, 347);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 18);
            this.label16.TabIndex = 130;
            this.label16.Text = "Collectarte";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(276, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(626, 392);
            this.panel1.TabIndex = 134;
            // 
            // cmditem
            // 
            this.cmditem.Location = new System.Drawing.Point(44, 413);
            this.cmditem.Name = "cmditem";
            this.cmditem.Size = new System.Drawing.Size(125, 28);
            this.cmditem.TabIndex = 133;
            this.cmditem.Text = "ItemList";
            this.cmditem.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(532, 298);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 18);
            this.label25.TabIndex = 132;
            this.label25.Text = "Collectarte";
            // 
            // Editpnl
            // 
            this.Editpnl.Controls.Add(this.fraitem);
            this.Editpnl.Controls.Add(this.Chkedtact);
            this.Editpnl.Controls.Add(this.label5);
            this.Editpnl.Controls.Add(this.btnsave);
            this.Editpnl.Controls.Add(this.btnaddrcan);
            this.Editpnl.Controls.Add(this.cbocr);
            this.Editpnl.Controls.Add(this.cbotype);
            this.Editpnl.Controls.Add(this.label23);
            this.Editpnl.Controls.Add(this.txtcrdlmt);
            this.Editpnl.Controls.Add(this.label24);
            this.Editpnl.Controls.Add(this.txtcrddays);
            this.Editpnl.Controls.Add(this.label17);
            this.Editpnl.Controls.Add(this.txtcrange);
            this.Editpnl.Controls.Add(this.label18);
            this.Editpnl.Controls.Add(this.txtcereg);
            this.Editpnl.Controls.Add(this.label19);
            this.Editpnl.Controls.Add(this.txtdiv);
            this.Editpnl.Controls.Add(this.label20);
            this.Editpnl.Controls.Add(this.txtecc);
            this.Editpnl.Controls.Add(this.label13);
            this.Editpnl.Controls.Add(this.label14);
            this.Editpnl.Controls.Add(this.txtcoll);
            this.Editpnl.Controls.Add(this.label21);
            this.Editpnl.Controls.Add(this.txtcst);
            this.Editpnl.Controls.Add(this.label22);
            this.Editpnl.Controls.Add(this.tngno);
            this.Editpnl.Controls.Add(this.label11);
            this.Editpnl.Controls.Add(this.txtmail);
            this.Editpnl.Controls.Add(this.label12);
            this.Editpnl.Controls.Add(this.txtpin);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.txtstate);
            this.Editpnl.Controls.Add(this.label10);
            this.Editpnl.Controls.Add(this.txtcity);
            this.Editpnl.Controls.Add(this.label8);
            this.Editpnl.Controls.Add(this.txtadd2);
            this.Editpnl.Controls.Add(this.label7);
            this.Editpnl.Controls.Add(this.txtadd1);
            this.Editpnl.Controls.Add(this.label6);
            this.Editpnl.Controls.Add(this.Txtctper);
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.txtcode);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.txtname);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtphone);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.label15);
            this.Editpnl.Controls.Add(this.Txtstid);
            this.Editpnl.Controls.Add(this.CmdItem1);
            this.Editpnl.Location = new System.Drawing.Point(19, 39);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(1074, 506);
            this.Editpnl.TabIndex = 157;
            this.Editpnl.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpnl_Paint);
            // 
            // fraitem
            // 
            this.fraitem.Controls.Add(this.HFG3);
            this.fraitem.Controls.Add(this.cmdcancel);
            this.fraitem.Location = new System.Drawing.Point(519, 64);
            this.fraitem.Name = "fraitem";
            this.fraitem.Size = new System.Drawing.Size(551, 236);
            this.fraitem.TabIndex = 206;
            // 
            // HFG3
            // 
            this.HFG3.BackgroundColor = System.Drawing.Color.White;
            this.HFG3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFG3.Location = new System.Drawing.Point(27, 29);
            this.HFG3.Name = "HFG3";
            this.HFG3.Size = new System.Drawing.Size(496, 320);
            this.HFG3.TabIndex = 0;
            // 
            // cmdcancel
            // 
            this.cmdcancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdcancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdcancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdcancel.Image")));
            this.cmdcancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdcancel.Location = new System.Drawing.Point(232, 355);
            this.cmdcancel.Name = "cmdcancel";
            this.cmdcancel.Size = new System.Drawing.Size(93, 38);
            this.cmdcancel.TabIndex = 154;
            this.cmdcancel.Text = "Back";
            this.cmdcancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdcancel.UseVisualStyleBackColor = false;
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(535, 396);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(70, 20);
            this.Chkedtact.TabIndex = 205;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label5.Location = new System.Drawing.Point(26, 206);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 18);
            this.label5.TabIndex = 204;
            this.label5.Text = "Contact Person2";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(523, 433);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(90, 38);
            this.btnsave.TabIndex = 179;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click_1);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(619, 433);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(93, 38);
            this.btnaddrcan.TabIndex = 201;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click_1);
            // 
            // cbocr
            // 
            this.cbocr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocr.FormattingEnabled = true;
            this.cbocr.Items.AddRange(new object[] {
            "Customer",
            "Supplier",
            "Customer/Supplier"});
            this.cbocr.Location = new System.Drawing.Point(754, 329);
            this.cbocr.Name = "cbocr";
            this.cbocr.Size = new System.Drawing.Size(225, 24);
            this.cbocr.TabIndex = 178;
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Items.AddRange(new object[] {
            "Dealer",
            "Distributor",
            "Manufacturer",
            "Seller"});
            this.cbotype.Location = new System.Drawing.Point(523, 329);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(223, 24);
            this.cbotype.TabIndex = 177;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label23.Location = new System.Drawing.Point(754, 260);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 18);
            this.label23.TabIndex = 196;
            this.label23.Text = "Credit Limit";
            // 
            // txtcrdlmt
            // 
            this.txtcrdlmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrdlmt.Location = new System.Drawing.Point(754, 279);
            this.txtcrdlmt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrdlmt.Name = "txtcrdlmt";
            this.txtcrdlmt.Size = new System.Drawing.Size(228, 22);
            this.txtcrdlmt.TabIndex = 176;
            this.txtcrdlmt.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label24.Location = new System.Drawing.Point(520, 260);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 18);
            this.label24.TabIndex = 194;
            this.label24.Text = "Credit Days";
            // 
            // txtcrddays
            // 
            this.txtcrddays.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrddays.Location = new System.Drawing.Point(523, 279);
            this.txtcrddays.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrddays.Name = "txtcrddays";
            this.txtcrddays.Size = new System.Drawing.Size(223, 22);
            this.txtcrddays.TabIndex = 175;
            this.txtcrddays.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label17.Location = new System.Drawing.Point(754, 210);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 18);
            this.label17.TabIndex = 192;
            this.label17.Text = "E-mail Id";
            // 
            // txtcrange
            // 
            this.txtcrange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrange.Location = new System.Drawing.Point(754, 228);
            this.txtcrange.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrange.Name = "txtcrange";
            this.txtcrange.Size = new System.Drawing.Size(228, 22);
            this.txtcrange.TabIndex = 174;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label18.Location = new System.Drawing.Point(520, 210);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 18);
            this.label18.TabIndex = 190;
            this.label18.Text = "Phone No.";
            // 
            // txtcereg
            // 
            this.txtcereg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcereg.Location = new System.Drawing.Point(523, 228);
            this.txtcereg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcereg.Name = "txtcereg";
            this.txtcereg.Size = new System.Drawing.Size(223, 22);
            this.txtcereg.TabIndex = 173;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label19.Location = new System.Drawing.Point(754, 160);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 18);
            this.label19.TabIndex = 188;
            this.label19.Text = "E-mail Id";
            // 
            // txtdiv
            // 
            this.txtdiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdiv.Location = new System.Drawing.Point(754, 178);
            this.txtdiv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdiv.Name = "txtdiv";
            this.txtdiv.Size = new System.Drawing.Size(228, 22);
            this.txtdiv.TabIndex = 172;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label20.Location = new System.Drawing.Point(520, 160);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 20);
            this.label20.TabIndex = 186;
            this.label20.Text = "Phone No.";
            // 
            // txtecc
            // 
            this.txtecc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtecc.Location = new System.Drawing.Point(523, 178);
            this.txtecc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtecc.Name = "txtecc";
            this.txtecc.Size = new System.Drawing.Size(223, 22);
            this.txtecc.TabIndex = 171;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label13.Location = new System.Drawing.Point(751, 308);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 18);
            this.label13.TabIndex = 184;
            this.label13.Text = "Type";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label14.Location = new System.Drawing.Point(517, 308);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 18);
            this.label14.TabIndex = 183;
            this.label14.Text = "Party Type";
            // 
            // txtcoll
            // 
            this.txtcoll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcoll.Location = new System.Drawing.Point(29, 225);
            this.txtcoll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcoll.Name = "txtcoll";
            this.txtcoll.Size = new System.Drawing.Size(459, 22);
            this.txtcoll.TabIndex = 162;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label21.Location = new System.Drawing.Point(520, 110);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(124, 18);
            this.label21.TabIndex = 181;
            this.label21.Text = "CST No && Date";
            // 
            // txtcst
            // 
            this.txtcst.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcst.Location = new System.Drawing.Point(523, 128);
            this.txtcst.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcst.Name = "txtcst";
            this.txtcst.Size = new System.Drawing.Size(459, 22);
            this.txtcst.TabIndex = 170;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label22.Location = new System.Drawing.Point(520, 60);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(122, 18);
            this.label22.TabIndex = 179;
            this.label22.Text = "TIN No &&  Date";
            // 
            // tngno
            // 
            this.tngno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tngno.Location = new System.Drawing.Point(523, 78);
            this.tngno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tngno.Name = "tngno";
            this.tngno.Size = new System.Drawing.Size(459, 22);
            this.tngno.TabIndex = 169;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label11.Location = new System.Drawing.Point(163, 410);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 18);
            this.label11.TabIndex = 177;
            this.label11.Text = "E-mail Id";
            // 
            // txtmail
            // 
            this.txtmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmail.Location = new System.Drawing.Point(166, 430);
            this.txtmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(322, 22);
            this.txtmail.TabIndex = 168;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label12.Location = new System.Drawing.Point(26, 410);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 18);
            this.label12.TabIndex = 175;
            this.label12.Text = "Pin";
            // 
            // txtpin
            // 
            this.txtpin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpin.Location = new System.Drawing.Point(29, 429);
            this.txtpin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpin.Name = "txtpin";
            this.txtpin.Size = new System.Drawing.Size(129, 22);
            this.txtpin.TabIndex = 167;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label9.Location = new System.Drawing.Point(260, 360);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 18);
            this.label9.TabIndex = 173;
            this.label9.Text = "State";
            // 
            // txtstate
            // 
            this.txtstate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstate.Location = new System.Drawing.Point(260, 379);
            this.txtstate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtstate.Name = "txtstate";
            this.txtstate.Size = new System.Drawing.Size(228, 22);
            this.txtstate.TabIndex = 166;
            this.txtstate.TextChanged += new System.EventHandler(this.txtstate_TextChanged);
            this.txtstate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtstate_KeyDown_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label10.Location = new System.Drawing.Point(26, 360);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 18);
            this.label10.TabIndex = 171;
            this.label10.Text = "City";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtcity
            // 
            this.txtcity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcity.Location = new System.Drawing.Point(29, 379);
            this.txtcity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcity.Name = "txtcity";
            this.txtcity.Size = new System.Drawing.Size(223, 22);
            this.txtcity.TabIndex = 165;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label8.Location = new System.Drawing.Point(26, 310);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 18);
            this.label8.TabIndex = 169;
            this.label8.Text = "Address2";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtadd2
            // 
            this.txtadd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd2.Location = new System.Drawing.Point(29, 329);
            this.txtadd2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtadd2.Name = "txtadd2";
            this.txtadd2.Size = new System.Drawing.Size(459, 22);
            this.txtadd2.TabIndex = 164;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label7.Location = new System.Drawing.Point(26, 260);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 18);
            this.label7.TabIndex = 167;
            this.label7.Text = "Address1";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtadd1
            // 
            this.txtadd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd1.Location = new System.Drawing.Point(29, 278);
            this.txtadd1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtadd1.Name = "txtadd1";
            this.txtadd1.Size = new System.Drawing.Size(459, 22);
            this.txtadd1.TabIndex = 162;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label6.Location = new System.Drawing.Point(26, 160);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 18);
            this.label6.TabIndex = 165;
            this.label6.Text = "Contact Person1";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // Txtctper
            // 
            this.Txtctper.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtctper.Location = new System.Drawing.Point(29, 178);
            this.Txtctper.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Txtctper.Name = "Txtctper";
            this.Txtctper.Size = new System.Drawing.Size(459, 22);
            this.Txtctper.TabIndex = 161;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label4.Location = new System.Drawing.Point(26, 110);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 18);
            this.label4.TabIndex = 161;
            this.label4.Text = "Code";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtcode
            // 
            this.txtcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcode.Location = new System.Drawing.Point(29, 128);
            this.txtcode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(223, 22);
            this.txtcode.TabIndex = 159;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(26, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 18);
            this.label3.TabIndex = 159;
            this.label3.Text = "Name";
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(29, 78);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(459, 22);
            this.txtname.TabIndex = 158;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.ForeColor = System.Drawing.Color.MidnightBlue;
            this.Phone.Location = new System.Drawing.Point(260, 110);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(56, 18);
            this.Phone.TabIndex = 163;
            this.Phone.Text = "Phone";
            // 
            // txtphone
            // 
            this.txtphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphone.Location = new System.Drawing.Point(260, 128);
            this.txtphone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(228, 22);
            this.txtphone.TabIndex = 160;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(213, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 157;
            this.label2.Text = "Details";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(686, 24);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 20);
            this.label15.TabIndex = 199;
            this.label15.Text = "Other Details";
            // 
            // Txtstid
            // 
            this.Txtstid.Location = new System.Drawing.Point(265, 330);
            this.Txtstid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Txtstid.Name = "Txtstid";
            this.Txtstid.Size = new System.Drawing.Size(42, 21);
            this.Txtstid.TabIndex = 202;
            // 
            // CmdItem1
            // 
            this.CmdItem1.BackColor = System.Drawing.Color.White;
            this.CmdItem1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdItem1.Image = global::Subcontract.Properties.Resources.Itlist111_35x35;
            this.CmdItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdItem1.Location = new System.Drawing.Point(881, 371);
            this.CmdItem1.Name = "CmdItem1";
            this.CmdItem1.Size = new System.Drawing.Size(101, 45);
            this.CmdItem1.TabIndex = 203;
            this.CmdItem1.Text = "Item list";
            this.CmdItem1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CmdItem1.UseVisualStyleBackColor = false;
            this.CmdItem1.Click += new System.EventHandler(this.CmdItem1_Click_1);
            // 
            // Frmparty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 532);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpnl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frmparty";
            this.Text = "Frmparty";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frmparty_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.fraitem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFG3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button cmditem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.ComboBox cbocr;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtcrdlmt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtcrddays;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtcrange;
        protected System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtcereg;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtdiv;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtecc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcoll;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtcst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tngno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtpin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtstate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtcity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtadd2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtadd1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txtctper;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txtstid;
        private System.Windows.Forms.Button CmdItem1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Panel fraitem;
        private System.Windows.Forms.DataGridView HFG3;
        private System.Windows.Forms.Button cmdcancel;

    }
}