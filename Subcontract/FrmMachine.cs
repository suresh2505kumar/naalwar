﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Naalwar
{
    public partial class FrmMachine : Form
    {
        public FrmMachine()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource bsMachhine = new BindingSource();
        private void FrmMachine_Load(object sender, EventArgs e)
        {
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            chckInActive.Checked = true;
            chckInActive_CheckedChanged(sender, e);
            LoadButtons(0);
        }
        private void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    GrBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    //btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckInActive.Visible = true;
                }
                else if (id == 1)
                {
                    GrBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    //btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckInActive.Visible = false;
                }

                else if (id == 2)
                {
                    GrBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    //btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckInActive.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButtons(1);
                chckActive.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMachineName.Text == string.Empty || txtMachineNo.Text == string.Empty)
                {
                    MessageBox.Show("Enter Machine Name and Number", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMachineNo.Focus();
                    return;
                }
                else
                {
                    int Active = 0;
                    if (chckActive.Checked == true)
                    {
                        Active = 1;
                    }
                    else
                    {
                        Active = 0;
                    }
                    if (btnSave.Text == "Save")
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@MachineNo",txtMachineNo.Text),
                            new SqlParameter("@MachineName",txtMachineName.Text),
                            new SqlParameter("@Brand",txtBrand.Text),
                            new SqlParameter("@Active",Active),
                            new SqlParameter("@Tag","Insert")
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Machine", para, conn);
                    }
                    else
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@MachineNo",txtMachineNo.Text),
                            new SqlParameter("@MachineName",txtMachineName.Text),
                            new SqlParameter("@Brand",txtBrand.Text),
                            new SqlParameter("@Active",Active),
                            new SqlParameter("@Tag","Update"),
                            new SqlParameter("@MUid",txtMachineNo.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Machine", para, conn);
                    }
                    MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Genclass.Module.ClearTextBox(this, GrBack);
                    chckInActive_CheckedChanged(sender, e);
                    LoadButtons(0);
                }
            }
            catch (Exception ex)        
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckInActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(chckInActive.Checked == true)
                {   
                    SqlParameter[] para = { new SqlParameter("@Muid", "0"), new SqlParameter("@Active", "1") };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_getMachine", para, conn);
                    bsMachhine.DataSource = dt;
                    LoadDataGrid(dt);
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@Muid", "0"), new SqlParameter("@Active", "0") };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_getMachine", para, conn);
                    bsMachhine.DataSource = dt;
                    LoadDataGrid(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadDataGrid(DataTable dt)
        {
            try
            {
                DataGridMachine.DataSource = null;
                DataGridMachine.AutoGenerateColumns = false;
                DataGridMachine.ColumnCount = 5;
                DataGridMachine.Columns[0].Name = "Muid";
                DataGridMachine.Columns[0].HeaderText = "Muid";
                DataGridMachine.Columns[0].DataPropertyName = "MUid";
                DataGridMachine.Columns[0].Visible = false;
                DataGridMachine.Columns[1].Name = "LoomNo";
                DataGridMachine.Columns[1].HeaderText = "Loom No";
                DataGridMachine.Columns[1].DataPropertyName = "MachineNo";
                DataGridMachine.Columns[1].Width = 290;
                DataGridMachine.Columns[2].Name = "LoomType";
                DataGridMachine.Columns[2].HeaderText = "Loom Type";
                DataGridMachine.Columns[2].DataPropertyName = "MachineName";
                DataGridMachine.Columns[2].Width = 200;
                DataGridMachine.Columns[3].Name = "LoomWidth";
                DataGridMachine.Columns[3].HeaderText = "Loom Width";
                DataGridMachine.Columns[3].DataPropertyName = "Brand";
              
                DataGridMachine.Columns[4].HeaderText = "Active";
                DataGridMachine.Columns[4].DataPropertyName = "Active";
                DataGridMachine.Columns[4].DataPropertyName = "Active";
                DataGridMachine.Columns[4].Visible = false;
                DataGridMachine.DataSource = bsMachhine;
                lblCount.Text = "of " + dt.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            LoadButtons(0);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridMachine.SelectedCells[0].RowIndex;
                txtMachineNo.Tag = DataGridMachine.Rows[Index].Cells[0].Value.ToString();
                txtMachineNo.Text = DataGridMachine.Rows[Index].Cells[1].Value.ToString();
                txtMachineName.Text = DataGridMachine.Rows[Index].Cells[2].Value.ToString();
                txtBrand.Text = DataGridMachine.Rows[Index].Cells[3].Value.ToString();
               
                int ac = Convert.ToInt32(DataGridMachine.Rows[Index].Cells[4].Value.ToString());
                if(ac == 1)
                {
                    chckActive.Checked = true;
                }
                else
                {
                    chckActive.Checked = false;
                }
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridMachine_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int i = DataGridMachine.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridMachine.CurrentCell = DataGridMachine.Rows[0].Cells[1];
                DataGridMachine.Rows[0].Selected = true;
                int i = DataGridMachine.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridMachine.CurrentCell.RowIndex;
                if (Cnt > 0)
                {
                    int i = DataGridMachine.CurrentCell.RowIndex - 1;
                    DataGridMachine.CurrentCell = DataGridMachine.Rows[i].Cells[1];
                    DataGridMachine.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnNxt_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridMachine.CurrentCell.RowIndex + 1;
                if (Cnt >= 0 && Cnt != DataGridMachine.Rows.Count)
                {
                    int i = DataGridMachine.CurrentCell.RowIndex + 1;
                    DataGridMachine.CurrentCell = DataGridMachine.Rows[i].Cells[1];
                    DataGridMachine.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            try
            {
                int i = DataGridMachine.Rows.Count - 1;
                DataGridMachine.CurrentCell = DataGridMachine.Rows[i].Cells[1];
                DataGridMachine.Rows[i].Selected = true;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
