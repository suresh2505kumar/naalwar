﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Naalwar
{
    public partial class FrmLookup1 : Form
    {
        public FrmLookup1()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        private void FrmLookup1_Load(object sender, EventArgs e)
        {
            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP3.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP3.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP4.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP4.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            //tabC.Visible = true;
            Genclass.canclclik = false;
            this.CenterToScreen();
            //Hfgp.RowHeadersVisible = true;
            //HFGP2.RowHeadersVisible = true;
            //HFGP3.RowHeadersVisible = true;
            //HFGP4.RowHeadersVisible = true;
            Genclass.Module.buttonstyleform(this);
            //tabC.Visible = true;

        }
        public void Loadgrp()
        {
            conn.Open();
            string qur = "select distinct b.uid,b.ItemGroup from itemm a inner join ItemGroup b on a.ItemGroup_UId=b.UId";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbogrp.DataSource = null;
            cbogrp.DataSource = tab;
            cbogrp.DisplayMember = "Itemgroup";
            cbogrp.ValueMember = "uid";
            //cbogrp.SelectedIndex = -1;
            conn.Close();
        }
        private void txtscr3_TextChanged(object sender, System.EventArgs e)
        {
            lookupload2();
        }
        private void lookupload()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql;
                Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                if (txtscr7.Text != "" || txtscr8.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr7.Text + "%'";
                        Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtscr8.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr7.Text + "%'";
                        Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr8.Text + "%'";
                    }

                }


                if (txtscr7.Text != "" || txtscr8.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr7.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    if (txtscr7.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                    }

                }

                if (txtscr7.Text != "")
                {
                    Genclass.strsqltmp = Genclass.strsqltmp + " and  " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (txtscr7.Text == "")
                {
                    Genclass.strsqltmp = Genclass.strsqltmp + "  order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }

                else if (txtscr8.Text != "")
                {
                    Genclass.strsqltmp = Genclass.strsqltmp + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else
                {
                    Genclass.strsqltmp = Genclass.strsqltmp + "  order by " + Genclass.FSSQLSortStr1 + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP3.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP3.AutoGenerateColumns = false;
                HFGP3.DataSource = null;
                HFGP3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP3.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFGP3.DataSource = tap;
                txtscr7.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void lookupload2()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql;
                Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                if (txtscr3.Text != "" || txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr3.Text + "%'";
                        Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr3.Text + "%'";
                        Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr4.Text + "%'";
                    }

                }


                if (txtscr3.Text != "" || txtscr4.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr3.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    else if (txtscr3.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                    }

                }

                if (Genclass.StrSrch != "" && txtscr3.Text != "" && Genclass.Dtype == 90)
                {
                    Genclass.strsqltmp = "select itemid,itemname,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid where a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab    order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch == "" && txtscr3.Text == "" && Genclass.Dtype == 90)
                {
                    Genclass.strsqltmp = "select itemid,itemname,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid where a.companyid=" + Genclass.data1 + "  group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab    order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch != "" && txtscr4.Text != "" && Genclass.Dtype == 90)
                {
                    Genclass.strsqltmp = "select itemid,itemname,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid where a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch1 + " group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab    order by " + Genclass.FSSQLSortStr1 + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch == "" && txtscr4.Text == "" && Genclass.Dtype == 90)
                {
                    Genclass.strsqltmp = "select itemid,itemname,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid where a.companyid=" + Genclass.data1 + " group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab    order by " + Genclass.FSSQLSortStr1 + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }

                else if (Genclass.StrSrch != "" && txtscr3.Text != "" && Genclass.Dtype == 10)
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,b.pqty-isnull(sum(z.pqty),0) as qty,Docno, b.uid as listuid from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0    order by " + Genclass.FSSQLSortStr + " ";

                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch == "" && txtscr3.Text == "" && Genclass.Dtype == 10)
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,b.pqty-isnull(sum(z.pqty),0) as qty,Docno, b.uid as listuid from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + "  group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0    order by " + Genclass.FSSQLSortStr + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch != "" && txtscr4.Text != "" && Genclass.Dtype == 10)
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,b.pqty-isnull(sum(z.pqty),0) as qty,Docno, b.uid as listuid from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch1 + " group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0    order by " + Genclass.FSSQLSortStr1 + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch == "" && txtscr4.Text == "" && Genclass.Dtype == 10)
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,b.pqty-isnull(sum(z.pqty),0) as qty,Docno, b.uid as listuid from   sTransactionsP a      inner join sTransactionsPList b on a.UId=b.TransactionsPUId and b.DocTypeID=90 LEFT join transactionsplist z on b.Uid=z.Refuid       and z.doctypeid=10 inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join        Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.active=1 and c.active=1        and m.companyid=" + Genclass.data1 + "  group by m.uid,m.name,m.address1,Docno,     Docdate,c.hsnid,c.itemname,        c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0    order by " + Genclass.FSSQLSortStr1 + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP4.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP4.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP4.AutoGenerateColumns = false;
                HFGP4.DataSource = null;
                HFGP4.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP4.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP4.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP4.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFGP4.DataSource = tap;
                txtscr3.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void lookupload3()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql;
                Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                if (txtscr5.Text != "" || txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr5.Text + "%'";
                        Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr5.Text + "%'";
                        Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr6.Text + "%'";
                    }

                }


                if (txtscr5.Text != "" || txtscr6.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr5.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    else
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch + "and itemgroup_uid=" + cbogrp.SelectedValue + "";
                    }

                }
                if (Genclass.StrSrch != "" && txtscr5.Text != "")
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch == "" && txtscr5.Text == "")
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch1 != "" && txtscr6.Text != "")
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr1 + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }
                else if (Genclass.StrSrch1 == "" && txtscr6.Text == "")
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "  order by " + Genclass.FSSQLSortStr1 + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP2.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP2.AutoGenerateColumns = false;
                HFGP2.DataSource = null;
                HFGP2.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP2.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFGP2.DataSource = tap;
                txtscr5.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void txtinp1()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp2()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp3()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtscr4_TextChanged(object sender, System.EventArgs e)
        {
            lookupload2();
        }

        private void txtscr5_TextChanged(object sender, System.EventArgs e)
        {
            lookupload3();
        }

        private void txtscr6_TextChanged(object sender, System.EventArgs e)
        {
            lookupload3();
        }

        private void txtscr7_TextChanged(object sender, System.EventArgs e)
        {
            lookupload();
        }

        private void txtscr8_TextChanged(object sender, System.EventArgs e)
        {
            lookupload();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            Genclass.ty1 = 2;
            txtinp1();
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            Genclass.ty2 = 3;
            txtinp2();
        }

        private void button5_Click(object sender, System.EventArgs e)
        {
            Genclass.ty3 = 4;
            txtinp3();
        }

        private void HFGP4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP4_Click(object sender, System.EventArgs e)
        {
            Genclass.ty1 = 2;
            txtinp1();
        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Genclass.ty2 = 3;
            txtinp2();
        }

        private void HFGP3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Genclass.ty3 = 4;
            txtinp3();
        }

        private void button6_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void button4_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP4_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtscr3.Focus();
        }

        private void HFGP2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtscr5.Focus();
        }

        private void HFGP3_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtscr7.Focus();
        }
    }
}
